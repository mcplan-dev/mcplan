#pragma warning (disable: 4180)

#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/domains/RelevantIrrelevant.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/search/ForwardSearchSparseSampling.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"
#include "mcplan/node/TreePrinter.hpp"

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>

// ---------------------------------------------------------------------------




template<typename Diagram>
class TopStateAbstraction : public mcplan::abstraction::StateAbstraction<Diagram>
{
public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::StateType;

    explicit TopStateAbstraction( AbstractType x )
        : x_( x )
    { }

    AbstractType apply( GroundType const& /*g*/ ) const override
    {
        return x_;
    }

private:
    AbstractType x_;
};

template<typename Diagram>
class RI_BottomActionAbstraction : public mcplan::abstraction::ActionAbstraction<Diagram>
{
public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::ActionType;

    AbstractType apply( GroundType const& g ) const override
    {
        // FIXME
        return static_cast<AbstractType>( 1 + g.k() );
    }
};

template<typename Diagram>
class RI_deltaDistribution : public mcplan::abstraction::ActionDistribution<Diagram>
{
public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::ActionType;

    GroundType sample( mcplan::SamplingContext& /*ctx*/, AbstractType const& y ) const override
    {
        return mcplan::domains::RelevantIrrelevant::Action( static_cast<int>(y) - 1 );
    }
};

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

int main( int /*argc*/, char* /*argv*/[] )
{
    std::cout << "test/forward_search_sparse_sampling" << std::endl;

    using namespace mcplan;
    namespace ri = mcplan::domains::RelevantIrrelevant;
    using D = ri::Domain;
    int const T = 10;
    int const nr = 2;
    int const ni = 2;
    ri::Parameters params( T, nr, ni );
    auto s0 = ri::StateBuilder( params ).finish();
    std::cout << *s0 << std::endl;

    // FIXME: Since each data type has a nested 'tag' typedef, we could
    // initialize a fusion::set and then automatically transform it to a map.
    // This would simplify the declaration and initialization of NodeData.
    struct NodeData {
        typedef boost::fusion::map<
            boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
            boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
            boost::fusion::pair<state_data::tag::value, state_data::value<D>>,
            boost::fusion::pair<state_data::tag::upper_value_bound, state_data::upper_value_bound<D>>,
            boost::fusion::pair<state_data::tag::lower_value_bound, state_data::lower_value_bound<D>>
        > StateData;

        typedef boost::fusion::map<
            boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
            boost::fusion::pair<action_data::tag::value, action_data::value<D>>,
            boost::fusion::pair<action_data::tag::upper_value_bound, action_data::upper_value_bound<D>>,
            boost::fusion::pair<action_data::tag::lower_value_bound, action_data::lower_value_bound<D>>
        > ActionData;

        typedef node::BagStorage<GroundStateNode<D, NodeData>> StateStorage;
        typedef node::BagStorage<GroundActionNode<D, NodeData>> ActionStorage;
    };

    D const domain;
    mcplan::SamplingContext sim_ctx( mcplan::SamplingContext::rng_t( 42 ) );
    mcplan::SamplingContext world_ctx( mcplan::SamplingContext::rng_t( 43 ) );
    ri::Simulator sim;
    ri::Simulator world;

    // Initialize an abstraction diagram
    using namespace mcplan::abstraction;
    AbstractionDiagram<D>::Graph g;
    using Vertex = AbstractionDiagram<D>::Vertex;
    Vertex x0 = boost::add_vertex( g );
    g[x0] = AbstractionDiagram<D>::StateVertex();
    RI_BottomActionAbstraction<AbstractionDiagram<D>> bottom_a;
    RI_deltaDistribution<AbstractionDiagram<D>> deltaDist_a;
    g[x0] = AbstractionDiagram<D>::StateVertex();
    boost::get<AbstractionDiagram<D>::StateVertex>(g[x0]).action_abstraction = &bottom_a;
    TopStateAbstraction<AbstractionDiagram<D>> top_s( x0 );
    for( int i = 0; i < domain.actions( *s0 ).size(); ++i ) {
        Vertex yi = boost::add_vertex( g );
        boost::add_edge( x0, yi, g );
        boost::add_edge( yi, x0, g );
        g[yi] = AbstractionDiagram<D>::ActionVertex();
        boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).state_abstraction = &top_s;
        boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).action_distribution = &deltaDist_a;
    }
    AbstractionDiagram<D> ad( std::move(g) );

    int const ss_width = 2;
    int const ss_depth = 2;
#if 1
    using NodeType = GroundStateNode<D, NodeData>;
    NodeType sn0( std::move(s0) );
    sn0.sample( *s0, 0 );
    GroundTreeBuilder<D, NodeData> builder( sim );
    auto& adapted_domain = domain;
#else
    using NodeType = AggregateStateNode<D, NodeData>;
    GroundStateNode<D, NodeData> gsn( std::move(s0) );
    NodeType sn0( x0 );
    sn0.sample( &gsn, *s0, 0 );
    AggregateTreeBuilder<D, NodeData> builder( ad, sim );
    AbstractDomain<D> adapted_domain( &ad );
#endif
    search::forward_search_sparse_sampling(
        sim_ctx, adapted_domain, builder, sn0, ss_width, ss_depth,
        []( auto const& /*s*/ ) { return 10; } );

    depth_first_traversal( sn0, node::TreePrinter() );

    std::cout << "Value: " << mcplan::get<state_data::tag::value>( sn0 ) << std::endl;

    return 0;
}
