#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/domains/Sysadmin.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>

// ---------------------------------------------------------------------------

struct TreePrinter
{
    int indent_size = 2;
    int indent = 0;

    template<typename NodeType>
    void enterNode( NodeType const& n )
    {
        std::cout << std::setw( indent ) << "" << n << std::endl;
        indent += indent_size;
    }

    template<typename NodeType>
    void exitNode( NodeType const& /*n*/ )
    {
        indent -= indent_size;
    }
};

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

int main( int /*argc*/, char* /*argv*/[] )
{
    using namespace mcplan;
    namespace sysadmin = mcplan::domains::Sysadmin;
    using D = sysadmin::Domain;
    int const T = 10;
    int const nc = 4;
    int const tt = 1;
    sysadmin::Parameters params( T, nc, tt );
    sysadmin::Topology topology(tt,nc);
    auto s0 = sysadmin::StateBuilder( params ).finish(&topology);
    std::cout << *s0 << std::endl;

    // FIXME: Since each data type has a nested 'tag' typedef, we could
    // initialize a fusion::set and then automatically transform it to a map.
    // This would simplify the declaration and initialization of NodeData.
    struct NodeData {
        typedef boost::fusion::map<
            boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
            boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
            boost::fusion::pair<state_data::tag::value, state_data::value<D>>
        > StateData;

        typedef boost::fusion::map<
            boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
            boost::fusion::pair<action_data::tag::value, action_data::value<D>>
        > ActionData;

        typedef node::SetStorage<GroundStateNode<D, NodeData>> StateStorage;
        typedef node::SetStorage<GroundActionNode<D, NodeData>> ActionStorage;
    };

    D const domain;
    mcplan::SamplingContext sim_ctx( mcplan::SamplingContext::rng_t( 42 ) );
    mcplan::SamplingContext world_ctx( mcplan::SamplingContext::rng_t( 43 ) );
    sysadmin::Simulator sim;
    sysadmin::Simulator world;

    int const ss_width = 2;
    int const ss_depth = 2;
    using NodeType = GroundStateNode<D, NodeData>;
//    using NodeType = AggregateStateNode<D, NodeData>;
    NodeType sn0( std::move(s0) );
    GroundTreeBuilder<D, NodeData> builder( sim );
    search::sparse_sample(
        sim_ctx, domain, builder, sn0, ss_width, ss_depth,
        []( auto const& /*s*/ ) { return 10; } );
//    search::sparse_sample( sim_ctx, domain, sim, sn, ss_width, ss_depth,
//                           []( auto const& /*s*/ ) { return 5; } );

    depth_first_traversal( sn0, TreePrinter() );
    return 0;
}
