#ifndef RI_INTERFACE_HPP
#define RI_INTERFACE_HPP

#define GROUND

#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/abstraction/BottomActionAbstraction.hpp"
#include "mcplan/abstraction/TopStateAbstraction.hpp"
#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/domains/RelevantIrrelevant.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/search/GreedyPolicy.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>


class ri_interface
{
public:
    ri_interface(std::string algo, unsigned width, unsigned depth, unsigned budget )
    {
        using namespace mcplan;
        namespace ri = mcplan::domains::RelevantIrrelevant;
        using D = ri::Domain;
        int const T = 10;
        int const nr = 2;
        int const ni = 2;
        ri::Parameters params( T, nr, ni );
        auto s0 = ri::StateBuilder( params ).finish();
        std::cout << *s0 << std::endl;


        // FIXME: Since each data type has a nested 'tag' typedef, we could
        // initialize a fusion::set and then automatically transform it to a map.
        // This would simplify the declaration and initialization of NodeData.
        struct NodeData {
            typedef boost::fusion::map<
                boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
                boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
                boost::fusion::pair<state_data::tag::value, state_data::value<D>>
            > StateData;

            typedef boost::fusion::map<
                boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
                boost::fusion::pair<action_data::tag::value, action_data::value<D>>
            > ActionData;

            typedef node::SetStorage<GroundStateNode<D, NodeData>> StateStorage;
            typedef node::SetStorage<GroundActionNode<D, NodeData>> ActionStorage;
        };

        D const domain;
        mcplan::SamplingContext sim_ctx( mcplan::SamplingContext::rng_t( 42 ) );
        mcplan::SamplingContext world_ctx( mcplan::SamplingContext::rng_t( 43 ) );
        ri::Simulator sim;
        ri::Simulator world;



    #ifdef GROUND
        using StateNodeType = GroundStateNode<D, NodeData>;
        using TreeBuilder = GroundTreeBuilder<D, NodeData>;
        TreeBuilder builder( sim );
        auto& adapted_domain = domain;
    #else

        // Initialize an abstraction diagram
        using namespace mcplan::abstraction;
        AbstractionDiagram<D>::Graph g;
        using Vertex = AbstractionDiagram<D>::Vertex;
        Vertex x0 = boost::add_vertex( g );
        g[x0] = AbstractionDiagram<D>::StateVertex();
        RI_BottomActionAbstraction<AbstractionDiagram<D>> bottom_a;
        g[x0] = AbstractionDiagram<D>::StateVertex();
        boost::get<AbstractionDiagram<D>::StateVertex>(g[x0]).action_abstraction = &bottom_a;
        TopStateAbstraction<AbstractionDiagram<D>> top_s( x0 );
        for( int i = 0; i < domain.actions( *s0 ).size(); ++i ) {
            Vertex yi = boost::add_vertex( g );
            boost::add_edge( x0, yi, g );
            boost::add_edge( yi, x0, g );
            g[yi] = AbstractionDiagram<D>::ActionVertex();
            boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).state_abstraction = &top_s;

        }
        AbstractionDiagram<D> ad( std::move(g) );

        using StateNodeType = AggregateStateNode<D, NodeData>;
        using TreeBuilder = AggregateTreeBuilder<D, NodeData>;

        TreeBuilder builder( ad, sim );

        AbstractDomain<D> adapted_domain( &ad );

    #endif
        GreedyPolicy<typename TreeBuilder::TreeDomain, StateNodeType> execution_policy(builder.treeDomain(domain));
        int const ss_width = width;
        int const ss_depth = depth;
        unsigned execution_horizon=4;


        //StateNodeType sn0( std::move(s0) );
        for(unsigned i=0; i< execution_horizon;i++)
        {

#ifdef GROUND
            StateNodeType sn0( std::move(s0) ); //--> Doubt(AA)- Should we not sample in ground version ?
            #else
            GroundStateNode<D, NodeData> gsn( std::move(s0) );
            StateNodeType sn0( x0 );
            sn0.sample( &gsn, gsn.s(), 0 );
#endif

            if(algo=="sparse_sample")
                search::sparse_sample(
                    sim_ctx, adapted_domain, builder, sn0, ss_width, ss_depth,
                    []( auto const& /*s*/ ) { return 10; } );
//            else
//                search::uct(
//                    sim_ctx, domain, builder, sn0, ss_width, ss_depth );

            bool node_added=false;
            auto const an = execution_policy( sn0,sim_ctx,node_added);

#ifdef GROUND
            auto actual_state=sn0.s();
            auto actual_action=an.get();
#else
            auto actual_action = ad.sampleAction( sim_ctx,sn0.label(), an.get() );
            auto actual_state=gsn.s();
#endif
            auto tr = sim.sampleTransition( sim_ctx,actual_state , actual_action );
            std::cout<<"\nState"<<actual_state<<"\nAction Executed" <<actual_action<<"\n";
            float action_reward = std::get<0>(tr).r();
            float state_reward = std::get<1>(tr).r();
            s0 = std::get<1>(tr).transfer_s();

            total_reward += state_reward + action_reward;
        }
        return total_reward;
        /* Printing Trees */
    //    std::cout << "=== Abstract tree ===" << std::endl;
    //    depth_first_traversal( sn0, TreePrinter() );
    //    std::cout << "Value: " << mcplan::get<state_data::tag::value>( sn0 ) << std::endl;

    //    std::cout << "=== Ground tree ===" << std::endl;
    //    depth_first_traversal( gsn, TreePrinter() );
    //    std::cout << "Value: " << mcplan::get<state_data::tag::value>( gsn ) << std::endl;


    }

};

#endif // RI_INTERFACE_HPP
