#ifndef ADVANCEDSYSADMIN_HPP
#define ADVANCEDSYSADMIN_HPP


//#define TOP_STATE_TOP_ACTION_ABSTRACTION
//#define HUB_ACTION_ABSTRACTION


#include "mcplan/domains/AdvancedSysadmin.hpp"
#include "mcplan/domains/AdvancedSysadmin/Action.hpp"
#include "mcplan/domains/AdvancedSysadmin/State.hpp"
#include "mcplan/domains/AdvancedSysadmin/Domain.hpp"
#include "mcplan/domains/AdvancedSysadmin/Simulator.hpp"
#include "mcplan/domains/AdvancedSysadmin/DomainSpecificAbstractions/TopActionAbstraction.hpp"
#include "mcplan/domains/AdvancedSysadmin/DomainSpecificAbstractions/Hub2ActionAbstraction.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/search/GreedyPolicy.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"
#include "mcplan/node/TreePrinter.hpp"

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>

class AdvancedSysadmin_interface
{
public:
    using AdvancedSysadmin = mcplan::domains::AdvancedSysadmin;
    using D = AdvancedSysadmin::Domain;

    struct NodeData {
        typedef boost::fusion::map<
            boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
            boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
            boost::fusion::pair<state_data::tag::value, state_data::value<D>>,
            //boost::fusion::pair<state_data::tag::upper_value_bound, state_data::upper_value_bound<D>>,
            //boost::fusion::pair<state_data::tag::lower_value_bound, state_data::lower_value_bound<D>>
            boost::fusion::pair<state_data::tag::avg_return, state_data::avg_return<D>>
        > StateData;

        typedef boost::fusion::map<
            boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
            boost::fusion::pair<action_data::tag::value, action_data::value<D>>,
            //boost::fusion::pair<state_data::tag::upper_value_bound, state_data::upper_value_bound<D>>,
            //boost::fusion::pair<state_data::tag::lower_value_bound, state_data::lower_value_bound<D>>
            boost::fusion::pair<action_data::tag::avg_return, action_data::avg_return<D>>
        > ActionData;

        typedef node::SetStorage<GroundStateNode<D, NodeData>> StateStorage;
        typedef node::SetStorage<GroundActionNode<D, NodeData>> ActionStorage;
    };


     AdvancedSysadmin_interface(mcplan::SamplingContext& sim_ctx,std::string algo, unsigned width, unsigned depth,  float&reward)
    {

            int const T = 20;
            int const nc = 11;
            int const MaxOn=4;
            int const tt = 2; // Type of Topology 0 for line, 1 for Ring , 2 for Hub

            AdvancedSysadmin::Parameters params( T, nc, MaxOn, tt );
            AdvancedSysadmin::Topology topology(tt,nc);
            auto s0 = AdvancedSysadmin::StateBuilder( params ).finish(&topology);
            //std::cout << *s0 << std::endl;

            D const domain;

            AdvancedSysadmin::Simulator sim;
            AdvancedSysadmin::Simulator world;
    #ifdef GROUND
        using StateNodeType = GroundStateNode<D, NodeData>;
        using TreeBuilder = GroundTreeBuilder<D, NodeData>;
        TreeBuilder builder( sim );
        auto& adapted_domain = domain;
    #else
            using namespace mcplan::abstraction;
            if( type <3)
                AbstractionDiagram<D> ad(4,domain,*s0);
            else // Domain Specific Abstractions
            {

            }

            using StateNodeType = AggregateStateNode<D, NodeData>;
            using TreeBuilder = AggregateTreeBuilder<D, NodeData>;

            TreeBuilder builder( ad, sim );

            AbstractDomain<D> adapted_domain( &ad );

     #endif




            AbstractionDiagram<D> ad( std::move(g) );
            using StateNodeType = AggregateStateNode<D, NodeData>;
            using TreeBuilder = AggregateTreeBuilder<D, NodeData>;
            TreeBuilder builder( ad, sim );
            AbstractDomain<D> adapted_domain( &ad );

        #endif
            GreedyPolicy<typename TreeBuilder::TreeDomain, StateNodeType> execution_policy(builder.treeDomain(domain));
            int const ss_width = width;
            int const ss_depth = depth;
            unsigned execution_horizon=T;
            //StateNodeType sn0( std::move(s0) );
            float total_reward=0;

               for(unsigned i=0; i< execution_horizon;i++)
               {

           #ifdef GROUND
                   StateNodeType sn0( std::move(s0) ); //--> Doubt(AA)- Should we not sample in ground version ?
           #else
                   GroundStateNode<D, NodeData> gsn( std::move(s0) );
                   StateNodeType sn0( x0 );
                   sn0.sample( &gsn, gsn.s(), 0 );
           #endif

                   if(algo=="sparse_sample")
                   {
                       search::sparse_sample(
                           sim_ctx, adapted_domain, builder, sn0, ss_width, ss_depth,
                           []( auto const& /*s*/ ) { return 10; } );
                   }
                       //            else
           //                search::uct(
           //                    sim_ctx, domain, builder, sn0, ss_width, ss_depth );

                   bool node_added=true;
                   auto const an = execution_policy( sn0,sim_ctx,node_added);

           #ifdef GROUND
                   auto actual_state=sn0.s();
                   auto actual_action=an.get();
           #else
                   auto actual_action = ad.sampleAction( sim_ctx,sn0.label(), an.get() );
                   auto actual_state=gsn.s();
           #endif
                   auto tr = sim.sampleTransition( sim_ctx,actual_state , actual_action );
                   //std::cout<<"\n State "<<actual_state<<"\n Action Executed " <<actual_action<<"\n";
                   float action_reward = std::get<0>(tr).r();
                   float state_reward = std::get<1>(tr).r();

                   s0 = std::get<1>(tr).transfer_s();
                   total_reward += state_reward + action_reward;
                   /* Printing Trees */
//                   std::cout << "=== Abstract tree ===" << std::endl;
//                   depth_first_traversal( sn0, mcplan::node::TreePrinter() );
//                   std::cout << "Value: " << mcplan::get<state_data::tag::value>( sn0 ) << std::endl;

//                   std::cout << "=== Ground tree ===" << std::endl;
//                   depth_first_traversal( gsn, mcplan::node::TreePrinter() );
//                   std::cout << "Value: " << mcplan::get<state_data::tag::value>( gsn ) << std::endl;
               }
               reward= total_reward;



    }
};

#endif // ADVANCEDSYSADMIN_HPP
