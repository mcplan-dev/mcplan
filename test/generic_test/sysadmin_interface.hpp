#ifndef SYSADMIN_HPP
#define SYSADMIN_HPP




#include "mcplan/abstraction/AbstractionDiagram.hpp"
//#include "mcplan/abstraction/BottomActionAbstraction.hpp"
#include "mcplan/abstraction/TopStateAbstraction.hpp"
//#include "mcplan/abstraction/SingleActionAbstraction.hpp"
#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/node/TreePrinter.hpp"
#include "mcplan/domains/Sysadmin.hpp"
#include "mcplan/domains/Sysadmin/Action.hpp"
#include "mcplan/domains/Sysadmin/State.hpp"
#include "mcplan/domains/Sysadmin/Domain.hpp"
#include "mcplan/domains/Sysadmin/Simulator.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/search/uct.hpp"

#include "mcplan/search/GreedyPolicy.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"

//#include "mcplan/domains/Sysadmin/DomainSpecificAbstractions/Hub1ActionAbstraction.hpp"

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>



//#define GROUND
namespace mcplan {


class sysadmin_interface
{
public:

    using sysadmin = mcplan::domains::Sysadmin;
    using D = sysadmin::Domain;

    struct NodeData {
        typedef boost::fusion::map<
            boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
            boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
            boost::fusion::pair<state_data::tag::value, state_data::value<D>>,
            boost::fusion::pair<state_data::tag::upper_value_bound, state_data::upper_value_bound<D>>,
            boost::fusion::pair<state_data::tag::lower_value_bound, state_data::lower_value_bound<D>>,
            boost::fusion::pair<state_data::tag::avg_return, state_data::avg_return<D>>
        > StateData;

        typedef boost::fusion::map<
            boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
            boost::fusion::pair<action_data::tag::value, action_data::value<D>>,
            boost::fusion::pair<action_data::tag::upper_value_bound, action_data::upper_value_bound<D>>,
            boost::fusion::pair<action_data::tag::lower_value_bound, action_data::lower_value_bound<D>>,
            boost::fusion::pair<action_data::tag::avg_return, action_data::avg_return<D>>
        > ActionData;

        typedef node::SetStorage<GroundStateNode<D, NodeData>> StateStorage;
        typedef node::SetStorage<GroundActionNode<D, NodeData>> ActionStorage;
    };

    sysadmin_interface(mcplan::SamplingContext& world_ctx,mcplan::SamplingContext& sim_ctx,std::string algo, unsigned width, unsigned depth, float&reward, unsigned abstractionType)
    {


        /* Domain Specific Parameters */
        int const T = 10;
        int const nc = 20;
        int const tt = 2;


        sysadmin::Parameters params( T, nc, tt );
        sysadmin::Topology topology(tt,nc);
        auto s0 = sysadmin::StateBuilder( params ).finish(&topology);
        //std::cout
        //std::cout <<"Here Starting"<< *s0 << std::endl;

        /* Initializing Node Data */

        D const domain( params );

        sysadmin::Simulator sim;
        sysadmin::Simulator world;
#ifdef GROUND
    using StateNodeType = GroundStateNode<D, NodeData>;
    using TreeBuilder = GroundTreeBuilder<D, NodeData>;
    TreeBuilder builder( sim );
    auto& adapted_domain = domain;

#else

    // Initialize an abstraction diagram
    using namespace mcplan::abstraction;
    //AbstractionDiagram<D>::Graph g;
    AbstractionDiagram<D> ad;
    if(abstractionType<=3)
    {
        AbstractionDiagram<D> ad1(abstractionType,domain,*s0);
        ad=ad1;
    }
    else
    {
        AbstractionDiagram<D>::Graph g;
        using Vertex = AbstractionDiagram<D>::Vertex;
        Vertex x0 = boost::add_vertex( g );
        g[x0] = AbstractionDiagram<D>::StateVertex();
        Vertex start_vertex =x0;

        std::vector<Vertex> leaves;

        Hub1ActionAbstraction<AbstractionDiagram<D>>* Hub_A=new Hub1ActionAbstraction<AbstractionDiagram<D>>(10);
        boost::get<AbstractionDiagram<D>::StateVertex>(g[x0]).action_abstraction =Hub_A;
        TopStateAbstraction<AbstractionDiagram<D>>* top_s=new TopStateAbstraction<AbstractionDiagram<D>>(x0);
        for(int i=0;i< 2; i++){
            Vertex yi = boost::add_vertex( g);
            boost::add_edge( x0, yi, g);
            boost::add_edge( yi, x0, g );
            g[yi] = AbstractionDiagram<D>::ActionVertex();
            boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).state_abstraction = top_s;
                        //boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).action_distribution = &deltaDist_a;
           leaves.push_back(yi);
        }
        Hub_A->setLeaves(leaves);

        AbstractionDiagram<D>ad1( std::move(g),start_vertex );
        ad=ad1;
    }

    using StateNodeType = AggregateStateNode<D, NodeData>;
    using TreeBuilder = AggregateTreeBuilder<D, NodeData>;

    TreeBuilder builder( ad, sim );

    AbstractDomain<D> adapted_domain( &ad );

#endif


    GreedyPolicy<typename TreeBuilder::TreeDomain, StateNodeType> execution_policy(builder.treeDomain(domain));
    int const ss_width = width;
    int const ss_depth = depth;
    unsigned execution_horizon=T;
    float total_reward=0;

       for(unsigned i=0; i< execution_horizon;i++)
       {
           //std::cout<<"here budget\n";
           sim_ctx.budget().reset();
           if(s0->isTerminal( params ))
           {
               std::cout<<"Reached Goal "<<*s0<<"\n";
               break;
           }
   #ifdef GROUND
           StateNodeType sn0( std::move(s0) );
           sn0.sample(sn0.s(),0);
   #else
           GroundStateNode<D, NodeData> gsn( std::move(s0) );

           StateNodeType sn0( (ad.start_node()) );
           sn0.sample( &gsn, gsn.s(), 0 );
   #endif

           if(algo=="sparse_sample")
               search::sparse_sample(
                   sim_ctx, adapted_domain, params, builder, sn0, ss_width, ss_depth,
                   [nc]( auto const& s ) {
                   using BitVectorType =long long;
                   unsigned nOns=0;
                    for (int i=0;i<nc;i++)
                    {
                      if((s.ComputerState()&(BitVectorType(1)<<i))!=0)
                          nOns+=1;
                    }
                    return nOns;});
           else //if (algo=="uct")
                   search::uct(
                       sim_ctx, domain, params, builder, sn0, ss_width, ss_depth );
           //else
           //    search::forward_search_sparse_sampling(
           //        sim_ctx, adapted_domain, builder, sn0, ss_width, ss_depth,[]( auto const& /*s*/ ) { return 10; } );

          //std::cout << "=== Ground tree ===" << std::endl;
           //depth_first_traversal( sn0, mcplan::node::TreePrinter() );

           auto const an = execution_policy( sn0,world_ctx,algo);


   #ifdef GROUND
           auto actual_state=sn0.s();
           auto actual_action=an.get();
   #else
           auto actual_action = ad.sampleAction( sim_ctx,sn0.label(), an.get() );
           auto actual_state=gsn.s();
   #endif
           auto tr = sim.sampleTransition( world_ctx, params, actual_state , actual_action );
           //std::cout<<"\nState"<<actual_state<<"\nAction Executed" <<actual_action<<"\n";
           float action_reward = std::get<0>(tr).r();
           float state_reward = std::get<1>(tr).r();
           s0 = std::get<1>(tr).transfer_s();
           //std::cout<<"\nNew State"<<*s0<<"\nAction Executed" <<actual_action<<"\n";
           total_reward += state_reward + action_reward;
       }
       reward= total_reward;



    }
};

}
#endif // SYSADMIN_HPP

