#include "mcplan/LogLevel.hpp"
#ifndef MCPLAN_LOG_LEVEL
#   define MCPLAN_LOG_LEVEL MCPLAN_LOG_LEVEL_INFO
#endif

// FIXME: Debugging
#pragma warning (disable : 4503)

#include <iostream>
#include <iomanip>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <string>
#include <vector>

#include <boost/throw_exception.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/math/constants/constants.hpp>
#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>
#include <boost/lexical_cast.hpp>

#include "mcplan/domains/Investment.hpp"
#include "mcplan/domains/Racetrack.hpp"
#include "mcplan/domains/Investment.hpp"
#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/abstraction/DecisionTreeStateAbstraction.hpp"
#include "mcplan/abstraction/FiniteActionSetAbstraction.hpp"
#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/domains/SphereNavigation.hpp"
#include "mcplan/domains/SphereNavigation/Action.hpp"
#include "mcplan/domains/SphereNavigation/State.hpp"
#include "mcplan/domains/SphereNavigation/Domain.hpp"
#include "mcplan/domains/SphereNavigation/Simulator.hpp"
#include "mcplan/domains/Sysadmin.hpp"
#include "mcplan/domains/Sysadmin/Action.hpp"
#include "mcplan/domains/Sysadmin/State.hpp"
#include "mcplan/domains/Sysadmin/Domain.hpp"
#include "mcplan/domains/Sysadmin/Simulator.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/node/TreePrinter.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/search/GreedyPolicy.hpp"
#include "mcplan/search/ForwardSearchSparseSampling.hpp"
#include "mcplan/search/ProgressiveAbstractFsss.hpp"
//#include "mcplan/search/uct.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"

#include "mcplan/Log.hpp"
#include "mcplan/Policy.hpp"


using namespace mcplan;
using namespace mcplan::domains;

class Options
{
public:
    explicit Options( std::map<std::string, std::string> const& m )
        : m_( m )
    { }

    bool is_set( std::string const& s ) const
    { return m_.find( s ) != m_.end(); }

    template<typename T>
    T at( std::string const& s ) const
    {
        T t;
        at_impl( s, t );
        return t;
    }

    unsigned int nruns() const
    {
        auto itr = m_.find( "nruns" );
        if( itr != m_.end() ) {
            return boost::lexical_cast<unsigned int>( itr->second );
        }
        else {
            return 1;
        }
    }

    std::string algorithm() const   { return at<std::string>("algorithm"); }
    std::string abstraction() const { return at<std::string>("abstraction"); }
    std::string refinement() const  { return at<std::string>("refinement"); }
    std::string domain() const      { return at<std::string>("domain"); }

    boost::optional<std::size_t> episode_length() const
    {
        auto itr = m_.find( "episode_length" );
        if( itr != m_.end() ) {
            return boost::lexical_cast<std::size_t>( itr->second );
        }
        else {
            return boost::none;
        }
    }

    int seed() const
    { return at<int>("seed"); }

    unsigned int budget() const
    { return at<unsigned int>("budget"); }

    unsigned int width() const
    { return at<unsigned int>("width"); }
    unsigned int depth() const
    { return at<unsigned int>("depth"); }

    std::string ad_init() const
    { return "Tree_TopState_RepeatAction"; }
    unsigned int ad_init_delta_t() const
    { return at<unsigned int>("ad_init_delta_t"); }
    unsigned int ad_init_max_depth() const
    {
        if( is_set( "ad_init_max_depth" ) ) {
            return at<unsigned int>("ad_init_max_depth");
        }
        else if( is_set( "depth" ) ) {
            return depth();
        }
        else {
            BOOST_THROW_EXCEPTION( std::out_of_range( "ad_init_max_depth" ) );
        }
    }

    std::string prog_select() const
    { return at<std::string>("prog_select"); }
    double prog_split_state_proportion() const
    { return at<double>("prog_split_state_proportion"); }

    double racetrack_slip() const
    {
        auto itr = m_.find( "racetrack_slip" );
        if( itr != m_.end() ) {
            return boost::lexical_cast<double>( itr->second );
        }
        else {
            return 0.2;
        }
    }

    std::string racetrack_track() const
    { return at<std::string>("racetrack_track"); }

private:
    void at_impl( std::string const& s, std::string& t ) const
    {
        try {
            t = m_.at( s );
        }
        catch( std::exception& ) {
            BOOST_THROW_EXCEPTION( std::out_of_range( s ) );
        }
    }

    template<typename T>
    void at_impl( std::string const& s, T& t ) const
    {
        try {
            t = boost::lexical_cast<T>( m_.at( s ) );
        }
        catch( std::exception& ) {
            BOOST_THROW_EXCEPTION( std::out_of_range( s ) );
        }
    }

private:
    std::map<std::string, std::string> m_;
};

// ---------------------------------------------------------------------------

template<typename Domain>
typename Domain::ParametersType createParameters( Options const& options );

template<>
Investment::Domain::ParametersType createParameters<Investment::Domain>( Options const& /*options*/ )
{
    return Investment::Parameters::Formula_A();
}

template<>
Racetrack::Domain::ParametersType createParameters<Racetrack::Domain>( Options const& options )
{
    double const slip = options.racetrack_slip();
    if( "bbs_large" == options.racetrack_track() ) {
        return Racetrack::Parameters::BartoBradtkeSingh_Large( slip );
    }
    else if( "bbs_small" == options.racetrack_track() ) {
        return Racetrack::Parameters::BartoBradtkeSingh_Small( slip );
    }
    else {
        throw std::invalid_argument( "racetrack_track" );
    }
}

template<>
SphereNavigation::Domain::ParametersType createParameters<SphereNavigation::Domain>( Options const& /*options*/ )
{
    //double const pi = boost::math::constants::pi<double>();
    int const T = 100;
    SphereNavigation::Position init_pos(1,0,0);
    SphereNavigation::Position goal_pos(1,0.01f,-0.03f);

    return SphereNavigation::Parameters( T,init_pos ,goal_pos );
}

template<>
Sysadmin::Domain::ParametersType createParameters<Sysadmin::Domain>( Options const& /*options*/ )
{
    int const T = 50;
    int numSystems=10;
    int TopologyType=1; //(0: Ring, 1: Bus, 2: Hub  ,>3: Custom)
    return Sysadmin::Parameters( T,numSystems,TopologyType);
}

// ---------------------------------------------------------------------------

template<typename Domain>
Domain createDomain( Options const& options, typename Domain::ParametersType const& parameters );

template<>
Investment::Domain createDomain<Investment::Domain>(
        Options const& /*options*/, Investment::Parameters const& parameters )
{
    return Investment::Domain( parameters );
}

template<>
Racetrack::Domain createDomain<Racetrack::Domain>(
        Options const& /*options*/, Racetrack::Parameters const& parameters )
{
    MCPLAN_LOG_TRACE(( std::cout << "createDomain()" << std::endl ));
    return Racetrack::Domain( parameters );
}

template<>
SphereNavigation::Domain createDomain<SphereNavigation::Domain>(
        Options const& /*options*/, SphereNavigation::Parameters const& parameters )
{
    return SphereNavigation::Domain( parameters );
}

template<>
Sysadmin::Domain createDomain<Sysadmin::Domain>(
        Options const& /*options*/, Sysadmin::Parameters const& parameters )
{
    return Sysadmin::Domain( parameters );
}

// ---------------------------------------------------------------------------

template<typename Domain>
typename Domain::SimulatorType createSimulator(
        Options const& options, typename Domain::ParametersType const& parameters );

template<>
Investment::Domain::SimulatorType createSimulator<Investment::Domain>(
        Options const& /*options*/, Investment::Domain::ParametersType const& /*parameters*/ )
{
    return Investment::Simulator();
}

template<>
Racetrack::Domain::SimulatorType createSimulator<Racetrack::Domain>(
        Options const& /*options*/, Racetrack::Domain::ParametersType const& /*parameters*/ )
{
    MCPLAN_LOG_TRACE(( std::cout << "createSimulator()" << std::endl ));
    return Racetrack::Simulator();
}

template<>
SphereNavigation::Domain::SimulatorType createSimulator<SphereNavigation::Domain>(
        Options const& /*options*/, SphereNavigation::Domain::ParametersType const& /*parameters*/ )
{
    return SphereNavigation::Simulator();
}

template<>
Sysadmin::Domain::SimulatorType createSimulator<Sysadmin::Domain>(
        Options const& /*options*/, Sysadmin::Domain::ParametersType const& /*parameters*/ )
{
    return Sysadmin::Simulator();
}

// ---------------------------------------------------------------------------

template<typename ProgPolicy>
struct CreateRefinementStrategy
{
    static typename ProgPolicy::RefinementStrategy call( Options const& options );
};

template<typename Domain, typename SearchSpace>
struct CreateRefinementStrategy<search::ProgressiveAbstractFsssPolicy<Domain, SearchSpace>>
{
    static typename search::ProgressiveAbstractFsssPolicy<Domain, SearchSpace>::RefinementStrategy
    call( Options const& options )
    {
        namespace ad = action_data::tag;
        if( "value" == options.prog_select() ) {
            return search::Tree_HeuristicMixedRefinementStrategy<
                    typename SearchSpace::Abstraction, ad::value>(
                options.width(), options.depth(), options.ad_init_delta_t(),
                options.prog_split_state_proportion() );
        }
        else if( "lower_value_bound" == options.prog_select() ) {
            return search::Tree_HeuristicMixedRefinementStrategy<
                    typename SearchSpace::Abstraction, ad::lower_value_bound>(
                options.width(), options.depth(), options.ad_init_delta_t(),
                options.prog_split_state_proportion() );
        }
        else if( "upper_value_bound" == options.prog_select() ) {
            return search::Tree_HeuristicMixedRefinementStrategy<
                    typename SearchSpace::Abstraction, ad::upper_value_bound>(
                options.width(), options.depth(), options.ad_init_delta_t(),
                options.prog_split_state_proportion() );
        }
        else {
            BOOST_THROW_EXCEPTION( std::invalid_argument( "prog_select" ) );
        }
    }
};

// ---------------------------------------------------------------------------

// FIXME: If .parameters() is part of the DomainConcept, we don't need to pass
// 'parameters' to this function.
template<typename Domain, typename SearchSpace>
std::unique_ptr<mcplan::Policy<Domain>> createPolicy(
    Options const& options, SamplingContext& sim_ctx, Domain const* domain, SearchSpace search_space )
{
    MCPLAN_LOG_TRACE(( std::cout << "createPolicy()" << std::endl ));
    if( "ss" == options.algorithm() ) {
        return std::make_unique<search::SparseSamplingPolicy<Domain, SearchSpace>>(
            sim_ctx, domain, search_space, options.width(), options.depth() );
    }
    else if( "fsss" == options.algorithm() ) {
        return std::make_unique<search::FSSSPolicy<Domain, SearchSpace>>(
            sim_ctx, domain, search_space, options.width(), options.depth()  );
    }
//    else if( "uct" == options.algorithm() ) {
//        return std::make_unique<search::UCTPolicy<Domain, SearchSpace>>(
//            sim_ctx, domain, parameters, search_space, options.width(), options.depth(), options.abstraction() );
//    }
    else {
        throw std::invalid_argument( "algorithm" );
    }
}

// ---------------------------------------------------------------------------

// FIXME: If .parameters() is part of the DomainConcept, we don't need to pass
// 'parameters' to this function.
template<typename Domain, typename SearchSpace>
std::unique_ptr<mcplan::Policy<Domain>> createProgPolicy(
    Options const& options, SamplingContext& sim_ctx, Domain const* domain, SearchSpace search_space )
{
    MCPLAN_LOG_TRACE(( std::cout << "createProgPolicy()" << std::endl ));
    if( "fsss" == options.algorithm() ) {
        using PolicyType = search::ProgressiveAbstractFsssPolicy<Domain, SearchSpace>;
        using RefinementStrategy = typename PolicyType::RefinementStrategy;
        auto refine = CreateRefinementStrategy<PolicyType>::call( options );
        return std::make_unique<search::ProgressiveAbstractFsssPolicy<Domain, SearchSpace>>(
            sim_ctx, domain, search_space, options.width(), options.depth(), refine );
    }
    else {
        throw std::invalid_argument( "algorithm" );
    }
}

// ---------------------------------------------------------------------------

template<typename Domain>
class PerDecisionBudget : public mcplan::Policy<Domain>
{
public:
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using ActionGenerator   = std::function<ActionType(SamplingContext&)>;

    explicit PerDecisionBudget( std::unique_ptr<Policy<Domain>> pi )
        : pi_( std::move(pi) )
    { }

    ActionGenerator operator ()( StateType const& s ) const override
    {
        auto f = (*pi_)( s );
        return [f]( SamplingContext& ctx ) {
            ctx.budget().reset();
            return f( ctx );
        };
    }

private:
    std::unique_ptr<Policy<Domain>> pi_;
};

template<typename Domain, typename AbstractionDiagramType>
typename AbstractSearchSpace<Domain, AbstractionDiagramType>::InitialAbstractionFactory
    createInitialAbstractionDiagram( Options const& options )
{
    MCPLAN_LOG_TRACE(( std::cout << "createInitialAbstractionDiagram()" << std::endl ));
    if( "Tree_TopState_RepeatAction" == options.ad_init() ) {
        auto const ad_init_delta_t = options.ad_init_delta_t();
        auto const ad_init_max_depth = options.ad_init_max_depth();
        return [ad_init_delta_t, ad_init_max_depth]( Domain const* domain ) {
            return AbstractionDiagramType::Tree_TopState_RepeatAction(
                domain, ad_init_delta_t, ad_init_max_depth );
        };
    }
    else {
        BOOST_THROW_EXCEPTION( std::invalid_argument( "ad_init" ) );
    }
}

template<typename Domain, typename Simulator>
std::unique_ptr<mcplan::Policy<Domain>> createAbstractionDiagramPolicy(
        Options const& options, SamplingContext& sim_ctx,
        Domain const* domain, Simulator sim )
{
    MCPLAN_LOG_TRACE(( std::cout << "createAbstractionDiagramPolicy()" << std::endl ));
    namespace ab = mcplan::abstraction;
    if( true ) { // TODO: Add configuration options for picking abstractions
        using Vertex = ab::AbstractionDiagramVertex;
        using SplitEvaluator = ab::AStarIrrelevanceSplitEvaluator<Domain>;
        using SAbstraction = ab::DecisionTreeStateAbstraction<Domain, Vertex, SplitEvaluator>;
        using AAbstraction = ab::FiniteActionSetAbstraction<Domain, Vertex>;
        using Abstraction = ab::AbstractionDiagram<Domain, SAbstraction, AAbstraction>;
        // TODO: Make this a parameter
        using SS_Strategy = CeilingSparseSamplingStrategy; //IIDSparseSamplingStrategy<>;
        auto ad_init = createInitialAbstractionDiagram<Domain, Abstraction>( options );
        if( "fixed" == options.refinement() ) {
            return createPolicy<Domain>( options, sim_ctx, domain,
                AbstractSearchSpace<Domain, Abstraction, SS_Strategy>( sim, ad_init ) );
        }
        else if( "prog" == options.refinement() ) {
            return createProgPolicy<Domain>( options, sim_ctx, domain,
                AbstractSearchSpace<Domain, Abstraction, SS_Strategy>( sim, ad_init ) );
        }
        else {
            BOOST_THROW_EXCEPTION( std::invalid_argument( "refinement" ) );
        }
    }
    else {
        BOOST_THROW_EXCEPTION(
            std::invalid_argument( "{ad_action_abstraction, ad_state_abstraction}" ) );
    }
}

template<typename Domain>
std::unique_ptr<mcplan::Policy<Domain>> createAgent(
        Options const& options, SamplingContext& sim_ctx, Domain const* domain)
{
    MCPLAN_LOG_TRACE(( std::cout << "createAgent()" << std::endl ));
    auto parameters = createParameters<Domain>( options );
    auto sim = createSimulator<Domain>( options, parameters );
    if( "ground" == options.abstraction() ) {
        return createPolicy<Domain>( options, sim_ctx, domain,
                                     GroundSearchSpace<Domain>( sim ) );
    }
    else if( "ad" == options.abstraction() ) {
        return createAbstractionDiagramPolicy( options, sim_ctx, domain, sim );
    }
    else {
        BOOST_THROW_EXCEPTION( std::invalid_argument( "abstraction" ) );
    }
}

// ---------------------------------------------------------------------------

template<typename Domain>
class RewardSequenceConsumer : public sim::TrajectoryConsumer<Domain>
{
public:
    using RewardType = typename Domain::RewardType;

    void consume( StateSample<Domain>&& ss ) override
    {
        MCPLAN_LOG(( std::cout << "state: (" << ss.s() << ", " << ss.r() << ")" << std::endl ));
        rs_.back() += ss.r();
    }

    void consume( ActionSample<Domain>&& as ) override
    {
        MCPLAN_LOG(( std::cout << "action: (" << as.a() << ", " << as.r() << ")" << std::endl ));
        rs_.push_back( as.r() );
    }

    std::vector<double> doubleValues() const
    {
        std::vector<double> result;
        for( RewardType const r : rs_ ) {
            result.push_back( static_cast<double>( r ) );
        }
        return result;
    }

private:
    std::vector<RewardType> rs_;
};

class Experiment
{
public:
    virtual std::vector<double> runEpisode( Options const& options, SamplingContext& world_ctx ) = 0;
};

template<typename Domain>
class DomainExperiment : public Experiment
{
public:
    using StateType = typename Domain::StateType;

    DomainExperiment( SamplingContext& sim_ctx )
        : sim_ctx_( sim_ctx )
    { }

    std::vector<double> runEpisode( Options const& options, SamplingContext& world_ctx ) override
    {
        auto parameters = createParameters<Domain>( options );
        auto domain = createDomain<Domain>( options, parameters );
        auto s0 = domain.initialState( world_ctx );
        MCPLAN_LOG(( std::cout << "initial state: " << *s0 << std::endl ));
        auto pi = createAgent<Domain>( options, sim_ctx_, &domain);
        auto sim = createSimulator<Domain>( options, parameters );
        RewardSequenceConsumer<Domain> consumer;
        sim.sampleTrajectory( world_ctx, consumer, domain, *s0, make_partial(*pi), options.episode_length() );
        return consumer.doubleValues();
    }

private:
    SamplingContext& sim_ctx_;
};

std::unique_ptr<Experiment> createExperiment( Options const& options, SamplingContext& sim_ctx )
{
    if( "Investment" == options.domain() ) {
        return std::make_unique<DomainExperiment<Investment::Domain>>( sim_ctx );
    }
    else if( "Racetrack" == options.domain() ) {
        return std::make_unique<DomainExperiment<Racetrack::Domain>>( sim_ctx );
    }
    else if( "SphereNavigation" == options.domain() ) {
        return std::make_unique<DomainExperiment<SphereNavigation::Domain>>( sim_ctx );
    }
    else if( "Sysadmin" == options.domain() ) {
        return std::make_unique<DomainExperiment<Sysadmin::Domain>>( sim_ctx );
    }
    else {
        throw std::invalid_argument( "domain" );
    }
}

// ---------------------------------------------------------------------------

std::pair<std::string, std::string> parse_option( std::string const& s )
{
    auto i = s.find( '=' );
    if( i == std::string::npos ) {
        BOOST_THROW_EXCEPTION( std::invalid_argument( "Not a 'key=value' pair" ) );
    }
    return std::make_pair( s.substr( 0, i ), s.substr( i+1 ) );
}

int main( int argc, char* argv[] ) try
{
    using namespace mcplan;
    std::map<std::string, std::string> options_map;
    for( std::size_t i = 1; i < argc; ++i ) {
        auto p = parse_option( argv[i] );
        MCPLAN_LOG(( std::cout << "cmd_option: " << p.first << " = " << p.second << std::endl ));
        options_map.insert( p );
    }
    Options options( options_map );

    std::vector<double> running_mean( options.nruns() + 1 );
    std::vector<double> TrialValue( options.nruns() );
    running_mean[0]=0;

    mcplan::SamplingContext::rng_t master_rng( options.seed() );
    std::uniform_int_distribution<mcplan::SamplingContext::rng_t::result_type> seed_distribution;
    for( unsigned i=1; i<= options.nruns(); i++)
    {
        mcplan::SamplingContext sim_ctx( mcplan::SamplingContext::rng_t( seed_distribution(master_rng) ),
                                         std::make_unique<SampleBudget>( options.budget() ) );
        mcplan::SamplingContext world_ctx( mcplan::SamplingContext::rng_t( seed_distribution(master_rng) ) );

        std::unique_ptr<Experiment> experiment = createExperiment( options, sim_ctx );
        MCPLAN_LOG(( std::cout << "[Episode " << i << "]" << std::endl ));
        std::vector<double> rs = experiment->runEpisode( options, world_ctx );
        MCPLAN_LOG(( std::cout << "[Episode " << i << " ok]" << std::endl ));
        double const total_reward = std::accumulate( rs.begin(), rs.end(), 0.0 );
        MCPLAN_LOG(( std::cout << "[Episode " << i << " total_reward: " << total_reward << "]" << std::endl ));
        running_mean[i]= (running_mean[i-1]*(i-1)+total_reward)/i;
        TrialValue[i-1]=total_reward;
    }
    double avg =running_mean[options.nruns()];
    double stdev = 0;
    for( unsigned i = 0; i < options.nruns(); ++i )
    {
        stdev += (avg - TrialValue[i]) * (avg - TrialValue[i]);
    }
    stdev = sqrt(stdev) / (options.nruns() - 1);
    MCPLAN_LOG(( std::cout << "[total_reward mean: " << avg << "]" << std::endl ));
    MCPLAN_LOG(( std::cout << "[total_reward std.dev: " << stdev << "]" << std::endl ));
    MCPLAN_LOG(( std::cout << "[total_reward std.error: " \
                 << (stdev / std::sqrt(options.nruns())) << "]" << std::endl ));
    MCPLAN_LOG(( std::cout << "[total_reward n: " << options.nruns() << "]" << std::endl ));
    MCPLAN_LOG(( std::cout << "[Alles gut!]" << std::endl ));
    return 0;
}
catch( std::exception& ex ) {
    std::cout << boost::diagnostic_information( ex ) << std::endl;
}

