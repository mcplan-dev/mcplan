#ifndef SPHERENAVIGATION_INTERFACE_HPP
#define SPHERENAVIGATION_INTERFACE_HPP



#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/domains/SphereNavigation.hpp"
#include "mcplan/domains/SphereNavigation/Action.hpp"
#include "mcplan/domains/SphereNavigation/State.hpp"
#include "mcplan/domains/SphereNavigation/Domain.hpp"
#include "mcplan/domains/SphereNavigation/Simulator.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/node/TreePrinter.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/search/GreedyPolicy.hpp"
#include "mcplan/search/ForwardSearchSparseSampling.hpp"
#include "mcplan/search/uct.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"
#include "mcplan/refinement/SplitStateNode.hpp"
#include "mcplan/refinement/UnpruneAction.hpp"

#include <boost/math/constants/constants.hpp>
#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>

//#define TEMPORAL_ABSTRACTION
//#define GROUND

namespace mcplan{

class SphereNavigation_interface{
public:

    using SphereNavigation = mcplan::domains::SphereNavigation;
    using D = SphereNavigation::Domain;
    struct NodeData {
        typedef boost::fusion::map<
            boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
            boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
            boost::fusion::pair<state_data::tag::value, state_data::value<D>>,
            boost::fusion::pair<state_data::tag::variance, state_data::variance<D>>,
            boost::fusion::pair<state_data::tag::upper_value_bound, state_data::upper_value_bound<D>>,
            boost::fusion::pair<state_data::tag::lower_value_bound, state_data::lower_value_bound<D>>,
            boost::fusion::pair<state_data::tag::avg_return, state_data::avg_return<D>>
        > StateData;

        typedef boost::fusion::map<
            boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
            boost::fusion::pair<action_data::tag::value, action_data::value<D>>,
            boost::fusion::pair<action_data::tag::variance, action_data::variance<D>>,
            boost::fusion::pair<action_data::tag::upper_value_bound, action_data::upper_value_bound<D>>,
            boost::fusion::pair<action_data::tag::lower_value_bound, action_data::lower_value_bound<D>>,
            boost::fusion::pair<action_data::tag::avg_return, action_data::avg_return<D>>
        > ActionData;

        typedef node::SetStorage<GroundStateNode<D, NodeData>> StateStorage;
        typedef node::SetStorage<GroundActionNode<D, NodeData>> ActionStorage;
    };

    SphereNavigation_interface(mcplan::SamplingContext& world_ctx,mcplan::SamplingContext& sim_ctx,std::string algo, unsigned width, unsigned depth,  float&reward, unsigned abstractionType)
    {


        //double const pi = boost::math::constants::pi<double>();
        int const T = 100;
        SphereNavigation::Position init_pos(1,0,0);
        SphereNavigation::Position goal_pos(1,-0.10,0.10);

        SphereNavigation::Parameters params( T,init_pos ,goal_pos );
        auto s0 = SphereNavigation::StateBuilder( params ).finish();




        D const domain( params );
        //mcplan::SamplingContext sim_ctx( mcplan::SamplingContext::rng_t( 42 ) );

        SphereNavigation::Simulator sim( params );
        SphereNavigation::Simulator world( params );

    #ifdef GROUND
        using StateNodeType = GroundStateNode<D, NodeData>;
        using TreeBuilder = GroundTreeBuilder<D, NodeData>;
        TreeBuilder builder( sim );
        auto& adapted_domain = domain;
    #else
        // Initialize an abstraction diagram
        using namespace mcplan::abstraction;
        //AbstractionDiagram<D>::Graph g;


        AbstractionDiagram<D> ad(abstractionType,domain,*s0);
        //AbstractionDiagram<D> ad( std::move(g) );


        using StateNodeType = AggregateStateNode<D, NodeData>;
        using ActionNodeType =AggregateActionNode<D,NodeData>;
        using TreeBuilder = AggregateTreeBuilder<D, NodeData>;

        TreeBuilder builder( ad, sim );

        AbstractDomain<D> adapted_domain( &ad );

    #endif


        GreedyPolicy<typename TreeBuilder::TreeDomain, StateNodeType> execution_policy(builder.treeDomain(domain));
        int const ss_width = width;
        int const ss_depth = depth;
        unsigned execution_horizon=1;//T;
        float total_reward=0;


        for(unsigned i=0; i< execution_horizon;i++)
        {

            sim_ctx.budget().reset();
            if(s0->isTerminal( params ))
            {

                break;
            }
#ifdef GROUND
            StateNodeType sn0( std::move(s0) );
            sn0.sample(sn0.s(),0);

#else

            GroundStateNode<D, NodeData> gsn( std::move(s0) );
            StateNodeType sn0( ad.start_node() );
            sn0.sample( &gsn, gsn.s(), 0 );
#endif

            if(algo=="sparse_sample") {

                search::sparse_sample(
                    sim_ctx, adapted_domain, params, builder, sn0, ss_width, ss_depth,
                    [&goal_pos]( auto const& s ) {
                    return -distance_euclidean( s.pos(), goal_pos ); } );

               //while(!sim_ctx.budget().expired())
               {
                   std::cout << i<<"=== Abstract tree ===" << std::endl;
                   depth_first_traversal( sn0, mcplan::node::TreePrinter() );
                   abstraction::UnpruneAction<AbstractDomain<D>,StateNodeType,ActionNodeType,AbstractionDiagram<D>> s(sim_ctx,sn0,ad);
                   //abstraction::SplitStateNode<Domain,StateNodeType,ActionNodeType,Diagram> s(x0,ctx,g_);
                   search::sparse_sample(
                       sim_ctx, adapted_domain, builder, sn0, ss_width, ss_depth,
                       [&goal_pos]( auto const& s ) {
                       return -distance_euclidean( s.pos(), goal_pos ); } );
                   std::cout << i<<"=== Abstract tree ===" << std::endl;
                   depth_first_traversal( sn0, mcplan::node::TreePrinter() );
               }
            }
            else if (algo=="uct")
                search::uct(
                    sim_ctx, domain, params, builder, sn0, ss_width, ss_depth );
            else
            {

                search::forward_search_sparse_sampling
                        (sim_ctx,adapted_domain,params,builder,sn0,ss_width,ss_depth,
                         [&goal_pos]( auto const& s ) {
                            return -distance_euclidean( s.pos(), goal_pos );});
               }
           //std::cout << i<<"=== Abstract tree ===" << std::endl;
            //depth_first_traversal( sn0, mcplan::node::TreePrinter() );
     //       std::cout << "Value: " << mcplan::get<state_data::tag::value>( sn0 ) << std::endl;

            //std::cout << "=== Ground tree ===" << std::endl;
            //depth_first_traversal( sn0, mcplan::node::TreePrinter() );
            //std::cout << "Value: " << mcplan::get<state_data::tag::avg>( sn0 ) << std::endl;

            auto const an = execution_policy( sn0,world_ctx,algo);

#ifdef GROUND
            auto actual_state=sn0.s();
            auto actual_action=an.get();
#else
            auto actual_action = ad.sampleAction( sim_ctx,sn0.label(), an.get() );
            auto actual_state=gsn.s();
#endif
            //std::cout<<"actual action"<<actual_action<<"\n";
            auto tr = sim.sampleTransition( world_ctx, params, actual_state , actual_action );
            //std::cout<<"\nState"<<actual_state<<"\nAction Executed" <<actual_action<<"\n";
            float action_reward = std::get<0>(tr).r();
            float state_reward = std::get<1>(tr).r();
            s0 = std::get<1>(tr).transfer_s();
            //std::cout<<"\nnew State"<<*s0<<"\nAction Executed" <<actual_action<<"\n";
            total_reward += state_reward + action_reward;
        }
        reward= total_reward;
        /* Printing Trees */
    //    std::cout << "=== Abstract tree ===" << std::endl;
    //    depth_first_traversal( sn0, TreePrinter() );
    //    std::cout << "Value: " << mcplan::get<state_data::tag::value>( sn0 ) << std::endl;

        //std::cout << "=== Ground tree ===" << std::endl;
       // depth_first_traversal( gsn, mcplan::node::TreePrinter() );
       // std::cout << "Value: " << mcplan::get<state_data::tag::value>( gsn ) << std::endl;

        //std::cout<<"reward final"<<reward<<"\n";
    }
};
}
#endif // SPHERE_NAVIGATION_H
