TEMPLATE = app

TARGET = generic_test

win32-msvc* {
    QMAKE_CXXFLAGS += -bigobj
}

SOURCES += \
    generic_test.cpp

HEADERS += \
#    sysadmin_interface.hpp \
#    ri_interface.hpp \
#    AdvancedSysadmin_interface.hpp \
#    CartPole_interface.hpp \
    SphereNavigation_interface.hpp
