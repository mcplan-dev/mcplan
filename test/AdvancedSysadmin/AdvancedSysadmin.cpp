#pragma warning (disable: 4180)

#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/domains/AdvancedSysadmin.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>

#define HUB_ABSTRACTION

// ---------------------------------------------------------------------------

struct TreePrinter
{
    int indent_size = 2;
    int indent = 0;

    template<typename NodeType>
    void enterNode( NodeType const& n )
    {
        std::cout << std::setw( indent ) << "" << n << std::endl;
        indent += indent_size;
    }

    template<typename NodeType>
    void exitNode( NodeType const& /*n*/ )
    {
        indent -= indent_size;
    }
};


template<typename Diagram>
class TopStateAbstraction : public mcplan::abstraction::StateAbstraction<Diagram>
{
public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::StateType;

    explicit TopStateAbstraction( AbstractType x )
        : x_( x )
    { }

    AbstractType apply( GroundType const& /*g*/ ) const override
    {
        return x_;
    }

private:
    AbstractType x_;
};

template<typename Diagram>
class Count_Ones_ActionAbstraction : public mcplan::abstraction::ActionAbstraction<Diagram>
{
    using BitVectorType = long long;

    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        AbstractType apply( GroundType const& g ) const override
        {
            // FIXME
            BitVectorType ActionVector=g.ComputerSwitch();
            int count_ons=0;
            for(int i=0;i<sizeof(BitVectorType)*8;i++)
            {
                if(ActionVector & BitVectorType(1<<i))
                    count_ons+=1;
            }
            return leaves_[count_ons];


        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const& y ) const override
        {
            BitVectorType GroundActionVector=BitVectorType(0);
            int i;
            for(i=0;i<leaves_.size();i++)
            {
                if(y==leaves_[i])
                    break;
            }
            for(int j=0;j<i;j++)
            {
                int k;
                do{
                    std::uniform_int_distribution<> pr( 0, i );
                    k=pr(ctx.rng());
                 }while(GroundActionVector&=BitVectorType(1<<k));
                 GroundActionVector|=BitVectorType(1<<k);

            }
            return GroundActionVector;

        }
    private :
        std::vector<AbstractType> leaves_;
};



// ---------------------------------------------------------------------------

template<typename Diagram>
class Hub_ActionAbstraction : public mcplan::abstraction::ActionAbstraction<Diagram>
{
    using BitVectorType = long long;

    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        AbstractType apply( GroundType const& g ) const override
        {
            // FIXME
            BitVectorType ActionVector=g.ComputerSwitch();


           if(ActionVector & BitVectorType(1))
               return leaves_[0];
           else
               return leaves_[1];


        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const& y ) const override
        {
            BitVectorType GroundActionVector=BitVectorType(0);
            if(y==leaves_[0])
                 GroundActionVector|=BitVectorType(1);
            int MaxOns = 3;
            if( GroundActionVector&=BitVectorType(1))
                MaxOns = 2;
            int nc=5;
            bool temp[nc]={0};
            for(int i=0;i<MaxOns;i++)
            {
                int comp_set;
                do{
                 std::uniform_int_distribution<> p( 1, nc );
                 comp_set=p( ctx.rng() );
                }while(temp[comp_set]);
                temp[comp_set]=1;
                 std::uniform_real_distribution<> p;
                 if(p(ctx.rng())>0.5)
                     GroundActionVector|=BitVectorType(1<< comp_set);
            }
            return GroundActionVector;

        }
    private :
        std::vector<AbstractType> leaves_;
};
// ---------------------------------------------------------------------------

int main( int /*argc*/, char* /*argv*/[] )
{
    using namespace mcplan;
    namespace AdvancedSysadmin = mcplan::domains::AdvancedSysadmin;
    using D = AdvancedSysadmin::Domain;
    int const T = 10;
    int const nc = 5;
    int const MaxOn=3;
    int const tt = 2; // Type of Topology 0 for line, 1 for Ring , 2 for Hub

    AdvancedSysadmin::Parameters params( T, nc, MaxOn, tt );
    AdvancedSysadmin::Topology topology(tt,nc);
    auto s0 = AdvancedSysadmin::StateBuilder( params ).finish(&topology);
    std::cout << *s0 << std::endl;

    // FIXME: Since each data type has a nested 'tag' typedef, we could
    // initialize a fusion::set and then automatically transform it to a map.
    // This would simplify the declaration and initialization of NodeData.
    struct NodeData {
        typedef boost::fusion::map<
            boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
            boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
            boost::fusion::pair<state_data::tag::value, state_data::value<D>>
        > StateData;

        typedef boost::fusion::map<
            boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
            boost::fusion::pair<action_data::tag::value, action_data::value<D>>
        > ActionData;

        typedef node::SetStorage<GroundStateNode<D, NodeData>> StateStorage;
        typedef node::SetStorage<GroundActionNode<D, NodeData>> ActionStorage;
    };

    D const domain;
    mcplan::SamplingContext sim_ctx( mcplan::SamplingContext::rng_t( 42 ) );
    mcplan::SamplingContext world_ctx( mcplan::SamplingContext::rng_t( 43 ) );
    AdvancedSysadmin::Simulator sim;
    AdvancedSysadmin::Simulator world;

    using namespace mcplan::abstraction;
    AbstractionDiagram<D>::Graph g;
    using Vertex = AbstractionDiagram<D>::Vertex;
    Vertex x0 = boost::add_vertex( g );
    g[x0] = AbstractionDiagram<D>::StateVertex();
    std::vector<Vertex> leaves;
#ifdef COUNT_ABSTRACTION
    Count_Ones_ActionAbstraction<AbstractionDiagram<D>> Count_Ones_A;
    //CP_deltaDistribution<AbstractionDiagram<D>> deltaDist_a;

    boost::get<AbstractionDiagram<D>::StateVertex>(g[x0]).action_abstraction = &Count_Ones_A;
    TopStateAbstraction<AbstractionDiagram<D>> top_s( x0 );

    for( int i = 0; i < MaxOn; ++i ) {
        Vertex yi = boost::add_vertex( g );
        boost::add_edge( x0, yi, g );
        boost::add_edge( yi, x0, g );
        g[yi] = AbstractionDiagram<D>::ActionVertex();
        boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).state_abstraction = &top_s;
        //boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).action_distribution = &deltaDist_a;
        leaves.push_back(yi);
    }
    Count_Ones_A.setLeaves(leaves);
#endif
#ifdef HUB_ABSTRACTION
    Hub_ActionAbstraction<AbstractionDiagram<D>> Hub_A;
    boost::get<AbstractionDiagram<D>::StateVertex>(g[x0]).action_abstraction = &Hub_A;
    TopStateAbstraction<AbstractionDiagram<D>> top_s( x0 );
    for(int i=0;i< 2; i++){
        Vertex yi = boost::add_vertex( g );
        boost::add_edge( x0, yi, g );
        boost::add_edge( yi, x0, g );
        g[yi] = AbstractionDiagram<D>::ActionVertex();
        boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).state_abstraction = &top_s;
        //boost::get<AbstractionDiagram<D>::ActionVertex>(g[yi]).action_distribution = &deltaDist_a;
        leaves.push_back(yi);
    }
    Hub_A.setLeaves(leaves);
#endif

    AbstractionDiagram<D> ad( std::move(g) );
    int const ss_width = 5;
    int const ss_depth = 5;

    using NodeType = AggregateStateNode<D, NodeData>;
    GroundStateNode<D, NodeData> gsn( std::move(s0) );
    NodeType sn0( x0 );
    sn0.sample( &gsn, *s0, 0 );
    AggregateTreeBuilder<D, NodeData> builder( ad, sim );
    AbstractDomain<D> adapted_domain( &ad );

    search::sparse_sample(
        sim_ctx, adapted_domain, builder, sn0, ss_width, ss_depth,
        []( auto const& /*s*/ ) { return 10; } );

    std::cout << "=== Abstract tree ===" << std::endl;
    depth_first_traversal( sn0, TreePrinter() );
    std::cout << "Value: " << mcplan::get<state_data::tag::value>( sn0 ) << std::endl;

    std::cout << "=== Ground tree ===" << std::endl;
    depth_first_traversal( gsn, TreePrinter() );
    std::cout << "Value: " << mcplan::get<state_data::tag::value>( gsn ) << std::endl;

    return 0;
}
