#include "mcplan/domains/Investment.hpp"

#include "mcplan/domains/Racetrack.hpp"

#include "mcplan/domains/SphereNavigation.hpp"
#include "mcplan/domains/SphereNavigation/Action.hpp"
#include "mcplan/domains/SphereNavigation/State.hpp"
#include "mcplan/domains/SphereNavigation/Domain.hpp"
#include "mcplan/domains/SphereNavigation/Simulator.hpp"

#include "mcplan/SamplingContext.hpp"

#include <boost/program_options.hpp>

#include <iomanip>
#include <iostream>
#include <string>

using namespace mcplan;
using namespace mcplan::domains;

using Options = boost::program_options::variables_map;

// ---------------------------------------------------------------------------

template<typename Domain>
typename Domain::ParametersType createParameters( Options const& options );

template<>
Investment::Domain::ParametersType createParameters<Investment::Domain>( Options const& options )
{
    std::cout << "createParameters<Investment>()" << std::endl;
    int const T = options["T"].as<int>();
    return Investment::Parameters::Formula_A();
}

template<>
Racetrack::Domain::ParametersType createParameters<Racetrack::Domain>( Options const& options )
{
    std::cout << "createParameters<SphereNavigation>()" << std::endl;
    int const T             = options["T"].as<int>();
    double const slip       = options["racetrack.slip"].as<double>();
    std::string const track = options["racetrack.track"].as<std::string>();
    auto construct = [=]( auto const& track ) {
        return Racetrack::Parameters( track, T, slip );
    };

    if( "bbs_large" == track ) {
        return construct( Racetrack::Tracks::BartoBradtkeSingh_Large() );
    }
    else if( "bbs_small" == track ) {
        return construct( Racetrack::Tracks::BartoBradtkeSingh_Small() );
    }
    else {
        throw std::invalid_argument( "racetrack.track" );
    }
}

template<>
SphereNavigation::Domain::ParametersType createParameters<SphereNavigation::Domain>( Options const& options )
{
    std::cout << "createParameters<SphereNavigation>()" << std::endl;

    double const pi = boost::math::constants::pi<double>();
    int const T = 100;
    SphereNavigation::Position init_pos(1,0,0);
    SphereNavigation::Position goal_pos(1,0.05,0.05);

    return SphereNavigation::Parameters( T,init_pos ,goal_pos );
}

// ---------------------------------------------------------------------------

template<typename Domain>
Domain createDomain( Options const& options, typename Domain::ParametersType const& parameters );

template<>
Investment::Domain createDomain<Investment::Domain>(
        Options const& options, Investment::Parameters const& parameters )
{
    return Investment::Domain( parameters );
}

template<>
Racetrack::Domain createDomain<Racetrack::Domain>(
        Options const& options, Racetrack::Parameters const& parameters )
{
    return Racetrack::Domain( parameters );
}

template<>
SphereNavigation::Domain createDomain<SphereNavigation::Domain>(
        Options const& options, SphereNavigation::Parameters const& parameters )
{
    return SphereNavigation::Domain( parameters );
}

// ---------------------------------------------------------------------------

template<typename Domain>
typename Domain::SimulatorType createSimulator(
        Options const& options, typename Domain::ParametersType const& parameters );

template<>
Investment::Domain::SimulatorType createSimulator<Investment::Domain>(
        Options const& options, Investment::Domain::ParametersType const& parameters )
{
    std::cout << "createSimulator<Investment>()" << std::endl;
    return Investment::Simulator();
}

template<>
Racetrack::Domain::SimulatorType createSimulator<Racetrack::Domain>(
        Options const& options, Racetrack::Domain::ParametersType const& parameters )
{
    std::cout << "createSimulator<Racetrack>()" << std::endl;
    return Racetrack::Simulator();
}

template<>
SphereNavigation::Domain::SimulatorType createSimulator<SphereNavigation::Domain>(
        Options const& options, SphereNavigation::Domain::ParametersType const& parameters )
{
    std::cout << "createSimulator<SphereNavigation>()" << std::endl;
    return SphereNavigation::Simulator( parameters );
}

// ---------------------------------------------------------------------------

template<typename Domain>
using HeuristicFunction = std::function<typename Domain::RewardType(typename Domain::StateType const&)>;

template<typename Domain>
HeuristicFunction<Domain> createHeuristic( Options const& options, typename Domain::ParametersType const& parameters )
{
    std::cout << "createHeuristic<T>()" << std::endl;
    return []( auto const& s ) { return typename Domain::RewardType( 0 ); };
}

template<>
HeuristicFunction<Racetrack::Domain> createHeuristic<Racetrack::Domain>(
        Options const& options, Racetrack::Parameters const& parameters )
{
    std::cout << "createHeuristic<Racetrack>()" << std::endl;
    return Racetrack::ShortestPathHeuristic( parameters );
}

template<>
HeuristicFunction<SphereNavigation::Domain> createHeuristic<SphereNavigation::Domain>(
        Options const& options, SphereNavigation::Parameters const& parameters )
{
    std::cout << "createHeuristic<SphereNavigation>()" << std::endl;
    SphereNavigation::Position const goal_position = parameters.goal();
    return [goal_position]( auto const& s ) {
        return -domains::distance_euclidean( s.pos(), goal_position );
    };
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void showState( Investment::Parameters const& p, Investment::State const& s )
{
    std::cout << s << std::endl;
}

void showState( Racetrack::Parameters const& p, Racetrack::State const& s )
{
    using namespace Racetrack;
    for( int y = p.terrain().size() - 1; y >= 0; --y ) {
        auto const& row = p.terrain()[y];
        for( int x = 0; x < row.size(); ++x ) {
            auto const& t = row[x];
            if( x == s.x && y == s.y ) {
                std::cout << "X";
            }
            else {
                switch( t ) {
                case TerrainType::Goal:
                    std::cout << "g"; break;
                case TerrainType::Start:
                    std::cout << "s"; break;
                case TerrainType::Track:
                    std::cout << "t"; break;
                case TerrainType::Wall:
                    std::cout << "W"; break;
                }
            }
        }
        std::cout << std::endl;
    }
    std::cout << s << std::endl;
}

// ---------------------------------------------------------------------------

template<typename Domain>
class InteractivePolicy : public Policy<Domain>
{
public:
    InteractivePolicy( Domain const& domain, typename Domain::ParametersType const& parameters,
                       HeuristicFunction<Domain> const& h )
        : domain_( domain ), parameters_( parameters ), h_( h ),
          t_( 0 )
    { }

    ActionGenerator operator ()( StateType const& s ) const override
    {
        std::cout << "=== State (t = " << t_ << ") ===" << std::endl;
        showState( parameters_, s );
        std::cout << "h(s) = " << h_(s) << std::endl;
        std::vector<ActionType> as;
        for( auto a : domain_.actions( s ) ) {
            std::cout << as.size() << ": " << a << std::endl;
            as.push_back( a );
        }
        int ai = -1;
        std::cin >> ai;
        ActionType const a = as[ai];
        std::cout << "--- Action: " << a << std::endl;
        return [a]( SamplingContext& ) { return a; };
    }

private:
    Domain domain_;
    typename Domain::ParametersType parameters_;
    HeuristicFunction<Domain> h_;
    int t_;
};

template<typename Domain>
class Consumer : public sim::TrajectoryConsumer<Domain>
{
public:
    explicit Consumer( typename Domain::ParametersType const& parameters )
        : parameters_( parameters ), return_( 0 )
    { }

    void consume( StateSample<Domain>&& ss ) override
    {
        std::cout << "+++ sr: " << ss.r() << std::endl;
        return_ += ss.r();
        s_ = ss.transfer_s();
    }

    void consume( ActionSample<Domain>&& as ) override
    {
        std::cout << "+++ ar: " << as.r() << std::endl;
        return_ += as.r();
    }

    void onTrajectoryEnd() override
    {
        showState( parameters_, *s_ );
        std::cout << "Total return: " << return_ << std::endl;
        return_ = 0;
    }

private:
    typename Domain::ParametersType parameters_;
    std::unique_ptr<typename Domain::StateType const> s_;

    typename Domain::RewardType return_;
};

template<typename Domain>
void runDomain( Options const& options )
{
    mcplan::SamplingContext world_ctx( mcplan::SamplingContext::rng_t( options["seed"].as<int>() ) );
    auto parameters = createParameters<Domain>( options );
    auto domain = createDomain<Domain>( options, parameters );
    std::cout << "DomainExperiment: parameters = " << parameters << std::endl;
    auto sim = createSimulator<Domain>( options, parameters );
    Consumer<Domain> consumer( parameters );
    auto s0 = domain.initialState( world_ctx );
    InteractivePolicy<Domain> pi( domain, parameters, createHeuristic<Domain>( options, parameters ) );
    sim.sampleTrajectory( world_ctx, consumer, parameters, *s0, make_partial(pi), options["T"].as<int>() );
}

void run( Options const& options )
{
    std::string const domain = options["domain"].as<std::string>();
    if( "investment" == domain ) {
        runDomain<Investment::Domain>( options );
    }
    else if( "racetrack" == domain ) {
        runDomain<Racetrack::Domain>( options );
    }
    else {
        throw std::invalid_argument( "domain" );
    }
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

int main( int argc, char* argv[] ) try
{
    using namespace mcplan;
    namespace popt = boost::program_options;
    popt::options_description desc( "Command line interaction with problem domains" );
    desc.add_options()
        ("domain", popt::value<std::string>(),
         "{cartpole, investment, racetrack, sphere, sysadmin}")
        ("seed", popt::value<int>()->default_value(42),
         "rng seed")
        ("T", popt::value<int>()->default_value(100),
         "episode length")
        ("racetrack.slip", popt::value<double>()->default_value(0),
         "random slip probability")
        ("racetrack.track", popt::value<std::string>()->default_value("bbs_small"),
         "track layout {bbs_large, bbs_small}");
    popt::variables_map vm;
    popt::store( popt::command_line_parser(argc, argv).options(desc).run(), vm );
    popt::notify( vm );

    run( vm );

    return 0;
}
catch( std::exception& ex ) {
    std::cout << ex.what() << std::endl;
}
