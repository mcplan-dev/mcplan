#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/domains/Racetrack.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundTreeBuilder.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/node/TreePrinter.hpp"
#include "mcplan/search/SparseSampling.hpp"

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/fusion/sequence/intrinsic/at_key.hpp"
#include <boost/fusion/sequence/io.hpp>

#include <iostream>
#include <iomanip>

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

int main( int /*argc*/, char* /*argv*/[] )
{
    using namespace mcplan;
    namespace Racetrack = mcplan::domains::Racetrack;
    using D = Racetrack::Domain;

    int const T = 10;
    double const slip = 0.2;
    Racetrack::Parameters parameters(
        Racetrack::Tracks::BartoBradtkeSingh_Small(), T, slip );
    D domain( parameters );
    mcplan::SamplingContext sim_ctx( mcplan::SamplingContext::rng_t( 42 ) );
    auto s0 = domain.initialState( sim_ctx );
    Racetrack::Simulator sim;

    std::cout << *s0 << std::endl;

    // FIXME: Since each data type has a nested 'tag' typedef, we could
    // initialize a fusion::set and then automatically transform it to a map.
    // This would simplify the declaration and initialization of NodeData.
    struct NodeData {
        typedef boost::fusion::map<
            boost::fusion::pair<state_data::tag::n, state_data::n<D>>,
            boost::fusion::pair<state_data::tag::ravg, state_data::ravg<D>>,
            boost::fusion::pair<state_data::tag::value, state_data::value<D>>
        > StateData;

        typedef boost::fusion::map<
            boost::fusion::pair<action_data::tag::n, action_data::n<D>>,
            boost::fusion::pair<action_data::tag::value, action_data::value<D>>
        > ActionData;

        typedef node::SetStorage<GroundStateNode<D, NodeData>> StateStorage;
        typedef node::SetStorage<GroundActionNode<D, NodeData>> ActionStorage;
    };

    int const ss_width = 4;
    int const ss_depth = 2;
    using NodeType = GroundStateNode<D, NodeData>;
    NodeType sn0( std::move(s0) );
    GroundTreeBuilder<D, NodeData> builder( sim );
    search::sparse_sample(
        sim_ctx, domain, builder, sn0, ss_width, ss_depth,
        []( auto const& /*s*/ ) { return 10; } );

    depth_first_traversal( sn0, node::TreePrinter() );
    return 0;
}
