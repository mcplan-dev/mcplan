
This is a  repository for mcplan,having source code for Monte Carlo Tree Search Algorithms for Planning.

* Specifically, we provide a framework to use abstractions in Monte Carlo Tree Search Algorithms. The current code supports 3 different MCTS algorithms : Sparse Sampling, Forward Search Sparse Sampling (FSSS) and UCT.
* **Ground Mode vs Abstract Mode**: All these algorithms can be run in a ground mode where there are no abstractions and tree is built in ground state space. In contrast, there is an abstract mode where the algorithm takes an additional input as an abstractions diagram which is currently used to filter which nodes are abstracted while building the tree. The abstract tree is then used to further build the tree and statistics are shared among nodes which are part of same abstract node. Abstraction sub-directory is used to specify abstractions.
* **Action and Temporal Abstractions**: In contrast to previous works which supports only state abstractions, the current framework uses action abstractions as well as temporal abstractions.
* **Generic and Domain Specific Abstractions**:  The code currently supports various types of abstraction diagram. An abstraction diagram could be generic one such as abstraction for open-loop policy or random policy to a domain specific abstraction diagram. 
* **Domains**:  Currently, the code supports CartPole, SphereNavigation,  Sysadmin, Advanced Version of Sysadmin and we will extend the same to many other domains in future.
* **Independency in Modules**:  The salient feature of this code is that each module is independent of each other. The algorithm code is completely independent of the domain and whether it is run for ground version or a particular kind of abstraction diagram.

**Future Plan:**

In future, we plan to implement different refinement strategies to refine the initial abstraction diagram. We plan to incrementally built the tree of refined abstraction diagram from previous version 
