#ifndef UNPRUNE_ACTION_HPP__
#define UNPRUNE_ACTION_HPP__


#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/refinement/SplitStateNode.hpp"

namespace mcplan { namespace abstraction {


template<typename StateNodeType, typename Diagram>
void UnpruneAction( StateNodeType& sn,mcplan::SamplingContext& ctx,Diagram& g,
                    bool use_default_action,unsigned default_action)
{
    auto pruned_actions=g.getNewAction( sn.label(), sn.sampleActualState(ctx) );
    //std::cout<<"Action unpruned is "<<action<<" State Node is "<<SplitNodePtr->label()<<"\n";
    for(auto it=pruned_actions.begin();it!=pruned_actions.end();it++)
    {
         g.UnpruneActionVertex( sn.label(), *it, use_default_action, default_action );
    }

   // g.refinetemporally();
}

// Gives nodes at depth d
template < typename StateNodeType>
void getDepthNodes(SamplingContext& ctx, stats::ReservoirSampler<StateNodeType*>& snstar_candidates,StateNodeType& sn,unsigned int depth)
{

   if((mcplan::get<mcplan::state_data::tag::n>( sn )>1)&&(depth==0))
        snstar_candidates( ctx, &sn );
   else
   {
        sn.visitSuccessors([&ctx,&snstar_candidates,&depth](auto& an_next)
        {
            an_next.visitSuccessors([&ctx,&snstar_candidates,&depth](auto &sn_next){
                mcplan::abstraction::getDepthNodes(ctx,snstar_candidates,sn_next,depth-1);
            }
            );
        }
        );
   }
}
template < typename Domain,typename StateNodeType, typename ActionNodeType,typename Diagram>
StateNodeType* TemporallyRefine(StateNodeType& sn0,mcplan::SamplingContext& ctx,Diagram& g, unsigned d)
{
    stats::ReservoirSampler<StateNodeType*> snstar_candidates;
    mcplan::abstraction::getDepthNodes(ctx,snstar_candidates,sn0,d);
    if(*boost::begin(snstar_candidates.samples()))
    {
        StateNodeType& SplitNode=**boost::begin(snstar_candidates.samples());
    //ActionNodeType* SplitNodeParent=SplitNode.getParent();

        UnpruneAction( SplitNode, ctx, g, true, 0 );
        return (&SplitNode);
     }
    else
        return nullptr;

}

}

    
}

#endif
