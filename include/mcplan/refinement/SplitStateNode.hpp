#ifndef MCPLAN_SPLITSTATENODE_HPP__
#define MCPLAN_SPLITSTATENODE_HPP__


#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"

#include "mcplan/Log.hpp"

#include <queue>
#include <cmath>

namespace mcplan {

namespace abstraction {

template < typename StateNodeType>
void getAllNodes(SamplingContext& ctx, stats::ReservoirSampler<std::pair<StateNodeType*,unsigned int>>& snstar_candidates,StateNodeType& sn,unsigned int depth);


template < typename StateNodeType, typename ActionNodeType>
std::pair<ActionNodeType*,StateNodeType*> getSplitNode(SamplingContext& ctx,StateNodeType& sn0);

template < typename Domain,typename StateNodeType, typename ActionNodeType,typename Diagram>
class SplitStateNode{
public:
    SplitStateNode( StateNodeType& sn0,SamplingContext& ctx,Diagram& g, unsigned int depth)
    {
       stats::ReservoirSampler<std::pair<StateNodeType*,unsigned int>> snstar_candidates;
       mcplan::abstraction::getAllNodes(ctx,snstar_candidates,sn0,depth);
       StateNodeType& SplitNode=*(*boost::begin(snstar_candidates.samples())).first;
       SplitNodeDepth_=(*boost::begin(snstar_candidates.samples())).second;
       //std::pair<ActionNodeType*,StateNodeType*> ParentChildPair=getSplitNode<StateNodeType,ActionNodeType>(ctx,sn0);
       SplitNodeParent_= (SplitNode.getParent());

       MCPLAN_LOG_DEBUG(( std::cout << "State split below " << *SplitNodeParent_ << std::endl ));

       StateNodeType* sn_top=(SplitNodeParent_->getParent());

//       sn0.setParent(nullptr);
//       while(sn_top->getParent()!=nullptr)
//       {
//           sn_top->resetbounds();
//           auto an_top=sn_top->getParent();
//           an_top->resetbounds();
//           sn_top=an_top->getParent();
//       }
//       sn0.resetbounds();

       g.SplitStateVertex(ctx,SplitNode.label(),SplitNodeParent_->label());

       BuildRefinedTree(ctx,*SplitNodeParent_,g);

    }
    ActionNodeType* parent()
    {
        return (SplitNodeParent_);
    }
    unsigned depth()
    {
        return SplitNodeDepth_;
    }



    void BuildRefinedTree(mcplan::SamplingContext& ctx,ActionNodeType& yn, Diagram& g)
    {
           using GroundActionNode =typename ActionNodeType::GroundNodeType;
           using GroundStateNode =typename StateNodeType::GroundNodeType;


           //delete (&yn);
           yn.clearSuccessors();

           //GroundActionNode const& an=yn.getActionNode(ctx); // Works if there is only single action
           std::queue<std::pair<GroundActionNode const*,ActionNodeType*>> action_node_queue;
           std::queue<std::pair<GroundStateNode*,StateNodeType*>> state_node_queue;
           auto& as=yn.contents();
           for(auto it=as.begin();it!=as.end();it++)
                action_node_queue.push(std::make_pair(*it,&yn));


           while(!action_node_queue.empty())
           {
               while(!action_node_queue.empty())
              {
                   std::pair<GroundActionNode const*,ActionNodeType*> anpair=action_node_queue.front();
                   action_node_queue.pop();
                   GroundActionNode const& an=*(anpair.first);
                   ActionNodeType& yn=*(anpair.second);
                   an.visitSuccessors([&yn,&state_node_queue,&g](auto &sn_next)
                   {

                        auto x = g.successor( yn.label(), sn_next.label());
                        StateNodeType& xn_next =yn.addSuccessor(x);

                        xn_next.addNode(sn_next);
                        GroundStateNode* grnd_ptr=&sn_next;
                        StateNodeType* agg_ptr=&xn_next;
                        state_node_queue.push(std::make_pair(grnd_ptr,agg_ptr));

                   });
              }
              while(!state_node_queue.empty())
              {
                   std::pair<GroundStateNode*,StateNodeType*> snpair=state_node_queue.front();
                   state_node_queue.pop();
                   GroundStateNode& sn= *(snpair.first);
                   StateNodeType& xn=*(snpair.second);
                   sn.visitSuccessors([&xn,&action_node_queue,&g](auto &an_next ){

                       auto y = g.successor(xn.label(),an_next.label());
                       ActionNodeType&yn_next = xn.addSuccessor(*y);
                       yn_next.addNode(an_next);
                       GroundActionNode const* grnd_ptr=&an_next;
                       ActionNodeType* agg_ptr=&yn_next;

                       action_node_queue.push(std::make_pair(grnd_ptr,agg_ptr));
                   });
              }
          }

    }



//    void clearAggregateActionTree(ActionNodeType& yn)
//    {
//        yn.visitSuccessors([](auto &xn_next)
//        {
//            clearAggregateStateTree(xn_next);
//        }
//                    );
//        delete yn;
//    }

//    void clearAggregateStateTree(StateNodeType& xn)
//    {
//            xn.visitSuccessors([](auto &yn_next)
//            {
//                clearAggregateActionTree(yn_next);
//            }
//                        );
//            delete xn;
//    }

private:
       ActionNodeType* SplitNodeParent_=nullptr;
       unsigned SplitNodeDepth_=0;
};

template < typename StateNodeType>
void getAllNodes(SamplingContext& ctx, stats::ReservoirSampler<std::pair<StateNodeType*,unsigned int>>& snstar_candidates,StateNodeType& sn,unsigned int depth)
{

   if(mcplan::get<mcplan::state_data::tag::n>( sn )>1)
        snstar_candidates( ctx, std::make_pair(&sn,depth) );
    sn.visitSuccessors([&ctx,&snstar_candidates,&depth](auto& an_next)
    {
        an_next.visitSuccessors([&ctx,&snstar_candidates,&depth](auto &sn_next){
            mcplan::abstraction::getAllNodes(ctx,snstar_candidates,sn_next,depth-1);
        }
        );
    }
    );
}
//template < typename StateNodeType, typename ActionNodeType>
//std::pair<ActionNodeType*,StateNodeType*> getSplitNode(SamplingContext& /*ctx*/,StateNodeType& sn0)
//{

//     std::queue<StateNodeType*> SplitStateNodeQueue;


//     float highestVariance=0;
//     StateNodeType* highVarNode;
//     sn0.visitSuccessors([&SplitStateNodeQueue](auto &an_next ){
//         an_next.visitSuccessors([&SplitStateNodeQueue](auto &sn_next)
//         {
//             StateNodeType* ptr=&sn_next;
//             SplitStateNodeQueue.push(ptr);

//         }

//                     );
//     }
//     );
//     while(!SplitStateNodeQueue.empty())
//     {
//         StateNodeType* snTemp=SplitStateNodeQueue.front();
//         SplitStateNodeQueue.pop();

//         if((!std::isinf(mcplan::get<state_data::tag::variance>( *snTemp )) )&& (highestVariance <= mcplan::get<state_data::tag::variance>( *snTemp )))
//         {
//             highVarNode=snTemp;

//         }
//         snTemp->visitSuccessors([&SplitStateNodeQueue](auto &an_next ){
//             an_next.visitSuccessors([&SplitStateNodeQueue](auto &sn_next)
//             {
//                 StateNodeType* ptr=&sn_next;
//                 SplitStateNodeQueue.push(ptr);

//             }

//                         );
//         }

//      );
//     }

//     return std::make_pair((highVarNode->getParent()),highVarNode);

//}

}
    
}

#endif
