#ifndef MCPLAN_POLICY_HPP__
#define MCPLAN_POLICY_HPP__


#include "mcplan/SamplingContext.hpp"

#include "boost/optional.hpp"

#include <functional>

namespace mcplan {


template<typename Domain>
class Policy
{
public:
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using ActionGenerator   = std::function<ActionType()>;

    /**
     * @brief Returns a callable object that when called returns a
     * sample from the action distribution of this Policy.
     *
     * For a deterministic policy, the action generator will always yield
     * the same action.
     *
     * The purpose of this design is to allow the same setState()/getAction()
     * split we used in the Java version, but to allow those operations to
     * be 'const' with respect to Policy. Splitting the operations allows
     * evaluation of stochastic policies to be more efficient, since any work
     * needed to convert the state to the policy's internal representation
     * only needs to be done once.
     * @param s
     * @return
     */
    virtual ActionGenerator operator()( StateType const& s) const = 0;
};

template<typename Domain>
class PartialDomain
{
public:
    using ParametersType    = typename Domain::ParametersType;
    using StateType         = typename Domain::StateType;
    using ActionType        = boost::optional<typename Domain::ActionType>;
    using RewardType        = typename Domain::RewardType;
    using SimulatorType     = typename Domain::SimulatorType;

    template<typename A>
    using ActionSet         = typename Domain::template ActionSet<A>;

    static RewardType Vmin() { return Domain::Vmin(); }
    static RewardType Vmax() { return Domain::Vmax(); }

    PartialDomain( Domain const& domain )
        : domain_( domain )
    { }

    ParametersType const& parameters() const
    {
        return domain_.parameters();
    }

    std::unique_ptr<StateType const> initialState( SamplingContext& ctx ) const
    {
        return domain_.initialState( ctx );
    }

    ActionSet<ActionType> actions( StateType const& s ) const
    {
        ActionSet<ActionType> as = domain_.actions( s );
        as.insert( boost::none );
        return as;
    }

private:
    Domain domain_;
};

template<typename Domain>
class PartialPolicy : public Policy<PartialDomain<Domain>>
{
public:
    PartialPolicy( Policy<Domain> const& pi )
        : pi_( pi )
    { }

    using StateType         = typename PartialDomain<Domain>::StateType;
    using ActionType        = typename PartialDomain<Domain>::ActionType;
    using ActionGenerator   = std::function<ActionType()>;
    ActionGenerator operator()( StateType const& s ) const override
    {
        auto f = pi_( s );
        return [f]() { return f(); };
    }

private:
    Policy<Domain> const& pi_;
};

template<typename Domain>
PartialPolicy<Domain> make_partial( Policy<Domain> const& pi )
{
    return PartialPolicy<Domain>( pi );
}

    
}

#endif
