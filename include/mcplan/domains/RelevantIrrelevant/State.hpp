#ifndef MCPLAN_DOMAINS_RELEVANTIRRELEVANT_STATE_HPP__
#define MCPLAN_DOMAINS_RELEVANTIRRELEVANT_STATE_HPP__


#include <iosfwd>
#include <memory>

#include <iostream> // FIXME: Debugging

namespace mcplan { namespace domains { namespace RelevantIrrelevant {
    
    
class Parameters
{
public:
    /**
     * @param T Episode length
     * @param nr Cardinality of relevant random variable
     * @param ni Cardinality of irrelevant random variable
     */
    Parameters( int T, int nr, int ni )
        : T_( T ), nr_( nr ), ni_( ni )
    { }
    
    Parameters( Parameters const& that )
        : T_( that.T_ ), nr_( that.nr_ ), ni_( that.ni_ )
    { }
    
    int T()  const { return T_; }
    int nr() const { return nr_; }
    int ni() const { return ni_; }
    
private:
    int const T_;
    int const nr_;
    int const ni_;
};

// ---------------------------------------------------------------------------

/**
 * @brief The State class
 */
class State
{
public:
    // FIXME: Debugging
    ~State();

    Parameters const& params() const { return params_; }
    int relevant() const { return relevant_; }
    int irrelevant() const { return irrelevant_; }
    int t() const { return t_; }

    bool isTerminal() const
    { return t_ == params_.T(); }
    
private:
    friend class StateBuilder;
    State( Parameters const& p, int relevant, int irrelevant, int t )
        : params_( p ), relevant_( relevant ), irrelevant_( irrelevant ), t_( t )
    { }
    
private:
    Parameters const& params_;
    int relevant_;
    int irrelevant_;
    int t_;
};

std::ostream& operator <<( std::ostream& out, State const& s )
{
    out << "{t=" << s.t() << ", r=" << s.relevant() << ", i=" << s.irrelevant() << "}";
    return out;
}

bool operator ==( State const& a, State const& b )
{
    return a.t() == b.t()
           && a.relevant() == b.relevant()
           && a.irrelevant() == b.irrelevant();
}

bool operator !=( State const& a, State const& b )
{
    return !(a == b);
}

bool operator <( State const& a, State const& b )
{
    if( a.relevant() < b.relevant() ) {
        return true;
    }
    else if( a.relevant() == b.relevant() ) {
        if( a.irrelevant() < b.irrelevant() ) {
            return true;
        }
        else if( a.irrelevant() == b.irrelevant() ) {
            return a.t() < b.t();
        }
    }

    return false;
}

// FIXME: Debugging
State::~State()
{
    std::cout << "~State(): " << *this << std::endl;
}

// ---------------------------------------------------------------------------

class StateBuilder
{
public:
    explicit StateBuilder( Parameters const& params )
        : params_( params ), relevant_( 0 ), irrelevant_( 0 ), t_( 0 )
    { }
    
    StateBuilder& relevant( int r ) { relevant_ = r; return *this; }
    StateBuilder& irrelevant( int i ) { irrelevant_ = i; return *this; }
    StateBuilder& t( int t ) { t_ = t; return *this; }
    
    std::unique_ptr<State const> finish() const
    {
        return std::unique_ptr<State const>( new State( params_, relevant_, irrelevant_, t_ ) );
    }
    
private:
    Parameters const& params_;
    int relevant_;
    int irrelevant_;
    int t_;
};
    
    
}}}


#endif

