#ifndef MCPLAN_DOMAINS_RELEVANTIRRELEVANT_DOMAIN_HPP__
#define MCPLAN_DOMAINS_RELEVANTIRRELEVANT_DOMAIN_HPP__


#include "mcplan/domains/RelevantIrrelevant/Action.hpp"
#include "mcplan/domains/RelevantIrrelevant/State.hpp"

#include "boost/container/flat_set.hpp"

#include <algorithm>
#include <vector>

namespace mcplan { namespace domains { namespace RelevantIrrelevant {


class Domain
{
public:
    using StateType     = State;
    using GroundStateType = State;
    using ActionType    = Action;
    using RewardType    = double;
    using ActionSet     = boost::container::flat_set<Action>;

    static constexpr RewardType Vmin = 0;
    static constexpr RewardType Vmax = 9999;
    Domain(){}
    unsigned max_actions() const
    {
        return 2;
    }
    ActionSet actions( State const& s ) const
    {
        std::vector<Action> as;
        as.reserve( s.params().nr() );
        for( int i = 0; i < s.params().nr(); ++i ) {
            as.push_back( Action( i ) );
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }
};


}}}


#endif
