#ifndef MCPLAN_DOMAINS_RELEVANTIRRELEVANT_ACTION_HPP__
#define MCPLAN_DOMAINS_RELEVANTIRRELEVANT_ACTION_HPP__


#include <iosfwd>

// FIXME: Debugging
#include <iostream>

namespace mcplan { namespace domains { namespace RelevantIrrelevant {


class Action
{
public:
    // FIXME: Debugging
//    ~Action()
//    {
//        std::cout << "~Action()" << std::endl;
//    }
    Action()
    {

    }

    Action( int k )
        : k_( k )
    { }
    
    int k() const { return k_; }
    bool operator==(Action const& a)
    {
        if(a.k()==this->k())
            return true;
        else
            return false;
    }

    friend std::ostream& operator<<( std::ostream& out, Action const& a )
    {
        out << "Action[k=" << a.k() << "]";
        return out;
    }
private:
    int k_;
};

bool operator <( Action a, Action b )
{ return a.k() < b.k(); }
    
    
}}}


#endif
