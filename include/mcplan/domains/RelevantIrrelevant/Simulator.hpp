#ifndef MCPLAN_DOMAINS_RELEVANTIRRELEVANT_SIMULATOR_HPP__
#define MCPLAN_DOMAINS_RELEVANTIRRELEVANT_SIMULATOR_HPP__


#include "mcplan/domains/RelevantIrrelevant/Domain.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"

#include "mcplan/ActionSample.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/StateSample.hpp"

#include <random>
#include <utility>

namespace mcplan { namespace domains { namespace RelevantIrrelevant {

class Simulator : public mcplan::sim::TransitionSimulator<Domain>
{
public:

    TransitionSample sampleTransition( SamplingContext& ctx,
                                       State const& s, Action const& a ) const override
    {
        double const ar = 0;
        ActionSample<Domain> an( a, ar );
        
        std::uniform_int_distribution<> pr( 0, s.params().nr() - 1 );
        std::uniform_int_distribution<> pi( 0, s.params().ni() - 1 );
        
        int const next_r = pr( ctx.rng() );
        int const next_i = pi( ctx.rng() );

        std::uniform_real_distribution<> preward;
        
        double const sr = (s.relevant() == a.k() ? 1.0 : 0.0); // + (s.irrelevant() * preward( ctx.rng() ));
        StateSample<Domain> sn( StateBuilder( s.params() )
            .relevant( next_r ).irrelevant( next_i ).t( s.t() + 1 ).finish(), sr );
        return std::make_tuple( std::move(an), std::move(sn) );
    }
};

}}}


#endif

