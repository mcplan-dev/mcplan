#ifndef MCPLAN_DOMAINS_SPHERENAVIGATION_HPP__
#define MCPLAN_DOMAINS_SPHERENAVIGATION_HPP__



namespace mcplan { namespace domains {

class SphereNavigation
{
public:
    class Position;
    class Position_Dot;
    class Parameters;
    class StateBuilder;
    class State;
    class Action;
    class Domain;
    class Simulator;

};

}
}


#endif
