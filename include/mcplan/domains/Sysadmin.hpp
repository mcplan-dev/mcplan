#ifndef MCPLAN_DOMAINS_SYSADMIN_HPP__
#define MCPLAN_DOMAINS_SYSADMIN_HPP__
namespace mcplan { namespace domains {

class Sysadmin
{
public:
    class State;
    class Action;
    class Domain;
    class Simulator;
    class StateBuilder;
    class Parameters;
    class Topology;

};
}
}


#endif
