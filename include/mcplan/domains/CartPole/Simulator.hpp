#ifndef MCPLAN_DOMAINS_CARTPOLE_SIMULATOR_HPP__
#define MCPLAN_DOMAINS_CARTPOLE_SIMULATOR_HPP__

#include "mcplan/domains/CartPole.hpp"
#include "mcplan/domains/CartPole/Domain.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"

#include "mcplan/ActionSample.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/StateSample.hpp"

#include <random>
#include <utility>
#define GRAVITY 9.8

namespace mcplan { namespace domains {

class CartPole::Simulator : public mcplan::sim::TransitionSimulator<Domain>
{
    using DimType =float;
public:

    TransitionSample sampleTransition( SamplingContext& ctx,
                                       State const& s, Action const& a ) const override
    {

        double const ar = 0;
        ActionSample<Domain> an( a, ar );

        DimType xacc,thetaacc,force,costheta,sintheta,temp, tau;
        float nBuckets=s.params().nActionBuckets();
        force = (a.k()>=nBuckets)? s.params().fmax()*(a.k()+1-nBuckets)/nBuckets: -1*s.params().fmax()*(a.k()+1)/nBuckets;
        costheta = cos(s.theta());
        sintheta = sin(s.theta());
        float polemass_length = s.params().polemass()*s.params().length();
        float total_mass = s.params().polemass()+s.params().cartmass();
        temp = (force + polemass_length* s.theta_dot() * s.theta_dot() * sintheta)
                        / total_mass;

        thetaacc = (GRAVITY * sintheta - costheta* temp)
                  / (s.params().length() * (4.0/3.0 - s.params().polemass() * costheta * costheta
                                                     / total_mass));
        //std::cout<<"Theta Acc"<<thetaacc<<"\n";
        xacc  = temp - polemass_length * thetaacc* costheta / total_mass;
        tau = s.params().tau();
        DimType new_x=0,new_x_dot=0,new_theta=0,new_theta_dot=0;
        //std::cout<<"Theta Acc"<<thetaacc <<"tau"<<tau<<"\n";
        new_x  =s.x()+ tau * s.x_dot();
        new_x_dot =s.x_dot()+ tau * xacc;
        new_theta =s.theta()+ tau * s.theta_dot();
        new_theta_dot =s.theta_dot()+ tau * thetaacc;
       //std::cout<<"State1"<<s<<"\n";
       //std::cout<<"New_State"<<new_x<<" "<<new_x_dot<<" "<<new_theta<<" "<<new_theta_dot<<"\n";
        double sr=0;
        if (new_x < -2.4 ||
                new_x > 2.4  ||
                new_theta < -0.20 ||
                new_theta > 0.20)
            sr=-1.0;
        else
            sr=0.0;
            StateSample<Domain> sn( StateBuilder( s.params() )
            .x(new_x).x_dot(new_x_dot).theta(new_theta).theta_dot(new_theta_dot).t( s.t() + 1 ).finish(),sr  );


        return std::make_tuple( std::move(an), std::move(sn) );
    }
};

}}


#endif

