#ifndef MCPLAN_DOMAINS_CARTPOLE_ACTION_HPP__
#define MCPLAN_DOMAINS_CARTPOLE_ACTION_HPP__

#include <iosfwd>

// FIXME: Debugging
#include <iostream>
#include <mcplan/domains/CartPole.hpp>

namespace mcplan { namespace domains {


class CartPole::Action
{
public:
    // FIXME: Debugging
//    ~Action()
//    {
//        std::cout << "~Action()" << std::endl;
//    }
    Action()
    {

    }

    Action( float k )
        : k_( k )
    { }
    
    float k() const { return k_; }
    bool operator==(Action const& a)
    {
        if(a.k()==this->k())
            return true;
        else
            return false;
    }

    friend std::ostream& operator<<( std::ostream& out, Action const& a )
    {
        out << "Action[k=" << a.k() << "]";
        return out;
    }
private:
    float k_;
};

bool operator <( CartPole::Action a, CartPole::Action b )
{ return a.k() < b.k(); }
    
    
}}


#endif
