#ifndef MCPLAN_DOMAINS_CARTPOLE_DOMAIN_HPP__
#define MCPLAN_DOMAINS_CARTPOLE_DOMAIN_HPP__

#include "mcplan/domains/CartPole.hpp"
#include "mcplan/domains/CartPole/Action.hpp"
#include "mcplan/domains/CartPole/State.hpp"

#include "boost/container/flat_set.hpp"

#include <algorithm>
#include <vector>

namespace mcplan { namespace domains {



class CartPole::Domain
{
public:
    using StateType     = State;
    using GroundStateType = State;
    using ActionType    = Action;
    using RewardType    = double;
    using ActionSet     = boost::container::flat_set<Action>;

    static constexpr RewardType Vmin = 0;
    static constexpr RewardType Vmax = std::numeric_limits<RewardType>::max();
    Domain(){}
    unsigned max_actions() const
    {
        return 5;
    }
    ActionSet actions( State const& s ) const
    {
        std::vector<Action> as;
        as.reserve(2* s.params().nActionBuckets());
        for( int i = 0; i <2* s.params().nActionBuckets(); ++i ) {
            as.push_back( Action( i ) );
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }
};



}}


#endif
