#ifndef MCPLAN_DOMAINS_CARTPOLE_STATE_HPP__
#define MCPLAN_DOMAINS_CARTPOLE_STATE_HPP__

#include "mcplan/domains/CartPole.hpp"
#include <iosfwd>
#include <memory>

#include <iostream> // FIXME: Debugging

namespace mcplan { namespace domains {
    

class CartPole::Parameters
{
    using DimType = float;
public:
    /**
     * @param T Episode length
     * @param nc Number of Computers
     * @param TopologyType (0: Ring, 1: Bus, 2: Hub  ,>3: Custom
     */
    Parameters( int T, DimType polemass, DimType cartmass, DimType length, DimType tau, DimType fmax, int nActionBuckets)
        : T_( T ), polemass_( polemass ), cartmass_( cartmass ),length_(length), tau_(tau),fmax_(fmax),nActionBuckets_(nActionBuckets)
    { }

    Parameters( Parameters const& that )
        : T_( that.T_ ), polemass_( that.polemass_ ), cartmass_( that.cartmass_ ),length_(that.length_),tau_(that.tau_),fmax_(that.fmax_),nActionBuckets_(that.nActionBuckets_)
    { }
    
    int T()  const { return T_; }
    DimType polemass() const { return polemass_; }
    DimType cartmass() const { return cartmass_; }
    DimType length() const { return length_;}
    DimType tau() const { return tau_;}
    int nActionBuckets() const {return nActionBuckets_;}
    DimType fmax() const { return fmax_;}
private:
    int const T_;
    DimType const polemass_;
    DimType const cartmass_;
    DimType const length_;
    DimType const tau_;
    DimType const fmax_;
    int const nActionBuckets_;

};



/**
 * @brief The State class
 */
class CartPole::State
{
public:
    using DimType = float;

    // FIXME: Debugging
    ~State()
    {
        //std::cout << "~State(): " << *this << std::endl;
    }

    Parameters const& params() const { return params_; }
    DimType x() const{ return x_;}
    DimType x_dot() const{ return x_dot_;}
    DimType theta() const{ return theta_;}
    DimType theta_dot() const{ return theta_dot_;}
    int t() const { return t_; }

    bool isTerminal() const
    {
        if (x_ < -2.4 ||  x_ > 2.4  ||
             theta_ < -12 ||
             theta_ > 12)
            return true;
        else
            return false;
    }

    friend std::ostream& operator<<( std::ostream& out, State const& s )
    {
        out<<"X:"<<s.x_<<",X_DOT:"<<s.x_dot_<<"THETA:"<<s.theta_<<"THETA_DOT:"<<s.theta_dot_<<"\n";
        return out;
    }
    bool operator==(  State const& s )
    {
        if((t_==s.t_) && (x_==s.x_) &&(x_dot_==s.x_dot_)&&(theta_==s.theta_)&&(theta_dot_=s.theta_dot_))
            return true;
        else
            return false;

    }

    
private:
    friend class StateBuilder;
    friend class Topology;

    State( Parameters const& p, DimType x, DimType x_dot, DimType theta, DimType theta_dot, int t)
        : params_( p ), x_(x),x_dot_(x_dot),theta_(theta),theta_dot_(theta_dot),t_(t)
    { }
    
private:
    Parameters const& params_;
    DimType x_;
    DimType x_dot_;
    DimType theta_;
    DimType theta_dot_;
    int t_;
};

bool operator <( CartPole::State const& a, CartPole::State const& b )
{
  if((a.x()<b.x())||((a.x()==b.x())&&(a.x_dot()<b.x_dot()))||((a.x()==b.x())&&(a.x_dot()==b.x_dot())&&(a.theta()<b.theta()))
     ||(((a.x()==b.x())&&(a.x_dot()==b.x_dot())&&(a.theta()==b.theta()))&&(a.theta_dot()<b.theta_dot())))
     return true;
   else
    return false;
}



class CartPole::StateBuilder
{
    using DimType =float;
public:


    explicit StateBuilder( Parameters const& params )
        : params_( params ), x_(0),x_dot_(0),theta_(0),theta_dot_(0), t_( 0 )
    { }
    
    StateBuilder& x( DimType x)
    { x_=x; return *this; }
    StateBuilder& x_dot( DimType x_dot)
    { x_dot_=x_dot; return *this; }
    StateBuilder& theta( DimType theta)
    { theta_=theta; return *this; }
    StateBuilder& theta_dot( DimType theta_dot)
    { theta_dot_=theta_dot; return *this; }
    StateBuilder& t( int t ) { t_ = t; return *this; }
    
    std::unique_ptr<State const> finish() const
    {


        return std::unique_ptr<State const>( new State( params_,x_,x_dot_,theta_,theta_dot_, t_ ) );
    }
    
private:
    Parameters const& params_;
    DimType x_;
    DimType x_dot_;
    DimType theta_;
    DimType theta_dot_;
    int t_;
};

}}


#endif

