#ifndef MCPLAN_DOMAINS_RACETRACK_ACTION_HPP
#define MCPLAN_DOMAINS_RACETRACK_ACTION_HPP


#include <ostream>


namespace mcplan { namespace domains { namespace Racetrack {

// ---------------------------------------------------------------------------

class Action
{
public:
    Action()
        : Action( 0, 0 )
    { }

    explicit Action( int k)
    {
        ddx_=k/3-1;
        ddy_=k%3-1;
    }

    //Converted to a single answer

    Action( int ddx, int ddy )
        : ddx_( ddx ), ddy_( ddy )
    { }
    // Fix: AA: To be done... Constrains as Integer valued action
    int k() const{return (ddx_+1)*3+(ddy_+1);}
    int ddx() const { return ddx_; }
    int ddy() const { return ddy_; }

private:
    int ddx_;
    int ddy_;
};

std::ostream& operator <<( std::ostream& out, Action const& a )
{
    out << "A{ddx: " << a.ddx() << ", ddy: " << a.ddy() << "}";
    return out;
}

bool operator ==( Action const& a, Action const& b )
{
    return a.ddx() == b.ddx() && a.ddy() == b.ddy();
}

bool operator !=( Action const& a, Action const& b )
{ return !(a == b); }

bool operator <( Action const& a, Action const& b )
{
    if( a.ddx() < b.ddx() ) {
        return true;
    }
    else if( a.ddx() == b.ddx() ) {
        return a.ddy() < b.ddy();
    }

    return false;
}

// ---------------------------------------------------------------------------

}}}

#endif
