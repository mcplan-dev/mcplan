#ifndef MCPLAN_DOMAINS_RACETRACK_DOMAIN_HPP__
#define MCPLAN_DOMAINS_RACETRACK_DOMAIN_HPP__

#include "mcplan/domains/Racetrack/Action.hpp"
#include "mcplan/domains/Racetrack/Parameters.hpp"
#include "mcplan/domains/Racetrack/ShortestPathHeuristic.hpp"
#include "mcplan/domains/Racetrack/State.hpp"

#include "mcplan/SamplingContext.hpp"

#include <boost/container/flat_set.hpp>
#include <boost/optional.hpp>

#include <algorithm>
#include <memory>
#include <random>
#include <string>
#include <vector>


namespace mcplan { namespace domains { namespace Racetrack {

// ---------------------------------------------------------------------------

class Simulator;

class Domain
{
public:
    using ParametersType = Parameters;
    using StateType     = State;
    using GroundStateType = State;
    using ActionType    = Action;
    using RewardType    = double;
    using SimulatorType = Simulator;

    static boost::optional<ActionType> DefaultAction()
    {
        return Action( 0, 0 );
    }

    template<typename A>
    using ActionSet     = boost::container::flat_set<A>;

    explicit Domain( Parameters const& parameters )
        : parameters_( parameters ), heuristic_( parameters )
    { }

    Parameters const& parameters() const
    {
        return parameters_;
    }

    double discount() const
    { return 1.0; }

    RewardType crash_reward() const
    {
        return -parameters_.T();
    }

    // -----------------------------------------------------------------------
    // StateHeuristic concept

    // FIXME: We should differentiate admissible and non-admissible heuristics
    RewardType evaluateState( StateType const& s ) const
    {
        return heuristic_( s );
    }

    // -----------------------------------------------------------------------
    // BoundedValue concept

    RewardType Vmin() const { return -parameters_.T() + crash_reward(); }
    RewardType Vmax() const { return 0; }

    RewardType future_value_min( StateType const& s ) const
    {
        if( s.goal ) {
            return 0;
        }
        RewardType v = -(parameters_.T() - s.t); // -1 per step-to-go
        if( !s.crashed ) {
            v += crash_reward(); // Haven't crashed yet -> might crash in future
        }
        return v;
    }

    RewardType future_value_min( StateType const& s, ActionType const& /*a*/ ) const
    {
        return std::min( RewardType(0), future_value_min( s ) + 1 ); // Add back the per-step cost from 'a'
    }

    RewardType future_value_max( StateType const& s ) const
    {
        if( s.goal ) {
            return 0;
        }
        else {
            auto h = evaluateState( s );
            return std::max( h, RewardType(-1) ); // Ensure returned value is an upper bound
        }
    }

    RewardType future_value_max( StateType const& /*s*/, ActionType const& /*a*/ ) const
    {
        return 0;
    }

    // -----------------------------------------------------------------------
    // FactoredRepresentation concept

    static constexpr std::size_t kNfeatures = 4;

    using FeaturesType = std::array<double, kNfeatures>;
    FeaturesType features( StateType const& s ) const
    {
        return {static_cast<double>(s.x), static_cast<double>(s.y),
                static_cast<double>(s.dx), static_cast<double>(s.dy)};
    }

    std::size_t Nfeatures() const
    { return kNfeatures; }

    // -----------------------------------------------------------------------
    // Domain concept

    std::unique_ptr<StateType const> initialState( SamplingContext& ctx ) const
    {
        std::uniform_int_distribution<std::size_t> p( 0, parameters_.starts().size() - 1 );
        std::size_t const start_idx = p( ctx.rng() );
        auto start = *std::next( parameters_.starts().begin(), start_idx );
        auto s = std::make_unique<State>();
        s->x = static_cast<int>( std::get<0>(start) );
        s->y = static_cast<int>( std::get<1>(start) );
        return std::move(s);
    }
    unsigned max_actions() const
    {
        return 9;
    }
    ActionSet<ActionType> all_actions( ) const
    {
        std::vector<ActionType> as;
        as.reserve( 9 );
        for( int ddx = -1; ddx <= 1; ++ddx ) {
            for( int ddy = -1; ddy <= 1; ++ddy ) {
                as.push_back( Action( ddx, ddy ) );
            }
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

    ActionSet<ActionType> actions( State const& /*s*/ ) const
    {
        std::vector<ActionType> as;
        as.reserve( 9 );
        for( int ddx = -1; ddx <= 1; ++ddx ) {
            for( int ddy = -1; ddy <= 1; ++ddy ) {
                as.push_back( Action( ddx, ddy ) );
            }
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

private:
    Parameters parameters_;
    ShortestPathHeuristic heuristic_;
};


}}} // mcplan::domains::Racetrack


#endif
