#ifndef MCPLAN_DOMAINS_RACETRACK_STATE_HPP
#define MCPLAN_DOMAINS_RACETRACK_STATE_HPP

#include "mcplan/domains/Racetrack/Parameters.hpp"

#include <ostream>
#include <tuple>


namespace mcplan { namespace domains { namespace Racetrack {

// ---------------------------------------------------------------------------

class State
{
public:
    int x;
    int y;
    int dx;
    int dy;

    bool crashed;
    bool goal;

    int t;

    State()
        : x( 0 ), y( 0 ), dx( 0 ), dy( 0 ),
          crashed( false ), goal( false ), t( 0 )
    { }

    bool isTerminal( Parameters const& parameters ) const
    {
        return crashed || goal || t >= parameters.T();
    }
};

std::ostream& operator <<( std::ostream& out, State const& s )
{
    out << "["
        << "t: " << s.t
        << ", x: " << s.x
        << ", y: " << s.y
        << ", dx: " << s.dx
        << ", dy: " << s.dy
        << ", crashed: " << s.crashed
        << ", goal: " << s.goal
        << "]";
    return out;
}

bool operator ==( State const& a, State const& b )
{
    return std::tie(a.x, a.y, a.dx, a.dy, a.crashed, a.goal, a.t)
           == std::tie(b.x, b.y, b.dx, b.dy, b.crashed, b.goal, b.t);
}

bool operator !=( State const& a, State const& b )
{ return !(a == b); }

bool operator <( State const& a, State const& b )
{
    return std::tie(a.x, a.y, a.dx, a.dy, a.crashed, a.goal, a.t)
           < std::tie(b.x, b.y, b.dx, b.dy, b.crashed, b.goal, b.t);
}

// ---------------------------------------------------------------------------

}}}

#endif
