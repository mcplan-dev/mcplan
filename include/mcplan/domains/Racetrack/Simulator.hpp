#ifndef MCPLAN_DOMAINS_RACETRACK_SIMULATOR_HPP__
#define MCPLAN_DOMAINS_RACETRACK_SIMULATOR_HPP__


#include "mcplan/algorithm/BresenhamRasterLine.hpp"
#include "mcplan/domains/Racetrack/Domain.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"


// Forward declaration of Simulator class
namespace mcplan { namespace domains { namespace Racetrack {
    class Simulator;
}}}

// Traits specializations in namespace mcplan::sim
namespace mcplan { namespace sim {
    template<>
    struct is_transition_simulator<mcplan::domains::Racetrack::Simulator>
    {
        static bool constexpr value = true;
    };
}}


namespace mcplan { namespace domains { namespace Racetrack {


class Simulator : public mcplan::sim::TransitionSimulator<Simulator, Domain>
{
public:
    TransitionSample sampleTransition(
        SamplingContext& ctx, Domain const& domain, State const& s, Action const& a ) const
    {
        auto sprime = std::make_unique<State>( s );
        if( domain.parameters().slip() > 0 ) {
            applyNoisyAction( ctx, domain.parameters(), *sprime, a );
        }
        else {
            sprime->dx += a.ddx();
            sprime->dy += a.ddy();
        }

        applyDynamics( domain.parameters(), *sprime );

        ActionSample<Domain> asample( a, -1 );
        Domain::RewardType r = (sprime->crashed ? domain.crash_reward() : 0);
        StateSample<Domain> ssample( std::move(sprime), r );
        return std::make_tuple( std::move(asample), std::move(ssample) );
    }

private:
    /**
     * Applies noise independently to both directions. Noise is of the
     * "either desired action or nothing" variety.
     * @param rng
     * @param s
     * @param slip
     */
    void applyNoisyAction( SamplingContext& ctx, Parameters const& parameters, State& s, Action const& a ) const
    {
        std::uniform_real_distribution<double> p( 0, 1 );

        if( p( ctx.rng() ) < parameters.slip() ) {
            // Don't update
        }
        else {
            s.dx += a.ddx();
        }

        if( p( ctx.rng() ) < parameters.slip() ) {
            // Don't update
        }
        else {
            s.dy += a.ddy();
        }
    }

    /**
     * Note: This function advances 's.t'.
     * @param rng
     * @param s
     * @param slip
     */

      void applyDynamics( Parameters const& parameters, State& s ) const
      {
          int const proj_x = s.x + s.dx;
          int const proj_y = s.y + s.dy;

          auto line = algorithm::bresenham_raster_line( s.x, s.y, proj_x, proj_y );

          decltype(line)::value_type const* prev = nullptr;
          for( auto const& p : line ) {
  //				System.out.println( "(" + p.x + ", " + p.y + ")" );
              auto const px = std::get<0>(p);
              auto const py = std::get<1>(p);
              if( px < 0 || px >= parameters.width()
                      || py < 0 || py >= parameters.height() ) {
                  s.crashed = true;
                  s.dx = 0;
                  s.dy = 0;
                  if( prev ) {
                      s.x = std::get<0>(*prev);
                      s.y = std::get<1>(*prev);
                  }
                  break;
              }
              TerrainType const terrain = parameters.terrain()[py][px];
  //				System.out.println( terrain );
              if( terrain == TerrainType::Wall ) {
                  s.crashed = true;
                  s.dx = 0;
                  s.dy = 0;
                  if( prev ) {
                      s.x = std::get<0>(*prev);
                      s.y = std::get<1>(*prev);
                  }
                  break;
              }
              else if( terrain == TerrainType::Goal ) {
                  s.goal = true;
                  s.x = px;
                  s.y = py;
                  break;
              }

              prev = &p;
          }

          if( !s.goal && !s.crashed ) {
              s.x = proj_x;
              s.y = proj_y;
          }

          s.t += 1;
      }
};


}}} // namespace


#endif
