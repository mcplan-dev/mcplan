#ifndef MCPLAN_DOMAINS_RACETRACK_PARAMETERS_HPP
#define MCPLAN_DOMAINS_RACETRACK_PARAMETERS_HPP

#include <boost/container/flat_set.hpp>

#include <algorithm>
#include <stdexcept>
#include <ostream>
#include <string>
#include <tuple>
#include <vector>


namespace mcplan { namespace domains { namespace Racetrack {

// ---------------------------------------------------------------------------

enum class TerrainType
{
    Track, Start, Goal, Wall
};

using TerrainGrid = std::vector<std::vector<TerrainType>>;

// ---------------------------------------------------------------------------

namespace Tracks {

    namespace detail {
        TerrainGrid parse( std::vector<std::string> const& spec, unsigned scale = 1 )
        {
            assert( scale > 0 );
            std::size_t const height = spec.size();
            std::size_t const width = spec[0].size();
            TerrainGrid terrain( height * scale );
            for( std::size_t i = 0; i < height; ++i ) {
                assert( spec[i].size() == width );
                for( std::size_t ii = 0; ii < scale; ++ii ) {
                    // We use 'height - i - 1' to invert the y-axis, so that y
                    // increases up and the track looks the same as the string array.
                    terrain[i*scale + ii] = TerrainGrid::value_type(
                        spec[height - i - 1].size() * scale );
                    auto& row = terrain[i*scale + ii];
                    for( std::size_t j = 0; j < spec[height - i - 1].size(); ++j ) {
                        char const ij = spec[height - i - 1][j];
                        for( std::size_t jj = 0; jj < scale; ++jj ) {
                            std::size_t const column = j*scale + jj;
                            if( 't' ==  ij ) {
                                row[column] = TerrainType::Track;
                            }
                            else if( 's' == ij ) {
                                row[column] = TerrainType::Start;
                            }
                            else if( 'g' == ij ) {
                                row[column] = TerrainType::Goal;
                            }
                            else if( 'w' == ij ) {
                                row[column] = TerrainType::Wall;
                            }
                            else {
                                std::ostringstream oss;
                                oss << "spec[" << i << "][" << j << "] = " << spec[i][j];
                                throw std::invalid_argument( oss.str().c_str() );
                            }
                        }
                    }
                }
            }

            return terrain;
        }
    } // detail

    TerrainGrid BartoBradtkeSingh_Small( unsigned const scale = 1 )
    {
        std::vector<std::string> spec = {
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwgggw",
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwtttw",
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwtttw",
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwtttw",
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwtttw",
            "sttttttttttttttttttttttttttttttttttttttw",
            "sttttttttttttttttttttttttttttttttttttttw",
            "sttttttttttttttttttttttttttttttttttttttw",
            "sttttttttttttttttttttttttttttttttttttttw",
            "wwwwtttttttttttttttttttttttttttttttttttw",
            "wwwwwwwwtttttttttttttttttttttttttttttttw",
            "wwwwwwwwwwwwtttttttttttttttttttttttttttw",
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"
        };
        return detail::parse( spec, scale );
    }

    TerrainGrid BartoBradtkeSingh_Large( unsigned const scale = 1 )
    {
        std::vector<std::string> spec = {
            "wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww",
            "wwwwwwwwwwwtttttttwwwwwwwwwwwwww",
            "wwwwwwwttttttttttttttwwwwwwwwwww",
            "wwwtttttttttttttttttttttwwwwwwww",
            "wwwttttttttttttttttttttttttwwwww",
            "wwwttttttttttttttttttttttttwwwww",
            "wwwttttttttttttttttttttttttwwwww",
            "wwwttttttttttwwttttttttttttwwwww",
            "wwwttttttttwwwwwtttttttttttwwwww",
            "wwwtttttttwwwwwwwttttttttttwwwww",
            "wwwttttttwwwwwttttttttttttwwwwww",
            "wwwtttttwwwwttttttttttttwwwwwwww",
            "wwwtttttwwwwttttttttttwwwwwwwwww",
            "wwttttttwwwwttttttttwwwwwwwwwwww",
            "wwttttttwwwwttttttwwwwwwwwwwwwww",
            "wwttttttwwwwtttttttttttttwwwwwww",
            "wwttttttwwwtttttttttttttttwwwwww",
            "wwtttttwwwwttttttttttttttttwwwww",
            "wwtttttwwwwtttttttttttttttttwwww",
            "wwtttttwwwwttttttttttttttttttwww",
            "wwtttttwwwwwttttttttttttttttttww",
            "wwtttttwwwwwwttttttttttttttttttw",
            "wwtttttwwwwwwwwwwwwwwwwttttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wttttttwwwwwwwwwwwwwwwwwtttttttw",
            "wsssssswwwwwwwwwwwwwwwwwgggggggw"
        };
        return detail::parse( spec, scale );
    }

}

// ---------------------------------------------------------------------------

class Parameters
{
    using Cell = std::tuple<std::size_t, std::size_t>;
    using CellSet = boost::container::flat_set<Cell>;

public:
    static Parameters BartoBradtkeSingh_Small( double const slip, unsigned int const scale = 1 )
    {
        return Parameters( Tracks::BartoBradtkeSingh_Small( scale ), 30, slip );
    }

    static Parameters BartoBradtkeSingh_Large( double const slip, unsigned int const scale = 1 )
    {
        return Parameters( Tracks::BartoBradtkeSingh_Large( scale ), 50, slip );
    }

    Parameters( TerrainGrid const& terrain, int const T, double const slip )
        : terrain_( terrain ), height_( static_cast<int>(terrain_.size()) ),
          width_( static_cast<int>(terrain_[0].size()) ),
          T_( T ), slip_( slip )
    {
        for( std::size_t i = 0; i < terrain_.size(); ++i ) {
            for( std::size_t j = 0; j < terrain_[i].size(); ++j ) {
                if( terrain_[i][j] == TerrainType::Start ) {
                    starts_.insert( Cell( j, i ) );
                }
                else if( terrain_[i][j] == TerrainType::Goal ) {
                    goals_.insert( Cell( j, i ) );
                }
            }
        }
    }

    TerrainGrid const& terrain() const
    { return terrain_; }

    int height() const
    { return height_; }

    int width() const
    { return width_; }

    int T() const
    { return T_; }

    CellSet const& starts() const
    { return starts_; }

    CellSet const& goals() const
    { return goals_; }

    double slip() const
    { return slip_; }

private:
    TerrainGrid terrain_;
    int height_;
    int width_;
    int T_;
    CellSet starts_;
    CellSet goals_;
    double slip_;
};

std::ostream& operator <<( std::ostream& out, Parameters const& p )
{
    out << "{terrain:" << std::endl;
    for( auto const& row : p.terrain() ) {
        for( auto const& t : row ) {
            switch( t ) {
            case TerrainType::Goal:
                out << "g"; break;
            case TerrainType::Start:
                out << "s"; break;
            case TerrainType::Track:
                out << "t"; break;
            case TerrainType::Wall:
                out << "W"; break;
            }
        }
        out << std::endl;
    }
    out << ", T: " << p.T() << ", slip: " << p.slip() << "}";
    return out;
}

// ---------------------------------------------------------------------------

}}} // mcplan::domains::Racetrack

#endif
