#ifndef MCPLAN_DOMAINS_RACETRACK_SHORTESTPATHHEURISTIC_HPP
#define MCPLAN_DOMAINS_RACETRACK_SHORTESTPATHHEURISTIC_HPP


#include "mcplan/domains/Racetrack/Parameters.hpp"
#include "mcplan/domains/Racetrack/State.hpp"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/johnson_all_pairs_shortest.hpp>

#include <algorithm>
#include <vector>


namespace mcplan { namespace domains { namespace Racetrack {


class ShortestPathHeuristic
{
private:
    using DistanceType = double;
    static bool constexpr allow_diagonals = true;

public:
    ShortestPathHeuristic( Parameters const& parameters )
        : width_( parameters.width() ), d_( parameters.width() * parameters.height() )
    {
        using Graph = boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS>;
        using Edge = std::pair<int, int>;

        // Build edge list
        std::vector<Edge> edges;
        std::vector<int> goals;
        for( int y = 0; y < parameters.height(); ++y ) {
            for( int x = 0; x < parameters.width(); ++x ) {
                auto const& terrain = parameters.terrain();
                TerrainType const t = terrain[y][x];
                if( t != TerrainType::Wall ) {
                    int const cur = index( x, y );
                    if( x > 0 && terrain[y][x-1] != TerrainType::Wall ) {
                        edges.push_back( Edge(cur, index( x - 1, y )) );
                    }
                    if( y > 0 && terrain[y-1][x] != TerrainType::Wall ) {
                        edges.push_back( Edge(cur, index( x, y - 1 )) );
                    }
                    if( allow_diagonals ) {
                        if( x > 0 && y > 0 && terrain[y-1][x-1] != TerrainType::Wall ) {
                            // Note that diagonals still have weight = 1 (not sqrt(2))
                            edges.push_back( Edge(cur, index( x - 1, y - 1 )) );
                        }
                    }

                    if( t == TerrainType::Goal ) {
                        goals.push_back( cur );
                    }
                }
            }
        }

        // Construct graph
        std::size_t const n = parameters.width() * parameters.height();
        using WeightType = int;
        auto const w = boost::make_static_property_map<typename Graph::edge_descriptor>( 1 );
        Graph g( edges.begin(), edges.end(), n );

        // Find shortest paths
        std::vector<std::vector<WeightType>> dm( n, std::vector<WeightType>( n, WeightType(0) ) );
        bool const success = boost::johnson_all_pairs_shortest_paths( g, dm, boost::weight_map(w) );
        assert( success );

        // Populate the flat distance vector with distance to the nearest goal
        for( int x = 0; x < parameters.width(); ++x ) {
            for( int y = 0; y < parameters.height(); ++y ) {
                int const cur = index( x, y );
                if( parameters.terrain()[y][x] == TerrainType::Wall ) {
                    // Set these to 0 so that they don't contribute to the
                    // maximum distance (the agent never ends up in these
                    // states anyway)
                    d_[cur] = 0;
                }
                else {
                    DistanceType d = std::numeric_limits<DistanceType>::max();
                    for( int goal : goals ) {
                        DistanceType const dg = dm[cur][goal];
                        d = std::min( d, dg );
                    }
                    d_[cur] = d;
                }
            }
        }

        // Normalize heuristic values in [0,1]
        DistanceType const max_d = *std::max_element( d_.begin(), d_.end() );
        std::transform( d_.begin(), d_.end(), d_.begin(),
                        [max_d]( DistanceType const& x ) { return x / max_d; } );
    }

    DistanceType operator ()( State const& s ) const
    {
        int const i = index( s.x, s.y );
        return -d_[i];
    }

private:
    int index( int const x, int const y ) const
    {
        return y*width_ + x;
    }

private:
    int width_;
    std::vector<DistanceType> d_;
};


}}}


#endif
