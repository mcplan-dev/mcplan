#ifndef MCPLAN_DOMAINS_SPHERENAVIGATION_SIMULATOR_HPP__
#define MCPLAN_DOMAINS_SPHERENAVIGATION_SIMULATOR_HPP__

#include "mcplan/domains/SphereNavigation.hpp"
#include "mcplan/domains/SphereNavigation/Domain.hpp"
#include "mcplan/sim/SimulatorTraits.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"

#include "mcplan/ActionSample.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/StateSample.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/math/constants/constants.hpp>

#include <random>
#include <utility>
#include <cassert>
#include <string>

// FIXME: debugging
#include <iostream>

// Forward declaration of Simulator class
//namespace mcplan { namespace domains {
//    class SphereNavigation::Simulator;
//}}

// Traits specializations in namespace mcplan::sim
namespace mcplan { namespace sim {
    template<>
    struct is_transition_simulator<mcplan::domains::SphereNavigation::Simulator>
    {
        static bool constexpr value = true;
    };
}}

// Definition of Simulator class
namespace mcplan { namespace domains {

class SphereNavigation::Simulator
    : public mcplan::sim::TransitionSimulator<SphereNavigation::Simulator, SphereNavigation::Domain>
{

    unsigned get_action(int a, int b) const {
        if(a==1 && b==0)
            return 0;
        else if(a==1 && b==1 )
            return 1;
        else if(a==0 && b==1 )
            return 2;
        else if(a==-1 && b==1 )
            return 3;
        else if(a==-1 && b==0 )
            return 4;
        else if(a==-1 && b==-1 )
            return 5;
        else if(a==0 && b==-1 )
            return 6;
        else
            return 7;
    }

    std::pair<int, int> direction(unsigned  a) const {

           switch( a ) {
               case 0: return std::make_pair(1, 0);
               case 1: return std::make_pair(1, 1);
               case 2: return std::make_pair(0, 1);
               case 3: return std::make_pair(-1, 1);
               case 4: return std::make_pair(-1, 0);
               case 5: return std::make_pair(-1, -1);
               case 6: return std::make_pair(0, -1);
               case 7: return std::make_pair(1, -1);
           default:
               throw std::invalid_argument( boost::lexical_cast<std::string>( a ) );
                     }
         }

public:
    TransitionSample sampleTransition( SamplingContext& ctx, Domain const& domain,
                                       State const& s, Action const& a ) const
    {
        //std::cout << "SphereNavigation::Simulator: sampleTransition( " << s << ", " << a << ")" << std::endl;

        float next_theta;
        float next_phi;


        double const action_cost=-1;
        double const ar = 0;
        unsigned curr_action= 0;//get_action(int((s.heading()).theta_dot()),int((s.heading()).phi_dot()));

        std::uniform_real_distribution<> p;
        auto r= p(ctx.rng());

        if(r<0.8)
            curr_action = 0;
        else if(r<0.9)
            curr_action = 1;
        else
            curr_action = -1;

        std::pair<int, int> dir = direction((a.k()+curr_action+8)%8); // +8 to calculate modulus of a positive quantity

        Position_Dot next_heading(0,static_cast<float>(dir.first),static_cast<float>(dir.second));

        next_theta = s.pos().theta()+dir.first/100.0f; // radius of sphere is 1m and speed is 1cm/sec
        next_phi = s.pos().phi()+dir.second/100.0f;

        float const pi = boost::math::constants::pi<float>();
        if(next_theta > pi)
            next_theta = -1* (2*pi - next_theta);
        if(next_theta  < -1 * pi)
            next_theta = 2*(pi- fabs(next_theta));
        if(next_phi > pi)
            next_phi = -1* (2*pi - next_phi);
        if(next_phi < -1 * pi)
            next_phi = 2*(pi- fabs(next_phi));
        Position next_pos(1,next_theta,next_phi);

        //std::cout << "SphereNavigation::Simulator: ActionSample" << std::endl;
        ActionSample<Domain> an( a, ar );

        double sr=0.0;
        sr+=action_cost;

        //std::cout << "SphereNavigation::Simulator: StateSample" << std::endl;
        StateSample<Domain> sn( StateBuilder( domain.parameters() )
            .pos(next_pos).heading(next_heading).t( s.t() + 1 ).finish(),sr  );


        return std::make_tuple( std::move(an), std::move(sn) );
    }
};


}}


#endif

