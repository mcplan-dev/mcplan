#ifndef MCPLAN_DOMAINS_SPHERENAVIGATION_ACTION_HPP__
#define MCPLAN_DOMAINS_SPHERENAVIGATION_ACTION_HPP__


#include <iosfwd>

// FIXME: Debugging
#include <iostream>
#include "mcplan/domains/SphereNavigation.hpp"

namespace mcplan { namespace domains {


class SphereNavigation::Action
{
public:
    Action()
    {  }

    Action( int k )
        : k_( k )
    { }
    
    int k() const { return k_; }

    friend std::ostream& operator<<( std::ostream& out, Action const& a )
    {
        out << "Action[k=" << a.k() << "]";
        return out;
    }
private:
    int k_;
};

bool operator==( SphereNavigation::Action const& a, SphereNavigation::Action const& b )
{
    return a.k() == b.k();
}

bool operator !=( SphereNavigation::Action const& a, SphereNavigation::Action const& b )
{
    return !(a.k() == b.k());
}

bool operator <( SphereNavigation::Action const& a, SphereNavigation::Action const& b )
{ return a.k() < b.k(); }
    
}}


#endif
