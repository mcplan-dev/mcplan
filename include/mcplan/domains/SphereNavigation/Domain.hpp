#ifndef MCPLAN_DOMAINS_SPHERENAVIGATION_DOMAIN_HPP__
#define MCPLAN_DOMAINS_SPHERENAVIGATION_DOMAIN_HPP__

#include "mcplan/domains/SphereNavigation.hpp"
#include "mcplan/domains/SphereNavigation/Action.hpp"
#include "mcplan/domains/SphereNavigation/State.hpp"

#include "boost/container/flat_set.hpp"

#include <algorithm>
#include <vector>

namespace mcplan { namespace domains {


class SphereNavigation::Domain
{
public:
    using ParametersType = Parameters;
    using SimulatorType = Simulator;
    using StateType     = State;
    using GroundStateType = State;
    using ActionType    = Action;
    using RewardType    = double;

    static boost::optional<ActionType> DefaultAction()
    {
        return Action( 0 );
    }

    template<typename A>
    using ActionSet     = boost::container::flat_set<A>;

    // -----------------------------------------------------------------------
    // StateHeuristic concept

    // FIXME: We should differentiate admissible and non-admissible heuristics
    RewardType evaluateState( StateType const& s ) const
    {
        SphereNavigation::Position const goal_position = parameters().goal();
        return -domains::distance_euclidean( s.pos(), goal_position );
    }

    // -----------------------------------------------------------------------
    // BoundedValue concept

    RewardType Rmin() const { return -1; }
    RewardType Rmax() const { return 0; }

    RewardType Vmin() const { return parameters_.T() * Rmin(); }
    RewardType Vmax() const { return parameters_.T() * Rmax(); }

    RewardType future_value_min( StateType const& s ) const
    {
        return Rmin() * (parameters_.T() - s.t());
    }

    RewardType future_value_min( StateType const& s, ActionType const& /*a*/ ) const
    {
        return Rmin() * (parameters_.T() - s.t() + 1);
    }

    RewardType future_value_max( StateType const& s ) const
    {
        return Rmax() * (parameters_.T() - s.t());
    }

    RewardType future_value_max( StateType const& s, ActionType const& /*a*/ ) const
    {
        return Rmax() * (parameters_.T() - s.t() + 1);
    }

    explicit Domain( Parameters const& parameters )
        : parameters_( parameters )
    { }

    Parameters const& parameters() const
    {
        return parameters_;
    }

    static std::size_t constexpr kNfeatures = 6;
    using FeaturesType = std::array<float, kNfeatures>;

    std::size_t Nfeatures() const
    { return kNfeatures; }

    FeaturesType features( StateType const& s ) const
    {
        return {s.pos().r(), s.pos().theta(), s.pos().phi(),
                s.heading().r_dot(), s.heading().theta_dot(), s.heading().phi_dot()};
    }

    std::unique_ptr<StateType const> initialState( SamplingContext& /*ctx*/ ) const
    {
        return SphereNavigation::StateBuilder( parameters_ ).finish();
    }

    unsigned max_actions() const
    {
        return 8;
    }

    ActionSet<Action> all_actions(  ) const
    {
        std::vector<Action> as;
        as.reserve( 8 );
        for( int i = 0; i < 8; ++i ) {
            as.push_back( Action( i ) );
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }
    ActionSet<Action> actions( State const& ) const
    {
        std::vector<Action> as;
        as.reserve( 8 );
        for( int i = 0; i < 8; ++i ) {
            as.push_back( Action( i ) );
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

private:
    Parameters parameters_;
};



}}


#endif
