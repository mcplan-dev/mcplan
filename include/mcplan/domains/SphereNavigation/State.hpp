#ifndef MCPLAN_DOMAINS_SPHERENAVIGATION_STATE_HPP__
#define MCPLAN_DOMAINS_SPHERENAVIGATION_STATE_HPP__

#define EPSILON 0.00001

#include <iosfwd>
#include <memory>
#include "mcplan/domains/SphereNavigation.hpp"
#include <iostream> // FIXME: Debugging

namespace mcplan { namespace domains {

class SphereNavigation::Position {
public:
    Position(float a=0, float b=0, float c=0)
    {
        r_ = a;
        theta_ = b;
        phi_ = c;
    }
    float r() const { return r_; }
    float theta() const { return theta_; }
    float phi() const { return phi_; }

    friend bool operator==( Position const& pos1, Position const& pos2 )
    {
        return ((fabs(pos1.r()-pos2.r())<EPSILON) && (fabs(pos1.theta()-pos2.theta())<EPSILON)
                   && (fabs(pos1.phi()-pos2.phi())<EPSILON));
    }
    void operator=( Position pos )
    {
       r_ = pos.r();
       theta_ = pos.theta();
       phi_ = pos.phi();
    }

    friend std::ostream& operator<<( std::ostream& out, Position const& pos )
    {
        out << "{r: " << pos.r() << ", theta: " << pos.theta() << ", phi: " << pos.phi() << "}";
        return out;
    }

private:
    float r_;
    float theta_;
    float phi_;

};

bool operator < ( SphereNavigation::Position const& pos1, SphereNavigation::Position const & pos2)
{
   return ((pos1.r()< pos2.r())||((fabs(pos1.r()- pos2.r())<EPSILON) &&(pos1.theta()<pos2.theta()))
           ||((fabs(pos1.r()-pos2.r())<EPSILON) &&(fabs(pos1.theta()-pos2.theta())<EPSILON) &&(pos1.phi()<pos2.phi())));
}

float distance_euclidean(SphereNavigation::Position const& pos1, SphereNavigation::Position const& pos2)
{

    if(pos1==pos2)
    {

        return 0;
    }
        else
     {

        return (sqrt(pos1.r()*pos1.r()+pos2.r()*pos2.r()-2*pos1.r()*pos2.r()
                 *(sin(pos1.theta())*sin(pos2.theta())*cos(pos1.phi()-pos2.phi())+cos(pos1.theta())*cos(pos2.theta()))));
    }
}

// ---------------------------------------------------------------------------

class SphereNavigation::Position_Dot
{
public:
    Position_Dot(float a=0, float b=0, float c=0)
    {
        r_dot_ = a;
        theta_dot_ = b;
        phi_dot_ = c;
    }
    float r_dot() const { return r_dot_; }
    float theta_dot() const { return theta_dot_; }
    float phi_dot() const { return phi_dot_; }


    friend std::ostream& operator<<( std::ostream& out, Position_Dot const& pos_dot )
    {
        out << "r_dot:"<<pos_dot.r_dot()<<", Theta_dot:"<<pos_dot.theta_dot()<<", Phi_dot"<<pos_dot.phi_dot()<<"\n";
        return out;
    }
    friend bool operator==( Position_Dot const& p1, Position_Dot const& p2 )
    {
        return ((fabs(p1.r_dot()-p2.r_dot())<EPSILON) && (fabs(p1.theta_dot()-p2.theta_dot())<EPSILON)
                   && (fabs(p1.phi_dot()-p2.phi_dot())<EPSILON));
    }
    void operator=(Position_Dot  p )
    {
       r_dot_ = p.r_dot();
       theta_dot_ = p.theta_dot();
       phi_dot_ = p.phi_dot();
    }

private:
    float r_dot_;
    float theta_dot_;
    float phi_dot_;
};

bool operator < ( SphereNavigation::Position_Dot const& p1, SphereNavigation::Position_Dot const & p2)
{
   return ((p1.r_dot()< p2.r_dot())||((fabs(p1.r_dot()-p2.r_dot())<EPSILON) &&(p1.theta_dot()<p2.theta_dot()))
           ||((fabs(p1.r_dot()-p2.r_dot())<EPSILON) &&(fabs(p1.theta_dot()-p2.theta_dot())<EPSILON) &&(p1.phi_dot()<p2.phi_dot())));
}

// ---------------------------------------------------------------------------

class SphereNavigation::Parameters
{
public:
    /**
     * @param T Episode length
     * @param init_pos: Initinal Position
     * @param goal: Goal Position
     */
    Parameters( int T,  Position init_pos, Position goal  )
        : T_( T ), init_pos_( init_pos ), goal_(goal )
    { }
    
    Parameters( Parameters const& that )
        : T_( that.T_ ), init_pos_( that.init_pos_ ), goal_( that.goal_ )
    { }
    
    int T()  const { return T_; }
    Position init_pos() const { return init_pos_; }
    Position goal() const { return goal_; }
    
private:
    int T_;
    Position const init_pos_;
    Position const goal_;

};

std::ostream& operator<<( std::ostream& out, SphereNavigation::Parameters const& p )
{
    out << "{T: " << p.T() << ", init_pos: " << p.init_pos() << ", goal: " << p.goal() << "}";
    return out;
}

// ---------------------------------------------------------------------------

/**
 * @brief The State class
 */
class SphereNavigation::State
{
public:

    Position pos() const{ return pos_; }
    Position_Dot heading() const { return heading_; }
    int t() const { return t_; }
    bool isTerminal( Parameters const& params ) const
    {
//        std::cout<< "SphereNavigation::State.isTerminal()" << std::endl;
//        std::cout << *this << std::endl;
//        std::cout << distance_euclidean(pos_, params.goal()) << std::endl;
        return (t_ == params.T())||(distance_euclidean(pos_, params.goal())< EPSILON);
    }

private:
    friend class StateBuilder;

    State( Parameters const& p, Position pos,  Position_Dot heading, int t )
        : params_( p ), pos_( pos), heading_(heading),t_( t )
    { }
    
private:
    Parameters const& params_;
    Position pos_;
    Position_Dot heading_;
    int t_;

};

std::ostream& operator <<( std::ostream& out, SphereNavigation::State const& s )
{
    out << "{pos: " << s.pos() << ", head: " << s.heading() << ", t: " << s.t() << "}";
    return out;
}

bool operator ==( SphereNavigation::State const& a, SphereNavigation::State const& b )
{
    return a.t() == b.t() && a.pos() == b.pos() && a.heading() == b.heading();
}

bool operator !=( SphereNavigation::State const& a, SphereNavigation::State const& b )
{
    return !(a == b);
}

bool operator <( SphereNavigation::State const& a, SphereNavigation::State const& b )
{
    if( a.pos() < b.pos() ) {
        return true;
    }
    else if( a.pos() == b.pos() ) {
        return a.heading() < b.heading();
    }

    return false;
}



class SphereNavigation::StateBuilder
{
public:

    explicit StateBuilder( Parameters const& params )
     : params_(params ),t_( 0 )
    {    }
    
    StateBuilder& pos( Position pos)
    { pos_ = pos; return *this; }

    StateBuilder& heading( Position_Dot heading)
    { heading_ = heading; return *this; }

    StateBuilder& t( int t ) { t_ = t; return *this; }
    
    std::unique_ptr<State const> finish() const
    {


        return std::unique_ptr<State const>( new State( params_,pos_,heading_, t_ ) );
    }
    
private:
    Parameters const& params_;
    Position  pos_;
    Position_Dot heading_;
    int t_;
};

}}


#endif

