#ifndef MCPLAN_DOMAINS_INVESTMENT_DOMAIN_HPP
#define MCPLAN_DOMAINS_INVESTMENT_DOMAIN_HPP


#include "mcplan/ostream.hpp"
#include "mcplan/SamplingContext.hpp"

#include <boost/container/flat_set.hpp>
#include <boost/optional.hpp>

#include <algorithm>
#include <memory>
#include <vector>


namespace mcplan { namespace domains { namespace Investment {

class Simulator;

// ---------------------------------------------------------------------------

class Parameters
{
public:
    static Parameters Formula_A()
    {
        int const T = 120;
        int const loan_amount = 1000;
        int const loan_duration = 24;
        double const loan_interest = 0.2;
        double const ar = 0.95;
        double const transaction_cost = -10;
        return Parameters( T, loan_amount, loan_duration, loan_interest, ar, transaction_cost,
                           { std::tuple<double,double>{5, 1},  std::tuple<double,double>{-5, 80} } );
    }

    static Parameters Formula_A_Long()
    {
        // These parameters are chosen so that 'safe' is better when
        // deltaT = 32 or 16, but 'risky' is better when deltaT = 8 or smaller
        int const T = 128;
        int const loan_amount = 1000;
        int const loan_duration = 32;
        double const loan_interest = 0.2;
        double const ar = 0.95;
        double const transaction_cost = -10;
        return Parameters( T, loan_amount, loan_duration, loan_interest, ar, transaction_cost,
                           { std::tuple<double,double>{5, 1},  std::tuple<double,double>{-5, 80} } );
    }

    Parameters( int const T, double const loan_amount, int const loan_duration,
                double const loan_interest, double const ar, double const transaction_cost,
                std::vector<std::tuple<double, double>> const& investment_parameters )
        : T_( T ), loan_amount_( loan_amount ), loan_duration_( loan_duration ),
          loan_interest_( loan_interest ), ar_( ar ),
          transaction_cost_( transaction_cost ),
          investment_parameters_( investment_parameters ),
          max_sale_price_magnitude_( 0 )
    {
        BOOST_ASSERT( this->T() > 0 );
        BOOST_ASSERT( this->loan_amount() > 0 );
        BOOST_ASSERT( this->loan_duration() > 0 );
        BOOST_ASSERT( this->loan_interest() >= 0 );
        BOOST_ASSERT( 0 <= this->ar() && this->ar() <= 1 );
        BOOST_ASSERT( this->transaction_cost() >= 0 );
        BOOST_ASSERT( this->n_investments() > 0 );
        for( auto p : investment_parameters_ ) {
            auto const trend_mu = std::get<0>(p);
            auto const noise_sigma = std::get<1>(p);
            BOOST_ASSERT( noise_sigma >= 0 );
            max_sale_price_magnitude_ = std::max( max_sale_price_magnitude_,
                std::abs( (this->T() * trend_mu) ) + 5 * noise_sigma );
        }
    }

    int T() const
    { return T_; }

    double loan_amount() const
    { return loan_amount_; }

    int loan_duration() const
    { return loan_duration_; }

    double loan_interest() const
    { return loan_interest_; }

    std::size_t n_investments() const
    { return investment_parameters_.size(); }

    std::tuple<double, double> investment_parameters( std::size_t const i ) const
    { return investment_parameters_[i]; }

    double ar() const
    { return ar_; }

    double transaction_cost() const
    { return transaction_cost_; }

    /**
     * @brief A high-probability bound on the magnitude of the reward received
     * due to taking the Sell action.
     * @return
     */
    double max_sale_price_magnitude() const
    { return max_sale_price_magnitude_; }

    friend std::ostream& operator <<( std::ostream& out, Parameters const& p );

private:
    int T_;
    double loan_amount_;
    int loan_duration_;
    double loan_interest_;
    double ar_;
    double transaction_cost_;
    std::vector<std::tuple<double, double>> investment_parameters_;

    double max_sale_price_magnitude_;
};

std::ostream& operator <<( std::ostream& out, Parameters const& p )
{
    out << "{T: " << p.T() << ", n: " << p.n_investments()
        << ", theta: " << p.investment_parameters_ << "}";
    return out;
}

// ---------------------------------------------------------------------------

class State
{
public:
    friend class Simulator;

    static constexpr int None = -1;

    explicit State( Parameters const& parameters )
        : t_( 0 ), loan_t_( None ), investment_( None ),
          purchase_price_( 0 ), price_true_( parameters.n_investments() ),
          price_market_( parameters.n_investments() )
    { }

    int t() const
    { return t_; }

    bool has_loan() const
    { return loan_t_ > 0; }

    int loan_t() const
    { return loan_t_; }

    bool has_investment() const
    { return investment_ != None; }

    std::size_t investment() const
    { return static_cast<std::size_t>(investment_); }

    double purchase_price() const
    { return purchase_price_; }

    double price_true( std::size_t const i ) const
    { return price_true_[i]; }

    std::vector<double> const& price_true() const
    { return price_true_; }

    double price_market( std::size_t const i ) const
    { return price_market_[i]; }

    std::vector<double> const& price_market() const
    { return price_market_; }

    bool isTerminal( Parameters const& parameters ) const
    {
        return t_ >= parameters.T();
    }

    friend std::ostream& operator <<( std::ostream&, State const& );
    friend bool operator ==( State const& a, State const& b );
    friend bool operator <( State const& a, State const& b );

private:
    int t_;
    int loan_t_;
    int investment_;
    double purchase_price_;
    std::vector<double> price_true_;
    std::vector<double> price_market_;
};

bool operator ==( State const& a, State const& b )
{
    return a.t() == b.t()
           && a.loan_t() == b.loan_t()
           && a.investment() == b.investment()
           && a.purchase_price() == b.purchase_price()
           && a.price_true_ == b.price_true_
           && a.price_market_ == b.price_market_;
}

bool operator !=( State const& a, State const& b )
{ return !(a == b); }

bool operator <( State const& a, State const& b )
{
    return std::tie(a.t_, a.loan_t_, a.investment_, a.purchase_price_,
                    a.price_true_, a.price_market_)
           < std::tie(b.t_, b.loan_t_, b.investment_, b.purchase_price_,
                      b.price_true_, b.price_market_);
}

std::ostream& operator <<( std::ostream& out, State const& s )
{
    out << "{t: " << s.t() << ", lt: " << s.loan_t()
        << ", i: ";
    if( s.has_investment() ) {
        out << s.investment();
    }
    else {
        out << "None";
    }
    out << ", p: " << s.purchase_price()
        << ", pt: " << s.price_true_ << ", pm: " << s.price_market_ << "}";
    return out;
}

// ---------------------------------------------------------------------------

enum class ActionKind
{
    wait,
    borrow,
    sell,
    buy
};

class Action
{
public:
    Action()
        : Action( 0 )
    { }

    explicit Action( int const k )
        : Action( ActionKind(std::min(k, 3)), (k >= 3 ? k - 3 : 0) )
    { }

    Action( ActionKind const kind, int const i = 0 )
        : kind_( kind ), i_( i )
    { }

    ActionKind kind() const
    { return kind_; }

    int i() const
    { return i_; }

private:
    ActionKind kind_;
    unsigned i_;

};

std::ostream& operator <<( std::ostream& out, Action const& a )
{
    out << "A{kind: ";
    switch( a.kind() ) {
    case ActionKind::wait:      out << "wait"; break;
    case ActionKind::borrow:    out << "borrow"; break;
    case ActionKind::buy:       out << "buy"; break;
    case ActionKind::sell:      out << "sell"; break;
    }
    out << ", i: " << a.i() << "}";
    return out;
}

bool operator ==( Action const& a, Action const& b )
{
    return a.kind() == b.kind() && a.i() == b.i();
}

bool operator !=( Action const& a, Action const& b )
{ return !(a == b); }

bool operator <( Action const& a, Action const& b )
{
    if( a.kind() < b.kind() ) {
        return true;
    }
    else if( a.kind() == b.kind() ) {
        return a.i() < b.i();
    }

    return false;
}

// ---------------------------------------------------------------------------

class Domain
{
public:
    using ParametersType = Parameters;
    using StateType     = State;
    using GroundStateType = State;
    using ActionType    = Action;
    using RewardType    = double;
    using SimulatorType = Simulator;

    static boost::optional<ActionType> DefaultAction()
    {
        return Action( ActionKind::wait );
    }

    template<typename A>
    using ActionSet     = boost::container::flat_set<A>;

    // -----------------------------------------------------------------------
    // StateHeuristic concept

    // FIXME: We should differentiate admissible and non-admissible heuristics
    RewardType evaluateState( StateType const& /*s*/ ) const
    {
        return 0;
    }

    // -----------------------------------------------------------------------
    // BoundedValue concept

    RewardType Rmin() const
    {
        return -parameters_.loan_amount() * (1.0 + parameters_.loan_interest())
             + -parameters_.max_sale_price_magnitude()
             + parameters_.transaction_cost();
    }

    RewardType Rmax() const
    {
        return parameters_.max_sale_price_magnitude() + parameters_.loan_amount();
    }

    RewardType future_value_min( StateType const& s ) const
    {
        return Rmin() * (parameters_.T() - s.t());
    }

    RewardType future_value_min( StateType const& s, ActionType const& /*a*/ ) const
    {
        return Rmin() * (parameters_.T() - s.t() - 1);
    }

    RewardType future_value_max( StateType const& s ) const
    {
        return Rmax() * (parameters_.T() - s.t());
    }

    RewardType future_value_max( StateType const& s, ActionType const& /*a*/ ) const
    {
        return Rmax() * (parameters_.T() - s.t() - 1);
    }

    explicit Domain( Parameters const& parameters )
        : parameters_( parameters )
    { }

    Parameters const& parameters() const
    {
        return parameters_;
    }

    double discount() const
    { return 1.0; }

    using FeaturesType = std::vector<double>;

    FeaturesType features( StateType const& s ) const
    {
        std::vector<double> phi {
            static_cast<double>(s.loan_t()), s.has_investment() ? 1.0 : 0.0,
            static_cast<double>(s.investment()), s.purchase_price() };
        phi.insert( phi.end(), s.price_true().begin(), s.price_true().end() );
        phi.insert( phi.end(), s.price_market().begin(), s.price_market().end() );
        return phi;
    }

    std::size_t Nfeatures() const
    { return 4 + 2*parameters_.n_investments(); }

    std::unique_ptr<StateType const> initialState( SamplingContext& /*ctx*/ ) const
    {
        return std::make_unique<StateType const>( parameters() );
    }
    ActionSet<ActionType> all_actions()const
    {
        std::vector<ActionType> as;
        as.push_back( { ActionKind::wait } );
        as.push_back( { ActionKind::borrow } );
        as.push_back( { ActionKind::sell } );

        for( std::size_t i = 0; i < parameters().n_investments(); ++i ) {
                as.push_back( { ActionKind::buy, int(i) } );

        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

    std::size_t max_actions() const
    { return 3 + parameters().n_investments(); }

    ActionSet<ActionType> actions( State const& s ) const
    {
        std::vector<ActionType> as;
        as.push_back( { ActionKind::wait } );
        if( !s.has_loan() ) {
            as.push_back( { ActionKind::borrow } );
        }
        if( s.has_investment() ) {
            as.push_back( { ActionKind::sell } );
        }
        for( std::size_t i = 0; i < parameters().n_investments(); ++i ) {
            if( !s.has_investment() || i != s.investment() ) {
                as.push_back( { ActionKind::buy, int(i) } );
            }
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

private:
    Parameters parameters_;
};


}}}


#endif
