#ifndef MCPLAN_DOMAINS_INVESTMENT_SIMULATOR_HPP
#define MCPLAN_DOMAINS_INVESTMENT_SIMULATOR_HPP

#include "mcplan/domains/Investment/Domain.hpp"

#include "mcplan/Log.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"


// Forward declaration of Simulator class
namespace mcplan { namespace domains { namespace Investment {
    class Simulator;
}}}

// Traits specializations in namespace mcplan::sim
namespace mcplan { namespace sim {
    template<>
    struct is_transition_simulator<mcplan::domains::Investment::Simulator>
    {
        static bool constexpr value = true;
    };
}}


namespace mcplan { namespace domains { namespace Investment {


class Simulator : public mcplan::sim::TransitionSimulator<Simulator, Domain>
{
public:
    TransitionSample sampleTransition(
        SamplingContext& ctx, Domain const& domain, State const& s, Action const& a ) const
    {
        typename Domain::ParametersType const& parameters = domain.parameters();
        auto sprime = std::make_unique<State>( s );
        double r = 0;
        double r_a = 0;

        // Action
        switch( a.kind() ) {
        case ActionKind::borrow:
            assert( !s.has_loan() );
            sprime->loan_t_ = parameters.loan_duration();
            r_a += parameters.transaction_cost();
            r += parameters.loan_amount();
            break;
        case ActionKind::sell:
            assert( s.has_investment() );
            r_a += parameters.transaction_cost();
            r += s.price_market( s.investment() ) - s.purchase_price();
            sprime->investment_ = State::None;
            sprime->purchase_price_ = 0;
            break;
        case ActionKind::buy:
            if( s.has_investment() ) {
                // Sell current investment
                r_a += parameters.transaction_cost();
                r += s.price_market( s.investment() ) - s.purchase_price();
            }
            r_a += parameters.transaction_cost();
            sprime->investment_ = a.i();
            sprime->purchase_price_ = s.price_market( a.i() );
            break;
        case ActionKind::wait:
            break;
        default:
            throw std::runtime_error( "Unhandled enum value" );
        };

        // Dynamics
        for( std::size_t i = 0; i < parameters.n_investments(); ++i ) {
            auto theta = parameters.investment_parameters( i );
            // True price follows Gaussian random walk
            std::normal_distribution<> ptrue( std::get<0>( theta ), 1 );
            double const pt = sprime->price_true( i ) + ptrue(ctx.rng());
            sprime->price_true_[i] = pt;
            // Market price is attracted to true price + 0-mean Gaussian noise
            std::normal_distribution<> pmarket( 0, std::get<1>( theta ) );
            double const step = sprime->price_true( i ) + pmarket(ctx.rng());
            double const d = pt - step;
            sprime->price_market_[i] = step + (1.0 - parameters.ar()) * d;
        }
        sprime->t_ += 1;
        if( sprime->loan_t() >= 0 ) {
            sprime->loan_t_ -= 1;
        }

        if( sprime->loan_t() == 0 ) {
            // Repay loan with interest
            r += -(1.0 + parameters.loan_interest()) * parameters.loan_amount();
            sprime->loan_t_ = State::None;
        }
        // Mandatory sale on last time step
        if( sprime->isTerminal( parameters ) ) {
            if( sprime->has_investment() ) {
                r += sprime->price_market( sprime->investment() ) - sprime->purchase_price();
                sprime->investment_ = State::None;
                sprime->purchase_price_ = 0;
            }
            if( sprime->has_loan() ) {
                r += -(1.0 + parameters.loan_interest()) * parameters.loan_amount();
                sprime->loan_t_ = State::None;
            }
        }

        // Return value
        ActionSample<Domain> asample( a, r_a );
        StateSample<Domain> ssample( std::move(sprime), r );
        return std::make_tuple( std::move(asample), std::move(ssample) );
    }
};

}}}


#endif
