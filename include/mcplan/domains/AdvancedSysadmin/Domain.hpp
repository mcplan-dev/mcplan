#ifndef MCPLAN_DOMAINS_ADVANCEDSYSADMIN_DOMAIN_HPP__
#define MCPLAN_DOMAINS_ADVANCEDSYSADMIN_DOMAIN_HPP__


#include "mcplan/domains/AdvancedSysadmin/Action.hpp"
#include "mcplan/domains/AdvancedSysadmin/State.hpp"
#include "mcplan/domains/AdvancedSysadmin.hpp"

#include "boost/container/flat_set.hpp"

#include <algorithm>
#include <vector>

namespace mcplan { namespace domains {


class AdvancedSysadmin::Domain
{
    using BitVectorType =long long;
public:
    using StateType     = State;
    using GroundStateType = State;
    using ActionType    = Action;
    using RewardType    = double;
    using ActionSet     = boost::container::flat_set<Action>;

    Domain(){};
    unsigned max_actions() const
    {
        return 5;
    }
    static constexpr RewardType Vmin = 0;
    static constexpr RewardType Vmax = std::numeric_limits<RewardType>::max();

   ActionSet actions( State const& ) const{
       assert("This function Should not be called for this domain\n");

   };


};



}}


#endif
