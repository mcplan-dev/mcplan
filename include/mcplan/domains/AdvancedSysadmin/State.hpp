#ifndef MCPLAN_DOMAINS_ADVANCEDSYSADMIN_STATE_HPP__
#define MCPLAN_DOMAINS_ADVANCEDSYSADMIN_STATE_HPP__


#include <iosfwd>
#include <memory>

#include <iostream> // FIXME: Debugging
#include "mcplan/domains/AdvancedSysadmin.hpp"

namespace mcplan { namespace domains {

    

class AdvancedSysadmin::Parameters
{
public:
    /**
     * @param T Episode length
     * @param nc Number of Computers
     * @param TopologyType (0: Ring, 1: Bus, 2: Hub  ,>3: Custom
     */
    Parameters( int T, int nc,  int MaxOn, int tt )
        : T_( T ), nc_( nc ),tt_( tt ),MaxOn_(MaxOn)
    { }
    
    Parameters( Parameters const& that )
        : T_( that.T_ ), nc_( that.nc_ ), tt_( that.tt_ ),MaxOn_(that.MaxOn_)
    { }
    
    int T()  const { return T_; }
    int nc() const { return nc_; }
    int MaxOn() const{ return MaxOn_;}
    int tt() const { return tt_; }
    
private:
    int const T_;
    int const nc_;
    int const tt_;
    int const MaxOn_;
};

class AdvancedSysadmin::Topology
{
    public:
        Topology(int tt, int nc)
        {
            connections_=new bool[nc*nc];
            switch(tt)
            {
                case 0: //Line Topology

                    for(int i=0;i<nc;i++)
                    {
                       for(int j=i;j<nc;j++)
                       {
                           if((i==j)||(i+1==j))
                           {
                                connections_[i*nc+j]=1;
                                connections_[j*nc+i]=1;
                           }

                       }
                    }
                break;
                case 1: //Ring Topology
                    connections_[0*nc+nc-1]=1;
                    connections_[(nc-1)*nc+0]=1;
                    for(int i=0;i<nc;i++)
                    {
                       for(int j=i;j<nc;j++)
                       {
                           if((i==j)||(i+1==j))
                           {
                                connections_[i*nc+j]=1;
                                connections_[j*nc+i]=1;
                           }

                       }
                    }
                break;
                case 2: //Hub topology
                    for(int i=0;i<nc;i++)
                    {
                        connections_[i*nc+i]=1;
                        connections_[i*nc+0]=1;
                        connections_[0*nc+i]=1;
                    }
                 break;


             }



        }
        Topology(bool connections[])
        {
            connections_=connections;
        }

     private:
        friend class State;
        bool* connections_;

};

/**
 * @brief The State class
 */
class AdvancedSysadmin::State
{
public:
    using BitVectorType = long long;

    // FIXME: Debugging
    ~State()
    {
        //std::cout << "~State(): " << *this << std::endl;
    }

    Parameters const& params() const { return params_; }
    BitVectorType ComputerState() const { return ComputerState_; }
    int t() const { return t_; }
    Topology* topology()const { return topology_;}
    bool isTerminal() const
    { return t_ == params_.T(); }

    friend std::ostream& operator<<( std::ostream& out, State const& s )
    {
        int nc =s.params_.nc();
        out << "Computer State=[";

        for(int i=0;i<nc;i++)
        {
            out<<","<< ((s.ComputerState_&(BitVectorType(1)<<i)) != 0);
        }
        out<< "], t="<<s.t_;


        return out;
    }
    bool operator==(  State const& s )
    {
        if((t_==s.t_) && (s.ComputerState_==ComputerState_))
            return true;
        else
            return false;

    }
    int getNumNbrs(int queryComp) const
    {
       int nbrs =0;
       int nc=params_.nc();
       for(int i=0;i<nc;i++)
       {
            if(i==queryComp)
                continue;
            if(topology_->connections_[queryComp*nc+1])
                nbrs+=1;
       }
       return nbrs;
    }
    int getNumActiveNbrs(int queryComp ) const
    {
        int active_nbrs =0;
        int nc = params_.nc();
        for(int i=0;i<nc;i++)
        {
             if(i==queryComp)
                 continue;
             if((topology_->connections_[queryComp*nc+1]) && (((ComputerState_&(BitVectorType(1)<<i))) != 0)) {
                 active_nbrs+=1;
             }
        }
        return active_nbrs;
    }

    
private:
    friend class StateBuilder;
    friend class Topology;

    State( Parameters const& p, BitVectorType ComputerState, int t,Topology* topology )
        : params_( p ), ComputerState_( ComputerState), t_( t ),topology_(topology)
    { }
    
private:
    Parameters const& params_;
    BitVectorType ComputerState_;
    int t_;
    Topology* topology_;
};

bool operator <( AdvancedSysadmin::State const& a, AdvancedSysadmin::State const& b )
{
    if( a.ComputerState() < b.ComputerState() ) {
        return true;
    }
    else if( a.ComputerState() == b.ComputerState() ) {
        return a.t() < b.t();
    }

    return false;
}



class AdvancedSysadmin::StateBuilder
{
public:
    using BitVectorType = State::BitVectorType;

    explicit StateBuilder( Parameters const& params )
        : params_( params ), ComputerState_( 0 ), t_( 0 )
    { }
    
    StateBuilder& ComputerState( BitVectorType ComputerState)
    { ComputerState_ = ComputerState; return *this; }

    StateBuilder& t( int t ) { t_ = t; return *this; }
    
    std::unique_ptr<State const> finish(Topology* topology) const
    {


        return std::unique_ptr<State const>( new State( params_,ComputerState_, t_,topology ) );
    }
    
private:
    Parameters const& params_;
    BitVectorType ComputerState_;
    int t_;
};

}}


#endif

