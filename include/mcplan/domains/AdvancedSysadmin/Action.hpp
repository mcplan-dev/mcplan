#ifndef MCPLAN_DOMAINS_ADVANCEDSYSADMIN_ACTION_HPP__
#define MCPLAN_DOMAINS_ADVANCEDSYSADMIN_ACTION_HPP__


#include <iosfwd>

// FIXME: Debugging
#include <iostream>
#include "mcplan/domains/AdvancedSysadmin.hpp"
namespace mcplan { namespace domains {

class AdvancedSysadmin::Action
{
    using BitVectorType = long long;
public:
    // FIXME: Debugging
//    ~Action()
//    {
//        std::cout << "~Action()" << std::endl;
//    }
    Action()
    {

    }

    Action( BitVectorType ComputerSwitch )
        : ComputerSwitch_( ComputerSwitch )
    { }
    
    int ComputerSwitch() const { return ComputerSwitch_; }
    bool operator==(Action const& a)
    {
        if(a.ComputerSwitch()==this->ComputerSwitch())
            return true;
        else
            return false;
    }

    friend std::ostream& operator<<( std::ostream& out, Action const& a )
    {

        out << "Action Vector=[";


        for(int i=0;i<11;i++)
        {
            out<<","<< ((a.ComputerSwitch()&(BitVectorType(1)<<i)) != 0);
        }
        out<< "]";


        return out;

    }
private:
    BitVectorType ComputerSwitch_;

};

bool operator <( AdvancedSysadmin::Action a, AdvancedSysadmin:: Action b )
{ return a.ComputerSwitch()<b.ComputerSwitch(); }
    
    
}}


#endif
