#ifndef COUNT_ONES_ACTION_ABSTRACTION_HPP__
#define COUNT_ONES_ACTION_ABSTRACTION_HPP__

template<typename Diagram>
class Count_Ones_ActionAbstraction : public mcplan::abstraction::ActionAbstraction<Diagram>
{
    using BitVectorType = long long;

    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        AbstractType apply( GroundType const& g ) const override
        {
            // FIXME
            BitVectorType ActionVector=g.ComputerSwitch();
            int count_ons=0;
            for(int i=0;i<sizeof(BitVectorType)*8;i++)
            {
                if(ActionVector & BitVectorType(1<<i))
                    count_ons+=1;
            }
            return leaves_[count_ons];


        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const& y ) const override
        {
            BitVectorType GroundActionVector=BitVectorType(0);
            int i;
            for(i=0;i<leaves_.size();i++)
            {
                if(y==leaves_[i])
                    break;
            }
            for(int j=0;j<i;j++)
            {
                int k;
                do{
                    std::uniform_int_distribution<> pr( 0, i );
                    k=pr(ctx.rng());
                 }while(GroundActionVector&=BitVectorType(1<<k));
                 GroundActionVector|=BitVectorType(1<<k);

            }
            return GroundActionVector;

        }
    private :
        std::vector<AbstractType> leaves_;
};

#endif
