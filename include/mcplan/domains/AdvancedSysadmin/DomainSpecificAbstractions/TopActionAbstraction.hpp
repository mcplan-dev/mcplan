#ifndef TOPACTION_ABSTRACTION_HPP__
#define TOPACTION_ABSTRACTION_HPP__

template<typename Diagram>
class TopActionAbstraction : public mcplan::abstraction::ActionAbstraction<Diagram>
{
    using BitVectorType = long long;

    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        AbstractType apply( GroundType const&  ) const override
        {
            // FIXME
            //BitVectorType ActionVector=g.ComputerSwitch();


           return leaves_[0];


        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const&  ) const override
        {

            BitVectorType GroundActionVector=BitVectorType(0);         
            unsigned MaxOns = 4;
            unsigned nc=11;
            bool *temp=new bool[11];
            std::uniform_int_distribution<> p( 0, nc-1 );
            std::uniform_real_distribution<> q;
            for(int i=0;i<MaxOns;i++)
            {
                int comp_set;

                do{
                 comp_set=p( ctx.rng() );

                }while(temp[comp_set]);
                temp[comp_set]=1;

                 if(q(ctx.rng())>0.5)
                     GroundActionVector|=BitVectorType(1<< comp_set);
            }
            return GroundActionVector;

        }
    private :
        std::vector<AbstractType> leaves_;
};


#endif
