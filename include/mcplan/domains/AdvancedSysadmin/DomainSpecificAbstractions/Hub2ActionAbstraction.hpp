#ifndef HUB2_ACTION_ABSTRACTION_HPP__
#define HUB2_ACTION_ABSTRACTION_HPP__

#include "mcplan/domains/AdvancedSysadmin.hpp"

template<typename Diagram>
class Hub2ActionAbstraction : public mcplan::abstraction::ActionAbstraction<Diagram>
{
    using BitVectorType = long long;

    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        AbstractType apply( GroundType const& g ) const override
        {
            // FIXME
            BitVectorType ActionVector=g.ComputerSwitch();


           if(ActionVector & BitVectorType(1))
               return leaves_[0];
           else
               return leaves_[1];


        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const& y ) const override
        {
            BitVectorType GroundActionVector=BitVectorType(0);
            if(y==leaves_[0])
                 GroundActionVector|=BitVectorType(1);
            int MaxOns = 4;
            if( GroundActionVector&=BitVectorType(1))
                MaxOns = 3;
            int nc=11;
            bool* temp=new bool[10];
            for(int i=0;i<MaxOns;i++)
            {
                int comp_set;
                do{
                 std::uniform_int_distribution<> p( 1, nc-1 );
                 comp_set=p( ctx.rng() );
                }while(temp[comp_set]);
                temp[comp_set]=1;
                 std::uniform_real_distribution<> p;
                 if(p(ctx.rng())>0.5)
                     GroundActionVector|=BitVectorType(1<< comp_set);
            }
            return GroundActionVector;

        }
    private :
        std::vector<AbstractType> leaves_;
};


#endif
