#ifndef MCPLAN_DOMAINS_ADVANCEDSYSADMIN_SIMULATOR_HPP__
#define MCPLAN_DOMAINS_ADVANCEDSYSADMIN_SIMULATOR_HPP__


#include "mcplan/domains/AdvancedSysadmin/Domain.hpp"
#include "mcplan/domains/AdvancedSysadmin.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"

#include "mcplan/ActionSample.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/StateSample.hpp"

#include <random>
#include <utility>

namespace mcplan { namespace domains {

class AdvancedSysadmin::Simulator : public mcplan::sim::TransitionSimulator<Domain>
{
public:

    TransitionSample sampleTransition( SamplingContext& ctx,
                                       State const& s, Action const& a ) const override
    {
        using BitVectorType = State::BitVectorType;
        double ar = 0;
        BitVectorType actionVector=a.ComputerSwitch();
        for(int i=0;i<s.params().nc();i++)
        {
            if((actionVector&(BitVectorType(1)<<i))==BitVectorType(1))
                ar+=1;
        }
        ActionSample<Domain> an( a, ar );
        BitVectorType NextComputerState=0;
        double sr=0;
        for(int i=0;i<s.params().nc();i++)
        {
            double m=1.0;
            if((actionVector&(BitVectorType(1)<<i))==0)
            {
                if((s.ComputerState() & (BitVectorType(1)<<i)))
                {
                    int nbrs=s.getNumNbrs(i);
                    int active_nbrs=s.getNumActiveNbrs(i);
                    m=0.45+0.5*(1.0+(float)active_nbrs)/(1+nbrs);
                }
                else
                    m=0.05;// Fix this ---s.params().reboot_prob();
            }
            std::uniform_real_distribution<> p;
            if(p(ctx.rng())<m)
            {
                NextComputerState|=(BitVectorType(1)<<i);
                sr+=1;             }
             else
                NextComputerState&=~(BitVectorType(1)<<i);

        }

        StateSample<Domain> sn( StateBuilder( s.params() )
            .ComputerState(NextComputerState  ).t( s.t() + 1 ).finish(s.topology()),sr  );


        return std::make_tuple( std::move(an), std::move(sn) );
    }
};

}}


#endif

