#ifndef MCPLAN_DOMAINS_ADVANCEDSYSADMIN_HPP__
#define MCPLAN_DOMAINS_ADVANCEDSYSADMIN_HPP__


namespace mcplan {namespace  domains {
class AdvancedSysadmin{
public:
    class State;
    class Action;
    class Parameters;
    class StateBuilder;
    class Domain;
    class Simulator;
    class Topology;

};

}

}

#endif
