#ifndef MCPLAN_DOMAINS_CARTPOLE_HPP__
#define MCPLAN_DOMAINS_CARTPOLE_HPP__




namespace mcplan { namespace domains {

class CartPole
{
public:
    class State;
    class Action;
    class Domain;
    class Simulator;
    class StateBuilder;
    class Parameters;


};
}
}



#endif
