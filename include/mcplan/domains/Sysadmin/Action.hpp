#ifndef MCPLAN_DOMAINS_SYSADMIN_ACTION_HPP__
#define MCPLAN_DOMAINS_SYSADMIN_ACTION_HPP__


#include <iosfwd>

// FIXME: Debugging
#include <iostream>
#include "mcplan/domains/Sysadmin.hpp"

namespace mcplan { namespace domains {

class Sysadmin::Action
{
public:
    Action()
        : k_( 0 )
    { }

    explicit Action( int k )
        : k_( k )
    { }
    
    int k() const { return k_; }

private:
    int k_;
};

std::ostream& operator<<( std::ostream& out, Sysadmin::Action const& a )
{
    out << "Action[k=" << a.k() << "]";
    return out;
}

bool operator ==( Sysadmin::Action const& a, Sysadmin::Action const& b )
{ return a.k() == b.k(); }

bool operator !=( Sysadmin::Action const& a, Sysadmin::Action const& b )
{ return !(a == b); }

bool operator <( Sysadmin::Action const& a, Sysadmin::Action const& b )
{ return a.k() < b.k(); }
    
    
}}


#endif
