#ifndef MCPLAN_DOMAINS_SYSADMIN_SIMULATOR_HPP__
#define MCPLAN_DOMAINS_SYSADMIN_SIMULATOR_HPP__


#include "mcplan/domains/Sysadmin/Domain.hpp"
#include "mcplan/domains/Sysadmin.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"

#include "mcplan/ActionSample.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/StateSample.hpp"

#include <random>
#include <utility>

// Forward declaration of Simulator class
//namespace mcplan { namespace domains {
//    class Sysadmin::Simulator;
//}}

// Traits specializations in namespace mcplan::sim
namespace mcplan { namespace sim {
    template<>
    struct is_transition_simulator<mcplan::domains::Sysadmin::Simulator>
    {
        static bool constexpr value = true;
    };
}}

namespace mcplan { namespace domains {

class Sysadmin::Simulator : public mcplan::sim::TransitionSimulator<Sysadmin::Simulator, Sysadmin::Domain>
{
public:

    TransitionSample sampleTransition( SamplingContext& ctx, Domain const& domain,
                                       State const& s, Action const& a ) const
    {
        using BitVectorType = State::BitVectorType;
        double const ar=0;
        double const f = a.k()==0 ? 0 : -1;
        ActionSample<Domain> an( a, ar );
        BitVectorType NextComputerState=0;
        double sr=0;
        //std::cout<<s<<"a "<<a<<"\n";
        for(int i=0;i<domain.parameters().nc();i++)
        {
            double m=1.0;
            if(a.k()!=i+1)
            {
                if((s.ComputerState() & (BitVectorType(1)<<i)))
                {

                    int nbrs=s.getNumNbrs(i);
                    int active_nbrs=s.getNumActiveNbrs(i);
                    m=0.45+0.5*(1.0+(float)active_nbrs)/(1+nbrs);
                }
                else
                {
                    m=0.05;// Fix this ---s.params().reboot_prob();

                }
            }

            std::uniform_real_distribution<> p;
            double r= p(ctx.rng());
            if(r<m)
            {
                NextComputerState|=(BitVectorType(1)<<i);
                sr+=1;             }
             else
                NextComputerState&=~(BitVectorType(1)<<i);
            //std::cout<<" m "<<m<<"r "<<r<<"\n";

         }

        sr+=f;
        StateSample<Domain> sn( StateBuilder( domain.parameters() )
            .ComputerState(NextComputerState  ).t( s.t() + 1 ).finish(s.topology()),sr  );


        return std::make_tuple( std::move(an), std::move(sn) );
    }
};

}}


#endif

