#ifndef MCPLAN_DOMAINS_SYSADMIN_DOMAIN_HPP__
#define MCPLAN_DOMAINS_SYSADMIN_DOMAIN_HPP__


#include "mcplan/domains/Sysadmin/Action.hpp"
#include "mcplan/domains/Sysadmin/State.hpp"
#include "mcplan/domains/Sysadmin.hpp"

#include "boost/container/flat_set.hpp"

#include <algorithm>
#include <vector>

namespace mcplan { namespace domains {


class Sysadmin::Domain
{
public:
    using ParametersType = Parameters;
    using StateType     = State;
    using GroundStateType = State;
    using ActionType    = Action;
    using RewardType    = double;
    using SimulatorType = Simulator;

    using StateFeatures = std::vector<double>;

    static boost::optional<ActionType> DefaultAction()
    {
        return Action( 0 );
    }

    template<typename A>
    using ActionSet     = boost::container::flat_set<A>;

    explicit Domain( ParametersType const& params )
        : params_( params )
    {
        topology_= new Topology(params_.tt(),params_.nc());

    }

    ParametersType const& parameters() const
    { return params_; }

    using FeaturesType = std::vector<double>;

    FeaturesType features( StateType const& s ) const
    {
        std::vector<double> phi;
        auto const bits = s.ComputerState();
        State::BitVectorType const one( 1 );
        for( int i = 0; i < params_.nc(); ++i ) {
            auto const mask = one << i;
            phi.push_back( (bits & mask) == 0 ? 0.0 : 1.0 );
        }
        return phi;
    }

    std::size_t Nfeatures() const
    { return params_.nc(); }

    std::unique_ptr<StateType const> initialState( SamplingContext& /*ctx*/ ) const
    {
        return Sysadmin::StateBuilder( params_ ).finish(topology_);
    }

    StateFeatures features( StateType const& s )
    {
        std::vector<double> phi;
        auto const bits = s.ComputerState();
        StateType::BitVectorType const one( 1 );
        for( int i = 0; i < params_.nc(); ++i ) {
            auto const mask = one << i;
            phi.push_back( (bits & mask) == 0 ? 0.0 : 1.0 );
        }
        return phi;
    }

    // -----------------------------------------------------------------------
    // StateHeuristic concept

    // FIXME: We should differentiate admissible and non-admissible heuristics
    RewardType evaluateState( StateType const& s ) const
    {
        return s.CountActiveSystems();
    }

    // -----------------------------------------------------------------------
    // BoundedValue concept

    RewardType Rmin() const { return -1; }
    RewardType Rmax() const { return params_.nc(); }

    RewardType Vmin() const { return params_.T() * Rmin(); }
    RewardType Vmax() const { return params_.T() * Rmax(); }

    RewardType future_value_min( StateType const& s ) const
    {
        return Rmin() * (params_.T() - s.t());
    }

    RewardType future_value_min( StateType const& s, ActionType const& /*a*/ ) const
    {
        return Rmin() * (params_.T() - s.t() + 1);
    }

    RewardType future_value_max( StateType const& s ) const
    {
        return Rmax() * (params_.T() - s.t());
    }

    RewardType future_value_max( StateType const& s, ActionType const& /*a*/ ) const
    {
        return Rmax() * (params_.T() - s.t() + 1);
    }

    unsigned max_actions() const
    {
        return params_.nc()+1;
    }

    ActionSet<Action> all_actions(  ) const
    {
        std::vector<Action> as;
        as.reserve( params_.nc()+1 ); /* 0 for No-operation and 1 for each computer */
        for( int i = 0; i < params_.nc()+1; ++i ) {
            as.push_back( Action( i ) );
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

    ActionSet<Action> actions( State const& /*s*/ ) const
    {
        std::vector<Action> as;
        as.reserve( params_.nc()+1 ); /* 0 for No-operation and 1 for each computer */
        for( int i = 0; i < params_.nc()+1; ++i ) {
            as.push_back( Action( i ) );
        }
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<Action>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

private:
    ParametersType params_;
    Topology* topology_;
};



}}


#endif
