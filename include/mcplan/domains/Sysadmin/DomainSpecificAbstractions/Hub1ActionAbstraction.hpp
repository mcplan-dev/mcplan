#ifndef HUB1_ACTION_ABSTRACTION_HPP__
#define HUB1_ACTION_ABSTRACTION_HPP__


#include "mcplan/abstraction/ActionAbstraction.hpp"

namespace mcplan { namespace abstraction {


template<typename Diagram>
class Hub1ActionAbstraction: public ActionAbstraction<Diagram>
{
    using BitVectorType = long long;

    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        Hub1ActionAbstraction(unsigned nc)
        {
            nc_=nc;
        }

        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        AbstractType apply( GroundType const& g ) const override
        {
            


           if(g.k()==0)
               return leaves_[0];
           else
               return leaves_[1];


        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const& y ) const override
        {
            if(y==leaves_[0])
                return 1;

            else
            {

                std::uniform_int_distribution<> p( 1, nc_);
                int comp_set=p( ctx.rng() );
                if(comp_set==1)
                    return 0;
                else
                    return comp_set;
            }
            

        }
    private :
        std::vector<AbstractType> leaves_;
        unsigned nc_;
};

}}
#endif
