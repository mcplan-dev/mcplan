#ifndef TOPACTION_ABSTRACTION_HPP__
#define TOPACTION_ABSTRACTION_HPP__

template<typename Diagram>
class TopActionAbstraction : public mcplan::abstraction::ActionAbstraction<Diagram>
{
    using BitVectorType = long long;

    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        AbstractType apply( GroundType const&  ) const override
        {
            // FIXME
            //BitVectorType ActionVector=g.ComputerSwitch();


           return leaves_[0];


        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const&  ) const override
        {
            int nActions =10+1;
           
            std::uniform_int_distribution<> p( 0, nActions );
            int i = p(ctx.rng());
            return i;

        }
    private :
        std::vector<AbstractType> leaves_;
};


#endif
