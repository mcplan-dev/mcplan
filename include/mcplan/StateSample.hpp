#ifndef MCPLAN_STATESAMPLE_HPP__
#define MCPLAN_STATESAMPLE_HPP__


#include <memory>

namespace mcplan {


template<typename Domain>
class StateSample
{
public:
    StateSample( std::unique_ptr<typename Domain::StateType const> s, typename Domain::RewardType const r )
        : s_( std::move(s) ), r_( r )
    { }

    StateSample( StateSample<Domain>&& that )
        : s_( std::move(that.s_) ), r_( std::move(that.r_) )
    { }

    StateSample<Domain>& operator=( StateSample<Domain>&& that )
    {
        s_ = std::move(that.s_);
        r_ = std::move(that.r_);
        return *this;
    }

    typename Domain::RewardType r() const { return r_; }

    typename Domain::StateType const& s() const { return *s_; }

    std::unique_ptr<typename Domain::StateType const> transfer_s()
    { return std::move(s_); }

private:
    std::unique_ptr<typename Domain::StateType const> s_;
    typename Domain::RewardType r_;
};


}


#endif
