#ifndef STATE_ABSTRACTION_HPP__
#define STATE_ABSTRACTION_HPP__


#include <memory>

namespace  mcplan { namespace abstraction {

namespace detail {

template<typename T>
struct deref_less
{
    bool operator ()( T const* a, T const* b ) const
    { return *a < *b; }
};

}

template<typename Diagram>
class StateAbstraction
{
public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::StateType;

    virtual ~StateAbstraction() { }
    virtual std::unique_ptr<StateAbstraction<Diagram>> copy() const = 0;

    virtual AbstractType apply( GroundType const& g ) = 0;
    virtual void split(AbstractType x1, AbstractType x2)=0;
    virtual void replaceAbstractStates(std::map<AbstractType,AbstractType> xOldNewMap)=0;
    virtual std::vector<GroundType const*> const&  GroundStates() const =0;
    virtual std::vector<AbstractType> const& AbstractVertices() const =0;

    using GroundToAbstractMapType = std::map<GroundType const*, AbstractType, detail::deref_less<GroundType>>;
    virtual GroundToAbstractMapType const& ground_to_abstract() const =0;
};


}}
#endif

