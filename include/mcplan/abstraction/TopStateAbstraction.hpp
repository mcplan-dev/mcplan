#ifndef TOP_STATE_ABSTRACTION_HPP__
#define TOP_STATE_ABSTRACTION_HPP__

#include "mcplan/abstraction/StateAbstraction.hpp"

namespace  mcplan { namespace abstraction {



template<typename Diagram>
class TopStateAbstraction : public StateAbstraction<Diagram>
{

public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::StateType;

    explicit TopStateAbstraction( AbstractType x )
        : x_( x )
    { }

    AbstractType apply( GroundType const &  /*g*/ )  override
    {
        return x_;
    }

private:
    AbstractType x_;
};

}}
#endif

