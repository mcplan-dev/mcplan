#ifndef BOTTOM_ACTION_ABSTRACTION_HPP__
#define BOTTOM_ACTION_ABSTRACTION_HPP__

#include "mcplan/abstraction/ActionAbstraction.hpp"

namespace mcplan{ namespace abstraction {



template<typename Diagram>
class BottomActionAbstraction : public ActionAbstraction<Diagram>
{

public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::ActionType;

    void setLeaves(std::vector<AbstractType> &leaves) override
    {
        leaves_=leaves;
    }


    boost::optional<AbstractType> apply( GroundType const& g ) const override
    {
        // FIXME
        return leaves_[g.k()];
    }
    GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const& y ) const override
    {

           for(unsigned i=0;i<leaves_.size();i++)
           {
               if(leaves_[i]==y)
                   return GroundType(i);
           }
           return 0;

    }

    void addAction(AbstractType const& y,GroundType const g) override
    {}
private:
    std::vector<AbstractType> leaves_;

};

}}

#endif
