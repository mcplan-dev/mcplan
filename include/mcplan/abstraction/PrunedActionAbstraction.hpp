#ifndef PRUNED_ACTION_ABSTRACTION_HPP__
#define PRUNED_ACTION_ABSTRACTION_HPP__

#include "mcplan/abstraction/ActionAbstraction.hpp"

#include "boost/optional.hpp"

#include <map>
#include <vector>
#include <random>

namespace mcplan{ namespace abstraction {



template<typename Diagram>
class PrunedActionAbstraction : public ActionAbstraction<Diagram>
{

public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::ActionType;

    void setLeaves(std::vector<AbstractType> &/*leaves*/) override
    {
        
    }


    boost::optional<AbstractType> apply( GroundType const& g ) const override
    {
        // FIXME
        auto it=ground_to_abstract_.find(g);
        if(it!=ground_to_abstract_.end()) {
            return it->second;
        }
        else {
            return boost::none;
        }
    }
    GroundType sampleAction( mcplan::SamplingContext& /*ctx*/, AbstractType const& y ) const override
    {

           for(unsigned i=0;i<GroundActions_.size();i++)
           {
               auto it=ground_to_abstract_.find(GroundActions_[i]);
                if(it->second==y)
                    return GroundActions_[i];

           }

        throw std::logic_error( "No GroundActions map to y" );
    }
    void addAction( AbstractType const& y, GroundType const a ) override
    {
    	AbstractVertices_.push_back(y);
        GroundActions_.push_back(a);
        ground_to_abstract_.insert(std::make_pair(a,y));
    }
private:
    std::vector<GroundType> GroundActions_;
    std::vector<AbstractType> AbstractVertices_;
    std::map<GroundType, AbstractType> ground_to_abstract_;

};

}}

#endif
