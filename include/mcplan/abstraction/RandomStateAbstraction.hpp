#ifndef RANDOM_STATE_ABSTRACTION_HPP__
#define RANDOM_STATE_ABSTRACTION_HPP__

#include "mcplan/abstraction/StateAbstraction.hpp"
#include <map>
#include <vector>
#include <random>

namespace  mcplan { namespace abstraction {


template<typename Diagram>
class RandomStateAbstraction : public StateAbstraction<Diagram>
{

public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::StateType;

//    void SetAbstractionMap(std::map<GroundType,AbstractType> g2a)
//    {
//        ground_to_abstract_=g2a;
//    }
//    void SetAbstractVertices(std::vector<AbstractType>& AbstractVertices)
//    {
//        AbstractVertices_=AbstractVertices;
//    }
//    void SetGroundStates(std::vector<GroundType>& GroundStates)
//    {
//        GroundStates_=GroundStates;
//    }

    explicit RandomStateAbstraction(std::vector<AbstractType> const& AbstractVertices)
        : AbstractVertices_( AbstractVertices )
    { }

    explicit RandomStateAbstraction( StateAbstraction<Diagram> const& that )
        : ground_to_abstract_( that.ground_to_abstract() ),
          AbstractVertices_( that.AbstractVertices() ),
          GroundStates_( that.GroundStates() )
    { }

    std::vector<GroundType const*> const& GroundStates() const override
    {
        return GroundStates_;
    }
    std::vector<AbstractType> const& AbstractVertices() const override
    {
        return AbstractVertices_;
    }

    GroundToAbstractMapType const& ground_to_abstract() const override
    {
        return ground_to_abstract_;
    }

    void split(AbstractType x1, AbstractType x2) override
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        for(unsigned i=0;i<AbstractVertices_.size();i++)
        {
            if(x1==AbstractVertices_[i])
                break;
        }
        AbstractVertices_.push_back(x2);
        for(auto it=GroundStates_.begin();it!=GroundStates_.end();it++)
        {
            if(ground_to_abstract_[*it]==x1)
            {
                std::uniform_real_distribution<> p(0,1);
                if(p(gen)>0.5)
                {
                    ground_to_abstract_[*it]=x2;
                }
            }
        }

    }
    void replaceAbstractStates (std::map<AbstractType, AbstractType> xOldNewMap) override
    {

        for(auto it=ground_to_abstract_.begin();it!=ground_to_abstract_.end();it++)
        {
            ground_to_abstract_[it->first]=xOldNewMap[it->second];

        }
        for(auto it=AbstractVertices_.begin();it!=AbstractVertices_.end();it++)
        {
            *it=xOldNewMap[*it];
        }



    }

    AbstractType apply(GroundType const&  s) override
    {
        auto itr = ground_to_abstract_.find( &s );
        if( itr != ground_to_abstract_.end() ) {
            return itr->second;
        }
        GroundStates_.push_back(&s);
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<std::size_t> p( 0, (AbstractVertices_.size())-1);
        std::size_t i = p(gen);

        ground_to_abstract_.insert(std::make_pair(&s,AbstractVertices_[i]));
        return AbstractVertices_[i];
    }


private:
    std::vector<GroundType const*> GroundStates_;
    std::vector<AbstractType> AbstractVertices_;
    GroundToAbstractMapType ground_to_abstract_;
};

}}
#endif

