#ifndef MCPLAN_ABSTRACTION_FINITEACTIONSETABSTRACTION_HPP
#define MCPLAN_ABSTRACTION_FINITEACTIONSETABSTRACTION_HPP

#include <ostream>

#include <boost/assert.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/optional/optional.hpp>
#include <boost/range/adaptor/map.hpp>

#include "mcplan/SamplingContext.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/stats/ReservoirSampler.hpp"


namespace mcplan { namespace abstraction {

// ---------------------------------------------------------------------------

template<typename Domain, typename AbstractType>
class FiniteActionSetAbstraction
{
    using ThisType          = FiniteActionSetAbstraction<Domain, AbstractType>;
    using ActionType        = typename Domain::ActionType;
    using SuccessorMap      = boost::container::flat_map<ActionType, AbstractType>;

public:

    // -----------------------------------------------------------------------
    // ActionAbstraction concept

    template<typename InputItr>
    FiniteActionSetAbstraction( InputItr first, InputItr last )
        : map_( first, last )
    { }

    // (Default copy and move)

    boost::optional<AbstractType> operator ()( ActionType const& a ) const
    {
        auto itr = map_.find( a );
        if( itr != map_.end() ) {
            return itr->second;
        }
        return boost::none;
    }

    boost::select_second_const_range<SuccessorMap> abstractVertices() const
    {
        return boost::adaptors::values(map_);
    }

    friend std::ostream& operator <<( std::ostream& out, FiniteActionSetAbstraction const& ab )
    {
        out << "FiniteActionSetAbstraction" << std::endl;
        for( auto const& p : ab.map_ ) {
            out << p.first << " => " << p.second << std::endl;
        }
        return out;
    }

    // -----------------------------------------------------------------------
    // ExpandableActionAbstraction concept?

    /**
     * @param fg :: ActionType -> AbstractType. Returns the action vertex to
     * which the chosen action should be mapped, updating the graph if necessary.
     */
    template<typename UpdateGraphFunction, typename ActionRange>
    std::tuple<ActionType, AbstractType> addAction(
        SamplingContext& ctx, UpdateGraphFunction fg, ActionRange const& actions )
    {
        // FIXME: hard-coded uniform choice
        stats::ReservoirSampler<ActionType> sampler;
        for( ActionType a : actions ) {
            sampler( ctx, a );
        }
        ActionType const choice = *std::begin(sampler.samples());
        AbstractType const y = fg();
        // Expect 'choice' is not currently in map_
        BOOST_VERIFY(( map_.emplace( choice, y ).second ));
        return std::make_tuple(choice, y);
    }

    template<typename MapType>
    void relabel( MapType const& relabeling )
    {
        for( auto& p : map_ ) {
            auto itr = relabeling.find( p.second );
            if( itr != relabeling.end() ) {
                p.second = itr->second;
            }
        }
    }

private:
    SuccessorMap map_;
};


}}


#endif
