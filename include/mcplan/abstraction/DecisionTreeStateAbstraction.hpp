#ifndef MCPLAN_ABSTRACTION_DECISIONTREESTATEABSTRACTION_HPP
#define MCPLAN_ABSTRACTION_DECISIONTREESTATEABSTRACTION_HPP

#include <memory>
#include <stack>
#include <vector>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/range/adaptor/sliced.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/assert.hpp>
#include "boost/throw_exception.hpp"
#include "boost/variant.hpp"

#include "mcplan/node/NodeData.hpp"
#include "mcplan/stats/ArgmaxAccumulator.hpp"
#include "mcplan/stats/Sampling.hpp"
#include "mcplan/algorithm/TreeTraversal.hpp"

#include "mcplan/Log.hpp"
#include "mcplan/SamplingContext.hpp"


namespace mcplan { namespace abstraction {


/**
 * @brief Binary decision tree type.
 * @tparam FeaturesType The feature vector type
 * @tparam LabelType The label type
 */
template<typename FeaturesType, typename LabelType, typename BinarySplit>
class DecisionTreeNode
{
private:
    struct Split
    {
        BinarySplit f;
        std::unique_ptr<DecisionTreeNode> left;
        std::unique_ptr<DecisionTreeNode> right;

        explicit Split( BinarySplit f, LabelType true_label, LabelType false_label )
            : f( f ), left( new DecisionTreeNode( true_label ) ),
              right( new DecisionTreeNode( false_label ) )
        { }

        Split( Split const& that )
            : f( that.f ), left( new DecisionTreeNode( *that.left ) ),
              right( new DecisionTreeNode( *that.right ) )
        { }

        Split& operator =( Split const& that )
        {
            if( this != &that ) {
                f = that.f;
                left.reset( new DecisionTreeNode( *that.left ) );
                right.reset( new DecisionTreeNode( *that.right ) );
            }
            return *this;
        }

        // (Default move)

        DecisionTreeNode* successor( FeaturesType const& s )
        { return (f(s) ? left.get() : right.get()); }

        DecisionTreeNode const* successor( FeaturesType const& s ) const
        { return (f(s) ? left.get() : right.get()); }
    };

    struct Printer : public boost::static_visitor<void>
    {
        std::ostream& out;
        Printer( std::ostream& out )
            : out( out )
        { }

        void operator ()( LabelType const& label )
        { out << "[= " << label << "]"; }

        void operator ()( Split const& split )
        { out << split.f; }
    };

public:
    explicit DecisionTreeNode( LabelType const label )
        : impl_( label )
    { }

    // (Default copy and move)

    bool isLeaf() const
    { return impl_.which() == 0; }

    LabelType& label()
    { return boost::get<LabelType>(impl_); }

    LabelType label() const
    { return boost::get<LabelType>(impl_); }

    DecisionTreeNode* successor( FeaturesType const& s )
    { return boost::get<Split>(impl_).successor( s ); }

    DecisionTreeNode const* successor( FeaturesType const& s ) const
    { return boost::get<Split>(impl_).successor( s ); }

    template<typename F>
    void visitSuccessors( F f )
    {
        if( isLeaf() ) {
            return;
        }
        f( *boost::get<Split>(impl_).left );
        f( *boost::get<Split>(impl_).right );
    }

    template<typename F>
    void visitSuccessors( F f ) const
    {
        if( isLeaf() ) {
            return;
        }
        f( *boost::get<Split>(impl_).left );
        f( *boost::get<Split>(impl_).right );
    }

    void split( BinarySplit f, LabelType true_label, LabelType false_label )
    {
        MCPLAN_LOG_TRACE(( std::cout << "DtNode: assigning to impl_" << std::endl ));
        impl_ = Split( f, true_label, false_label );
    }

    friend std::ostream& operator <<( std::ostream& out, DecisionTreeNode const& n )
    {
        Printer printer( out );
        boost::apply_visitor( printer, n.impl_ );
        return out;
    }

private:
    boost::variant<LabelType, Split> impl_;
};

template<typename DtNode>
struct DecisionTreePrinter
{
    std::ostream& out;
    int depth;

    DecisionTreePrinter( std::ostream& out )
        : out( out ), depth( 0 )
    { }

    bool enterNode( DtNode const& n )
    {
        for( int d = 0; d < depth; ++d ) {
            out << "-";
        }
        out << n << std::endl;
        depth += 1;
        return true;
    }

    void exitNode( DtNode const& /*n*/ )
    {
        depth -= 1;
    }
};

class FeatureValueSplit
{
public:
    explicit FeatureValueSplit( std::size_t const feature, double const value )
        : feature_( feature ), value_( value )
    { }

    template<typename FeatureVector>
    bool operator ()( FeatureVector const& phi ) const
    {
        return phi[feature_] < value_;
    }

    friend std::ostream& operator <<( std::ostream& out, FeatureValueSplit const& split )
    {
        out << "[" << split.feature_ << " < " << split.value_ << "]";
        return out;
    }

private:
    std::size_t feature_;
    double value_;
};

// ---------------------------------------------------------------------------

template<typename Domain, typename Field = mcplan::action_data::tag::value>
struct AStarIrrelevanceSplitEvaluator
{
    // 'asn' could be replaced by a list of actions
    template<typename AbstractStateNode, typename GroundStateNodeRange>
    double operator ()( AbstractStateNode const* asn,
                        GroundStateNodeRange a, GroundStateNodeRange b )
    {
        namespace ba        = boost::accumulators;
        using R             = typename Domain::RewardType;
        using Statistics    = ba::accumulator_set<R, ba::stats<ba::tag::mean>>;

        // Construct an indexing scheme for the abstract actions
        MCPLAN_LOG_TRACE(( std::cout << "AStarIrrelevanceSplitEvaluator: indexing actions" << std::endl ));
        std::map<typename Domain::ActionType, std::size_t> actions;
        asn->visitSuccessors( [&actions]( auto const& aan ) {
            actions.insert( {aan.a(), actions.size()} );
        });

        // Compute the mean value of each action over the ground action nodes
        // belonging to the two partitions. Look up the action index in the
        // 'actions' map because the order of the GANs is not specified and
        // there could be GANs that are not part of the abstract tree.
        MCPLAN_LOG_TRACE(( std::cout << "AStarIrrelevanceSplitEvaluator: computing action means" << std::endl ));
        std::vector<Statistics> Fa( actions.size() );
        std::vector<Statistics> Fb( actions.size() );
        for( auto const* gsn : a ) {
            gsn->visitSuccessors( [&Fa, &actions]( auto const& gan ) {
                auto const itr = actions.find( gan.a() );
                if( itr != actions.end() ) {
                    Fa[itr->second]( mcplan::get<Field>(gan) );
                }
            });
        }
        for( auto const* gsn : b ) {
            gsn->visitSuccessors( [&Fb, &actions]( auto const& gan ) {
                auto const itr = actions.find( gan.a() );
                if( itr != actions.end() ) {
                    Fb[itr->second]( mcplan::get<Field>(gan) );
                }
            });
        }

        // Extract mean values
        MCPLAN_LOG_TRACE(( std::cout << "AStarIrrelevanceSplitEvaluator: extracting mean values" << std::endl ));
        namespace range = boost::adaptors;
        auto arange = range::transform( Fa, []( auto const& acc ) {
            return boost::accumulators::mean(acc); } );
        auto brange = range::transform( Fb, []( auto const& acc ) {
            return boost::accumulators::mean(acc); } );
        std::vector<R> amean( arange.begin(), arange.end() );
        std::vector<R> bmean( brange.begin(), brange.end() );

        // A* irrelevance heuristic
        MCPLAN_LOG_TRACE(( std::cout << "AStarIrrelevanceSplitEvaluator: computing heuristic" << std::endl ));
        std::size_t const amax = std::distance(
            amean.begin(), std::max_element( amean.begin(), amean.end() ) );
        std::size_t const bmax = std::distance(
            bmean.begin(), std::max_element( bmean.begin(), bmean.end() ) );
        R const da = std::abs( amean[amax] - bmean[amax] );
        R const db = std::abs( amean[bmax] - bmean[bmax] );
        return da + db;
    }
};

// ---------------------------------------------------------------------------

template<typename Domain, typename AbstractType, typename SplitEvaluator>
class DecisionTreeStateAbstraction
{
    using GroundType        = typename Domain::StateType;
    using Features          = typename Domain::FeaturesType;

    template<typename AbstractStateNode>
    using NodeFeatureTuple  = std::tuple<
        typename AbstractStateNode::GroundNodeType const*, Features>;

    // TODO: Make split representation a parameter
    using DtNode = DecisionTreeNode<Features, AbstractType, FeatureValueSplit>;

public:

    // -----------------------------------------------------------------------
    // StateAbstraction concept

    explicit DecisionTreeStateAbstraction( Domain const* domain, AbstractType const x0 )
        : domain_( domain ), root_( x0 ), evaluate_split_()
    { }

    // (Default copy and move)

    /**
     * @brief operator () Maps ground states to abstract states.
     * @param g Ground state
     * @return Abstract state
     */
    AbstractType operator ()( GroundType const& g ) const
    {
        Features const phi = domain_->features( g );
        DtNode const* dn = &root_;
        while( !dn->isLeaf() ) {
            dn = dn->successor( phi );
        }
        return dn->label();
    }

    // TODO: Better to return a range and construct it lazily?
    /**
     * @brief abstractVertices The image of the abstraction function.
     * @return unspecified-sequence-type of AbstractType
     */
    std::vector<AbstractType> const& abstractVertices() const
    {
        std::vector<AbstractType> v;
        std::stack<DtNode*> s;
        s.push( &root_ );
        while( !s.empty() ) {
            DtNode* dn = s.top();
            s.pop();
            if( dn->isLeaf() ) {
                v.push_back( dn->label() );
            }
            else {
                dn->visitSuccessors( [&]( DtNode& succ ) {
                    s.push( &succ );
                });
            }
        }
        return v;
    }

    // -----------------------------------------------------------------------
    // RefineableStateAbstraction concept

    // 'asn' is only needed to pass to chooseSplit()
    /**
     * @brief Refine the abstraction by splitting abstract state 'x'. One of
     * 'x1' or 'x2' may be equal to 'x'.
     * @param ctx
     * @param x Label to split
     * @param x1 Label for one half of the new split
     * @param x2 Label for the other half of the new split
     * @param asn Data to base the decision on
     */
    template<typename AbstractStateNode>
    void refine( SamplingContext& ctx, AbstractType const& x,
                 AbstractType x1, AbstractType x2, AbstractStateNode const* asn )
    {
        MCPLAN_LOG_TRACE(( std::cout << "DtStateAbstraction.refine( "       \
                           << x << ", " << x1 << ", " << x2 << std::endl    \
                           << *asn << " )" << std::endl ));

        // FIXME: We should not assume that groundStateNodes() has a .size() function
        BOOST_ASSERT( x1 != x2 );
        BOOST_ASSERT( asn->groundStateNodes().size() > 1 );
        BOOST_ASSERT( asn->label() == x );
        DtNode* dn = find( x );
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
        if( !dn ) {
            MCPLAN_LOG_DEBUG(( std::cout << "DtStateAbstraction: ERROR: abstract type '" \
                               << x << "' not in decision tree" << std::endl ));
            mcplan::depth_first_traversal( root_, DecisionTreePrinter<DtNode>(std::cout) );
        }
#endif
        assert( dn );

        std::tuple<std::size_t, double> const split = chooseSplit( ctx, asn );
        auto f = FeatureValueSplit( std::get<0>(split), std::get<1>(split) );
        MCPLAN_LOG_TRACE(( std::cout << "DtStateAbtsraction: split = " << f << std::endl ));
        MCPLAN_LOG_TRACE(( std::cout << "DtStateAbstraction: dn->split()" << std::endl ));
        dn->split( f, x1, x2 );
    }

    template<typename MapType>
    void relabel( MapType const& relabeling )
    {
        std::stack<DtNode*> s;
        s.push( &root_ );
        while( !s.empty() ) {
            DtNode* dn = s.top();
            s.pop();
            if( dn->isLeaf() ) {
                auto itr = relabeling.find( dn->label() );
                if( itr != relabeling.end() ) {
                    dn->label() = itr->second;
                }
            }
            else {
                dn->visitSuccessors( [&]( DtNode& succ ) {
                    s.push( &succ );
                });
            }
        }
    }

public:
    friend std::ostream& operator <<( std::ostream& out, DecisionTreeStateAbstraction const& ab )
    {
        out << "DecisionTreeStateAbstraction" << std::endl;
        mcplan::depth_first_traversal( ab.root_, DecisionTreePrinter<DtNode>(out) );
        return out;
    }

private:
    // (split feature, split value)
    // 'asn' could be replaced by a list of 'gsn' and a list of actions (to pass
    // to the evaluate function)
    /**
     * @brief Select a (feature, value) split to refine the decision tree.
     * @param ctx
     * @param asn
     */
    template<typename AbstractStateNode>
    std::tuple<std::size_t, double> chooseSplit( SamplingContext& ctx,
                                                 AbstractStateNode const* asn )
    {
        using GroundStateNode = typename AbstractStateNode::GroundNodeType;

        MCPLAN_LOG_DEBUG(( std::cout << "DtStateAbstraction: chooseSplit()" << std::endl \
                                     << *asn << std::endl ));

        MCPLAN_LOG_TRACE(( std::cout << "DtStateAbstraction: computing features" << std::endl ));
        std::vector<NodeFeatureTuple<AbstractStateNode>> phi;
        for( auto const* gsn : asn->groundStateNodes() ) {
            phi.push_back( std::make_tuple(gsn, domain_->features( gsn->s() )) );
        }

        using Split = std::tuple<std::size_t, double>;
        stats::ArgmaxAccumulator<Split> argmax;
        for( std::size_t i = 0; i < domain_->Nfeatures(); ++i ) {
            MCPLAN_LOG_TRACE(( std::cout << "DtStateAbstraction: eval feature " << i << std::endl ));
            // Sort by feature 'i'
            std::sort( phi.begin(), phi.end(), [i]( auto const& a, auto const& b ) {
                return std::get<1>(a)[i] < std::get<1>(b)[i];
            });

            // Test all consecutive pairs for split values
            MCPLAN_LOG_TRACE(( std::cout << "DtStateAbstraction: testing splits " << std::endl ));
            for( std::size_t j = 1; j < phi.size(); ++j ) {
                Features const& f0 = std::get<1>(phi[j-1]);
                Features const& f1 = std::get<1>(phi[j]);
                if( f1[i] > f0[i] ) {
                    // Possible split value
                    double const split_value = (f0[i] + f1[i]) / 2.0;
                    MCPLAN_LOG_TRACE(( std::cout << "DtStateAbstraction: testing split at " \
                                                 << split_value << std::endl ));
                    namespace ba = boost::adaptors;
                    auto ordered_gsns = ba::transform(
                        phi, [](auto const& t) -> GroundStateNode const* { return std::get<0>(t); } );
//                    auto a = ba::slice( ordered_gsns, 0, j );
//                    auto b = ba::slice( ordered_gsns, j, phi.size() );
                    auto a = boost::make_iterator_range(
                        std::begin(ordered_gsns), std::next(std::begin(ordered_gsns), j) );
                    auto b = boost::make_iterator_range(
                        std::next(std::begin(ordered_gsns), j), std::end(ordered_gsns) );
                    double const score = evaluate_split_( asn, a, b );    
                    argmax( Split(i, split_value), score );
                }
            }
        }
        return stats::uniform_choice( ctx, argmax.max_range() );
    }

    /**
     * @brief find Get the (leaf) DtNode associated with 'label'.
     * @param label
     * @return
     */
    DtNode* find( AbstractType const label )
    {
        std::stack<DtNode*> s;
        s.push( &root_ );
        while( !s.empty() ) {
            DtNode* dn = s.top();
            s.pop();
            if( dn->isLeaf() ) {
                if( dn->label() == label ) {
                    return dn;
                }
            }
            else {
                dn->visitSuccessors( [&]( DtNode& succ ) {
                    s.push( &succ );
                });
            }
        }
        return nullptr;
    }

private:
    Domain const* domain_;
    DtNode root_;
    SplitEvaluator evaluate_split_;
};


}}


#endif
