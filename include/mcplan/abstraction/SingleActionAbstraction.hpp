#ifndef SINGLE_ACTION_ABSTRACTION_HPP__
#define SINGLE_ACTION_ABSTRACTION_HPP__

#include "mcplan/abstraction/ActionAbstraction.hpp"
namespace mcplan { namespace abstraction {

template<typename Diagram>
class SingleActionAbstraction : public ActionAbstraction<Diagram>
{

    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::ActionType;
public:
    SingleActionAbstraction(GroundType g)
    {
        g_=g;
    }



    void setLeaves(std::vector<AbstractType> &leaves) override
    {
        leaves_=leaves;
    }
    boost::optional<AbstractType> apply( GroundType const&  ) const override
    {

        return ( leaves_[0]);
    }
    GroundType sampleAction( mcplan::SamplingContext& /*ctx*/, AbstractType const&  ) const override
    {

        return g_;
    }


    private:
        GroundType g_;
        std::vector<AbstractType> leaves_;
};
}}
#endif
