#ifndef ACTION_ABSTRACTION_HPP__
#define ACTION_ABSTRACTION_HPP__

#include <boost/optional.hpp>

namespace mcplan{ namespace abstraction {

template<typename Diagram>
class ActionAbstraction
{
public:
    using AbstractType  = typename Diagram::Vertex;
    using GroundType    = typename Diagram::ActionType;

    virtual ~ActionAbstraction() { }
    virtual boost::optional<AbstractType> apply( GroundType const& g ) const = 0;
    virtual GroundType sampleAction(SamplingContext& ctx, AbstractType const & y)const = 0;
    virtual void setLeaves (std::vector<AbstractType>& leaves)=0;
    virtual void addAction(AbstractType const& y, GroundType const g) =0;
};



}}

#endif
