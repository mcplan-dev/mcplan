#ifndef MCPLAN_ABSTRACTION_TRANSITIONSIMULATORADAPTER_HPP__
#define MCPLAN_ABSTRACTION_TRANSITIONSIMULATORADAPTER_HPP__

#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"

namespace mcplan { namespace abstraction {


template<typename Domain>
class TransitionSimulatorAdapter : public sim::TransitionSimulator<AbstractDomain<Domain>>
{
public:
    using GroundStateType = typename Domain::StateType;
    using GroundActionType = typename Domain::ActionType;

    TransitionSimulatorAdapter( TransitionSimulator<Domain>& sim, AbstractionDiagram<Domain>& g )
        : sim_( sim ), g_( g )
    { }

    TransitionSample sampleTransition(
        SamplingContext& ctx, StateType const& s, ActionType const& a ) const override
    {

    }

private:
    AbstractDomain<Domain> dom_;
    TransitionSimulator<Domain>& sim_;
    AbstractionDiagram<Domain>& g_;

};


} }

#endif
