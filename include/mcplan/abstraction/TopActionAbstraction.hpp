#ifndef TOPACTION_ABSTRACTION_HPP__
#define TOPACTION_ABSTRACTION_HPP__

#include "mcplan/abstraction/ActionAbstraction.hpp"

namespace mcplan { namespace  abstraction {


template<typename Diagram>
class TopActionAbstraction : public ActionAbstraction<Diagram>
{


    public:
        using AbstractType  = typename Diagram::Vertex;
        using GroundType    = typename Diagram::ActionType;
        TopActionAbstraction(unsigned nactions)
        {
            nactions_=nactions;
        }
        void setLeaves(std::vector<AbstractType>& leaves)  override
        {
            leaves_=leaves;
        }

        boost::optional<AbstractType> apply( GroundType const&  ) const override
        {
            return leaves_[0];
        }
        GroundType sampleAction( mcplan::SamplingContext& ctx, AbstractType const&  ) const override
        {


            std::uniform_int_distribution<> p( 0, nactions_ );
            int i = p(ctx.rng());
            return i;

        }
        void addAction(AbstractType const& y,GroundType const g) override
        {}
    private :
        std::vector<AbstractType> leaves_;
        unsigned nactions_;
};
}}

#endif
