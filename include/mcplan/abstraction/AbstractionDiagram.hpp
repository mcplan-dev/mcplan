#ifndef MCPLAN_ABSTRACTION_ABSTRACTIONDIAGRAM_HPP
#define MCPLAN_ABSTRACTION_ABSTRACTIONDIAGRAM_HPP


// FIXME: Debugging
#include <iostream>
#include <typeinfo>

#include <memory>
#include <ostream>
#include <stack>

#include <boost/container/flat_set.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/variant.hpp>

#include "mcplan/Log.hpp"
#include "mcplan/SamplingContext.hpp"


#define TOP_STATE_BOTTOM_ACTION_ABSTRACTION 1
#define TOP_STATE_TOP_ACTION_ABSTRACTION 2
#define TOP_STATE_SINGLE_ACTION_ABSTRACTION 3
#define TOP_STATE_DEFAULT_ACTION_ABSTRACTION 4

#define TOP_STATE_COUNT_ACTION_ABSTRACTION 5




namespace mcplan { namespace abstraction {


// FIXME: We're going to use this typedef to figure out the type of the graph
// vertices, because StateAbstraction needs to know that. I'm guessing that
// vertex_descriptor is *not* required to be the same for different vertex and
// edge property types.
template<typename VertexType = boost::no_property, typename EdgeType = boost::no_property>
using AbstractionDiagramGraph = boost::adjacency_list<
    boost::vecS, boost::vecS, boost::bidirectionalS, VertexType, EdgeType>;
using AbstractionDiagramVertex = boost::graph_traits<AbstractionDiagramGraph<>>::vertex_descriptor;

template<typename Domain, typename AbstractionDiagramT>
class AbstractDomain;

/**
 * @brief Implements an abstraction diagram with configurable per-node
 * abstractions.
 * @tparam DomainT
 * @tparam StateAbstractionT
 * @tparam ActionAbstractionT
 */
template<typename DomainT, typename StateAbstractionT, typename ActionAbstractionT>
class AbstractionDiagram
{
public:
    using Domain = DomainT;
    using StateAbstraction = StateAbstractionT;
    using ActionAbstraction = ActionAbstractionT;
    using ThisType = AbstractionDiagram<Domain, StateAbstraction, ActionAbstraction>;
    using StateType = typename Domain::StateType;
    using ActionType = typename Domain::ActionType;

    // -----------------------------------------------------------------------
    // Graph types

    struct StateVertex
    {
        // Note: I would prefer for this to be a unique_ptr, but it seems that
        // Boost.Graph requires bundled properties to be Copyable
        std::shared_ptr<ActionAbstraction> action_abstraction;

        /**
         * @brief Makes a deep copy of this.
         * @return
         */
        StateVertex clone() const
        {
            // State vertices can have null abstractions if they are leafs
            StateVertex cp = action_abstraction ?
                             StateVertex { std::make_shared<ActionAbstraction>( *action_abstraction ) } :
                             StateVertex { std::shared_ptr<ActionAbstraction>() };
            return cp;
        }
    };

    struct ActionVertex
    {
        // Note: I would prefer for this to be a unique_ptr, but it seems that
        // Boost.Graph requires bundled properties to be Copyable
        std::shared_ptr<StateAbstraction> state_abstraction;

        /**
         * @brief Makes a deep copy of this.
         * @return
         */
        ActionVertex clone() const
        {
            ActionVertex cp {
                std::make_shared<StateAbstraction>( *state_abstraction )
            };
            return cp;
        }
    };

    using VertexType = boost::variant<StateVertex, ActionVertex>;
    using EdgeType  = boost::no_property;
    using Graph     = AbstractionDiagramGraph<VertexType, EdgeType>;
    using Vertex    = typename boost::graph_traits<Graph>::vertex_descriptor;
    using Edge      = typename boost::graph_traits<Graph>::edge_descriptor;
    // See note on AbstractionDiagramGraph above
    static_assert( std::is_same<Vertex, AbstractionDiagramVertex>::value,
                   "Failed to determine type of vertex_descriptor" );

    using adjacency_iterator = typename boost::graph_traits<Graph>::adjacency_iterator;
    using inv_adjacency_iterator = typename Graph::inv_adjacency_iterator;

private:
    // -----------------------------------------------------------------------

    struct PrintVertex : boost::static_visitor<void>
    {
        std::ostream& out;

        PrintVertex( std::ostream& out )
            : out( out )
        { }

        void operator ()( StateVertex const& sv )
        {
            if( sv.action_abstraction ) {
                out << *sv.action_abstraction;
            }
            else {
                out << "(leaf)";
            }
        }

        void operator ()( ActionVertex const& av )
        {
            BOOST_ASSERT( av.state_abstraction );
            out << *av.state_abstraction;
        }
    };

    // -----------------------------------------------------------------------

    struct clone_vertex_visitor : public boost::static_visitor<VertexType>
    {
        VertexType operator ()( StateVertex const& x ) const
        { return x.clone(); }

        VertexType operator ()( ActionVertex const& y ) const
        { return y.clone(); }
    };

    VertexType cloneVertex( VertexType const& v ) const
    {
        return boost::apply_visitor( clone_vertex_visitor(), v );
    }

    // -----------------------------------------------------------------------

    template<typename MapType>
    struct relabel_successors_visitor : public boost::static_visitor<void>
    {
        MapType const& relabeling;

        relabel_successors_visitor( MapType const& relabeling )
            : relabeling( relabeling )
        { }

        void operator ()( StateVertex const& x ) const
        { return x.action_abstraction->relabel( relabeling ); }

        void operator ()( ActionVertex const& y ) const
        { return y.state_abstraction->relabel( relabeling ); }
    };

    template<typename MapType>
    void relabelSuccessors( Vertex const v, MapType const& relabeling ) const
    {
        boost::apply_visitor( relabel_successors_visitor<MapType>( relabeling ), g_[v] );
    }

    // -----------------------------------------------------------------------

    /**
     * @brief If 'x' has no parents, removes 'x' and all vertices that are only
     * reachable from 'x'.
     * @param x
     */
    void pruneGraph( Vertex const x )
    {
        MCPLAN_LOG_DEBUG(( std::cout << "ad: pruneGraph( " << x << " )" << std::endl ));
        auto const xpred = boost::inv_adjacent_vertices( x, g_ );
        if( std::distance( xpred.first, xpred.second ) == 0 ) {
            // x is orphan
            MCPLAN_LOG_DEBUG(( std::cout << "ad: 'x' is an orphan" << std::endl ));
            std::vector<Vertex> orphans;
            orphans.push_back( x );
            // dfs descendants of 'x'
            std::stack<Vertex> s;
            s.push( x );
            while( !s.empty() ) {
                Vertex const v = s.top();
                s.pop();

                MCPLAN_LOG_DEBUG(( std::cout << "ad: examining " << v << std::endl ));
                for( auto succ = boost::adjacent_vertices( v, g_ );
                     succ.first != succ.second; ++succ.first )
                {
                    Vertex const w = *succ.first;
                    MCPLAN_LOG_DEBUG(( std::cout << "ad: examining " << v << " -> " << w << std::endl ));
                    auto pred = boost::inv_adjacent_vertices( w, g_ );
                    if( std::distance( pred.first, pred.second ) == 1 ) {
                        // w has only 1 parent, which will be removed
                        // when 'x' is removed -> w is an orphan
                        MCPLAN_LOG_DEBUG(( std::cout << "ad: 'w' is an orphan" << std::endl ));
                        orphans.push_back( w );
                        s.push( w );
                    }
                }
            }
            // Remove orphans and their edges
            for( Vertex const v : orphans ) {
                MCPLAN_LOG_DEBUG(( std::cout << "ad: clear " << v << std::endl ));
                boost::clear_vertex( v, g_ );
                MCPLAN_LOG_DEBUG(( std::cout << "ad: remove " << v << std::endl ));
                boost::remove_vertex( v, g_ );
            }
        }
    }

    // -----------------------------------------------------------------------

    struct StateVertexSelector : public boost::static_visitor<bool>
    {
        bool operator ()( StateVertex const& /*x*/ )
        { return true; }

        bool operator ()( ActionVertex const& /*y*/ )
        { return false; }
    };

    struct ActionVertexSelector : public boost::static_visitor<bool>
    {
        bool operator ()( StateVertex const& /*x*/ )
        { return false; }

        bool operator ()( ActionVertex const& /*y*/ )
        { return true; }
    };

    template<typename Vertex, typename VertexSelector>
    class dfs_vertex_visitor : public boost::default_dfs_visitor
    {
    public:
        dfs_vertex_visitor( std::vector<Vertex>* v )
            : v_( v )
        { }

        template<typename Graph>
        void discover_vertex( Vertex u, Graph const& g ) const
        {
            if( boost::apply_visitor( VertexSelector(), g[u] ) ) {
                v_->push_back( u );
            }
        }

    private:
        std::vector<Vertex>* v_;
    };

    // -----------------------------------------------------------------------

public:
    // -----------------------------------------------------------------------
    // AbstractionConcept

    using AliasAbstractDomain =  AbstractDomain<Domain, ThisType>;
    using AbstractActionLabel = Vertex;
    using AbstractStateLabel = Vertex;

    // -----------------------------------------------------------------------
    // RefineableAbstractionConcept

    using AbstractionInitializer = std::function<AbstractActionLabel(Graph&)>;

    // -----------------------------------------------------------------------

    AbstractionDiagram() = delete;
    AbstractionDiagram( AbstractionDiagram const& ) = delete;
    AbstractionDiagram( AbstractionDiagram&& ) = delete;

    /**
     * @brief AbstractionDiagram
     * @param domain
     * @param g
     * @param start_vertex Must be an action vertex
     */
    AbstractionDiagram( Domain const* domain, Graph&& g, Vertex start_vertex )
        : domain_( domain ), g_( std::move(g) ), start_vertex_( start_vertex )
    {
        terminal_vertex_ = boost::add_vertex( g_ );
    }

    struct Initialize_Tree_TopState_RepeatAction
    {
        Domain const* domain;
        int deltaT;
        int max_depth;
        int start_depth;

        /**
         * @brief Initializes a tree-structured abstraction with fixed-length
         * macro actions.
         * @param domain
         * @param deltaT Length of macro actions
         * @param max_depth Max depth of abstraction tree
         * @param start_depth Can be set to a value > 0 to initialize a subtree
         * of the abstraction.
         */
        Initialize_Tree_TopState_RepeatAction(
            Domain const* domain, int const deltaT, int const max_depth, int const start_depth = 0 )
            : domain( domain ), deltaT( deltaT ), max_depth( max_depth ), start_depth( start_depth )
        {
            BOOST_ASSERT( start_depth <= max_depth );
        }

        Vertex operator()( Graph& g ) const
        {
            Vertex const y0 = boost::add_vertex( g );
            g[y0] = ActionVertex();
            Vertex const x0 = boost::add_vertex( g );
            g[x0] = StateVertex();

            boost::add_edge( y0, x0, g );
            boost::get<ActionVertex>(g[y0]).state_abstraction
                = std::make_shared<StateAbstraction>( domain, x0 );

            // Depth-first construction of AD tree
            std::stack<std::tuple<Vertex, int, boost::optional<ActionType>>> s;
            s.push( std::make_tuple(x0, start_depth, boost::none) );
            while( !s.empty() ) {
                auto const t = s.top();
                s.pop();
                Vertex const x          = std::get<0>(t);
                int const d             = std::get<1>(t);
                auto const maybe_aprev  = std::get<2>(t);

                if( d == max_depth ) {
                    // Leaf
                    boost::get<StateVertex>(g[x]).action_abstraction.reset();
                    continue;
                }

                std::vector<std::pair<ActionType, Vertex>> ay;
                if( d % deltaT == 0 ) {
                    // Every action
                    for( std::size_t i = 0; i < domain->max_actions(); ++i ) {
                        Vertex const y = boost::add_vertex( g );
                        g[y] = ActionVertex();
                        ActionType a( static_cast<int>(i) );
    //                    A->addAction( y, a );
                        ay.push_back( {a, y} );
                        boost::add_edge( x, y, g );

                        Vertex const xprime = boost::add_vertex( g );
                        g[xprime] = StateVertex();
                        boost::get<ActionVertex>(g[y]).state_abstraction
                            = std::make_shared<StateAbstraction>( domain, xprime );
                        boost::add_edge( y, xprime, g );

                        s.push( std::make_tuple(xprime, d+1, a) );
                    }
                }
                else {
                    // Only default action
                    Vertex const y = boost::add_vertex( g );
                    g[y] = ActionVertex();
                    auto maybe_default_action = Domain::DefaultAction();
                    assert( static_cast<bool>(maybe_default_action) || static_cast<bool>(maybe_aprev) );
                    ActionType const a( maybe_default_action ? *maybe_default_action : *maybe_aprev );
    //                A->addAction( y, a );
                    ay.push_back( {a, y} );
                    boost::add_edge( x, y, g );

                    Vertex const xprime = boost::add_vertex( g );
                    g[xprime] = StateVertex();
                    boost::get<ActionVertex>(g[y]).state_abstraction
                        = std::make_shared<StateAbstraction>( domain, xprime );
                    boost::add_edge( y, xprime, g );

                    s.push( std::make_tuple(xprime, d+1, a) );
                }
                boost::get<StateVertex>(g[x]).action_abstraction
                    = std::make_shared<ActionAbstraction>( ay.begin(), ay.end() );
            }

            return y0;
        }
    };

    /**
     * @brief Creates a tree-structued AD with state abstraction Top and
     * temporal coarsening of deltaT.
     *
     * If Domain::DefaultAction() != boost::none, then the abstract actions are
     *      a_t <default> ... <default> a_{t+deltaT},
     * otherwise the abstract actions are
     *      a_t a_t ... a_t a_{t+deltaT}
     * @param domain
     * @param deltaT
     * @param max_depth
     * @return
     */
    static std::unique_ptr<AbstractionDiagram<Domain, StateAbstraction, ActionAbstraction>>
    create( Domain const* domain, AbstractionInitializer f )
    {
        using Diagram = AbstractionDiagram<Domain, StateAbstraction, ActionAbstraction>;
        Graph g;
        Vertex const y0 = f( g );
        return std::make_unique<Diagram>( domain, std::move(g), y0 );
    }

    static std::unique_ptr<AbstractionDiagram<Domain, StateAbstraction, ActionAbstraction>>
    Tree_TopState_RepeatAction( Domain const* domain, int const deltaT, int const max_depth )
    {
        return create( domain, Initialize_Tree_TopState_RepeatAction( domain, deltaT, max_depth ) );
    }

    // -----------------------------------------------------------------------

    /**
     * @brief Replaces the state abstraction 'yparent -> x' with the refined
     * abstracton 'yparent -> {x1, x2}'.
     * @param ctx
     * @param yparent Action vertex
     * @param x Label of abstract set to refine
     * @param data Forwarded to abstraction implementation
     * @return std::vector<Vertex> The new state vertices created by this call
     */
    template<typename... RefinementData>
    std::vector<Vertex> splitStateVertex(
        SamplingContext& ctx, Vertex const& yparent, Vertex const& x, RefinementData&&... data )
    {
        MCPLAN_LOG_TRACE(( std::cout << "AbstractionDiagram.splitStateVertex()" << std::endl ));
        assertNotTerminal( x );
        // If 'yparent' is the only parent of 'x', we only need to create one
        // new label. Otherwise we create two.
        auto const xpred = boost::inv_adjacent_vertices( x, g_ );
        bool const reuse_x = (1 == std::distance( xpred.first, xpred.second ));

        // Create two new labels
        Vertex const x1 = (reuse_x ? x : boost::add_vertex(g_));
        Vertex const x2 = boost::add_vertex(g_);
        // Initialize them to copies of x
        // Replace x with {x1, x2} in transition graph
        if( !reuse_x ) {
            g_[x1] = boost::get<StateVertex>(g_[x]).clone();
            boost::add_edge( yparent, x1, g_ );
        }
        g_[x2] = boost::get<StateVertex>(g_[x]).clone();
        boost::add_edge( yparent, x2, g_ );
        for( Vertex const ysucc : successors( x ) ) {
            if( !reuse_x ) {
                boost::add_edge( x1, ysucc, g_ );
            }
            boost::add_edge( x2, ysucc, g_ );
        }
        if( !reuse_x ) {
            boost::remove_edge( yparent, x, g_ );
        }

        // Check for orphaned vertices
//        pruneGraph( x );

        // Choose the actual abstraction refinement
        MCPLAN_LOG_TRACE(( std::cout << "ad: calling state_abstraction->refine()" << std::endl ));
        boost::get<ActionVertex>(g_[yparent]).state_abstraction->refine(
            ctx, x, x1, x2, std::forward<RefinementData>(data)... );
        return {x1, x2};
    }

    /**
     * @brief Adds the edge '(x, a, y)' to the diagram, where 'a' is a new
     * action chosen by the action abstraction in 'x' according to 'data'.
     * @param f :: Graph -> ActionLabel. Returns the action
     * vertex to which the added action should be mapped, updating the graph
     * if necessary.
     */
    template<typename... RefinementData>
    std::tuple<ActionType, Vertex> addAction(
        SamplingContext& ctx, Vertex const& x, AbstractionInitializer f, RefinementData&&... data )
    {
        MCPLAN_LOG_TRACE(( std::cout << "AbstractionDiagram.addAction()" << std::endl ));
        assertNotTerminal( x );
        auto fg = [f, this]() { return f(g_); };
        std::tuple<ActionType, Vertex> t = boost::get<StateVertex>(g_[x]).action_abstraction->addAction(
            ctx, fg, std::forward<RefinementData>(data)... );
        Vertex const y = std::get<1>(t);
        boost::add_edge( x, y, g_ ); // Edge might exist but that's ok
        return t;
    }

    /**
     * @brief Endows a vertex with its own exclusive copies of each of its
     * successor vertices.
     * Each copy has the same successors as the original and its own copy of
     * the VertexType instance from the original.
     * @param u
     */
    void unzip( Vertex const u )
    {
        MCPLAN_LOG_TRACE(( std::cout << "ad: unzip( " << u << " )" << std::endl ));
        assertNotTerminal( u );
        // Notation: 'u -> v -> w' is a chain of three vertices
        auto succ_range = successors( u );
        // Need a copy so that we don't have to worry about invalidating iterators
        std::vector<Vertex> succs( succ_range.begin(), succ_range.end() );
        // Map from old successors of 'u' to new successors of 'u'
        std::map<Vertex, Vertex> relabeling;
        for( Vertex const& v : succs ) {
            MCPLAN_LOG_TRACE(( std::cout << "ad: successor " << v << std::endl ));
            if( boost::distance( predecessors( v ) ) == 1 ) {
                // No other parents -> no need to copy
                MCPLAN_LOG_TRACE(( std::cout << "ad: only one parent" << std::endl ));
                continue;
            }

            MCPLAN_LOG_TRACE(( std::cout << "ad: updating graph" << std::endl ));
            // New copy of 'v' in 'u -> v'
            Vertex const v_cp = boost::add_vertex(g_);
            relabeling[v] = v_cp;
            // For each 'v -> w' create 'v_cp -> w'
            for( Vertex const& w : successors( v ) ) {
                boost::add_edge(v_cp, w, g_);
            }
            // Switch 'u -> v' to 'u -> v_cp'
            boost::remove_edge(u, v, g_);
            boost::add_edge(u, v_cp, g_);

            // Copy vertex data (state abstraction or action set)
            MCPLAN_LOG_TRACE(( std::cout << "ad: cloneVertex()" << std::endl ));
            g_[v_cp] = cloneVertex( g_[v] );
            MCPLAN_LOG_TRACE(( std::cout << "ad: finished successor " << v << std::endl ));
        }
        // Update the successor labels of 'u'
        relabelSuccessors( u, relabeling );
    }

    // -----------------------------------------------------------------------
    // Accessors

    /**
     * @brief Returns the start node, which is an action node with no parents.
     * @return
     */
    Vertex start_node() const
    { return start_vertex_; }

    /**
     * @brief Returns the terminal node. The terminal node is implicitly a
     * successor of every action node, and StateType instances for which
     * .isTerminal() is true are mapped to terminal_node.
     * @return
     */
    Vertex terminal_node() const
    { return terminal_vertex_; }

    /**
     * @brief True iff 'v' is the terminal node.
     * @param v
     * @return
     */
    bool isTerminal( Vertex const v ) const
    { return v == terminal_vertex_; }

    /**
     * @brief isActionVertex
     * @param v
     * @return
     */
    bool isActionVertex( Vertex const v ) const
    { return boost::apply_visitor( ActionVertexSelector(), g_[v] ); }

    /**
     * @brief isStateVertex
     * @param v
     * @return
     */
    bool isStateVertex( Vertex const v ) const
    { return boost::apply_visitor( StateVertexSelector(), g_[v] ); }

    /**
     * @brief Maps a state to a successor vertex.
     * @param y
     * @param s
     * @return
     */
    Vertex successor( Vertex y, StateType const& s ) const
    {
        if( s.isTerminal( domain_->parameters() ) ) {
            return terminal_vertex_;
        }
        ActionVertex const& av = boost::get<ActionVertex>( g_[y] );
        return (*av.state_abstraction)( s );
    }

    /**
     * @brief Maps an action to a successor vertex, or returns boost::none
     * if the action is not allowed.
     * @param x
     * @param a
     * @return
     */
    boost::optional<Vertex> successor( Vertex x, ActionType const& a ) const
    {
        StateVertex const& sv = boost::get<StateVertex>( g_[x] );
        return (*sv.action_abstraction)( a );
    }

    boost::iterator_range<adjacency_iterator> successors( Vertex v ) const
    {
        if( isTerminal( v ) ) {
            return { };
        }
        auto vs = boost::adjacent_vertices( v, g_ );
        return boost::make_iterator_range( vs.first, vs.second );
    }

    boost::iterator_range<inv_adjacency_iterator> predecessors( Vertex v ) const
    {
        assertNotTerminal( v ); // Cannot be implemented without a domain model
        auto vs = boost::inv_adjacent_vertices( v, g_ );
        return boost::make_iterator_range( vs.first, vs.second );
    }

    // FIXME: Can this return a range?
    std::vector<Vertex> stateVertices() const
    {
        std::vector<Vertex> v;
        boost::depth_first_search( g_, boost::root_vertex(start_vertex_).
                                       visitor(dfs_vertex_visitor<Vertex, ActionVertexSelector>( &v )) );
        return v;
    }

    // FIXME: Can this return a range?
    std::vector<Vertex> actionVertices() const
    {
        std::vector<Vertex> v;
        boost::depth_first_search( g_, boost::root_vertex(start_vertex_).
                                       visitor(dfs_vertex_visitor<Vertex, ActionVertexSelector>( &v )) );
        return v;
    }

    // -----------------------------------------------------------------------

    friend std::ostream& operator <<( std::ostream& out, AbstractionDiagram const& ad )
    {
        out << "AbstractionDiagram : Graph" << std::endl;
        out << "start:    " << ad.start_node() << std::endl;
        out << "terminal: " << ad.terminal_node() << std::endl;
        for( auto p = boost::vertices(ad.g_); p.first != p.second; ++p.first ) {
            StateVertexSelector is_state;
            if( boost::apply_visitor( is_state, ad.g_[*p.first] ) ) {
                out << "X_";
            }
            else {
                out << "Y_";
            }
            out << *p.first << "\t:";
            for( auto adj = boost::adjacent_vertices(*p.first, ad.g_); adj.first != adj.second; ++adj.first ) {
                out << " ";
                if( boost::apply_visitor( is_state, ad.g_[*adj.first] ) ) {
                    out << "X_";
                }
                else {
                    out << "Y_";
                }
                out << *adj.first;
            }
            out << std::endl;
        }
        out << "AbstractionDiagram : Abstractions" << std::endl;
        for( auto p = boost::vertices(ad.g_); p.first != p.second; ++p.first ) {
            out << *p.first << "\t:";
            PrintVertex printer(out);
            boost::apply_visitor( printer, ad.g_[*p.first] );
            out << std::endl;
        }
        return out;
    }

private:
    inline void assertNotTerminal( Vertex const x ) const
    { BOOST_ASSERT( x != terminal_vertex_ ); }

private:
    Graph g_;
    Vertex start_vertex_;
    Vertex terminal_vertex_;
    Domain const* domain_;
};

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

template<typename Domain, typename AbstractionDiagramT>
class AbstractDomain
{
public:
    using ParametersType = typename Domain::ParametersType;
    using StateType     = typename AbstractionDiagramT::AbstractStateLabel;
    using ActionType    = typename AbstractionDiagramT::AbstractActionLabel;
    using RewardType    = typename Domain::RewardType;
    using ActionSet     = boost::container::flat_set<ActionType>;
    using GroundStateType = typename Domain::StateType;

    explicit AbstractDomain( AbstractionDiagramT const* g )
        : g_( g )
    { }

    ActionSet actions( StateType const& s ) const
    {
        auto adj = g_->successors( s );
        std::vector<ActionType> as( adj.begin(), adj.end() );
        std::sort( as.begin(), as.end() );
        return boost::container::flat_set<ActionType>(
            boost::container::ordered_unique_range_t(), as.begin(), as.end() );
    }

private:
    AbstractionDiagramT const* g_;
};


}}


#endif
