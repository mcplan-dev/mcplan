#ifndef MCPLAN_BANDIT_FINITEBANDIT_HPP__
#define MCPLAN_BANDIT_FINITEBANDIT_HPP__


#include "mcplan/bandit/Bandit.hpp"

#include "boost/range.hpp"
#include "boost/range/algorithm/copy.hpp"

#include <vector>

namespace mcplan { namespace bandit {


/**
 * Extends Bandit<T> for bandits that have a finite number of arms.
 */
template<typename T>
class FiniteBandit : public Bandit<T>
{
public:
    template<typename SinglePassReadableRange>
    FiniteBandit( SinglePassReadableRange const& arms, eval_t eval )
        : eval_( eval )
    {
        boost::copy( arms, std::back_inserter(arms_) );
        assert( !arms_.empty() );
    }

    /**
     * @brief Returns the index of the arm returned by best().
     * @return
     */
    virtual int bestIndex() const = 0;

    T const& best() const override { return arm( bestIndex() ); }

    /**
     * @brief Returns the arm with index i.
     * @param i
     * @return
     */
    T const& arm( int const i ) const { return arms_[i]; }

    /**
     * @brief Returns the number of arms.
     * @return
     */
    std::size_t size() const { return arms_.size(); }

protected:
    double eval( T const& t )
    {
        return eval_( t );
    }

private:
    std::vector<T> arms_;
    eval_t eval_;
};


}}


#endif
