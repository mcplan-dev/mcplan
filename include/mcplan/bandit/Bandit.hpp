#ifndef MCPLAN_BANDIT_BANDIT_HPP__
#define MCPLAN_BANDIT_BANDIT_HPP__


#include <cassert>
#include <functional>

namespace mcplan { namespace bandit {


/**
 * The most general interface for multi-armed bandits.
 */
template<typename T>
class Bandit
{
public:
    /**
     * @brief rng_t A function that yields random integers
     */
    typedef std::function<int()> rng_t;
    typedef std::function<double(T const&)> eval_t;

    /**
     * @brief Sample from the next arm.
     * @param rng
     */
    virtual void sample( rng_t rng ) = 0;

    /**
     * @brief Returns the current best arm.
     * @return
     */
    virtual T const& best() const = 0;

    /**
     * @brief Returns true if and only if the algorithm has converged to an
     * optimum in the (epsilon, delta) sense.
     * @param epsilon
     * @param delta
     * @return true if sampling has converged
     */
    inline
    bool converged( double const epsilon, double const delta ) const
    {
        assert( epsilon >= 0 && epsilon <= 1 );
        assert( delta >= 0 && delta <= 1 );
        if( delta == 0 || epsilon == 1 ) {
            return true;
        }
        else {
            return convergedImpl( epsilon, delta );
        }
    }

protected:
    virtual ~Bandit() { }

    /**
     * @brief Overridden by subclasses to provide their convergence test.
     * It is guaranteed that epsilon \in [0,1) and delta \in (0,1].
     * @param epsilon
     * @param delta
     * @return
     */
    virtual bool convergedImpl( double const epsilon, double const delta ) const = 0;
};


}}


#endif
