#ifndef MCPLAN_BANDIT_CYCLICFINITEBANDIT_HPP__
#define MCPLAN_BANDIT_CYCLICFINITEBANDIT_HPP__


#include "mcplan/bandit/FiniteBandit.hpp"

#include "boost/accumulators/accumulators.hpp"
#include "boost/accumulators/statistics/mean.hpp"

namespace mcplan { namespace bandit {


/**
 * A "degenerate" bandit for evaluating deterministic arms. It samples the
 * arms in order, and reports (0,1)-converged if every arm has been tried
 * at least once.
 */
template<typename T>
class CyclicFiniteBandit : public FiniteBandit<T>
{
public:
    template<typename SinglePassReadableRange>
    CyclicFiniteBandit( SinglePassReadableRange const& arms, eval_t eval )
        : FiniteBandit<T>( arms, eval )
        , all_( false ), next_( 0 ), istar_( 0 ), rstar_( std::numeric_limits<double>::min() )
    { }

    void sample( rng_t /*rng*/ ) override
    {
        namespace ba = boost::accumulators;

        int const i = next_++;
        if( i == stats_.size() ) {
            stats_.push_back( Statistics() );
        }

        Statistics& stats = stats_[i];
        T const& t = arm( i );
        double const rsample = eval( t );
        stats( rsample );
        if( ba::mean(stats) > rstar_ ) {
            rstar_ = ba::mean(stats);
            istar_ = i;
        }

        if( next_ == size() ) {
            next_ = 0;
            all_ = true;
        }
    }

    int bestIndex() const override
    {
        return istar_;
    }

    // FIXME: Debugging
    double value( int i ) const
    {
        return boost::accumulators::mean(stats_[i]);
    }

protected:
    bool convergedImpl( double const /*epsilon*/, double const /*delta*/ ) const override
    {
        return all_;
    }

private:
    bool all_;
    int next_;
    int istar_;
    double rstar_;

    using Statistics = boost::accumulators::accumulator_set<
        double, boost::accumulators::features<boost::accumulators::tag::mean>>;
    std::vector<Statistics> stats_;
};


}}


#endif
