#ifndef MCPLAN_BUDGET_HPP__
#define MCPLAN_BUDGET_HPP__


#include <limits>

// TODO: Debugging
#include <iostream>

namespace mcplan {

// ---------------------------------------------------------------------------

class Budget
{
public:
    using value_type = unsigned int;

    Budget()
        : expired_( false )
    { }

    virtual ~Budget() { }

    bool expired() const
    {
        // FIXME: Debugging
       // if( expired_ ) { std::cout << "Budget expired!" << std::endl; }
        return expired_;
    }

    void reset()
    {
        resetImpl();
        expired_ = false;
    }

    virtual void onSample() = 0;
    virtual value_type limit() const = 0;
    virtual value_type value() const = 0;

protected:
    virtual void resetImpl() = 0;

    void setExpired()
    { expired_ = true; }

private:
    bool expired_;
};

// ---------------------------------------------------------------------------

class InfiniteBudget : public Budget
{
public:
    value_type limit() const override
    {
        return std::numeric_limits<value_type>::max();
    }

    value_type value() const override
    {
        return value_type(0);
    }

    void onSample() override
    { }

protected:
    void resetImpl() override
    { }
};

// ---------------------------------------------------------------------------

class SampleBudget : public Budget
{
public:
    explicit SampleBudget( value_type limit )
        : samples_( 0 ), limit_( limit )
    { }

    value_type limit() const override
    {
        return limit_;
    }

    value_type value() const override
    {
        return samples_;
    }

    void onSample() override
    {
        samples_ += 1;
        if( samples_ >= limit_ ) {
            setExpired();

        }
    }

protected:
    void resetImpl() override
    {
        samples_ = 0;
    }

private:
    value_type samples_;
    value_type limit_;
};

// ---------------------------------------------------------------------------

}


#endif
