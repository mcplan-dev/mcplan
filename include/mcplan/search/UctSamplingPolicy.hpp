#ifndef MCPLAN_SEARCH_UCTSAMPLINGPOLICY_HPP__
#define MCPLAN_SEARCH_UCTSAMPLINGPOLICY_HPP__

#include "mcplan/node/NodeData.hpp"

#include "mcplan/Policy.hpp"
#include "mcplan/SamplingContext.hpp"

#include <boost/optional.hpp>

#include <cassert>
#include <cmath>
#include <random>

namespace mcplan {



// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

template<typename SearchDomain, typename StateNode>
class UctTreePolicy
{
public:
    using ActionNode = typename StateNode::SuccessorType;
    using StateType     = typename SearchDomain::StateType;
    using ActionType = boost::optional<typename SearchDomain::ActionType>;
    using RewardType    = typename SearchDomain::RewardType;

private:
    struct Selector;

public:

    UctTreePolicy( SearchDomain const& dom, StateNode const* sn0 )
        : dom_( dom ), sn0_( sn0 )
    { }

    RewardType c( StateNode const& /*sn*/, ActionNode const& /*an*/ ) const
    {
        return 1.0; // TODO: generalize
    }

    Selector operator()( StateNode const& sn ) const
    {
        return Selector( *this, sn );
    }

private:
    SearchDomain dom_;
    StateNode const* sn0_;
};

template<typename SearchDomain, typename StateNode>
struct UctTreePolicy<SearchDomain, StateNode>::Selector
{
    using ActionNode    = typename StateNode::SuccessorType;

    UctTreePolicy const& outer_;
    StateNode const& sn_;

    Selector( UctTreePolicy const& outer, StateNode const& sn )
        : outer_( outer ), sn_( sn )
    { }

    ActionType operator()( mcplan::SamplingContext& ctx )
    {
        using RewardType = typename StateNode::RewardType;

        if( mcplan::get<mcplan::state_data::tag::n>( sn_ ) == 0 ) {
            return boost::none;
        }

        // Set of actions that have no ActionNode successor yet
        auto as = outer_.dom_.actions( sn_.label() );
        if(!sn_.isLeaf()) {
            sn_.visitSuccessors( [&as]( auto const& an ) {
                as.erase( an.label() );
            });
        }

        if( !as.empty() ) {
            std::uniform_int_distribution<std::size_t> p( 0, as.size() - 1 );
            return *(as.begin() + p(ctx.rng()));
        }

        std::vector<ActionNode*> best;
        RewardType best_score = -std::numeric_limits<RewardType>::max(); // FIXME: not generic enough

        sn_.visitSuccessors( [this,&best_score,&best]( auto& an ) {
            RewardType const c      = outer_.c( sn_, an );
            RewardType const score  = mcplan::get<mcplan::action_data::tag::avg_return>( an )
                                    + c * (std::sqrt( std::log( mcplan::get<mcplan::state_data::tag::n>( sn_ ) )
                                           / mcplan::get<mcplan::action_data::tag::n>( an) ));

            if( score > best_score ) {
                best.clear();
                best_score = score;
            }
            if( score >= best_score ) {
                best.push_back(&an);
            }
        });
        std::uniform_int_distribution<std::size_t> p( 0, best.size() - 1 );
        std::size_t const istar = p(ctx.rng());
        assert( best[istar] );
        return best[istar]->label();
    }
};

}

#endif
