#ifndef MCPLAN_SEARCH_GREEDYPOLICY_HPP__
#define MCPLAN_SEARCH_GREEDYPOLICY_HPP__

//#include "mcplan/node/NodeData.hpp"
//#include "mcplan/SamplingContext.hpp"

#include <boost/optional.hpp>

#include <cassert>
#include <cmath>
#include <random>

namespace mcplan {

// FIXME: This should probably implement Policy
template<typename SearchDomain, typename StateNode>
class GreedyPolicy
{
public:
    using ActionNode = typename StateNode::SuccessorType;
    using ActionType = typename SearchDomain::ActionType;

    explicit GreedyPolicy( SearchDomain const dom )
        : dom_( dom )
    { }
    int num_action(StateNode const &sn) const
    {
        return dom_.actions(sn.label());
    }

    boost::optional<ActionType> operator()( StateNode const& sn, SamplingContext& ctx, std::string algo ) const
    {


        using RewardType = typename StateNode::RewardType;

        if(sn.isLeaf()) {

            auto RandomActionSet=dom_.actions(sn.label());
            std::uniform_int_distribution<> pr( 0, RandomActionSet.size()-1 );
            int k=pr(ctx.rng());

            return *(RandomActionSet.begin()+k);
        }
        else {



             std::vector<typename StateNode::SuccessorType*> best;
            RewardType best_value = -1*std::numeric_limits<RewardType>::max(); // FIXME: not generic enough

            sn.visitSuccessors( [&sn,&best_value,&best,&algo]( auto& an ) {

                RewardType current_reward;
                if(algo=="uct")
                    current_reward=get<action_data::tag::avg_return> (an );
                else
                    current_reward=get<action_data::tag::value> (an );
                if( current_reward > best_value ) {

                    best.clear();
                    best_value = current_reward;
                    best.push_back(&an);
                }
                if (current_reward == best_value) {

                    best.push_back(&an);
                }
            });

            std::uniform_int_distribution<> pr( 0, best.size() -1);

            int best_action=pr(ctx.rng());

            assert( best[best_action] );
            return best[best_action]->label();
        }
    }

private:
    SearchDomain const dom_;

};

}

#endif
