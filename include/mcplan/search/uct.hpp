#ifndef UCT_HPP
#define UCT_HPP

#include "mcplan/sim/TransitionSimulator.hpp"

#include "mcplan/node/NodeData.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/Policy.hpp"
#include "mcplan/sim/PolicyRolloutEvaluator.hpp"
#include "mcplan/search/UctSamplingPolicy.hpp"
#include "mcplan/node/TreePrinter.hpp"
#include "mcplan/refinement/SplitStateNode.hpp"
//#include "mcplan/refinement/UnpruneAction.hpp"

#include "boost/range/begin.hpp"
#include <math.h>

#include <stack>

// FIXME: Debugging
#include <iostream>

namespace mcplan { namespace search {

template<typename Domain>
class RandomPolicy : public Policy<Domain>
{

    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using ActionGenerator   = std::function<ActionType(SamplingContext&)>;
public:
    RandomPolicy(Domain const& dom)
        : dom_( dom )
    { }

     ActionGenerator operator()( StateType const& s ) const override
     {
         auto as = dom_.actions( s );
         return [as]( auto& ctx ) {
             std::uniform_int_distribution<std::size_t> p( 0, as.size() - 1 );
             return *(as.begin() + p(ctx.rng()));
         };
     }
private:
      Domain const& dom_;

};


template<typename Domain>
struct UCTMinimalNodeData
{
    using StateData = boost::fusion::map<
        boost::fusion::pair<state_data::tag::n, state_data::n<Domain>>,
        boost::fusion::pair<state_data::tag::variance, state_data::variance<Domain>>,
        boost::fusion::pair<state_data::tag::ravg, state_data::ravg<Domain>>,
        boost::fusion::pair<state_data::tag::avg_return, state_data::avg_return<Domain>>
    >;

    using ActionData = boost::fusion::map<
        boost::fusion::pair<action_data::tag::n, action_data::n<Domain>>,
        boost::fusion::pair<action_data::tag::avg_return, action_data::avg_return<Domain>>
    >;

    typedef node::SetStorage<GroundStateNode<Domain, UCTMinimalNodeData>> StateStorage;
    typedef node::SetStorage<GroundActionNode<Domain, UCTMinimalNodeData>> ActionStorage;

//    template<typename ActionNodeType>
//    using StateStorage = node::SetStorage<ActionNodeType>;

//    template<typename StateNodeType>
//    using ActionStorage = node::SetStorage<StateNodeType>;
};


template<typename StateNodeType,typename Domain,typename TreeBuilder>
void uct( mcplan::SamplingContext& ctx, Domain const& dom,
          typename Domain::ParametersType const& params, TreeBuilder&  builder,
          StateNodeType& sn0, int const width, int const depth ){


    UctTreePolicy<typename TreeBuilder::TreeDomain, StateNodeType> tree_policy(builder.treeDomain(dom), &sn0);

    auto eval = [&dom, &builder, &ctx, &params]( auto const& s, int curr_depth ) {

        sim::PolicyRolloutEvaluator<Domain> rollout( builder.simulator(), ctx, params, s,curr_depth );

        return rollout( RandomPolicy<Domain>( dom ));;
    };



    for( int i = 0;i<width;i++){

         if( !ctx.budget().expired() ) {

        builder.sampleTrajectory( ctx, params, sn0, tree_policy, eval,depth,i );
        //std::cout <<"UCT Tree\n";
        //depth_first_traversal( sn0, mcplan::node::TreePrinter() );

         }
    }
}


template<
    typename Domain,
    typename SearchSpace,
    typename NodeData = search::UCTMinimalNodeData<Domain>
>
class UCTPolicy : public Policy<Domain>
{
public:
    using ParametersType    = typename Domain::ParametersType;
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using RewardType        = typename Domain::RewardType;

    using TreeBuilder       = typename SearchSpace::template TreeBuilderType<NodeData>;
    using StateNodeType     = typename TreeBuilder::StateNodeType;
    using ActionNodeType    = typename TreeBuilder::ActionNodeType;
    using ActionGenerator   = std::function<ActionType(SamplingContext&)>;
    UCTPolicy( SamplingContext& sim_ctx, Domain const& domain, ParametersType const& parameters,
               SearchSpace search_space, int const width, int const depth, std::string const& /*abstraction*/ )
        : sim_ctx_( sim_ctx ), domain_( domain ), parameters_( parameters ),
          search_space_( search_space ), width_( width ), depth_( depth )
    {
       // std::cout << "UCTPolicy: parameters = " << parameters_ << std::endl;


    }

    ActionGenerator operator()( StateType const& s ) const override
    {
        //std::cout << "UCTPolicy: operator()( " << s << " )" << std::endl;
        return [&]( SamplingContext& world_ctx ) {
            //std::cout<< s<<"\n";
            sim_ctx_.budget().reset();

            TreeBuilder builder = search_space_.template createTreeBuilder<NodeData>( domain_ );
            StateNodeType sn = builder.rootState( std::make_unique<StateType>( s ) );
            uct(sim_ctx_, domain_, parameters_, builder, sn, width_, depth_ );
            std::cout << "=== UCT tree ===" << std::endl;
            depth_first_traversal( sn, mcplan::node::TreePrinter() );
           //std::cout << "Value: " << mcplan::get<state_data::tag::avg_return>( sn ) << std::endl;

//            while(!sim_ctx_.budget().expired())
//            {
//                    builder_.refine(sim_ctx_,sn);
//                    sparse_sample( sim_ctx_, domain_, parameters_, builder, sn, width_, depth_, h_ );
//            }
            ActionNodeType const* anstar = node::argmax<action_data::tag::avg_return>( sn,world_ctx );
            return anstar->sampleAction( world_ctx );
        };
    }

private:
    SamplingContext& sim_ctx_;
    Domain const& domain_;
    ParametersType parameters_;
    SearchSpace search_space_;
    int width_;
    int depth_;
};


}}



#endif // UCT_HPP
