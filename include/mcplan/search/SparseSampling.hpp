#ifndef MCPLAN_SEARCH_SPARSESAMPLING_HPP__
#define MCPLAN_SEARCH_SPARSESAMPLING_HPP__


#include <stack>

#include "boost/fusion/container.hpp"
#include "boost/fusion/support.hpp"
#include "boost/range/begin.hpp"

#include "mcplan/Log.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"
#include "mcplan/stats/Sampling.hpp"

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
#include "mcplan/node/TreePrinter.hpp"
#endif


namespace mcplan { namespace search {

template<typename Domain, typename TreeBuilder, typename StateNodeType>
void ss_expand( Domain const* domain, TreeBuilder* builder, StateNodeType* sn, int const depth )
{
    MCPLAN_LOG_TRACE(( std::cout << "ss: ss_expand " << *sn << std::endl ));
    if( sn->isTerminal( domain->parameters() ) ) {
        MCPLAN_LOG_TRACE(( std::cout << "ss: terminal" << std::endl ));
        // Nothing to do
        sn->leaf( 0 );
    }
    else if( depth == 0 ) {
        MCPLAN_LOG_TRACE(( std::cout << "ss: leaf" << std::endl ));
        // Add heuristic value
        sn->leaf( builder->evaluateLeaf( domain, sn ) );
    }
    else {
        MCPLAN_LOG_TRACE(( std::cout << "ss: nonterminal" << std::endl ));
        // Filter action set to erase actions for which succesors already exist
        auto as = builder->missingActions( domain, sn );
        // Add successors for each remaining action
        for( auto const& a : as ) {
            builder->addSuccessor( sn, a );
        }
    }
}

/**
 * @brief Constructs a sparse sampled tree rooted in sn.
 * @param ctx
 * @param domain
 * @param builder
 * @param sn
 * @param width
 * @param depth
 * @post sn is the root node of an SS(width, depth) tree
 */
template<typename Domain, typename TreeBuilder, typename StateNodeType>
void sparse_sample( mcplan::SamplingContext& ctx, Domain const* domain,
                    TreeBuilder* builder, StateNodeType* sn,
                    int const width, int const depth )
{
    ss_expand( domain, builder, sn, depth );
    sn->visitSuccessors( [&ctx, domain, sn, builder, width, depth]( auto& an ) {
        builder->sparseSample( ctx, *domain, sn, &an, width );
        if( !ctx.budget().expired() ) {
            an.visitSuccessors( [&ctx, domain, builder, width, depth]( auto& snprime ) {
                sparse_sample( ctx, domain, builder, &snprime, width, depth - 1 );
            });
        }
        an.backup();
    });
    sn->backup();
}

// ---------------------------------------------------------------------------

template<typename Domain>
struct SparseSamplingMinimalNodeData
{
    using StateData = boost::fusion::map<
        boost::fusion::pair<state_data::tag::n, state_data::n<Domain>>,
        boost::fusion::pair<state_data::tag::variance, state_data::variance<Domain>>,
        boost::fusion::pair<state_data::tag::ravg, state_data::ravg<Domain>>,
        boost::fusion::pair<state_data::tag::value, state_data::value<Domain>>
    >;

    using ActionData = boost::fusion::map<
        boost::fusion::pair<action_data::tag::n, action_data::n<Domain>>,
        boost::fusion::pair<action_data::tag::ravg, action_data::ravg<Domain>>,
        boost::fusion::pair<action_data::tag::value, action_data::value<Domain>>
    >;

    using StateStorage = node::SetStorage<GroundStateNode<Domain, SparseSamplingMinimalNodeData>>;
    using ActionStorage = node::SetStorage<GroundActionNode<Domain, SparseSamplingMinimalNodeData>>;
};

template<
    typename Domain,
    typename SearchSpace,
    typename NodeData = search::SparseSamplingMinimalNodeData<Domain>
>
class SparseSamplingPolicy : public Policy<Domain>
{
public:

    using ParametersType    = typename Domain::ParametersType;
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using RewardType        = typename Domain::RewardType;

    using TreeBuilder       = typename SearchSpace::template TreeBuilderType<NodeData>;
    using StateNodeType     = typename TreeBuilder::StateNodeType;
    using ActionNodeType    = typename TreeBuilder::ActionNodeType;
    using ActionGenerator   = std::function<ActionType()>;
    SparseSamplingPolicy( SamplingContext& sim_ctx, Domain const* domain,
                          SearchSpace search_space, int const width, int const depth )
        : sim_ctx_( sim_ctx ), domain_( domain ),
          search_space_( search_space ), width_( width ), depth_( depth )
    { }

    ActionGenerator operator()( StateType const& s ) const override
    {
        MCPLAN_LOG_TRACE(( std::cout << "SparseSamplingPolicy: operator()( " << s << " )" << std::endl ));
        return [this, &s]() {
            sim_ctx_.budget().reset();
            TreeBuilder builder = search_space_.template createTreeBuilder<NodeData>(
                domain_, std::make_unique<StateType>( s ) );
            StateNodeType* sn0 = builder.rootStateNode();
            MCPLAN_LOG_TRACE(( std::cout<< "SparseSamplingPolicy: Root State Built" << std::endl ));
            sparse_sample( sim_ctx_, domain_, &builder, sn0, width_, depth_ );

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
            node::printSearchTree( std::cout, *sn0 );
#endif
            auto max_set = node::argmax<action_data::tag::value>( *sn0 );
            auto const* anstar = stats::uniform_choice( sim_ctx_, max_set );
            return anstar->a();
        };
    }

private:
    SamplingContext& sim_ctx_;
    Domain const* domain_;
    SearchSpace search_space_;
    int width_;
    int depth_;
};


}}


#endif
