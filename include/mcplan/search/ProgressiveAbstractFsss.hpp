#ifndef MCPLAN_SEARCH_PROGRESSIVEABSTRACTFSSS_HPP
#define MCPLAN_SEARCH_PROGRESSIVEABSTRACTFSSS_HPP


#include "mcplan/abstraction/AbstractionDiagram.hpp"
#include "mcplan/node/AggregateTreeBuilder.hpp"
#include "mcplan/node/NodeData.hpp"
#include "mcplan/search/ForwardSearchSparseSampling.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/stats/ArgmaxAccumulator.hpp"
#include "mcplan/stats/ReservoirSampler.hpp"

#include "mcplan/Log.hpp"
#include "mcplan/null_output_iterator.hpp"

#include <boost/assert.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/weighted_mean.hpp>
#include <boost/accumulators/statistics/weighted_variance.hpp>
#include <boost/optional/optional.hpp>

#include <random>
#include <vector>

namespace mcplan { namespace search {


namespace detail {
    template<typename ActionPropertyTagT, typename AbstractStateNode>
    typename AbstractStateNode::RewardType ground_succ_property_variance( AbstractStateNode const& asn )
    {
        using ActionPropertyTag = ActionPropertyTagT;
        using RewardType    = typename AbstractStateNode::RewardType;
        using GroundActionNodeType = typename AbstractStateNode::SuccessorType::GroundNodeType;
        using namespace boost::accumulators;
        accumulator_set<RewardType, features<tag::weighted_mean>, int> asn_mean;
        asn.visitSuccessors( [&asn, &asn_mean]( auto const& aan ) {
            // Note [msvc]: Can't use namespace alias from enclosing scope in
            // lambda function (error C1001)
            accumulator_set<RewardType, features<tag::weighted_variance>, int> gan_variance;
            for( GroundActionNodeType const* gan : aan.groundActionNodes() ) {
                gan_variance( mcplan::get<ActionPropertyTag>( *gan ),
                              weight = mcplan::get<mcplan::action_data::tag::n>( *gan ) );
            }
            asn_mean( weighted_variance( gan_variance ),
                      weight = mcplan::get<mcplan::action_data::tag::n>( aan ) );
        });
        return weighted_mean( asn_mean );
    }

    template<typename AbstractActionNode>
    void backup_to_root_until(
        AbstractActionNode* aan,
        boost::optional<typename AbstractActionNode::SuccessorType::LabelType> const label )
    {
        MCPLAN_LOG_TRACE(( std::cout << "prog: backup_to_root():" << *aan << std::endl ));
        using AbstractStateNode = typename AbstractActionNode::SuccessorType;

        AbstractStateNode* ps = nullptr;
        AbstractActionNode* pa = aan;
        while( pa ) {
            MCPLAN_LOG_TRACE(( std::cout << "prog: pa->backup(): " << *pa << std::endl ));
            pa->backup();
            ps = pa->parent();
            MCPLAN_LOG_TRACE(( std::cout << "prog: ps->backup(): " << *ps << std::endl ));
            ps->backup();
            pa = ps->parent();

            // Stop early
            if( label && ps->label() == *label ) {
                MCPLAN_LOG_TRACE(( std::cout << "prog: backup_to_root(): stopping at " << *ps << std::endl ));
                break;
            }
        }
    }

    template<typename AbstractActionNode> inline
    void backup_to_root( AbstractActionNode* aan )
    { return backup_to_root_until( aan, boost::none ); }

    // -----------------------------------------------------------------------
    // State splitting

    template<typename TreeBuilder>
    class UpSampleDfsVisitor
    {
    public:
        using Domain = typename TreeBuilder::Domain;
        using AbstractActionNode = typename TreeBuilder::AbstractActionNode;
        using AbstractStateNode = typename TreeBuilder::AbstractStateNode;

        UpSampleDfsVisitor( SamplingContext& ctx, Domain const* domain, TreeBuilder* builder,
                            int const width, int const search_depth )
            : ctx_( ctx ), domain_( domain ), builder_( builder ), width_( width ),
              search_depth_( search_depth ), dfs_depth_( 0 )
        { }

        bool enterNode( AbstractActionNode const& )
        { return true; }

        bool enterNode( AbstractStateNode& )
        {
            ++dfs_depth_; // @post
            return true;
        }

        void exitNode( AbstractActionNode& aan )
        { aan.backup(); }

        void exitNode( AbstractStateNode& asn )
        {
            --dfs_depth_; // @pre

            // If a node has no successors but is not a terminal search node,
            // we don't want to touch it so that its V/L/U properties retain
            // their default values.
            bool const unexpanded = asn.isLeaf() &&
                                    !asn.isTerminal( domain_->parameters() ) &&
                                    dfs_depth_ < search_depth_;
            if( unexpanded ) {
                return;
            }

            // This will add all missing action node successors
            ss_expand( domain_, builder_, &asn, dfs_depth_ );
            if( !asn.isLeaf() ) {
                // Sample actions
                asn.visitSuccessors( [this, &asn]( auto& aan ) {
                    builder_->sparseSample( ctx_, *domain_, &asn, &aan, width_ );
                    aan.backup();
                });
                asn.backup();
            }
        }

    private:
        SamplingContext& ctx_;
        Domain const* domain_;
        TreeBuilder* builder_;
        int width_;
        int search_depth_;

        int dfs_depth_;
    };

    template<typename TreeBuilder>
    void up_sample( SamplingContext& ctx, typename TreeBuilder::Domain const* domain,
                    TreeBuilder* builder, typename TreeBuilder::StateNodeType* root,
                    int const width, int const search_depth )
    {
        depth_first_traversal(
            *root, UpSampleDfsVisitor<TreeBuilder>( ctx, domain, builder, width, search_depth ) );
    }

    // -----------------------------------------------------------------------

    /**
     * @brief Collects all AbstractStateNodes 'asn' such that
     * 'asn.label() == x' and no ancestor of 'asn' is labeled with 'x'.
     */
    template<typename TreeBuilder>
    class LeastUpperBoundDfsVisitor
    {
    public:
        using AbstractActionNode = typename TreeBuilder::AbstractActionNode;
        using AbstractStateNode = typename TreeBuilder::AbstractStateNode;
        using AbstractStateLabel = typename TreeBuilder::AbstractStateLabel;

        explicit LeastUpperBoundDfsVisitor( AbstractStateLabel const x )
            : x_( x )
        { }

        /**
         * @brief The set of AbstractStateNodes 'asn' such that
         * 'asn.label() == x' and no ancestor of 'asn' is labeled with 'x'.
         * @return
         */
        std::vector<AbstractStateNode*> const& leastUpperBound() const
        { return lub_; }

        bool enterNode( AbstractActionNode const& )
        { return true; }

        bool enterNode( AbstractStateNode& asn )
        {
            if( asn.label() == x_ ) {
                lub_.push_back( &asn );
                return false;
            }
            else {
                // Continue search
                return true;
            }
        }

        void exitNode( AbstractActionNode const& )
        { }

        void exitNode( AbstractStateNode const& )
        { }

    private:
        AbstractStateLabel const x_;
        std::vector<AbstractStateNode*> lub_;
    };

    template<typename TreeBuilder>
    void split_state( SamplingContext& ctx, typename TreeBuilder::Domain const* domain,
                      TreeBuilder* builder, typename TreeBuilder::StateNodeType* root,
                      typename TreeBuilder::AbstractStateLabel x,
                      int const width, int const search_depth )
    {
        using AbstractStateLabel = typename TreeBuilder::AbstractStateLabel;
        using AbstractActionNode = typename TreeBuilder::ActionNodeType;
        using AbstractStateNode = typename TreeBuilder::StateNodeType;
        // Collect a set of subtree roots that covers all nodes labeled 'x'
        LeastUpperBoundDfsVisitor<TreeBuilder> lub( x );
        depth_first_traversal( *root, lub );
        // Rebuild each subtree
        for( AbstractStateNode* asn : lub.leastUpperBound() ) {
            MCPLAN_LOG_DEBUG(( std::cout << "prog: Rebuilding " << *asn << std::endl ));
            AbstractActionNode* parent = asn->parent();
            // Divide the ground histories appropriately
            auto const roots = builder->rebuildStateSubtree( *domain, parent, {x} );

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
            MCPLAN_LOG_DEBUG(( std::cout << "prog: AFTER rebuild" << std::endl ));
            node::printSearchTree( std::cout, *builder->rootStateNode() );
#endif
            for( AbstractStateNode* root : roots ) {
                MCPLAN_LOG_TRACE(( std::cout << "prog: up_sample " << *root << std::endl ));
                up_sample( ctx, domain, builder, root, width, search_depth );
            }

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
            MCPLAN_LOG_DEBUG(( std::cout << "prog: AFTER up_sample()" << std::endl ));
            node::printSearchTree( std::cout, *builder->rootStateNode() );
#endif
            backup_to_root( parent );

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
            MCPLAN_LOG_DEBUG(( std::cout << "prog: AFTER backup_to_root()" << std::endl ));
            node::printSearchTree( std::cout, *builder->rootStateNode() );
#endif
        }
    }

    // -----------------------------------------------------------------------
    // Action set expansion

    template<typename TreeBuilder>
    class AddActionDfsVisitor
    {
    public:
        using Domain = typename TreeBuilder::Domain;
        using ActionType = typename TreeBuilder::ActionType;
        using AbstractActionNode = typename TreeBuilder::AbstractActionNode;
        using AbstractStateNode = typename TreeBuilder::AbstractStateNode;
        using AbstractStateLabel = typename TreeBuilder::AbstractStateLabel;

        AddActionDfsVisitor( SamplingContext& ctx, Domain const* domain,
                             TreeBuilder* builder,
                             AbstractStateLabel const x, ActionType a,
                             int const width )
            : ctx_( ctx ), domain_( domain ), builder_( builder ),
              x_( x ), a_( a ), width_( width )
        { }

        template<typename NodeType>
        bool enterNode( NodeType const& )
        { return true; }

        void exitNode( AbstractActionNode const& )
        { }

        void exitNode( AbstractStateNode& asn )
        {
            if( asn.label() == x_ ) {
                MCPLAN_LOG_DEBUG(( std::cout << "prog: Adding action " << a_ << " in " << asn << std::endl ));
                // Add the new action and sparse-sample it
                AbstractActionNode* aan = builder_->addSuccessor( &asn, a_ );
                builder_->sparseSample( ctx_, *domain_, &asn, aan, width_ );
                aan->backup();
                asn.backup();

                // Backup to root (or first ancestor with label 'x')
                AbstractStateNode* ps = &asn;
                AbstractActionNode* pa = ps->parent();
                while( pa ) {
                    MCPLAN_LOG_TRACE(( std::cout << "prog: Backup " << *pa << std::endl ));
                    pa->backup();
                    ps = pa->parent();
                    MCPLAN_LOG_TRACE(( std::cout << "prog: Backup " << *ps << std::endl ));
                    ps->backup();
                    pa = ps->parent();

                    if( ps->label() == x_ ) {
                        // We will backup to the root starting from 'ps' later
                        // since its label is 'x_'. Depth first traversal order
                        // guarantees that all of ps's descendants are backed
                        // up before then.
                        break;
                    }
                }
            }
        }

    private:
        SamplingContext& ctx_;
        Domain const* domain_;
        TreeBuilder* builder_;
        AbstractStateLabel const x_;
        ActionType a_;
        int width_;
    };

    template<typename TreeBuilder>
    void add_action( SamplingContext& ctx, typename TreeBuilder::Domain const* domain,
                     TreeBuilder* builder, typename TreeBuilder::StateNodeType* root,
                     typename TreeBuilder::AbstractStateLabel x,
                     typename TreeBuilder::ActionType a, int const width )
    {
        using Visitor = AddActionDfsVisitor<TreeBuilder>;
        depth_first_traversal( *root, Visitor( ctx, domain, builder, x, a, width ) );
    }

    // -----------------------------------------------------------------------
    // Unzipping

    template<typename Abstraction>
    void unzip_recursive( Abstraction* ab,
                          typename Abstraction::AbstractStateLabel x,
                          std::size_t max_depth )
    {
        if( max_depth > 0 ) {
            ab->unzip( x );
            for( auto s : ab->successors( x ) ) {
                ab->unzip( s );
                for( auto ss : ab->successors( s ) ) {
                    unzip_recursive( ab, ss, max_depth - 1 );
                }
            }
        }
    }
} // detail

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

template<typename Abstraction, typename ActionPropertyTag = mcplan::action_data::tag::value>
class Tree_HeuristicMixedRefinementStrategy
{
public:
    using AbstractActionLabel = typename Abstraction::AbstractActionLabel;
    using AbstractStateLabel = typename Abstraction::AbstractStateLabel;
    using AbstractionInitializer = typename Abstraction::AbstractionInitializer;

    // TODO: It feels wrong that we need the width here. Maybe the parameter
    // should be an 'Sampler' function.
    explicit Tree_HeuristicMixedRefinementStrategy(
        int const width, int const depth, int const deltaT,
        double const state_split_proportion )
        : width_( width ), depth_( depth ), deltaT_( deltaT ),
          state_split_proportion_( state_split_proportion ),
          depth_priority_( depth_ + 1 ) // {0, ..., depth}
    {
        BOOST_ASSERT( width >= 0 );
        BOOST_ASSERT( depth >= 0 );
        BOOST_ASSERT( 0 <= state_split_proportion_ && state_split_proportion_ <= 1 );
        // The order in which we prioritize depths for adding actions.
        std::vector<std::size_t> depth_order;
        depth_order.push_back( 0 ); // Multiples of deltaT are highest priority
        // Construction algorithm: traverse a bit vector repeatedly, each time
        // setting the middle flag in each "run" of 10.0.01 to 10.1.01. The
        // index of the middle element is the next depth in the order.
        std::vector<bool> b( deltaT_ + 1, false );
        b[0] = b[b.size() - 1] = true;
        while( depth_order.size() < deltaT_ ) {
            std::size_t sentinel = b.size();
            std::size_t first = sentinel;
            for( std::size_t i = 1; i < b.size(); ++i ) {
                if( !b[i] && first == sentinel ) {
                    first = i - 1; // b[i-1] was true
                }
                else if( b[i] && first != sentinel ) {
                    std::size_t const last = i;
                    std::size_t const idx = first + ((last - first) / 2);
                    b[idx] = true;
                    depth_order.push_back( idx );
                    first = sentinel;
                }
            }
        }
        MCPLAN_LOG_INFO(( std::cout << "pafsss: depth_order: " << depth_order << std::endl ));
        // Convert order to priority
        int p = 0;
        for( std::size_t i = 0; i < depth_order.size(); ++i ) {
            for( std::size_t d = depth_order[i]; d <= depth_; d += deltaT_ ) {
                depth_priority_[d] = p;
            }
            p -= 1;
        }
        MCPLAN_LOG_INFO(( std::cout << "pafsss: depth_priority: " << depth_priority_ << std::endl ));
    }

    /**
     *
     */
    template<typename Domain, typename... BuilderArgs>
    bool operator ()(
        SamplingContext& ctx, Domain const* domain,
        AggregateTreeBuilder<Domain, BuilderArgs...>* builder )
    {
        using TreeBuilder           = AggregateTreeBuilder<Domain, BuilderArgs...>;
       // using Abstraction           = typename TreeBuilder::Abstraction;
        using AbstractActionNode    = typename TreeBuilder::AbstractActionNode;
        using AbstractStateNode     = typename TreeBuilder::AbstractStateNode;
        using AbstractActionLabel   = typename TreeBuilder::AbstractActionLabel;
        using AbstractStateLabel    = typename TreeBuilder::AbstractStateLabel;
        using ActionVertex          = typename TreeBuilder::AbstractActionLabel;
        using ActionType            = typename Domain::ActionType;
        using RewardType            = typename Domain::RewardType;

        MCPLAN_LOG_DEBUG(( std::cout << "pafsss: examining refinement candidates" << std::endl ));
        // Search over all asn's that are not the root
        using Transition = std::tuple<AbstractStateNode*, int>;
        mcplan::stats::ArgmaxAccumulator<Transition, RewardType> split_candidates;
        mcplan::stats::ArgmaxAccumulator<Transition, RewardType> add_action_candidates;
        builder->visitAbstractStateNodes(
                [this, &split_candidates, &add_action_candidates, builder, domain]( auto* asn, int const depth ) {
            // Don't consider: root node and leaf nodes
            // Note: leaf includes {terminal, unexpanded, at max search depth}
            if( depth > 0 && !asn->isLeaf() ) {
                MCPLAN_LOG_TRACE(( std::cout << "pafsss: candidate (d = " << depth \
                                   << "): " << *asn << std::endl ));
                // Consider state splitting if both a split and an action choice
                // are available.
                if( !asn->isPure() && asn->Nsuccessors() > 1 ) {
                    auto const score = detail::ground_succ_property_variance<ActionPropertyTag>( *asn );
                    MCPLAN_LOG_TRACE(( std::cout << "pafsss: (d = " << depth << \
                                       ") split_state candidate: " << *asn << std::endl ));
                    MCPLAN_LOG_TRACE(( std::cout << "pafsss: score: " << score << std::endl ));
                    split_candidates( std::make_tuple(asn, depth), score );
                }
                // Consider adding an action if there are actions to add
                std::vector<ActionType> all;
                std::vector<ActionType> pruned;
                builder->partitionActions( domain, asn,
                    std::back_inserter( all ), null_output_iterator(), std::back_inserter( pruned ) );
                if( pruned.size() > 0 ) {
                    // Intuition: If a state has a low upper bound all of the
                    // current actions are bad. If most of the actions are
                    // pruned, there are more potential good actions.
//                    double const ratio = static_cast<double>(pruned.size()) /
//                                         static_cast<double>(all.size());
//                    auto const score = ratio; // * -mcplan::get<mcplan::state_data::tag::upper_value_bound>( *asn );
                    double const score = depth_priority_[depth];
                    MCPLAN_LOG_TRACE(( std::cout << "pafsss: (d = " << depth << \
                                       ") add_action candidate: " << *asn << std::endl ));
                    MCPLAN_LOG_TRACE(( std::cout << "pafsss: score: " << score << std::endl ));
                    add_action_candidates( std::make_tuple(asn, depth), score );
                }
            }
        });

        MCPLAN_LOG_DEBUG(( std::cout << "pafsss: choosing refinement" << std::endl ));
        // Choose refinement modality and perform split
        std::bernoulli_distribution p( state_split_proportion_ );
        bool const choose_split_state = p(ctx.rng());
        bool const split_state_available = !boost::empty( split_candidates.max_range() );
        bool const add_action_available = !boost::empty( add_action_candidates.max_range() );
        auto* ad = builder->ad();
        // State split
        if( split_state_available && (choose_split_state || !add_action_available) ) {
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
            MCPLAN_LOG_TRACE(( std::cout << "Refinement candidates: score " \
                               << split_candidates.score() << std::endl ));
            for( auto asn : split_candidates.max_range() ) {
                MCPLAN_LOG_TRACE(( std::cout << asn << std::endl ));
            }
#endif
            auto choice = stats::uniform_choice( ctx, split_candidates.max_range() );
            AbstractStateNode* asn = std::get<0>(choice);
            int const choice_depth = std::get<1>(choice);

            MCPLAN_LOG_DEBUG(( std::cout << "pafsss: performing split state" << std::endl ));
            AbstractStateLabel const x = asn->label();
            MCPLAN_LOG_TRACE(( std::cout << "pafsss: ad->splitStateVertex( " << x << " }" << std::endl ));
            auto xs = ad->splitStateVertex( ctx, asn->parent()->label(), x, asn );
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
            std::cout << "pafsss: AbstractionDiagram after split:" << std::endl;
            std::cout << *ad << std::endl;
#endif
            MCPLAN_LOG_TRACE(( std::cout << "pafsss: unzip" << std::endl ));
            for( auto x : xs ) {
                detail::unzip_recursive( ad, x, depth_ - choice_depth );
            }
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
            std::cout << "pafsss: AbstractionDigram after unzip:" << std::endl;
            std::cout << *ad << std::endl;
#endif
            MCPLAN_LOG_TRACE(( std::cout << "pafsss: split_state" << std::endl ));
            // TODO: Searching the entire tree for nodes labeled 'x' is
            // unnecessarily expensive when we expect few matches.
            detail::split_state( ctx, domain, builder, builder->rootStateNode(), x, width_, depth_ );
            return true;
        }
        // Action split
        else if( add_action_available && (!choose_split_state || !split_state_available) ) {
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_DEBUG
            MCPLAN_LOG_DEBUG(( std::cout << "Refinement candidates: score " \
                               << add_action_candidates.score() << std::endl ));
            for( auto asn : add_action_candidates.max_range() ) {
                MCPLAN_LOG_DEBUG(( std::cout << asn << std::endl ));
            }
#endif
            auto choice = stats::uniform_choice( ctx, add_action_candidates.max_range() );
            AbstractStateNode* asn = std::get<0>(choice);
            int const choice_depth = std::get<1>(choice);

            MCPLAN_LOG_DEBUG(( std::cout << "pafsss: performing add action" << std::endl ));
            AbstractStateLabel const x = asn->label();
            // +1 because we're adding children to 'choice', which is at 'depth'
            int const start_depth = choice_depth + 1;
            // FIXME: Where do these parameters come from?
            auto f = typename Abstraction::Initialize_Tree_TopState_RepeatAction(
                 domain, deltaT_, depth_, start_depth );
            auto p = ad->addAction( ctx, x, f, builder->prunedActions( domain, asn ) );
            ActionType a = std::get<0>(p);
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
            std::cout << "pafsss: AbstractionDiagram after addAction:" << std::endl;
            std::cout << *ad << std::endl;
#endif
            // TODO: Searching the entire tree for nodes labeled 'x' is
            // unnecessarily expensive when we expect few matches.
            detail::add_action( ctx, domain, builder, builder->rootStateNode(), x, a, width_ );
            return true;
        }
        // No refinement available
        else {
            MCPLAN_LOG_DEBUG(( std::cout << "pafsss: no refinements available" << std::endl ));
            return false;
        }
    }

private:
    int width_;
    int depth_;
    int deltaT_;
    double state_split_proportion_;

    std::vector<double> depth_priority_;
};

// ---------------------------------------------------------------------------

template<
    typename Domain,
    typename SearchSpace,
    typename NodeData = search::FSSSMinimalNodeData<Domain>
>
class ProgressiveAbstractFsssPolicy : public Policy<Domain>
{
public:
    using ParametersType    = typename Domain::ParametersType;
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using RewardType        = typename Domain::RewardType;
    using ActionGenerator   = std::function<ActionType()>;
    using TreeBuilder       = typename SearchSpace::template TreeBuilderType<NodeData>;
    using StateNodeType     = typename TreeBuilder::StateNodeType;
    using ActionNodeType    = typename TreeBuilder::ActionNodeType;

    using RefinementStrategy = std::function<
        bool(SamplingContext& ctx, Domain const* domain, TreeBuilder* builder)>;

    ProgressiveAbstractFsssPolicy(
        SamplingContext& sim_ctx, Domain const* domain,
        SearchSpace search_space, int const width, int const depth,
        RefinementStrategy refine )
        : sim_ctx_( sim_ctx ), domain_( domain ),
          search_space_( search_space ), width_( width ), depth_( depth ), refine_( refine )
    { }

    ActionGenerator operator()( StateType const& s ) const override
    {
        return [&]() {
            MCPLAN_LOG_DEBUG(( std::cout << "pafsss: acting in " << s << std::endl ));

            sim_ctx_.budget().reset();
            TreeBuilder builder = search_space_.template createTreeBuilder<NodeData>(
                domain_, std::make_unique<StateType>( s ) );
            StateNodeType* sn0 = builder.rootStateNode();
            forward_search_sparse_sampling( sim_ctx_, domain_, &builder, sn0, width_, depth_ );
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
            if( fsss_converged( sn0 ) ) {
                MCPLAN_LOG_DEBUG(( std::cout << "pafsss: search converged" << std::endl ));
            }
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
            MCPLAN_LOG_DEBUG(( std::cout << "pafsss: Initial FSSS tree" << std::endl ));
            node::printSearchTree( std::cout, *sn0 );
#endif

            while( !sim_ctx_.budget().expired() ) {
                bool const refined = refine_( sim_ctx_, domain_, &builder );
                if( !refined ) {
                    MCPLAN_LOG_INFO(( std::cout << "pafsss: Tree is fully refined" << std::endl ));
                    break;
                }

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
                MCPLAN_LOG_DEBUG(( std::cout << "AFTER REFINEMENT:" << std::endl ));
                node::printSearchTree( std::cout, *sn0 );
#endif

                forward_search_sparse_sampling( sim_ctx_, domain_, &builder, sn0, width_, depth_ );
#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
                if( fsss_converged( sn0 ) ) {
                    MCPLAN_LOG_DEBUG(( std::cout << "pafsss: search converged" << std::endl ));
                }
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
                MCPLAN_LOG_DEBUG(( std::cout << "AFTER FSSS:" << std::endl ));
                node::printSearchTree( std::cout, *sn0 );
#endif
            }

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
            MCPLAN_LOG_DEBUG(( std::cout << "pafsss: Final FSSS tree" << std::endl ));
            node::printSearchTree( std::cout, *sn0 );
#endif

            auto const* anstar = stats::uniform_choice( sim_ctx_,
                node::argmax<action_data::tag::lower_value_bound>( *sn0 ) );

            MCPLAN_LOG_DEBUG(( std::cout << "pafsss: astar = " << anstar->a() << std::endl ));
            MCPLAN_LOG_INFO(( std::cout << "pafsss: budget = " << sim_ctx_.budget().value() \
                               << " / " << sim_ctx_.budget().limit() << std::endl ));
            return anstar->a();
        };
    }

private:
    SamplingContext& sim_ctx_;
    Domain const* domain_;
    SearchSpace search_space_;
    int width_;
    int depth_;
    RefinementStrategy refine_;
};


}}


#endif
