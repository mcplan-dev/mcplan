#ifndef MCPLAN_SEARCH_FORWARDSEARCHSPARSESAMPLING_HPP__
#define MCPLAN_SEARCH_FORWARDSEARCHSPARSESAMPLING_HPP__

#include <stack>

#include "boost/range/begin.hpp"

#include "mcplan/node/NodeData.hpp"
#include "mcplan/search/SparseSampling.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"
#include "mcplan/stats/Sampling.hpp"

#include "mcplan/Log.hpp"
#include "mcplan/SamplingContext.hpp"

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
#include "mcplan/node/TreePrinter.hpp"
#endif


namespace mcplan { namespace search {


template<typename StateNodeType>
bool fsss_converged( StateNodeType const* sn )
{
    MCPLAN_LOG_TRACE(( std::cout << "fsss: fsss_converged " << *sn << std::endl ));
    // Greatest lower bound
    typename StateNodeType::RewardType Lmax;
    // Upper bound of the node with the greatest lower bound
    typename StateNodeType::RewardType Ustar;
    // Greatest upper bound excluding Ustar
    typename StateNodeType::RewardType Umax;
    bool first = true;
    sn->visitSuccessors( [&Lmax, &Ustar, &Umax, &first]( auto const& an ) {
        if( first ) {
            Lmax = mcplan::get<mcplan::action_data::tag::lower_value_bound>( an );
            Umax = Ustar = mcplan::get<mcplan::action_data::tag::upper_value_bound>( an );
            first = false;
        }
        else {
            auto La = mcplan::get<mcplan::action_data::tag::lower_value_bound>( an );
            if( La > Lmax ) {
                Lmax = La;
                // Since Lmax changed, Ustar is now a candidate for Usecond
                if( Ustar > Umax ) {
                    Umax = Ustar;
                }
                Ustar = mcplan::get<mcplan::action_data::tag::upper_value_bound>( an );
            }
            else {
                auto Ua = mcplan::get<mcplan::action_data::tag::upper_value_bound>( an );
                if( Ua > Umax ) {
                    Umax = Ua;
                }
            }
        }
    } );
    return Lmax >= Umax;
}

template<typename StateNodeType>
typename StateNodeType::SuccessorType* fsss_select_action( mcplan::SamplingContext& ctx, StateNodeType* sn )
{
    MCPLAN_LOG_TRACE(( std::cout << "fsss: fsss_select_action " << *sn << std::endl ));
    using ActionNodeType = typename StateNodeType::SuccessorType;
    // Get action with the largest U(s, a) value, breaking ties randomly
    return stats::uniform_choice( ctx,
        node::argmax<action_data::tag::upper_value_bound>( *sn ) );
}

template<typename ActionNodeType>
typename ActionNodeType::SuccessorType* fsss_select_state( mcplan::SamplingContext& ctx, ActionNodeType* an )
{
    MCPLAN_LOG_TRACE(( std::cout << "fsss: fsss_select_state " << *an << std::endl ));
    using StateNodeType = typename ActionNodeType::SuccessorType;
    // Get the state successor with the largest value of U(s') - L(s'),
    // breaking ties randomly
    typename StateNodeType::RewardType Dmax;
    stats::ReservoirSampler<StateNodeType*> snstar_candidates;
    an->visitSuccessors( [&ctx, &Dmax, &snstar_candidates]( auto& snprime ) {
        if( snstar_candidates.n() == 0 ) {
            snstar_candidates( ctx, &snprime );
            Dmax = mcplan::get<mcplan::state_data::tag::upper_value_bound>( snprime )
                   - mcplan::get<mcplan::state_data::tag::lower_value_bound>( snprime );
        }
        else {
            auto Ds = mcplan::get<mcplan::state_data::tag::upper_value_bound>( snprime )
                      - mcplan::get<mcplan::state_data::tag::lower_value_bound>( snprime );
            if( Ds > Dmax ) {
                Dmax = Ds;
                snstar_candidates.reset();
                snstar_candidates( ctx, &snprime );
            }
            else if( Ds == Dmax ) {
                snstar_candidates( ctx, &snprime );
            }
        }
    });
    return *boost::begin(snstar_candidates.samples());
}

template<typename Domain, typename TreeBuilder, typename StateNodeType>
void fsss_trial( mcplan::SamplingContext& ctx, Domain const* domain,
                 TreeBuilder* builder,
                 StateNodeType* sn, int const width, int const depth )
{
    MCPLAN_LOG_TRACE(( std::cout << "fsss: fsss_trial " << *sn << std::endl ));
    // Sample actions
    sn->visitSuccessors( [&ctx, domain, builder, sn, width, depth]( auto& an ) {
        MCPLAN_LOG_TRACE(( std::cout << "fsss: sparseSample() " << an << std::endl ));
        builder->sparseSample( ctx, *domain, sn, &an, width );
        MCPLAN_LOG_TRACE(( std::cout << "fsss: backup() " << an << std::endl ));
        an.backup();
    });

    // Pick successors
    // If budget expired, there might be no successors and 'snstar' would be
    // null. We wouldn't do more sampling anyway, so we ignore the cases where
    // there *are* successors.
    using ActionNodeType = typename StateNodeType::SuccessorType;
    if( !ctx.budget().expired() ) {
        ActionNodeType* anstar = fsss_select_action( ctx, sn );
        StateNodeType* snstar = fsss_select_state( ctx, anstar );
        ss_expand( domain, builder, snstar, depth - 1 );
        if( !snstar->isLeaf() ) {
            fsss_trial( ctx, domain, builder, snstar, width, depth - 1 );
        }
        anstar->backup();
    }
    sn->backup();
}

/**
 * Constructs a sparse sampled tree rooted in sn.
 * @param ctx
 * @param domain Must model BoundedValueDomain
 * @param builder
 * @param sn
 * @param width
 * @param depth
 * @post sn is the root node of an FSSS(width, depth) tree
 */
template<typename Domain, typename TreeBuilder, typename StateNodeType>
void forward_search_sparse_sampling( mcplan::SamplingContext& ctx, Domain const* domain,
                                     TreeBuilder* builder, StateNodeType* sn0,
                                     int const width, int const depth )
{
    MCPLAN_LOG_TRACE(( std::cout << "fsss: root state " << *sn0 << std::endl ));
    ss_expand( domain, builder, sn0, depth );
    while( !ctx.budget().expired() ) {
        fsss_trial( ctx, domain, builder, sn0, width, depth );
        if( fsss_converged( sn0 ) ) {
            break;
        }
    }
}

// ---------------------------------------------------------------------------

template<typename Domain>
struct FSSSMinimalNodeData
{
    using StateData = boost::fusion::map<
        boost::fusion::pair<state_data::tag::n, state_data::n<Domain>>,
        boost::fusion::pair<state_data::tag::variance, state_data::variance<Domain>>,
        boost::fusion::pair<state_data::tag::ravg, state_data::ravg<Domain>>,
        boost::fusion::pair<state_data::tag::value, state_data::value<Domain>>,
        boost::fusion::pair<state_data::tag::upper_value_bound, state_data::upper_value_bound<Domain>>,
        boost::fusion::pair<state_data::tag::lower_value_bound, state_data::lower_value_bound<Domain>>
    >;

    using ActionData = boost::fusion::map<
        boost::fusion::pair<action_data::tag::n, action_data::n<Domain>>,
        boost::fusion::pair<action_data::tag::ravg, action_data::ravg<Domain>>,
        boost::fusion::pair<action_data::tag::value, action_data::value<Domain>>,
        boost::fusion::pair<action_data::tag::upper_value_bound, action_data::upper_value_bound<Domain>>,
        boost::fusion::pair<action_data::tag::lower_value_bound, action_data::lower_value_bound<Domain>>
    >;

    typedef node::SetStorage<GroundStateNode<Domain, FSSSMinimalNodeData>> StateStorage;
    typedef node::SetStorage<GroundActionNode<Domain, FSSSMinimalNodeData>> ActionStorage;
};

// ---------------------------------------------------------------------------

template<
    typename Domain,
    typename SearchSpace,
    typename NodeData = search::FSSSMinimalNodeData<Domain>
>
class FSSSPolicy : public Policy<Domain>
{
public:
    using ParametersType    = typename Domain::ParametersType;
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using RewardType        = typename Domain::RewardType;

    using TreeBuilder       = typename SearchSpace::template TreeBuilderType<NodeData>;
    using StateNodeType     = typename TreeBuilder::StateNodeType;
    using ActionNodeType    = typename TreeBuilder::ActionNodeType;
    using ActionGenerator   = std::function<ActionType()>;

    FSSSPolicy( SamplingContext& sim_ctx, Domain const* domain,
                SearchSpace search_space, int const width, int const depth )
        : sim_ctx_( sim_ctx ), domain_( domain ),
          search_space_( search_space ), width_( width ), depth_( depth )
    { }

    ActionGenerator operator()( StateType const& s ) const override
    {
        return [&]() {
            MCPLAN_LOG_TRACE(( std::cout << "FSSSPolicy::operator()" << std::endl ));
            sim_ctx_.budget().reset();
            MCPLAN_LOG_TRACE(( std::cout << "fsss: createTreeBuilder()" << std::endl ));
            TreeBuilder builder = search_space_.template createTreeBuilder<NodeData>(
                domain_, std::make_unique<StateType>( s ) );
            MCPLAN_LOG_TRACE(( std::cout << "fsss: rootStateNode()" << std::endl ));
            StateNodeType* sn0 = builder.rootStateNode();
            forward_search_sparse_sampling(
                sim_ctx_, domain_, &builder, sn0, width_, depth_ );

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
            if( fsss_converged( sn0 ) ) {
                MCPLAN_LOG_DEBUG(( std::cout << "fsss: search converged" << std::endl ));
            }
#endif

            auto max_set = node::argmax<action_data::tag::lower_value_bound>( *sn0 );
            auto const* anstar = stats::uniform_choice( sim_ctx_, max_set );

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
            node::printSearchTree( std::cout, *sn0 );
            MCPLAN_LOG_DEBUG(( std::cout << "a* = " << anstar->a() << std::endl ));
#endif

            MCPLAN_LOG_DEBUG(( std::cout << "fsss: astar = " << anstar->a() << std::endl ));
            MCPLAN_LOG_INFO(( std::cout << "fsss: budget = " << sim_ctx_.budget().value() \
                               << " / " << sim_ctx_.budget().limit() << std::endl ));

            return anstar->a();
        };
    }

private:
    SamplingContext& sim_ctx_;
    Domain const* domain_;
    SearchSpace search_space_;
    int width_;
    int depth_;
};

}}


#endif
