#ifndef MCPLAN_ALGORITHM_TREETRAVERSAL_HPP__
#define MCPLAN_ALGORITHM_TREETRAVERSAL_HPP__


#include <boost/optional/optional.hpp>

#include <limits>

namespace mcplan {


/**
 * @brief Trivial implementation of the DfsVisitor concept.
 */
struct DfsVisitor
{
    /**
     * @brief Called when encountering node 'n' for the first time.
     * @return If false, do not enter the node (i.e. do not search the subtree
     * below 'n' and do not call 'exitNode(n)').
     */
    template<typename NodeType>
    bool enterNode( NodeType& /*n*/ )
    { return true; }

    /**
     * @brief Called just after finishing searching the subtree rooted at 'n'.
     */
    template<typename NodeType>
    void exitNode( NodeType& /*n*/ )
    { }
};

/**
 * @brief Visits the nodes of a tree in depth-first order.
 *
 * @param n Satisfies the TreeNode concept
 * @param f Satisfies the DfsVisitor concept
 */
template<typename NodeType, typename Visitor>
void depth_first_traversal( NodeType& n, Visitor&& f )
{
    if( ! f.enterNode( n ) ) {
        return;
    }
    n.visitSuccessors( [&]( auto& succ ) {
        depth_first_traversal( succ, f );
    });
    f.exitNode( n );
}

/**
 * @brief Visits the nodes of a tree in depth-first order.
 *
 * @param n Satisfies the TreeNode concept
 * @param f Satisfies the DfsVisitor concept
 */
template<typename NodeType, typename Visitor>
void depth_first_traversal( NodeType const& n, Visitor&& f )
{
    if( ! f.enterNode( n ) ) {
        return;
    }
    n.visitSuccessors( [&]( auto const& succ ) {
        depth_first_traversal( succ, f );
    });
    f.exitNode( n );
}

/**
 * @brief Visits the nodes of a tree in depth-first order.
 *
 * @param n Satisfies the TreeNode concept
 * @param f Satisfies the DfsVisitor concept
 */
template<typename NodeType, typename Visitor>
void depth_first_traversal( NodeType& n, Visitor&& f, std::size_t max_depth )
{
    if( ! f.enterNode( n ) ) {
        return;
    }
    if( max_depth > 0 ) {
        n.visitSuccessors( [&]( auto& succ ) {
            depth_first_traversal( succ, f, max_depth - 1 );
        });
    }
    f.exitNode( n );
}

/**
 * @brief Visits the nodes of a tree in depth-first order.
 *
 * @param n Satisfies the TreeNode concept
 * @param f Satisfies the DfsVisitor concept
 */
template<typename NodeType, typename Visitor>
void depth_first_traversal( NodeType const& n, Visitor&& f, std::size_t max_depth )
{
    if( ! f.enterNode( n ) ) {
        return;
    }
    if( max_depth > 0 ) {
        n.visitSuccessors( [&]( auto const& succ ) {
            depth_first_traversal( succ, f, max_depth - 1 );
        });
    }
    f.exitNode( n );
}


/*
template<typename StateNodeType, typename Visitor>
void dfs_states( StateNodeType& sn, Visitor&& f )
{
    using StateNode = typename TreeBuilder::StateNodeType;
    std::vector<std::pair<StateNode*, bool>> s;
    s.push_back( {&sn, false} );
    while( !s.empty() ) {
        std::pair<StateNode*, bool>& p = s.back();
        if( p.second ) { // Already handled its children
            f.exitNode( *p.first );
            s.pop_back();
        }
        else { // Not encountered yet
            f.enterNode( *p.first );
            p.second = true;
            auto const i = s.end();
            p.first->visitSuccessors( [&s]( auto& an ) {
                an.visitSuccessors( [&s]( auto& succ ) {
                    s.push_back( {&succ, false} );
                });
            });
            // This way we traverse subtrees in the same order as
            // visitSuccessors()
            std::reverse( i, s.end() );
        }
    }
}
*/

}


#endif
