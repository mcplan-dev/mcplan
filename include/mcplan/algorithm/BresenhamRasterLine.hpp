#ifndef MCPLAN_ALGORITHM_BRESENHAMRASTERLINE_HPP__
#define MCPLAN_ALGORITHM_BRESENHAMRASTERLINE_HPP__


#include <cmath>
#include <tuple>
#include <vector>

namespace mcplan { namespace algorithm {

/**
 * Bresenham's algorithm for rasterizing a line.
 * <p>
 * Note that this algorithm does *not* return every cell that the line
 * would pass through. The advantage of Bresenham's algorithm is that it uses
 * only integer arithmetic, so it is exact.
 * <p>
 * Adapted from:
 * http://tomasjanecek.cz/en/clanky/post/trivial-dda-and-bresenham-algorithm-for-a-line-in-java
 * <p>
 * Note that the linked implementation does not properly add the starting
 * cell if the line consists of more than one pixel.
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @return
 */
std::vector<std::tuple<int, int>> bresenham_raster_line( int x1, int y1, int const x2, int const y2 )
{
    using Point = std::tuple<int, int>;
    std::vector<Point> line;
    line.emplace_back( x1, y1 );
    if( !(x1 == x2 && y1 == y2) ) {
        int const dx = std::abs( x2 - x1 );
        int const dy = std::abs( y2 - y1 );
        int diff = dx - dy;

        int const shift_x = (x1 < x2 ? 1 : -1);
        int const shift_y = (y1 < y2 ? 1 : -1);
        do { // We know the condition is satisfied the first time around
            int const p = 2 * diff;
            if( p > -dy ) {
                diff -= dy;
                x1 += shift_x;
            }
            if( p < dx ) {
                diff += dx;
                y1 += shift_y;
            }
            line.emplace_back( x1, y1 );
        } while( !(x1 == x2 && y1 == y2) );
    }
    return line;
}

}} // namespace

#endif
