#ifndef MCPLAN_OSTREAM_HPP
#define MCPLAN_OSTREAM_HPP

#include <ostream>
#include <tuple>
#include <vector>

namespace mcplan { namespace detail {

template<class Tuple, std::size_t N>
struct TuplePrinter
{
    static void print( std::ostream& out, Tuple const& t )
    {
        TuplePrinter<Tuple, N-1>::print( out, t );
        out << ", " << std::get<N-1>(t);
    }
};

template<class Tuple>
struct TuplePrinter<Tuple, 1>
{
    static void print( std::ostream& out, Tuple const& t )
    {
        out << std::get<0>( t );
    }
};

}}

namespace std {

template<typename... Args>
std::ostream& operator <<( std::ostream& out, std::tuple<Args...> const& t )
{
    out << "(";
    mcplan::detail::TuplePrinter<decltype(t), sizeof...(Args)>::print( out, t );
    out << ")";
    return out;
}

template<typename T>
std::ostream& operator<<( std::ostream& out, std::vector<T> const& v )
{
    out << "[";
    for( std::size_t i = 0; i < v.size(); ++i ) {
        if( i > 0 ) {
            out << ", ";
        }
        out << v[i];
    }
    out << "]";
    return out;
}

}


#endif
