#ifndef MCPLAN_STATS_ARGMAXACCUMULATOR_HPP
#define MCPLAN_STATS_ARGMAXACCUMULATOR_HPP


#include <vector>

#include "boost/range/iterator_range.hpp"

namespace mcplan { namespace stats {


template<typename ArgType, typename ScoreType = double>
class ArgmaxAccumulator
{
    using MaxCollectionType = std::vector<ArgType>;

public:
    void operator ()( ArgType const& arg, ScoreType const score )
    {
        if( max_.empty() ) {
            max_score_ = score;
            max_.push_back( arg );
        }
        else if( score >= max_score_ ) {
            if( score > max_score_ ) {
                max_.clear();
                max_score_ = score;
            }
            max_.push_back( arg );
        }
    }

    boost::iterator_range<typename MaxCollectionType::const_iterator>
    max_range() const
    {
        return boost::make_iterator_range( max_.begin(), max_.end() );
    }

    ScoreType score() const
    {
        return max_score_;
    }

private:
    std::vector<ArgType> max_;
    ScoreType max_score_;
};


}}


#endif
