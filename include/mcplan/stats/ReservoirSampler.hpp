#ifndef MCPLAN_STATS_RESERVOIRSAMPLER_HPP__
#define MCPLAN_STATS_RESERVOIRSAMPLER_HPP__


#include "mcplan/SamplingContext.hpp"

#include <boost/range/iterator.hpp>
#include <boost/range/iterator_range.hpp>

#include <array>
#include <cassert>
#include <iterator>
#include <limits>
#include <stdexcept>

namespace mcplan { namespace stats {


/**
 * Implements <em>reservoir sampling</em>, which is an algorithm for sampling
 * k elements uniformly at random from a sequence of indeterminate length.
 */
template<typename T, std::size_t k = 1>
class ReservoirSampler
{
    static_assert( k > 0, "Drawing 0 samples is pointless" );

private:
    std::size_t const not_accepted = std::numeric_limits<std::size_t>::max();

public:
    ReservoirSampler()
        : samples_(), n_( 0 ), pending_( not_accepted )
    { }

    void reset()
    {
        n_ = 0;
        pending_ = not_accepted;
    }

    /**
     * @brief Offer a new element
     * @param ctx
     * @param t
     * @return True if the sample was accepted
     */
    bool operator()( SamplingContext& ctx, T const& t )
    {
        if( accept( ctx ) ) {
            add( t );
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @brief Returns true if the next sample will be accepted.
     * The client must then call add() if and only if accept() returns true.
     * This method together with add() allows you to see if an object would
     * be accepted before constructing it, which is useful if construction
     * is expensive.
     * @param ctx
     * @return
     */
    bool accept( SamplingContext& ctx )
    {
        assert( pending_ == not_accepted );
        if( n_ < k ) {
            pending_ = n_;
        }
        else {
            std::uniform_int_distribution<std::size_t> p( 0, n_ );
            std::size_t const j = p( ctx.rng() );
            if( j < k ) {
                pending_ = j;
            }
            else {
                pending_ = not_accepted;
            }
        }
        n_ += 1;
        if( n_ == not_accepted ) {
            throw std::overflow_error( "too many samples" );
        }
        return pending_ != not_accepted;
    }

    /**
     * @brief Add an already-accepted sample.
     * Must be called if and only if the last call to accept() returned true.
     * @param t
     */
    void add( T const& t )
    {
        assert( pending_ != not_accepted );
        samples_[pending_] = t;
        pending_ = not_accepted;
    }

    /**
     * @brief Returns the current sample set.
     * The range will contain min(n(), k) elements.
     * @return
     */
    boost::iterator_range<typename std::array<T, k>::const_iterator> samples() const
    { return boost::make_iterator_range_n( samples_.begin(), std::min(n_, k) ); }

    /**
     * @brief The number of samples offered (not necessarily accepted) so far.
     * @return
     */
    std::size_t n() const
    { return n_; }

private:
    std::array<T, k> samples_;
    std::size_t n_;
    std::size_t pending_;
};


} }


#endif
