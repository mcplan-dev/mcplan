#ifndef MCPLAN_STATS_SAMPLING_HPP
#define MCPLAN_STATS_SAMPLING_HPP


#include <boost/range/iterator.hpp>

#include "mcplan/stats/ReservoirSampler.hpp"

#include "mcplan/SamplingContext.hpp"

namespace mcplan { namespace stats {


// TODO: Make a specialized version for random access containers
template<typename Range>
auto uniform_choice( SamplingContext& ctx, Range const& r ) ->
    typename Range::value_type
{
//    using T = typename boost::range_value<Range const>::type;
    using T = typename Range::value_type;
    ReservoirSampler<T> s;
    for( auto const& e : r ) {
        s( ctx, e );
    }
    return *std::begin(s.samples());
}


}}


#endif
