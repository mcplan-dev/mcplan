#ifndef MCPLAN_LOG_HPP
#define MCPLAN_LOG_HPP

#include "mcplan/LogLevel.hpp"

#if !defined(NDEBUG) && !defined(MCPLAN_LOG_LEVEL)
#   define MCPLAN_LOG_LEVEL MCPLAN_LOG_LEVEL_ALL
#endif

#if MCPLAN_LOG_LEVEL > MCPLAN_LOG_LEVEL_NONE
#   include <iostream>
#endif

#if MCPLAN_LOG_LEVEL > MCPLAN_LOG_LEVEL_NONE
#   define MCPLAN_LOG(x) x
#else
#   define MCPLAN_LOG(x) do {} while(false)
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_FATAL
#   define MCPLAN_LOG_FATAL(x) x
#else
#   define MCPLAN_LOG_FATAL(x) do {} while(false);
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_ERROR
#   define MCPLAN_LOG_ERROR(x) x
#else
#   define MCPLAN_LOG_ERROR(x) do {} while(false);
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_WARN
#   define MCPLAN_LOG_WARN(x) x
#else
#   define MCPLAN_LOG_WARN(x) do {} while(false);
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_INFO
#   define MCPLAN_LOG_INFO(x) x
#else
#   define MCPLAN_LOG_INFO(x) do {} while(false);
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_DEBUG
#   define MCPLAN_LOG_DEBUG(x) x
#else
#   define MCPLAN_LOG_DEBUG(x) do {} while(false);
#endif

#if MCPLAN_LOG_LEVEL >= MCPLAN_LOG_LEVEL_TRACE
#   define MCPLAN_LOG_TRACE(x) x
#else
#   define MCPLAN_LOG_TRACE(x) do {} while(false)
#endif

#endif
