#ifndef MCPLAN_SAMPLINGCONTEXT_HPP__
#define MCPLAN_SAMPLINGCONTEXT_HPP__


#include "mcplan/Budget.hpp"

#include <memory>
#include <random>

namespace mcplan {
    
    
class SamplingContext
{
public:
    using rng_t = std::mt19937;

    SamplingContext() = delete;
    SamplingContext( SamplingContext const& ) = delete;
    SamplingContext& operator =( SamplingContext const& ) = delete;

    SamplingContext( rng_t&& rng )
        : SamplingContext( std::move(rng), std::make_unique<InfiniteBudget>() )
    { }
    
    SamplingContext( rng_t&& rng, std::unique_ptr<Budget> budget )
        : rng_( rng ), budget_( std::move(budget) )
    {}

    rng_t& rng()
    { return rng_; }

    rng_t const& rng() const
    { return rng_; }

    Budget& budget()
    { return *budget_; }

    Budget const& budget() const
    { return *budget_; }
    
private:
    rng_t rng_;
    std::unique_ptr<Budget> budget_;
};

    
}

#endif
