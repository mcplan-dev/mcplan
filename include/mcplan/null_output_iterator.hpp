#ifndef MCPLAN_NULLOUTPUTITERATOR_HPP
#define MCPLAN_NULLOUTPUTITERATOR_HPP

#include <iterator>

namespace mcplan {

/**
 * @brief An OutputIterator that does nothing.
 */
struct null_output_iterator
    : std::iterator<std::output_iterator_tag, null_output_iterator>
{
    template<typename T>
    void operator=(T const&) { }

    null_output_iterator& operator ++()
    { return *this; }

    null_output_iterator operator ++(int)
    { return *this; }

    null_output_iterator & operator*() { return *this; }
};

}

#endif
