#ifndef MCPLAN_LOGLEVEL_HPP
#define MCPLAN_LOGLEVEL_HPP

#define MCPLAN_LOG_LEVEL_NONE   0
#define MCPLAN_LOG_LEVEL_FATAL  10
#define MCPLAN_LOG_LEVEL_ERROR  20
#define MCPLAN_LOG_LEVEL_WARN   30
#define MCPLAN_LOG_LEVEL_INFO   40
#define MCPLAN_LOG_LEVEL_DEBUG  50
#define MCPLAN_LOG_LEVEL_TRACE  60
#define MCPLAN_LOG_LEVEL_ALL MCPLAN_LOG_LEVEL_TRACE

#endif
