#ifndef MCPLAN_ANYTIMEPOLICY_HPP__
#define MCPLAN_ANYTIMEPOLICY_HPP__

#include "mcplan/Policy.hpp"

namespace mcplan {


template<typename Domain>
class AnytimePolicy : public Policy<Domain>
{
public:
    virtual bool improve() = 0;
};

    
}

#endif
