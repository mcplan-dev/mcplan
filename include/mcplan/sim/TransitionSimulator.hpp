#ifndef MCPLAN_SIM_TRANSITIONSIMULATOR_HPP__
#define MCPLAN_SIM_TRANSITIONSIMULATOR_HPP__

#include "mcplan/sim/SimulatorTraits.hpp"
#include "mcplan/sim/TrajectorySimulator.hpp"

#include "mcplan/Policy.hpp"
#include "mcplan/SamplingContext.hpp"

#include "boost/optional/optional_io.hpp"

#include <cassert>
#include <tuple>

// FIXME: debugging
#include <iostream>

namespace mcplan { namespace sim {
    

/**
 * Base class for simulators that can sample from P(s'|s, a). Implements
 * TrajectorySimulator in terms of transition samples.
 */
template<typename Derived, typename Domain, typename Enabled = void>
class TransitionSimulator
{ };

template<typename Derived, typename Domain>
class TransitionSimulator<Derived, Domain, std::enable_if_t<is_transition_simulator<Derived>::value>>
{
    using ParametersType    = typename Domain::ParametersType;
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using RewardType        = typename Domain::RewardType;

public:

    using TransitionSample = std::tuple<ActionSample<Domain>, StateSample<Domain>>;

    /**
     * @brief sampleTransition
     * @param ctx
     * @param s
     * @param a
     * @return Tuple of (ActionSample, StateSample), where each sample contains
     * an Action/State and its sampled reward.
     */
//    virtual TransitionSample sampleTransition(
//        SamplingContext& ctx, ParametersType const& params,
//        StateType const& s, ActionType const& a ) const = 0;
    
    /**
     * @brief Samples a complete trajectory by repeatedly calling sampleTransition().
     * The trajectory is a sequence of (ActionSample, StateSample) tuples, not
     * including the initial state 's0'. The trajectory is output by calling
     * member functions of 'consumer'. Each such call transfers ownership of
     * the corresponding sample instance to 'consumer'. The starting state 's0'
     * is *not* passed to 'consumer', since it is not owned by TransitionSimulator.
     * @param ctx
     * @param consumer
     * @param s0 Initial state
     * @param pi Sampling policy
     * @param depth_limit Maximum number of actions in sampled trajectory
     */
    void sampleTrajectory( SamplingContext& ctx, TrajectoryConsumer<Domain>& consumer,
                           Domain const& domain, StateType const& s0,
                           Policy<PartialDomain<Domain>> const& pi,
                           boost::optional<std::size_t> depth_limit ) const
    {
        if( (depth_limit && *depth_limit == 0) || s0.isTerminal( domain.parameters() )) {
            return;
        }

        // This gets a little complicated because we need to make sure that
        // states don't get consumed until the simulator is done using them
        // to simulate the next state. We basically unroll the loop for one
        // step to achieve this.

        StateType const* s = &s0;
        //std::cout << "TransitionSimulator: [0] calling policy" << std::endl;
        auto a = pi( *s )();
        if( !a ) {
            // Policy terminated the episode
            std::cout << "TransitionSimulator: [0] terminate action" << std::endl;
            consumer.onTrajectoryEnd();
            return;
        }
       // std::cout << "TransitionSimulator: [0] sample action " << a << std::endl;
        TransitionSample tr0 = static_cast<Derived const*>(this)->sampleTransition( ctx, domain, *s, *a );
        ctx.budget().onSample();
        //std::cout << "TransitionSimulator: consume action" << std::endl;
        // Consume first action
        consumer.consume( std::move( std::get<0>(tr0) ) );
        s = &std::get<1>(tr0).s();
        int t = 1; // t = 1 because we already sampled one transition

        while( !ctx.budget().expired() &&
               (!depth_limit || t < *depth_limit) &&
               !s->isTerminal( domain.parameters() ) ) {
           // std::cout << "TransitionSimulator: [" << t << "] calling policy" << std::endl;
            a = pi( *s )();
            if( !a ) {
                // Policy terminated the episode
                //std::cout << "TransitionSimulator: [" << t << "] terminate action" << std::endl;
                break;
            }
            //std::cout << "TransitionSimulator: [" << t << "] sample action " << a << std::endl;
            TransitionSample tr = static_cast<Derived const*>(this)->sampleTransition( ctx, domain, *s, *a );
            ctx.budget().onSample();
            // Consume old state
            //std::cout << "TransitionSimulator: consume state" << std::endl;
            consumer.consume( std::move( std::get<1>(tr0) ) );
            // Finished with old sample
            tr0 = std::move(tr);
            // Consume new action
            //std::cout << "TransitionSimulator: consume action" << std::endl;
            consumer.consume( std::move( std::get<0>(tr0) ) ); // Consume action

            //std::cout << "TransitionSimulator: ... returned" << std::endl;

            // Advance
            s = &std::get<1>(tr0).s();
            t += 1;

            //std::cout << "TransitionSimulator: end of loop" << std::endl;
        }

        // Consume final state
        consumer.consume( std::move( std::get<1>(tr0) ) );
        consumer.onTrajectoryEnd();
    }
};

template<typename Derived, typename Domain>
struct is_trajectory_simulator<TransitionSimulator<Derived, Domain>>
{
    static bool constexpr value = true;
};

template<typename Derived, typename Domain>
struct is_transition_simulator<TransitionSimulator<Derived, Domain>>
{
    static bool constexpr value = true;
};
    
    
}
}


#endif

