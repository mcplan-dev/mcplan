#ifndef MCPLAN_SIM_TRAJECTORYSIMULATOR_HPP__
#define MCPLAN_SIM_TRAJECTORYSIMULATOR_HPP__


#include "mcplan/ActionSample.hpp"
#include "mcplan/Policy.hpp"
#include "mcplan/SamplingContext.hpp"
#include "mcplan/StateSample.hpp"

namespace mcplan { namespace sim {


template<typename Domain>
class TrajectoryConsumer
{
public:
    virtual void consume( StateSample<Domain>&& sn )  = 0;
    virtual void consume( ActionSample<Domain>&& an ) = 0;

    virtual void onTrajectoryEnd()
    { }
};

/**
 * Interface for simulators that can sample a complete trajectory under a
 * fixed policy.
 */
template<typename Domain>
class TrajectorySimulator
{
    using ParametersType    = typename Domain::ParametersType;
    using StateType         = typename Domain::StateType;
    using ActionType        = typename Domain::ActionType;
    using RewardType        = typename Domain::RewardType;
public:
    virtual void sampleTrajectory(
        SamplingContext& ctx, TrajectoryConsumer<Domain>& consumer,
        ParametersType const& params, StateType const& s0,
        Policy<Domain> const& pi, int depth_limit ) const = 0;
};
    
    
}} // namespace


#endif
