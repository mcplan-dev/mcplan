#ifndef MCPLAN_SIM_SIMULATORTRAITS_HPP
#define MCPLAN_SIM_SIMULATORTRAITS_HPP


namespace mcplan { namespace sim {


template<typename T>
struct is_trajectory_simulator
{
    static bool constexpr value = false;
};

// ---------------------------------------------------------------------------

template<typename T>
struct is_transition_simulator
{
    static bool constexpr value = false;
};



}}


#endif
