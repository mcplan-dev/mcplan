#ifndef MCPLAN_SIM_POLICYROLLOUTEVALUATOR_HPP__
#define MCPLAN_SIM_POLICYROLLOUTEVALUATOR_HPP__


#include "mcplan/sim/TrajectorySimulator.hpp"

#include "mcplan/ActionSample.hpp"
#include "mcplan/StateSample.hpp"

#include <type_traits>

namespace mcplan { namespace sim {


/**
 * Evalutes states by the return of a policy rollout.
 */
template<typename Domain>
class PolicyRolloutEvaluator
{
private:
    class RewardSum : public TrajectoryConsumer<Domain>
    {
    public:
        RewardSum() : v_( 0 ) { }

        inline typename Domain::RewardType v() const { return v_; }

        void consume( StateSample<Domain>&& ss ) override
        {
            v_ += ss.r();
        }

        void consume( ActionSample<Domain>&& as ) override
        {
            v_ += as.r();
        }

    private:
        typename Domain::RewardType v_;
    };

public:

    PolicyRolloutEvaluator( typename Domain::SimulatorType const& sim, SamplingContext& ctx,
                            typename Domain::ParametersType const& params,
                            typename Domain::GroundStateType const s0, int depth_limit )
        : sim_( sim ), ctx_( ctx ), params_( params ), s0_( s0 ), depth_limit_( depth_limit )
    { }

    typename Domain::RewardType operator()( Policy<Domain> const& pi ) const
    {
        RewardSum sum;

        sim_.sampleTrajectory( ctx_, sum, params_, s0_, make_partial( pi ), depth_limit_ );


        return sum.v();
    }

private:
    typename Domain::SimulatorType sim_;
    SamplingContext& ctx_;
    typename Domain::ParametersType params_;
    typename Domain::StateType const& s0_;
    int depth_limit_;
};


}}


#endif
