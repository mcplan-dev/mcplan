#ifndef MCPLAN_NODE_NODEDATA_HPP__
#define MCPLAN_NODE_NODEDATA_HPP__

#include <memory>
#include <ostream>
#include <vector>

#include "boost/accumulators/accumulators.hpp"
#include "boost/accumulators/statistics/stats.hpp"
#include "boost/accumulators/statistics/mean.hpp"
#include <boost/accumulators/statistics/weighted_mean.hpp>
#include <boost/accumulators/framework/parameters/weight.hpp>
#include <boost/fusion/sequence/intrinsic/at_key.hpp>
#include <boost/fusion/sequence/intrinsic/value_at_key.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "mcplan/stats/ReservoirSampler.hpp"



namespace mcplan {

/**
 * Get a property value for a Node. This function should be used in preference
 * to the 'get()' member function for portability.
 * @param n The node instance
 * @return Same as n.get<Field>()
 */
template<typename Field, typename NodeType>
auto get( NodeType const& n ) -> decltype(std::declval<NodeType>().template get<Field>())
{
    return n.template get<Field>();
}

/**
 * Set a property value for a Node. This function should be used in preference
 * to the 'set()' member function for portability.
 * @param n The node instance
 * @param x The new value
 */
template<typename Field, typename NodeType>
void set( NodeType const& n, decltype(std::declval<NodeType>().template get<Field>()) const& x )
{
    n.template set<Field>( x );
}

// ---------------------------------------------------------------------------

namespace state_data {
    namespace tag {
        struct states               { };
        struct n                    { };
        struct value                { };
        struct avg_return          { };
        struct variance             {};
        struct ravg                 { };
        struct upper_value_bound    { };
        struct lower_value_bound    { };
    }
}
namespace action_data {
    namespace tag {
        struct n                    { };
        struct ravg                 { };
        struct value                { };
        struct avg_return         { };
        struct variance             {};
        struct upper_value_bound    { };
        struct lower_value_bound    { };
    }
}

// ---------------------------------------------------------------------------

namespace node {

    namespace detail {

        template<typename Field, typename NodeType, typename SuccessorType>
        struct argmax_impl
        {
            static std::vector<SuccessorType*> call( NodeType& n )
            {
                using T = decltype(std::declval<SuccessorType>().template get<Field>());

                std::vector<SuccessorType*> max_elements;
                T max_value = std::numeric_limits<T>::lowest();
                bool first = true;
                n.visitSuccessors( [&max_elements, &max_value, &first]( SuccessorType& succ ) {
                    if( first ) {
                        max_elements.push_back( &succ );
                        max_value = mcplan::get<Field>( succ );
                        first = false;
                    }
                    else {
                        T v = mcplan::get<Field>( succ );
                        if( v >= max_value ) {
                            if( v > max_value ) {
                                max_value = v;
                                max_elements.clear();
                            }
                            max_elements.push_back( &succ );
                        }
                    }
                } );
                return max_elements;
            }
        };
    }

    /**
     * Fold a binary function over the successors of 'n'.
     * @param n The node instance
     * @param f :: ResultType -> (value_type of Field) -> ResultType
     * @param x0 Initial value
     * @return
     */
    template<typename Field, typename NodeType, typename FunctionType, typename ResultType>
    ResultType fold( NodeType const& n, FunctionType f, ResultType const& x0 )
    {
        using SuccessorType = typename NodeType::SuccessorType;
        ResultType x = x0;
        n.visitSuccessors( [&x,&f]( SuccessorType const& succ ) {
            x = f( x, mcplan::get<Field>( succ ) );
        } );
        return x;
    }

    template<typename Field, typename NodeType>
    std::vector<typename NodeType::SuccessorType*> argmax( NodeType& n )
    {
        return detail::argmax_impl<Field, NodeType, typename NodeType::SuccessorType>::call(n);
    }

    template<typename Field, typename NodeType>
    std::vector<typename NodeType::SuceesorType const*> argmax( NodeType const& n )
    {
        return detail::argmax_impl<Field, NodeType const, typename NodeType::SuccessorType const>::call(n);
    }

    /**
     * @brief Returns a vector containing the value of Field for each successor
     * of node n.
     */
    template<typename Field, typename NodeType>
    auto successor_property( NodeType const& n )
    -> std::vector<decltype(std::declval<typename NodeType::SuccessorType>().template get<Field>())>
    {
        using SuccessorType = typename NodeType::SuccessorType;
        using T = decltype(std::declval<SuccessorType>().template get<Field>());
        std::vector<T> v;

        n.visitSuccessors( [&v]( SuccessorType& succ ) {
            v.push_back( mcplan::get<Field>( succ ) );
        } );
        return v;
    }

    template<typename StateNodeType>
    auto actions( StateNodeType const& n ) -> std::vector<typename StateNodeType::Domain::ActionType>
    {
        using A = typename StateNodeType::Domain::ActionType;
        std::vector<A> v;

        n.visitSuccessors( [&v]( auto const& succ ) {
            v.push_back( succ.a() );
        } );
        return v;
    }

    /**
     * Stores successors as a <em>bag</em>. Insertion is efficient but
     * duplicate elements will get their own nodes.
     */
    template<typename NodeType>
    class BagStorage
    {
    private:
        using LabelType = typename NodeType::LabelType;
        using Container = std::vector<std::unique_ptr<NodeType>>;

    public:
        typedef typename Container::iterator    iterator;
        typedef typename Container::const_iterator const_iterator;

        template<typename... Args>
        NodeType* addSuccessor( LabelType /*label*/, Args&&... succ )
        {
            successors_.push_back( std::make_unique<NodeType>( std::forward<Args>(succ)... ) );
            return successors_.back().get();
        }

        bool empty() const
        { return successors_.empty(); }

        iterator begin()
        { return successors_.begin(); }

        const_iterator begin() const
        { return successors_.begin(); }

        iterator end()
        { return successors_.end(); }

        const_iterator end() const
        { return successors_.end(); }

        template<typename Functor>
        void visitSuccessors( Functor f ) const
        {
            for( std::unique_ptr<NodeType> const& succ : successors_ ) {
                f( *succ );
            }
        }

    private:
        Container successors_;
    };

    template<typename NodeType>
    class SetStorage
    {
    private:
        using LabelType = typename NodeType::LabelType;
        using Container = boost::container::flat_map<
            typename NodeType::LabelType, std::unique_ptr<NodeType>>;

    public:
        typedef typename Container::iterator    iterator;
        typedef typename Container::const_iterator const_iterator;

        template<typename... Args>
        NodeType* addSuccessor( LabelType label, Args&&... succ )
        {
            iterator itr = successors_.find( label );
            if( itr == successors_.end() ) {
                auto result = successors_.insert( typename Container::value_type(
                    label, std::make_unique<NodeType>( std::forward<Args>(succ)... ) ) );
                itr = result.first;
            }
            return itr->second.get();
        }

        bool empty() const
        { return successors_.empty(); }

        iterator begin()
        { return successors_.begin(); }

        const_iterator begin() const
        { return successors_.begin(); }

        iterator end()
        { return successors_.end(); }

        const_iterator end() const
        { return successors_.end(); }

        template<typename Functor>
        void visitSuccessors( Functor f ) const
        {
            for( auto const& e : successors_ ) {
                std::unique_ptr<NodeType> const& succ = e.second;
                f( *succ );
            }
        }

    private:
        Container successors_;
    };
}

// TODO: We'll want to have concept checks for the data field types.

// TODO: We may want to extend some of these to vector rewards. See:
// http://www.boost.org/doc/libs/1_61_0/doc/html/accumulators/user_s_guide.html
//  #accumulators.user_s_guide.the_accumulators_framework.extending_the_accumulators_framework.operators_ex

// ---------------------------------------------------------------------------
// ActionNode data

namespace action_data {

    template<typename Domain>
    class n
    {
    public:
        typedef tag::n  tag;
        typedef int     value_type;

        n() : n_( 0 ) { }

        template<typename ActionNodeType>
        void sample( ActionNodeType const& /*an*/, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::ActionType const& /*a*/,
                     typename Domain::RewardType /*r*/,
                     int const nsamples = 1 )
        {
            n_ += nsamples;
        }

        template<typename ActionNodeType>
        void backup( ActionNodeType const& /*a*/ )
        { }

        template<typename ActionNodeType>
        void backup( ActionNodeType const& /*a*/, typename Domain::RewardType )
        { }

        value_type get() const
        { return n_; }

        friend std::ostream& operator<<( std::ostream& out, n const& x )
        {
            out << "n: " << x.get();
            return out;
        }

    private:
        int n_;
    };

    /**
     * @brief Average immediate reward.
     * Requires: action_data::tag::n
     */
    template<typename Domain>
    class ravg
    {
    public:
        using tag        = tag::ravg;
        using value_type = typename Domain::RewardType;

        ravg() : ravg_( 0 ) { }

        template<typename ActionNodeType>
        void sample( ActionNodeType const& an, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::ActionType const& /*a*/,
                     typename Domain::RewardType r,
                     int const nsamples = 1 )
        {
            int const n = mcplan::get<mcplan::action_data::tag::n>( an );
            // Incremental average
            ravg_ = (ravg_*(n-nsamples) + r*nsamples) / n;
        }

        template<typename ActionNodeType>
        void backup( ActionNodeType const& /*a*/ )
        { }

        template<typename ActionNodeType>
        void backup( ActionNodeType const& /*a*/, typename Domain::RewardType )
        { }

        value_type get() const
        { return ravg_; }

        friend std::ostream& operator<<( std::ostream& out, ravg const& x )
        {
            out << "ravg: " << x.get();
            return out;
        }

    private:
        value_type ravg_;
    };

    // -----------------------------------------------------------------------

    namespace detail {
        template<typename Domain, typename StateTag>
        class weighted_avg_backup_type
        {
        public:
            typedef typename Domain::RewardType value_type;

            weighted_avg_backup_type()
                : v_( 0 )
            { }

            explicit weighted_avg_backup_type( value_type const& initial_value )
                : v_( initial_value )
            { }

            template<typename ActionNodeType>
            void backup( ActionNodeType const& an )
            {
                value_type const ravg = mcplan::get<action_data::tag::ravg>( an );

                namespace ba = boost::accumulators;
                ba::accumulator_set<
                    value_type, ba::features<ba::tag::count, ba::tag::weighted_mean>, int> acc;
                an.visitSuccessors( [&acc]( typename ActionNodeType::SuccessorType const& sn ) {
                    acc( mcplan::get<StateTag>( sn ),
                         boost::accumulators::weight = mcplan::get<mcplan::state_data::tag::n>( sn ) );
                } );
                if( ba::count( acc ) > 0 ) {
                    v_ = ravg + ba::weighted_mean( acc );
                }
                else {
                    v_ = ravg;
                }
            }

            template<typename ActionNodeType>
            void backup( ActionNodeType const& an, value_type /*traj*/ )
            {
                // TODO: Is this right?
                backup( an );
            }

            value_type get() const
            { return v_; }

        protected:
            value_type v_;
        };
    } // detail

    template<typename Domain>
    class value
        : public detail::weighted_avg_backup_type<Domain, state_data::tag::value>
    {
    public:
        template<typename ActionNodeType>
        void sample( ActionNodeType const& an, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::ActionType const& /*a*/,
                     typename Domain::RewardType /*r*/,
                     int const /*nsamples*/ = 1 )
        {
            this->v_ = mcplan::get<action_data::tag::ravg>( an );
        }
    };

    template<typename Domain>
    std::ostream& operator<<( std::ostream& out, value<Domain> const& x )
    {
        out << "V: " << x.get();
        return out;
    }

    template<typename Domain>
    class upper_value_bound
        : public detail::weighted_avg_backup_type<Domain, state_data::tag::upper_value_bound>
    {
    public:
        template<typename ActionNodeType>
        void sample( ActionNodeType const& an, Domain const& domain,
                     typename Domain::StateType const& s,
                     typename Domain::ActionType const& a,
                     typename Domain::RewardType r,
                     int const nsamples = 1 )
        {
            unsigned int n = mcplan::get<action_data::tag::n>( an );
            auto const U = r + domain.future_value_max( s, a );
            // If this is the first time that 'sample' has been called
            if( n == nsamples ) {
                this->v_ = U;
            }
            else {
                this->v_ = (this->v_*(n-nsamples) + U*nsamples) / n;
            }
        }
    };

    template<typename Domain>
    std::ostream& operator<<( std::ostream& out, upper_value_bound<Domain> const& x )
    {
        out << "U: " << x.get();
        return out;
    }

    template<typename Domain>
    class lower_value_bound
        : public detail::weighted_avg_backup_type<Domain, state_data::tag::lower_value_bound>
    {
    public:
        template<typename ActionNodeType>
        void sample( ActionNodeType const& an, Domain const& domain,
                     typename Domain::StateType const& s,
                     typename Domain::ActionType const& a,
                     typename Domain::RewardType r,
                     int const nsamples = 1 )
        {
            unsigned int n = mcplan::get<action_data::tag::n>( an );
            auto const L = r + domain.future_value_min( s, a );
            // If this is the first time that 'sample' has been called
            if( n == nsamples ) {
                this->v_ = L;
            }
            else {
                this->v_ = (this->v_*(n-nsamples) + L*nsamples) / n;
            }
        }
    };

    template<typename Domain>
    std::ostream& operator<<( std::ostream& out, lower_value_bound<Domain> const& x )
    {
        out << "L: " << x.get();
        return out;
    }

    // -----------------------------------------------------------------------

    template<typename Domain>
    class variance
    {

    public:

        typedef typename Domain::RewardType value_type;

        value_type get() const
        { return variance_; }

        template<typename ActionNodeType>
        void sample( ActionNodeType const& /*an*/, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::ActionType const& /*a*/,
                     typename Domain::RewardType /*r*/,
                     int const /*nsamples*/ = 1 )
        { }

        template<typename ActionNodeType>
        void backup( ActionNodeType const& /*an*/ )
        {
//            namespace ba = boost::accumulators;
//            ba::accumulator_set<value_type, ba::features<ba::tag::mean>, int> acc;
//            an.visitSuccessors( [&acc]( auto const& sn ) {
//                acc( mcplan::get<mcplan::state_data::tag::avg_return>( sn ),
//                     boost::accumulators::weight = mcplan::get<mcplan::state_data::tag::n>( sn ) );
//            } );
//            avg_return_ = ba::mean( acc );
        }

        template<typename ActionNodeType>
        void backup(  ActionNodeType const& an, value_type traj_value )
        {
            double count= mcplan::get<mcplan::action_data::tag::n>( an );
            value_type old_variance=mcplan::get<mcplan::action_data::tag::variance>( an );
            value_type mean = mcplan::get<mcplan::action_data::tag::avg_return>( an );
            value_type old_mean = (mean*count-traj_value)/(count-1);
            if(isnan(old_mean) || isinf(old_mean))
                variance_=0;
            else
            variance_=(old_variance*(count-1)+(traj_value-mean)*(traj_value-old_mean))/(count);

        }

        friend std::ostream& operator<<( std::ostream& out, variance const& x )
        {
            out << "variance: " << x.get();
            return out;
        }

    private:
        value_type variance_;
    };

//-------------------------------------
    template<typename Domain>
    class avg_return
    {

    public:

        typedef typename Domain::RewardType value_type;

        value_type get() const
        { return avg_return_; }

        template<typename ActionNodeType>
        void sample( ActionNodeType const& /*an*/, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::ActionType const& /*a*/,
                     typename Domain::RewardType /*r*/,
                     int const /*nsamples*/ = 1 )
        { }

        template<typename ActionNodeType>
        void backup( ActionNodeType const& /*an*/ )
        {
//            namespace ba = boost::accumulators;
//            ba::accumulator_set<value_type, ba::features<ba::tag::mean>, int> acc;
//            an.visitSuccessors( [&acc]( auto const& sn ) {
//                acc( mcplan::get<mcplan::state_data::tag::avg_return>( sn ),
//                     boost::accumulators::weight = mcplan::get<mcplan::state_data::tag::n>( sn ) );
//            } );
//            avg_return_ = ba::mean( acc );
        }

        template<typename ActionNodeType>
        void backup(  ActionNodeType const& an, value_type traj_value )
        {
            unsigned count=mcplan::get<mcplan::action_data::tag::n>( an );
            value_type old_value=mcplan::get<mcplan::action_data::tag::avg_return>( an );
            avg_return_=(old_value*(count-1)+traj_value)/(count);
        }

        friend std::ostream& operator<<( std::ostream& out, avg_return const& x )
        {
            out << "avg_return: " << x.get();
            return out;
        }

    private:
        value_type avg_return_;
    };
}

// ---------------------------------------------------------------------------
// StateNode data

namespace state_data {

    // -----------------------------------------------------------------------


    template<typename Domain>
    class n
    {
    public:
        typedef tag::n  tag;
        typedef int     value_type;

        n() : n_( 0 ) { }

        template<typename StateNodeType>
        void sample( StateNodeType const& /*sn*/, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::RewardType /*r*/,
                     int const nsamples = 1 )
        {
            n_ += nsamples;
        }

        template<typename StateNodeType>
        void backup( StateNodeType const& /*sn*/ )
        { }

        template<typename StateNodeType>
        void backup( StateNodeType const& /*sn*/,typename Domain::RewardType )
        { }

        template<typename StateNodeType>
        void leaf( StateNodeType const& /*sn*/, typename Domain::RewardType /*r*/ )
        { }

        value_type get() const
        { return n_; }

        friend std::ostream& operator<<( std::ostream& out, n const& x )
        {
            out << "n: " << x.get();
            return out;
        }

    private:
        int n_;
    };

    template<typename Domain>
    class avg_return
    {

    public:
       typedef tag::avg_return  tag;

        typedef typename Domain::RewardType value_type;

        template<typename StateNodeType>
        void sample( StateNodeType const& /*sn*/, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::RewardType /*r*/,
                     int const /*nsamples*/ = 1 )
        { }

        value_type get() const
        { return avg_return_; }

        template<typename StateNodeType>
        void backup( StateNodeType const& /*sn*/ )
        {
//            value_type const ravg = mcplan::get<tag::ravg>( sn );
//            if( sn.isLeaf() ) {
//                avg_return_ = ravg;
//                return;
//            }

//            typename StateNodeType::SuccessorType const* astar = nullptr;
//            sn.visitSuccessors( [&astar]( auto const& an ) {
//                if( !astar || mcplan::get<action_data::tag::avg_return>( an ) > mcplan::get<action_data::tag::avg_return>( *astar ) ) {
//                    astar = &an;
//                }
//            } );
//            value_type const vfuture = mcplan::get<action_data::tag::avg_return>( *astar );
//            avg_return_ = ravg + vfuture;
        }

        template<typename StateNodeType>
        void backup( StateNodeType const& sn, value_type traj_value )
        {
			double count= mcplan::get<mcplan::state_data::tag::n>( sn );
            value_type old_value=mcplan::get<mcplan::state_data::tag::avg_return>( sn );
			avg_return_=(old_value*(count-1)+traj_value)/(count);
        }


        template<typename StateNodeType>
        void leaf( StateNodeType const& sn, typename Domain::RewardType r )
        {
            avg_return_ = mcplan::get<state_data::tag::ravg>( sn ) + r;
        }


        friend std::ostream& operator<<( std::ostream& out, avg_return const& x )
        {
            out << "avg_return: " << x.get();
            return out;
        }

    private:
        value_type avg_return_;
    };

    template<typename Domain>
    class variance
    {

    public:
       typedef tag::variance  tag;

        typedef typename Domain::RewardType value_type;

        variance() : variance_( 0 )
        { }

        template<typename StateNodeType>
        void sample( StateNodeType const& /*sn*/, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::RewardType /*r*/,
                     int const /*nsamples*/ = 1 )
        { }

        value_type get() const
        { return variance_; }

        template<typename StateNodeType>
        void backup( StateNodeType const& /*sn*/ )
        {

        }

        template<typename StateNodeType>
        void backup( StateNodeType const& sn, value_type traj_value )
        {
            double count= mcplan::get<mcplan::state_data::tag::n>( sn );
            value_type old_variance=mcplan::get<mcplan::state_data::tag::variance>( sn );
            value_type mean = mcplan::get<mcplan::state_data::tag::avg_return>( sn );
            value_type old_mean = (mean*count-traj_value)/(count-1);
            if(std::isnan(old_mean) || std::isinf(old_mean))
                variance_=0;
            else
            variance_=(old_variance*(count-1)+(traj_value-mean)*(traj_value-old_mean))/(count);

        }


        template<typename StateNodeType>
        void leaf( StateNodeType const& /*sn*/, typename Domain::RewardType /*r*/ )
        {
            variance_=0;
        }


        friend std::ostream& operator<<( std::ostream& out, variance const& x )
        {
            out << "Variance: " << x.get();
            return out;
        }

    private:
        value_type variance_=0;
    };


    // -----------------------------------------------------------------------

    namespace detail {
        /**
         * @brief Base class for types that implement Bellman backups.
         * Requires: state_data::tag::ravg, action_data::ActionTag
         */
        template<typename Domain, typename ActionTag>
        class bellman_backup_type
        {
        public:
            using value_type = typename Domain::RewardType;

            template<typename StateNodeType>
            void backup( StateNodeType const& sn )
            {
                value_type const ravg = mcplan::get<state_data::tag::ravg>( sn );

                if( sn.isLeaf() ) {
                    v_ = ravg;
                    return;
                }

                typename StateNodeType::SuccessorType const* astar = nullptr;
                sn.visitSuccessors( [&astar,&sn]( typename StateNodeType::SuccessorType const& an ) { //auto const& an ) {
                    if( !astar || mcplan::get<ActionTag>( an ) > mcplan::get<ActionTag>( *astar ) ) {
                        astar = &an;
                    }
                } );
                value_type const vfuture = mcplan::get<ActionTag>( *astar );
                v_ = ravg + vfuture;

            }

            template<typename StateNodeType>
            void backup( StateNodeType const& sn, value_type /*v*/ )
            {
                // TODO: Is this right?
                backup( sn );
            }

            template<typename StateNodeType>
            void leaf( StateNodeType const& sn, typename Domain::RewardType r )
            {
                v_ = mcplan::get<state_data::tag::ravg>( sn ) + r;
            }

            value_type get() const
            { return v_; }
public:
            bellman_backup_type()
            { }

            explicit bellman_backup_type( value_type const& initial_value )
                : v_( initial_value )
            { }

        protected:
            value_type v_;
        };
    }

    template<typename Domain>
    class value
        : public detail::bellman_backup_type<Domain, action_data::tag::value>
    {
    public:
        template<typename StateNodeType>
        void sample( StateNodeType const& sn, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::RewardType /*r*/,
                     int const /*nsamples*/ = 1 )
        {
            this->v_ = mcplan::get<state_data::tag::ravg>( sn );
        }
    };

    template<typename Domain>
    std::ostream& operator<<( std::ostream& out, value<Domain> const& x )
    {
        out << "V: " << x.get();
        return out;
    }

    template<typename Domain>
    class upper_value_bound
        : public detail::bellman_backup_type<Domain, action_data::tag::upper_value_bound>
    {
    public:
        template<typename StateNodeType>
        void sample( StateNodeType const& sn, Domain const& domain,
                     typename Domain::StateType const& s,
                     typename Domain::RewardType r,
                     int const nsamples = 1 )
        {
            unsigned int n = mcplan::get<state_data::tag::n>( sn );
            auto const U = r + domain.future_value_max( s );
            // If this is the first time that 'sample' has been called
            if( n == nsamples ) {
                this->v_ = U;
            }
            else {
                this->v_ = (this->v_*(n-nsamples) + U*nsamples) / n;
            }
        }
    };

    template<typename Domain>
    std::ostream& operator<<( std::ostream& out, upper_value_bound<Domain> const& x )
    {
        out << "U: " << x.get();
        return out;
    }

    template<typename Domain>
    class lower_value_bound
        : public detail::bellman_backup_type<Domain, action_data::tag::lower_value_bound>
    {
    public:
        template<typename StateNodeType>
        void sample( StateNodeType const& sn, Domain const& domain,
                     typename Domain::StateType const& s,
                     typename Domain::RewardType r,
                     int const nsamples = 1 )
        {
            unsigned int n = mcplan::get<state_data::tag::n>( sn );
            auto const L = r + domain.future_value_min( s );
            // If this is the first time that 'sample' has been called
            if( n == nsamples ) {
                this->v_ = L;
            }
            else {
                this->v_ = (this->v_*(n-nsamples) + L*nsamples) / n;
            }
        }
    };

    template<typename Domain>
    std::ostream& operator<<( std::ostream& out, lower_value_bound<Domain> const& x )
    {
        out << "L: " << x.get();
        return out;
    }

    // -----------------------------------------------------------------------

    /**
     * @brief Average immediate reward.
     * Requires: state_data::tag::n
     */
    template<typename Domain>
    class ravg
    {
    public:
        typedef tag::ravg                   tag;
        typedef typename Domain::RewardType value_type;

        template<typename StateNodeType>
        void sample( StateNodeType const& sn, Domain const& /*domain*/,
                     typename Domain::StateType const& /*s*/,
                     typename Domain::RewardType r,
                     int const nsamples = 1 )
        {
            unsigned int n = mcplan::get<mcplan::state_data::tag::n>( sn );
            ravg_ = (ravg_*(n-nsamples) + r*nsamples) / n;
        }

        template<typename StateNodeType>
        void backup( StateNodeType const& /*sn*/ )
        { }

        template<typename StateNodeType>
        void backup( StateNodeType const& /*sn*/ , value_type /*v*/)
        { }

        template<typename StateNodeType>
        void leaf( StateNodeType const& /*sn*/, typename Domain::RewardType /*r*/ )
        { }

        value_type get() const
        { return ravg_; }

        friend std::ostream& operator<<( std::ostream& out, ravg const& x )
        {
            out << "ravg: " << x.get();
            return out;
        }

    private:
        value_type ravg_;
    };

    // -----------------------------------------------------------------------
}


}


#endif
