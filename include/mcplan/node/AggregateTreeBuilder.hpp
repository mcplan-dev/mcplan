#ifndef MCPLAN_NODE_AGGREGATETREEBUILDER_HPP__
#define MCPLAN_NODE_AGGREGATETREEBUILDER_HPP__

#include <set>

#include "boost/assert.hpp"
#include <boost/throw_exception.hpp>
#include "boost/accumulators/accumulators.hpp"
#include "boost/accumulators/statistics/stats.hpp"
#include <boost/accumulators/statistics/weighted_mean.hpp>
#include <boost/accumulators/framework/parameters/weight.hpp>
#include <boost/optional/optional.hpp>
#include "boost/range/adaptor/filtered.hpp"
#include "boost/range/adaptor/map.hpp"
#include "boost/range/iterator_range_core.hpp"

#include "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/TreePrinter.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"

#include "mcplan/Log.hpp"
#include "mcplan/SamplingContext.hpp"

namespace mcplan {

namespace detail {

    template<typename NodeType, typename Function>
    struct PreOrderDfsTraversal
    {
        Function f;
        int depth;

        PreOrderDfsTraversal( Function f )
            : f( f ), depth( 0 )
        { }

        bool enterNode( NodeType& n )
        {
            f( &n, depth );
            depth += 1;
            return true;
        }

        bool enterNode( typename NodeType::SuccessorType& /*n*/ )
        {
            return true;
        }

        void exitNode( NodeType& /*n*/ )
        {
            depth -= 1;
        }

        void exitNode( typename NodeType::SuccessorType& /*n*/ )
        { }
    };

}

// ---------------------------------------------------------------------------

struct EmpiricalSamplingDistribution
{
    template<typename AbstractStateNode>
    static typename AbstractStateNode::GroundNodeType*
    sample_ground_node( SamplingContext& ctx, AbstractStateNode* xn )
    {
        auto const n = mcplan::get<state_data::tag::n>( *xn );
        std::uniform_int_distribution<std::size_t> p( 0, n - 1 );
        std::size_t const r = p( ctx.rng() );
        std::size_t i = 0;
        auto itr = xn->groundStateNodes().begin();
        BOOST_ASSERT( itr != xn->groundStateNodes().end() );
        while( i < n ) {
            i += mcplan::get<state_data::tag::n>(**itr);
            if( i > r ) {
                break;
            }
            std::advance( itr, 1 );
        }
        return *itr;
    }
};

template<typename SamplingDistribution = EmpiricalSamplingDistribution>
struct IIDSparseSamplingStrategy
{
    /**
     * @brief sparseSampleIID
     * @param ctx
     * @param domain
     * @param xn
     * @param yn
     * @param width
     * @post get<n>(*yn) == width || ctx.budget().expired()
     */
    template<typename TreeBuilder>
    static void sparse_sample( TreeBuilder* builder, SamplingContext& ctx,
                               typename TreeBuilder::Domain const& domain,
                               typename TreeBuilder::AbstractStateNode* xn,
                               typename TreeBuilder::AbstractActionNode* yn,
                               std::size_t const width )
    {
        while( mcplan::get<mcplan::action_data::tag::n>( *yn ) < width &&
               !ctx.budget().expired() ) {
            auto* sn = SamplingDistribution::sample_ground_node( ctx, xn );
            auto* an = sn->addSuccessor( yn->a() );
            builder->sampleGroundTransition( ctx, domain, yn, sn, an );
            ctx.budget().onSample();
        }
    }
};

struct CeilingSparseSamplingStrategy
{
    /**
     * @brief sparseSampleIID
     * @param ctx
     * @param domain
     * @param xn
     * @param yn
     * @param width
     * @post get<n>(*yn) == width || ctx.budget().expired()
     */
    template<typename TreeBuilder>
    static void sparse_sample( TreeBuilder* builder, SamplingContext& ctx,
                               typename TreeBuilder::Domain const& domain,
                               typename TreeBuilder::AbstractStateNode* xn,
                               typename TreeBuilder::AbstractActionNode* yn,
                               std::size_t const width )
    {
        using ActionType = typename TreeBuilder::ActionType;
        using GroundStateNode = typename TreeBuilder::GroundStateNode;
        ActionType const a = yn->a();
        int k = static_cast<int>(
            std::ceil( width / static_cast<double>(xn->NgroundStateNodes()) ));
        while( k * xn->NgroundStateNodes() < width ) {
            k += 1;
            if( k < 0 ) {
                BOOST_THROW_EXCEPTION( std::overflow_error( "'width' is too large" ) );
            }
        }
        while( mcplan::get<action_data::tag::n>(*yn) < width ) {
            bool expired = false;
            for( GroundStateNode* sn : xn->groundStateNodes() ) {
                if( ctx.budget().expired() ) {
                    expired = true;
                    break;
                }
                auto* an = sn->addSuccessor( a );
                if( mcplan::get<action_data::tag::n>(*an) < k ) {
                    builder->sampleGroundTransition( ctx, domain, yn, sn, an );
                    ctx.budget().onSample();
                }
            }
            if( expired ) {
                break;
            }
        }
    }
};


// TODO: Currently 'AggregateTreeBuilder' is a misleading name because this
// class assumes that the abstraction is specifically an AbstractionDiagram,
// whereas Aggregate(Action|State)Node is agnostic. Should either rename, or
// formalize an AbtractionConcept and change AbstractionDiagramT -> AbstractionT
template<typename DomainT, typename AbstractionDiagramT, typename Data,
         typename SparseSamplingStrategy = void>
class AggregateTreeBuilder
{    
public:
    using Domain = DomainT;
    using Abstraction = AbstractionDiagramT;

    using StateType     = typename Domain::StateType;
    using ActionType    = typename Domain::ActionType;
    using ActionSet     = typename Domain::template ActionSet<ActionType>; //boost::container::flat_set<ActionType>;

    using AbstractActionLabel   = typename Abstraction::AbstractActionLabel;
    using AbstractStateLabel    = typename Abstraction::AbstractStateLabel;
    using ParametersType        = typename Domain::ParametersType;
    using RewardType            = typename Domain::RewardType;
    using SimulatorType         = typename Domain::SimulatorType;
    using TreeDomain            = typename Abstraction::AliasAbstractDomain;
    using ActionNodeType        = AggregateActionNode<Domain, Abstraction, Data>;
    using StateNodeType         = AggregateStateNode<Domain, Abstraction, Data>;

//    using LabelSet =typename boost::container::flat_set<LabelType>;

    using GroundStateNode       = typename StateNodeType::GroundNodeType;
    using GroundActionNode      = typename ActionNodeType::GroundNodeType;
    using AbstractStateNode     = StateNodeType;
    using AbstractActionNode    = ActionNodeType;

private:
    using VertexToStateNodeMap = std::multimap<AbstractStateLabel, AbstractStateNode*>;

public:
    AggregateTreeBuilder( Domain const* domain, SimulatorType const& sim,
                          std::unique_ptr<Abstraction> g, std::unique_ptr<StateType> s0,
                          RewardType r0 = RewardType(0) )
        : sim_( sim ), g_( std::move(g) ),
          gsn0_( new GroundStateNode( std::move(s0) ) ),
          asn0_( new AbstractStateNode( g_->successor( g_->start_node(), gsn0_->s() ),
                                        nullptr /*no parent node*/ ) )
    {
        asn0_->sample( gsn0_.get(), *domain, gsn0_->s(), r0 );

        MCPLAN_LOG_TRACE(( std::cout << "Initialized AggregateTreeBuilder:" \
                           << std::endl << *g_ << std::endl ));
    }

    /**
     * @brief treeDomain
     * @deprecated
     * @param domain
     * @return
     */
    TreeDomain treeDomain( Domain const& domain ) const
    { return abstraction::AbstractDomain<Domain, Abstraction>( g_.get() ); }

    // TODO: Should these be part of AbstractTreeBuilder concept? (Probably
    // called 'abstraction()' in that context)
    Abstraction* ad()
    { return g_.get(); }

    Abstraction const* ad() const
    { return g_.get(); }

    // -----------------------------------------------------------------------
    // TreeBuilder concept

    SimulatorType const& simulator() const
    { return sim_; }

    StateNodeType* rootStateNode()
    { return asn0_.get(); }

    StateNodeType const* rootStateNode() const
    { return asn0_.get(); }

    /**
     * @brief The legal actions in state node 'sn'.
     * @param domain
     * @param sn
     * @return
     */
    ActionSet actions( Domain const* domain, StateNodeType const* sn )
    {
        ActionSet u = unconstrainedActions( domain, sn );
        if( u.empty() ) {
            return u;
        }
        auto legal = u | boost::adaptors::filtered( [this, sn]( auto const& a ) {
            return static_cast<bool>(g_->successor( sn->label(), a ));
        });
        return ActionSet( legal.begin(), legal.end() );
    }

    /**
     * @brief The legal actions in 'sn' for which no successors
     * of 'sn' currently exist.
     * @param domain
     * @param sn
     * @return
     */
    ActionSet missingActions( Domain const* domain, StateNodeType const* sn )
    {
        std::vector<ActionType> existing;
        sn->visitSuccessors( [&existing]( auto const& an ) {
            existing.push_back( an.a() );
        });
        std::sort( existing.begin(), existing.end() );
        auto as = actions( domain, sn );
        typename Domain::template ActionSet<ActionType> diff;
        std::set_difference( as.begin(), as.end(), existing.begin(), existing.end(),
                             std::inserter( diff, diff.end() ) );
        return diff;
    }

    AbstractActionNode* addSuccessor( StateNodeType* sn, ActionType a )
    {
        AbstractActionLabel const y = *(g_->successor( sn->label(), a ));
        return sn->addSuccessor( a, y );
    }

    AbstractStateNode* addSuccessor( ActionNodeType* yn, AbstractStateLabel xprime )
    {
        auto abstract_succ_pair = yn->addSuccessor( xprime );
//        if( abstract_succ_pair.second ) {
//            x_to_asn_.insert( {xprime, abstract_succ_pair.first} );
//        }
        return abstract_succ_pair.first;
    }

    RewardType evaluateLeaf( Domain const* domain, StateNodeType const* sn ) const
    {
        // TODO: Make the method of combining heuristics customizable
        namespace ba = boost::accumulators;
        ba::accumulator_set<
            RewardType, ba::features<ba::tag::count, ba::tag::weighted_mean>, int> acc;
        for( auto const* gsn : sn->groundStateNodes() ) {
            acc( domain->evaluateState( gsn->s() ),
                 boost::accumulators::weight = mcplan::get<mcplan::state_data::tag::n>( *gsn ) );
        }
        BOOST_ASSERT( ba::count( acc ) > 0 );
        return ba::weighted_mean( acc );
    }

    /**
     * @brief sparseSample
     * @param ctx
     * @param domain
     * @param xn
     * @param yn
     * @param width
     * @post get<n>(*yn) >= width || ctx.budget().expired()
     */
    void sparseSample( SamplingContext& ctx, Domain const& domain,
                       AbstractStateNode* xn, AbstractActionNode* yn, std::size_t const width )
    {
        SparseSamplingStrategy::sparse_sample( this, ctx, domain, xn, yn, width );
    }

    void sampleTransition( SamplingContext& ctx, Domain const& domain,
                           AbstractStateNode* xn, AbstractActionNode* yn )
    {
        // FIXME: Sampling distribution should be customizable
        auto* sn = EmpiricalSamplingDistribution::sample_ground_node( ctx, xn );
        auto* an = sn->addSuccessor( yn->a() );
        sampleGroundTransition( ctx, domain, yn, sn, an );
    }

    void sampleGroundTransition( SamplingContext& ctx, Domain const& domain,
                                 AbstractActionNode* yn,
                                 GroundStateNode* sn, GroundActionNode* an )
    {
        BOOST_ASSERT( yn->a() == an->a() );
        auto const a = yn->a();
        auto tr = sim_.sampleTransition( ctx, domain, sn->s(), a );
        auto xprime = g_->successor( yn->label(), std::get<1>(tr).s() );
        AbstractStateNode* xn_prime = addSuccessor( yn, xprime );
        GroundStateNode* sn_prime = an->addSuccessor( std::get<1>(tr).transfer_s() );
        yn->sample( an, domain, sn->s(), std::get<0>(tr).a(), std::get<0>(tr).r() );
        xn_prime->sample( sn_prime, domain, sn_prime->s(), std::get<1>(tr).r() );
    }

    template<typename TreePolicy, typename Evaluator>
    void sampleTrajectory( SamplingContext& ctx, Domain const& domain,
                           AbstractStateNode& x0,
                           TreePolicy& tree_policy, Evaluator eval, int depth ,int /*times*/)
    {

        auto& sn0 = x0.sampleState( ctx );
        //x0.sample(&sn0,sn0.s(),0);
        bool node_added=false;



        RewardType ret = sampleTrajectoryImpl( ctx, domain, x0, sn0, tree_policy, eval, node_added, depth );

        sn0.backup( ret );
        x0.backup( ret );
    }

    // -----------------------------------------------------------------------
    // AbstractTreeBuilder concept

    /**
     * @brief The actions that would be legal in 'sn' if there were
     * no constraints imposed by the abstraction.
     * @param domain
     * @param sn
     * @return
     */
    ActionSet unconstrainedActions( Domain const* domain, StateNodeType const* sn )
    {
        // TODO: We're hardcoding the "intersection of action sets" model here
        auto const& ground_nodes = sn->groundStateNodes();
        if( ground_nodes.empty() ) {
            return ActionSet();
        }
        else {
            ActionSet inter;
            for( auto gsn : sn->groundStateNodes() ) {
                auto as = domain->actions( gsn->s() );

                if( inter.empty() ) {
                    inter.insert( as.begin(), as.end() );
                }
                else {
                    ActionSet tmp = std::move(inter);
                    inter = ActionSet();
                    std::set_intersection( tmp.begin(), tmp.end(), as.begin(), as.end(),
                                           std::inserter( inter, inter.end() ) );
                }
            }
            return inter;
        }
    }

    /**
     * @brief The set difference 'unconstrainedActions() - actions()'.
     * @param domain
     * @param sn
     * @return
     */
    ActionSet prunedActions( Domain const* domain, StateNodeType const* sn )
    {
        ActionSet u = unconstrainedActions( domain, sn );
        if( u.empty() ) {
            return u;
        }
        auto pruned = u | boost::adaptors::filtered( [this, sn]( auto const& a ) {
            return !static_cast<bool>(g_->successor( sn->label(), a ));
        });
        return ActionSet( pruned.begin(), pruned.end() );
    }

    /**
     * @brief Computes the results of unconstrainedActions(), actions(), and
     * prunedActions() simultaneously. Substantially more efficient than
     * doing them separately.
     */
    template<typename OutItr1, typename OutItr2, typename OutItr3>
    void partitionActions( Domain const* domain, StateNodeType const* sn,
                           OutItr1 out_unconstrained,
                           OutItr2 out_constrained,
                           OutItr3 out_pruned )
    {
        auto const u = unconstrainedActions( domain, sn );
        if( u.empty() ) {
            return;
        }
        for( auto const& a : u ) {
            *out_unconstrained++ = a;
            bool const keep = static_cast<bool>(g_->successor( sn->label(), a ));
            if( keep ) {
                *out_constrained++ = a;
            }
            else {
                *out_pruned++ = a;
            }
        }
    }

    template<typename Function>
    void visitAbstractStateNodes( Function f )
    {
        depth_first_traversal(
            *asn0_, detail::PreOrderDfsTraversal<StateNodeType, Function>( f ) );
    }

    template<typename Function>
    void visitAbstractStateNodes( Function f ) const
    {
        depth_first_traversal(
            *asn0_, detail::PreOrderDfsTraversal<StateNodeType const, Function>( f ) );
    }

    // TODO: Do we want to have the key-lookup versions below?

//    boost::iterator_range<AbstractStateNode*> abstractStateNodes( AbstractStateLabel x )
//    {
//        auto r = x_to_asn_.equal_range( x );
//        return boost::make_iterator_range( r.first, r.second );
//    }

//    boost::iterator_range<AbstractStateNode const*> abstractStateNodes( AbstractStateLabel x ) const
//    {
//        auto r = x_to_asn_.equal_range( x );
//        return boost::make_iterator_range( r.first, r.second );



    // -----------------------------------------------------------------------
    // RefineableTreeBuilder concept

    /**
     * @brief Rebuilds the subtrees rooted at parent->successor(x) for x in xs.
     * Generally, you will call this function after altering the abstraction
     * diagram so that parent->successor(x) is split into multiple nodes.
     * @param parent
     * @param xs
     * @return The set of state nodes that are roots of rebuilt subtrees
     * (i.e. their parent is 'parent' and they were created or altered by this function).
     */
    std::set<AbstractStateNode*>
    rebuildStateSubtree( Domain const& domain, AbstractActionNode* const parent,
                         std::initializer_list<AbstractStateLabel> xs )
    {
        std::stack<std::tuple<GroundStateNode*, AbstractActionNode*>> gsn_stack;
        for( AbstractStateLabel x : xs ) {
            // Remove the abstract subtree below 'parent'
            std::unique_ptr<AbstractStateNode> old_root = parent->detachSuccessor( x );
            // Prepare to divide the GSN trees that were in 'old_root'
            for( auto gsn : old_root->groundStateNodes() ) {
                gsn_stack.push( std::make_tuple(gsn, parent) );
            }
            // FIXME: Need to update x_to_asn_ map

            old_root.reset(); // Free memory from old abstract subtree
        }
        // Build the abstract subtrees by traversing the ground subtrees in
        // depth-first order and applying the abstraction
        std::set<AbstractStateNode*> roots;
        while( !gsn_stack.empty() ) {
            GroundStateNode* gsn;
            AbstractActionNode* aan_parent;
            std::tie(gsn, aan_parent) = gsn_stack.top();
            gsn_stack.pop();

            AbstractStateLabel const xnew = g_->successor( aan_parent->label(), gsn->s() );
            // TODO: Here is where we would add a new entry to x_to_asn
            AbstractStateNode* asn_new = aan_parent->addSuccessor( xnew ).first;
            asn_new->addNode( gsn, domain );
            // If the new node is a direct descendant of the parent of the
            // removed node(s), it is the root of a rebuilt subtree.
            if( aan_parent == parent ) {
                roots.insert( asn_new );
            }

            gsn->visitSuccessors( [&]( auto& gan ) {
                boost::optional<AbstractActionLabel> const maybe_ynew = g_->successor( xnew, gan.a() );
                BOOST_ASSERT( maybe_ynew );
                AbstractActionNode* aan_new = asn_new->addSuccessor( gan.a(), *maybe_ynew );
                aan_new->addNode( gsn, &gan, domain );
                gan.visitSuccessors( [&]( auto& gsn_succ ) {
                    gsn_stack.push( std::make_tuple(&gsn_succ, aan_new) );
                });
            });
        }
        return roots;
    }

private:

    template<typename TreePolicy, typename Evaluator>
    RewardType sampleTrajectoryImpl( SamplingContext& ctx, Domain const& domain,
                               AbstractStateNode& xn, GroundStateNode& sn,
                               TreePolicy& tree_policy, Evaluator eval, bool& node_added, int depth )
    {

        if(depth ==0 || sn.isTerminal( domain.parameters() ) ) {

            return eval(sn.s(),depth);
        }

        auto maybe_y = tree_policy( xn )( ctx );

        if(!maybe_y ) {

            RewardType ret = eval( sn.s() ,depth);
            return ret;
        }
        else {

            auto y = *maybe_y;

            auto a = g_->sampleAction( ctx, xn.label(), y );

            auto& an = sn.addSuccessor( a );

            auto& yn = xn.addSuccessor( y );



            auto tr = sim_.sampleTransition( ctx, domain.parameters(), sn.s(), a );


            ctx.budget().onSample();

            auto const& sprime = std::get<1>(tr).s();

            auto xprime = g_->successor( yn.label(), sprime );

            auto& abstract_succ = yn.addSuccessor( xprime );

            auto& ground_succ = an.addSuccessor( sprime, std::get<1>(tr).transfer_s() );
            RewardType ret =0;
            yn.sample( &an, domain, sn.s(), std::get<0>(tr).a(), std::get<0>(tr).r() );
            abstract_succ.sample( &ground_succ, domain, sprime, std::get<1>(tr).r() );

            if(!ctx.budget().expired())
                ret = sampleTrajectoryImpl( ctx, domain, abstract_succ, ground_succ, tree_policy, eval,node_added,depth-1 );

            ret += std::get<1>(tr).r();
            ground_succ.backup( ret );
            abstract_succ.backup( ret );
            // Action nodes
            ret += std::get<0>(tr).r();
            an.backup( ret );
            yn.backup( ret );
            // If A NODE IS VISITED K TIMES , SPLIT THAT NODE
//            if((mcplan::get<state_data::tag::n>( abstract_succ ))%3 ==0)
//            {
//                abstraction::UnpruneAction<Domain,StateNodeType,ActionNodeType,Diagram> s(abstract_succ,ctx,g_,true);
//               // std::cout<<"Split done\n";
//            }

            return ret;
        }
    }

private:
    SimulatorType sim_;
    std::unique_ptr<Abstraction> g_;

    std::unique_ptr<GroundStateNode> gsn0_;
    std::unique_ptr<AbstractStateNode> asn0_;

//    VertexToStateNodeMap x_to_asn_;
};



template<typename DomainT, typename AbstractionT, typename SparseSamplingStrategy = void>
class AbstractSearchSpace
{
public:
    using Domain = DomainT;
    using Abstraction = AbstractionT;

    template<typename NodeData>
    using TreeBuilderType = AggregateTreeBuilder<Domain, Abstraction, NodeData, SparseSamplingStrategy>;

    using StateType = typename Domain::StateType;
    using RewardType = typename Domain::RewardType;
    using SimulatorType = typename Domain::SimulatorType;
    using InitialAbstractionFactory = std::function<std::unique_ptr<Abstraction>(Domain const*)>;

    AbstractSearchSpace( SimulatorType const& sim, InitialAbstractionFactory init )
        : sim_( sim ), init_( init )
    { }

    template<typename NodeData>
    TreeBuilderType<NodeData> createTreeBuilder( Domain const* domain,
                                                 std::unique_ptr<StateType> s0,
                                                 RewardType r0 = RewardType(0) ) const
    {
        return TreeBuilderType<NodeData>( domain, sim_, init_( domain ), std::move(s0), r0 );
    }

private:
    SimulatorType sim_;
    InitialAbstractionFactory init_;
};


}


#endif
