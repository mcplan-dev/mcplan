#ifndef MCPLAN_NODE_GROUNDTREEBUILDER_HPP
#define MCPLAN_NODE_GROUNDTREEBUILDER_HPP


#include "mcplan/sim/TrajectorySimulator.hpp"
#include "mcplan/sim/TransitionSimulator.hpp"
#include "mcplan/node/GroundActionNode.hpp"
#include "mcplan/node/GroundStateNode.hpp"


#include <stack>

namespace mcplan {


template<typename DomainT, typename Data>
class GroundTreeBuilder
{
public:
    using Domain = DomainT;
    using StateType     = typename Domain::StateType;
    using ActionType    = typename Domain::ActionType;
    using ActionSet     = typename boost::container::flat_set<ActionType>;
    using RewardType    = typename Domain::RewardType;
    using SimulatorType = typename Domain::SimulatorType;

    using TreeDomain = Domain;
    using StateNodeType = GroundStateNode<Domain, Data>;
    using ActionNodeType = GroundActionNode<Domain, Data>;

    /**
     * @deprecated
     * @brief treeDomain
     * @param dom
     * @return
     */
    Domain treeDomain(Domain dom) const
    { return dom; }

    explicit GroundTreeBuilder( Domain const* domain, SimulatorType const& sim,
                                std::unique_ptr<StateType> s0, RewardType r0 = RewardType(0) )
        : sim_( sim ), gsn0_( new StateNodeType( std::move(s0) ) )
    {
        gsn0_->sample( *domain, gsn0_->s(), r0 );
    }

    StateNodeType* rootStateNode()
    { return gsn0_.get(); }

    StateNodeType const* rootStateNode() const
    { return gsn0_.get(); }

    ActionSet actions( Domain const* domain, StateNodeType const* sn )
    {
        return domain->actions( sn->s() );
    }

    ActionSet missingActions( Domain const* domain, StateNodeType const* sn )
    {
        std::vector<ActionType> existing;
        sn->visitSuccessors( [&existing]( auto const& an ) {
            existing.push_back( an.a() );
        });
        std::sort( existing.begin(), existing.end() );
        auto as = actions( domain, sn );
        typename Domain::template ActionSet<ActionType> diff;
        std::set_difference( as.begin(), as.end(), existing.begin(), existing.end(),
                             std::inserter( diff, diff.end() ) );
        return diff;
    }

    SimulatorType const& simulator() const
    { return sim_; }

    RewardType evaluateLeaf( Domain const* domain, StateNodeType const* sn ) const
    {
        return domain->evaluateState( sn->s() );
    }

    void addSuccessor( StateNodeType* sn, ActionType a )
    {
        sn->addSuccessor( a );
    }

    StateNodeType* extendTrajectory(SamplingContext& ctx, GroundStateNode<Domain, Data>& x,GroundActionNode<Domain, Data>& y ) const
    {

        auto tr = sim_.sampleTransition( ctx, x.currentState_(), y.label() );
        auto s_prime=std::get<1>(tr).s();
        GroundStateNode<Domain, Data>* * sn_prime;
        bool state_found=false;
        y.visitSuccessors([&sn_prime,&s_prime,&state_found](auto &sn_next)
        {

            if(sn_next.stateExists_(s_prime))
            {
                sn_prime=&sn_next;
                state_found=true;
            }
        }
        );
        if(state_found==false)
            sn_prime = &y.addSuccessor( std::get<1>(tr).transfer_s() );
        sn_prime->setCurrentStateIndex_(s_prime);


        return sn_prime;
    }

    std::enable_if_t<mcplan::sim::is_transition_simulator<SimulatorType>::value>
    sparseSample( SamplingContext& ctx, Domain const& domain,
                  StateNodeType* sn, ActionNodeType* an, int const width ) const
    {
        while( mcplan::get<mcplan::action_data::tag::n>( *an ) < width &&
               !ctx.budget().expired() ) {
            sampleTransition( ctx, domain, sn, an );
            ctx.budget().onSample();
        }
    }

    std::enable_if_t<mcplan::sim::is_transition_simulator<SimulatorType>::value>
    sampleTransition( SamplingContext& ctx, Domain const& domain,
                      StateNodeType* sn, ActionNodeType* an ) const
    {
        auto tr = sim_.sampleTransition( ctx, domain, sn->s(), an->a() );
        an->sample( domain, sn->s(), std::get<0>(tr).a(), std::get<0>(tr).r() );
        auto* succ = an->addSuccessor( std::get<1>(tr).transfer_s() );
        succ->sample( domain, succ->s(), std::get<1>(tr).r() );
    }

    struct Consumer : public mcplan::sim::TrajectoryConsumer<Domain>
    {
        Domain domain;
        std::stack<std::tuple<StateNodeType*, RewardType>>  sn_stack;
        std::stack<std::tuple<ActionNodeType*, RewardType>> an_stack;
        typename Domain::RewardType rollout;
        double rollout_discount;
        bool in_tree;

        Consumer( Domain const& domain, StateNodeType* sn0 )
            : domain( domain ), rollout( 0 ), rollout_discount( 1 ), in_tree( true )
        {
            sn_stack.push( sn0 );
        }

        void consume( StateSample<Domain>&& ss ) override
        {
            if( in_tree ) {
                StateNodeType& sn = an_stack.top().addSuccessor( ss.s() );
                sn_stack.push( {&sn, ss.r()} );
                sn.sample( ss.s(), ss.r() );
                if( mcplan::get<state_data::tag::n>( sn ) == 1 ) {
                    in_tree = false;
                    rollout = 0;
                }
            }
            else {
                rollout += rollout_discount * ss.r();
                rollout_discount *= domain.discount();
            }
        }

        void consume( ActionSample<Domain>&& as ) override
        {
            if( in_tree ) {
                ActionNodeType& an = sn_stack.top().addSuccessor( as.a() );
                an_stack.push( {&an, as.r()} );
                an.sample( as.a(), as.r() );
            }
            else {
                rollout += rollout_discount * as.r();
            }
        }

        // Performs a backup along the path to the root
        // TODO: Should we wait to do the 'sample()' operations here (instead
        // of in 'consume()')? Doing so would make the update "atomic".
        void onTrajectoryEnd() override
        {
            while( !sn_stack.empty() ) {
                // Update state node s'
                StateNodeType* sn;
                RewardType sr;
                std::tie(sn, sr) = sn_stack.top();
                sn_stack.pop();
                if( sn->isLeaf() ) {
                    sn->leaf( rollout );
                }
                else {
                    sn->backup( rollout );
                }
                rollout = domain.discount() * (rollout + sr);

                if( !an_stack.empty() ) {
                    // Update action node a
                    ActionNodeType* an;
                    RewardType ar;
                    std::tie(an, ar) = an_stack.top();
                    an_stack.pop();
                    an->backup( rollout );
                    rollout += ar;
                }
            }
            assert( an_stack.empty() );
        }
    };

//    template<typename TrajectorySamplingPolicy, typename Evaluator>
//    std::enable_if_t<mcplan::sim::is_trajectory_simulator<SimulatorType>::value>
//    sampleTrajectory( SamplingContext& ctx, typename Domain::ParametersType const& parameters,
//                      GroundStateNode<Domain, Data>& sn0, TrajectorySamplingPolicy const& pi,
//                      Evaluator eval )
//    {
////        SamplingContext& ctx, TrajectoryConsumer<Domain>& consumer,
////                                   ParametersType const& params, StateType const& s0,
////                                   Policy<Domain> const& pi,int depth_limit ) const
//        sim_.sampleTrajectory( ctx, consumer, parameters, sn0.s(), pi, depth_limit );

//    }

    template<typename TreePolicy, typename Evaluator>
    void sampleTrajectory( SamplingContext& ctx, Domain const& domain,
                           GroundStateNode<Domain, Data>& sn0,
                           TreePolicy& tree_policy, Evaluator eval, int depth, int /*times*/ )
    {
        sn0.sample(domain, sn0.s(),0);
        bool node_added=false;
        RewardType ret = sampleTrajectoryImpl( ctx, domain, sn0, tree_policy, eval,node_added,depth );

        sn0.backup( ret );

    }

private:
    template<typename TreePolicy, typename Evaluator>
    RewardType sampleTrajectoryImpl( SamplingContext& ctx, Domain const& domain,
                                GroundStateNode<Domain, Data>& sn,
                               TreePolicy& tree_policy, Evaluator eval, bool& node_added, int depth )
    {

        if(depth ==0 || sn.isTerminal( domain.parameters() ))
            return eval(sn.s(),depth);
        auto maybe_a = tree_policy( sn )( ctx );

        if( !maybe_a ) {
            // Switch to evaluation

            RewardType ret = eval( sn.s(),depth );
            return ret;
        }
        else {

            auto a = *maybe_a;

            auto& an = sn.addSuccessor( a );

            auto tr = sim_.sampleTransition( ctx, domain.parameters(), sn.s(), an.a() );
            ctx.budget().onSample();
            auto const& sprime = std::get<1>(tr).s();
            auto& succ = an.addSuccessor( sprime, std::get<1>(tr).transfer_s() );
            an.sample( std::get<0>(tr).a(), std::get<0>(tr).r() );
            succ.sample( domain, succ.s(), std::get<1>(tr).r() );


            RewardType ret=0;
            if(!ctx.budget().expired())
            {
                ret = sampleTrajectoryImpl( ctx, domain, succ, tree_policy, eval,node_added,depth-1 );


            }



            // State succ
            ret += std::get<1>(tr).r();
            succ.backup( ret );

            // Action nodes
            ret += std::get<0>(tr).r();
            an.backup( ret );

            return ret;
        }
    }

private:
    SimulatorType sim_;
    std::unique_ptr<StateNodeType> gsn0_;
};

// ---------------------------------------------------------------------------

template<typename Domain>
class GroundSearchSpace
{
public:
    template<typename NodeData>
    using TreeBuilderType = GroundTreeBuilder<Domain, NodeData>;

    using StateType = typename Domain::StateType;
    using RewardType = typename Domain::RewardType;
    using SimulatorType = typename Domain::SimulatorType;
    using LeafHeuristic = std::function<RewardType(StateType const&)>;

    explicit GroundSearchSpace( SimulatorType const& sim )
        : sim_( sim )
    { }

    template<typename NodeData>
    TreeBuilderType<NodeData> createTreeBuilder( Domain const* domain,
                                                 std::unique_ptr<StateType> s0,
                                                 RewardType r0 = RewardType(0) ) const
    {
        return TreeBuilderType<NodeData>( domain, sim_, std::move(s0), r0 );
    }

private:
    SimulatorType sim_;
};


}


#endif
