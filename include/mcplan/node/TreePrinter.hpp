#ifndef MCPLAN_NODE_TREEPRINTER_HPP__
#define MCPLAN_NODE_TREEPRINTER_HPP__

#include "mcplan/algorithm/TreeTraversal.hpp"
#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/NodeData.hpp"

#include <iomanip>
#include <ostream>


namespace mcplan { namespace node {

class TreePrinter
{
public:
    static constexpr int default_indent_size = 2;

    TreePrinter()
        : TreePrinter( std::cout )
    { }

    explicit TreePrinter( int indent_size )
        : TreePrinter( std::cout, indent_size )
    { }

    explicit TreePrinter( std::ostream& out, int indent_size = default_indent_size )
        : out_( out ), indent_size_( indent_size ), indent_( 0 )
    { }

    template<typename NodeType>
    bool enterNode( NodeType const& n )
    {
        out_ << std::setw( indent_ ) << "" << n << std::endl;
        indent_ += indent_size_;
        return true;
    }

    template<typename NodeType>
    void exitNode( NodeType const& /*n*/ )
    {
        indent_ -= indent_size_;
    }

private:
    std::ostream& out_;
    int indent_size_;
    int indent_;
};

template<typename... Args>
void printSearchTree( std::ostream& out, AggregateStateNode<Args...> const& sn )
{
    out << "=== Abstract SS tree ===" << std::endl;
    depth_first_traversal( sn, mcplan::node::TreePrinter() );
    out << "Value: " << mcplan::get<state_data::tag::value>( sn ) << std::endl;
    out << "=== Ground SS tree ===" << std::endl;
    depth_first_traversal( **sn.groundStateNodes().begin(), mcplan::node::TreePrinter() );
    out << "Value: " << mcplan::get<state_data::tag::value>( **sn.groundStateNodes().begin() ) << std::endl;
}

template<typename... Args>
void printSearchTree( std::ostream& out, GroundStateNode<Args...> const& sn )
{
    out << "=== Ground SS tree ===" << std::endl;
    depth_first_traversal( sn, mcplan::node::TreePrinter() );
    out << "Value: " << mcplan::get<state_data::tag::value>( sn ) << std::endl;
}

}}

#endif
