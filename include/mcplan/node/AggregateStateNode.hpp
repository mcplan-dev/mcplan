#ifndef MCPLAN_NODE_AGGREGATESTATENODE_HPP__
#define MCPLAN_NODE_AGGREGATESTATENODE_HPP__

#include <memory>
#include <sstream>
//#include <type_traits>
#include <vector>

#include <boost/container/flat_map.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/fusion/sequence/intrinsic/at_key.hpp>
#include <boost/fusion/sequence/intrinsic/value_at_key.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "mcplan/SamplingContext.hpp"
// Not included: "mcplan/node/AggregateActionNode.hpp"
#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"

namespace mcplan {

// ---------------------------------------------------------------------------

namespace detail {
    auto transformed_state_range = [](auto const& container) {
        using namespace boost::adaptors;
        return container | transformed( []( auto p ) { return p->s(); } );
    };

    struct get_state_from_pointer
    {
        template<typename Node>
        auto operator()( Node const& p ) -> decltype(p->s())
        {
            return p->s();
        }
    };

    template<typename Container>
    using StateRange = boost::transformed_range<get_state_from_pointer, Container>;
}

// TODO: Why do we need this forward declaration?
template<
    typename Domain, typename Abstraction, typename Data,
    typename GroundStateNodeType
>
class AggregateActionNode;

template<
    typename DomainT, typename Abstraction, typename Data,
    typename GroundStateNodeType = GroundStateNode<DomainT, Data>
>
class AggregateStateNode
{
public:
    using Domain        = DomainT;
    using LabelType     = typename Abstraction::AbstractStateLabel;
    using ThisType      = AggregateStateNode<Domain, Abstraction, Data, GroundStateNodeType>;

    using ParametersType = typename Domain::ParametersType;
    using StateType     = typename Domain::StateType;
    using ActionType    = typename Domain::ActionType;
    using RewardType    = typename Domain::RewardType;
    using SuccessorType = AggregateActionNode<Domain, Abstraction, Data, GroundStateNodeType>;

    using StateData     = typename Data::StateData;
    using ActionData    = typename Data::ActionData;
    using GroundNodeType    = GroundStateNodeType;
    using GroundSuccessorType = typename GroundNodeType::SuccessorType;
    using GroundStateType = typename Domain::GroundStateType;

    using StatePointer  = std::shared_ptr<StateType const>;

private:
    using Container     = boost::container::flat_set<GroundNodeType*>;
    using SuccessorContainer = boost::container::flat_map<
        ActionType, std::unique_ptr<SuccessorType>>;

public:
    AggregateStateNode() = delete;

    AggregateStateNode( LabelType label, SuccessorType* parent, StateData&& data = StateData() )
        : label_( label ), data_( std::move(data) ), parent_( parent )
    { }

    AggregateStateNode( ThisType const& ) = delete;
    ThisType& operator =( ThisType const& ) = delete;

    AggregateStateNode( ThisType&& that )
        : label_( std::move(that.label_) ), data_( std::move(that.data_) ),
          ss_( std::move(that.ss_) ), parent_( std::move(that.parent_) ),
          successors_( std::move(that.successors_) )
    { }

    ThisType& operator =( ThisType&& that )
    {
        if( this != &that ) {
            label_ = std::move(that.label_);            
            data_ = std::move(that.data_);
            ss_ = std::move(that.ss_);
            parent_ = std::move(that.parent_);
            successors_ = std::move(that.successors_);
        }
        return *this;
    }

    // -----------------------------------------------------------------------

    SuccessorType* parent()
    { return parent_; }

    SuccessorType const* parent() const
    { return parent_; }

    bool isTerminal( ParametersType const& params ) const
    { return (*ss_.begin())->s().isTerminal( params ); }

    bool isLeaf() const
    { return successors_.empty(); }

    bool isPure() const
    {
        if( ss_.size() > 1 ) {
            GroundNodeType const* other = nullptr;
            for( GroundNodeType const* gsn : ss_ ) {
                // FIXME: This is going to break if we change to returning
                // a pointer from s()
                if( other && other->s() != gsn->s() ) {
                    return false;
                }
                other = gsn;
            }
        }

        return true;
    }

    std::size_t Nsuccessors() const
    { return successors_.size(); }

    template<typename Functor>
    void visitSuccessors( Functor f )
    {
        for( auto& e : successors_ ) {
            f( *e.second );
        }
    }

    template<typename Functor>
    void visitSuccessors( Functor f ) const
    {
        for( auto const& e : successors_ ) {
            f( *e.second );
        }
    }

    StateData& data()
    { return data_; }

    StateData const& data() const
    { return data_; }

    // -----------------------------------------------------------------------

    LabelType const& label() const
    { return label_; }

    Container const& groundStateNodes() const
    { return ss_; }

    std::size_t NgroundStateNodes() const
    { return ss_.size(); }

    detail::StateRange<Container> states() const
    {
        return detail::transformed_state_range( ss_ );
    }

    GroundNodeType const* sampleState( SamplingContext& ctx ) const
    {
        std::uniform_int_distribution<std::size_t> p( 0, ss_.size() - 1 );
        std::size_t const i = p( ctx.rng() );
        auto itr = ss_.begin();
        std::advance( itr, i );
        return *itr;
    }

    GroundNodeType* sampleState( SamplingContext& ctx )
    {
        std::uniform_int_distribution<std::size_t> p( 0, ss_.size() - 1 );
        std::size_t const i = p( ctx.rng() );
        auto itr = ss_.begin();
        std::advance( itr, i );
        return *itr;
    }

    // -----------------------------------------------------------------------
    // Mutating operations

    SuccessorType* addSuccessor( ActionType const a, LabelType const& y )
    {
        auto itr = successors_.find( a );
        if( itr == successors_.end() ) {
            auto succ = std::make_unique<SuccessorType>( a, y, this );
            auto result = successors_.insert(
                typename SuccessorContainer::value_type( a, std::move(succ) ) );
            itr = result.first;
        }
        return itr->second.get();
    }

    void sample( GroundNodeType* gsn, Domain const& domain, StateType const& s, RewardType r )
    {
        gsn->sample( domain, s, r );
        ss_.insert( gsn );
        //assert( (*ss_.begin())->s().isTerminal() == gsn->isTerminal() );

        boost::fusion::for_each( data_, [this, &domain, &s, &r]( auto& d ) {
            d.second.sample( *this, domain, s, r );
        } );
    }
    void addNode( GroundNodeType* gsn, Domain const& domain )
    {
        ss_.insert(gsn);
//        StateData& sData=gsn.getData();
//        boost::fusion::for_each( data_, [this,&sData]( auto& a ) { a.second.addNewData( *this,sData); } );
        RewardType const r = mcplan::get<state_data::tag::ravg>( *gsn );
        int const n = mcplan::get<state_data::tag::n>( *gsn );
        boost::fusion::for_each( data_,
            [this, &gsn, &domain, &r, n]( auto& d ) { d.second.sample( *this, domain, gsn->s(), r, n ); } );
    }

    void backup()
    {
        std::for_each( ss_.begin(), ss_.end(), []( auto& gsn ) { gsn->backup(); } );
        boost::fusion::for_each( data_, [this]( auto& d ) { d.second.backup( *this ); } );
    }
    void resetbounds()
    {
        std::for_each( ss_.begin(), ss_.end(), []( auto& gsn ) { gsn->resetbounds(); } );
        boost::fusion::for_each( data_, [this]( auto& d ) { d.second.resetbounds( *this ); } );
    }
   void backup(RewardType traj_value)
   {
            std::for_each( ss_.begin(), ss_.end(), [traj_value]( auto& gsn ) { gsn->backup(traj_value); } );
            boost::fusion::for_each( data_, [this,traj_value]( auto& d ) { d.second.backup( *this,traj_value ); } );
   }

    void leaf( RewardType r )
    {
        std::for_each( ss_.begin(), ss_.end(), [&r]( auto& gsn ) { gsn->leaf( r ); } );
        boost::fusion::for_each( data_, [this, &r]( auto& d ) { d.second.leaf( *this, r ); } );
    }

    template<typename Field>
    typename boost::fusion::result_of::value_at_key<StateData, Field>::type::value_type
    get() const
    {
        return boost::fusion::at_key<Field>(data_).get();
    }

    template<typename Field>
    void set( typename boost::fusion::result_of::value_at_key<StateData, Field>::type::value_type const& x )
    {
        boost::fusion::at_key<Field>(data_).set( x );
    }

    // -----------------------------------------------------------------------


    friend std::ostream& operator<<( std::ostream& out, ThisType const& sn )
    {
        out << "AS [" << (&sn) << "] (" << sn.label() << ") " << sn.data_ << " [";
        std::for_each( sn.ss_.begin(), sn.ss_.end(),
                       [&out]( auto const& s ) { out << *s << ";"; } );
        out << "]";
        return out;
    }

private:
    LabelType label_;
    StateData data_;
    Container ss_;

    SuccessorType* parent_;
    SuccessorContainer successors_;
};

template<typename... Args>
bool operator==( AggregateStateNode<Args...> const& a, AggregateStateNode<Args...> const& b )
{
    return a.id() == b.id();
}

template<typename... Args>
bool operator!=( AggregateStateNode<Args...> const& a, AggregateStateNode<Args...> const& b )
{
    return !(a == b);
}

template<typename... Args>
std::size_t hash( AggregateStateNode<Args...> const& a )
{
    return std::size_t( a.id() );
}

template<typename... Args>
bool operator<( AggregateStateNode<Args...> const& a, AggregateStateNode<Args...> const& b )
{
    return a.id() < b.id();
}

// ---------------------------------------------------------------------------

} // namespace mcplan

// Hash operators in namespace std
namespace std {
    template<typename... Args>
    struct hash<mcplan::AggregateStateNode<Args...>>
    {
        std::size_t operator()( mcplan::AggregateStateNode<Args...> const& xn ) const
        {
            return mcplan::hash( xn );
        }
    };
}

#endif
