#ifndef MCPLAN_NODE_AGGREGATEACTIONNODE_HPP__
#define MCPLAN_NODE_AGGREGATEACTIONNODE_HPP__

#include <memory>
#include <sstream>
#include <type_traits>
#include <vector>

#include <boost/container/flat_map.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/fusion/sequence/intrinsic/at_key.hpp>
#include <boost/fusion/sequence/intrinsic/value_at_key.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "mcplan/node/AggregateStateNode.hpp"
#include "mcplan/node/GroundActionNode.hpp"

#include "mcplan/SamplingContext.hpp"

namespace mcplan {

template<
    typename DomainT, typename Abstraction, typename Data,
    typename GroundStateNodeType = GroundStateNode<DomainT, Data>
>
class AggregateActionNode
{
public:
    using Domain        = DomainT;
    using LabelType     = typename Abstraction::AbstractActionLabel;
    using ThisType      = AggregateActionNode<Domain, Abstraction, Data, GroundStateNodeType>;

    using StateType     = typename Domain::StateType;
    using ActionType    = typename Domain::ActionType;

    using RewardType    = typename Domain::RewardType;
    using SuccessorType = AggregateStateNode<Domain, Abstraction, Data, GroundStateNodeType>;
    using GroundNodeType    = GroundActionNode<Domain, Data>;
    using GroundSuccessorType = typename GroundNodeType::SuccessorType;

    using StateData     = typename Data::StateData;
    using ActionData    = typename Data::ActionData;

private:
    using Container     = boost::container::flat_set<GroundNodeType*>;
    using SuccessorLabel = typename SuccessorType::LabelType;
    using SuccessorContainer = boost::container::flat_map<
        SuccessorLabel, std::unique_ptr<SuccessorType>>;

public:
    AggregateActionNode() = delete;

    AggregateActionNode( ActionType a, LabelType label,
                         SuccessorType* parent, ActionData&& data = ActionData() )
        : a_( a ), label_( label ), data_( std::move(data) ), parent_( parent )
    { }

    AggregateActionNode( AggregateActionNode const& ) = delete;
    AggregateActionNode& operator =( AggregateActionNode const& ) = delete;

    AggregateActionNode( ThisType&& that )
        : a_( std::move(that.a_) ), label_( std::move(that.label_) ),
          data_( std::move(that.data_) ),
          as_( std::move(that.as_) ), parent_( std::move(that.parent_) ),
          successors_( std::move(that.successors_) )
    { }

    ThisType& operator =( ThisType&& that )
    {
        if( this != &that ) {
            a_ = std::move(that.a_);
            label_ = std::move(that.label_);
            data_ = std::move(that.data_);
            as_ = std::move(that.as_);
            parent_ = std::move(that.parent_);
            successors_ = std::move(that.successors_);
        }
        return *this;
    }

    // -----------------------------------------------------------------------

    ActionData& data()
    { return data_; }

    ActionData const& data() const
    { return data_; }

    SuccessorType* parent()
    { return parent_; }

    SuccessorType const* parent() const
    { return parent_; }

    bool isLeaf() const
    { return successors_.empty(); }

    template<typename Functor>
    void visitSuccessors( Functor f )
    {
        for( auto& succ : successors_ ) {
            f( *succ.second );
        }
    }

    template<typename Functor>
    void visitSuccessors( Functor f ) const
    {
        for( auto const& succ : successors_ ) {
            f( *succ.second );
        }
    }

    /**
     * @brief Ensures that a successor with label 'x' exists and returns it.
     * @param x
     * @return A pair 'p' where 'p.second' is true iff a new node instance
     * was created (i.e. successor for label 'x' did not exist).
     */
    std::pair<SuccessorType*, bool> addSuccessor( LabelType const& x )
    {
        auto itr = successors_.find( x );
        if( itr == successors_.end() ) {
            auto succ = std::make_unique<SuccessorType>( x, this );
            auto result = successors_.insert(
                typename SuccessorContainer::value_type( x, std::move(succ) ) );
            itr = result.first;
            return {itr->second.get(), true};
        }
        else {
            return {itr->second.get(), false};
        }
    }

    void clearSuccessors()
    {
        successors_.clear();
    }

    std::unique_ptr<SuccessorType> detachSuccessor( SuccessorLabel const x )
    {
        std::unique_ptr<SuccessorType> succ = std::move( successors_[x] );
        successors_.erase( x );
        return succ;
    }

    // -----------------------------------------------------------------------

    ActionType a() const
    { return a_; }

    LabelType label() const
    { return label_; }

    boost::iterator_range<typename Container::iterator> groundActionNodes()
    {
        return boost::make_iterator_range( as_.begin(), as_.end() );
    }

    boost::iterator_range<typename Container::const_iterator> groundActionNodes() const
    {
        return boost::make_iterator_range( as_.begin(), as_.end() );
    }

    // -----------------------------------------------------------------------
    // Node data operations
    Container& contents()
    {
        return as_;
    }

    void sample( GroundNodeType* gan, Domain const& domain,
                 StateType const& s, ActionType const& a, RewardType r )
    {
        gan->sample( domain, s, a, r );
        as_.insert( gan );

        boost::fusion::for_each( data_,
            [this, &domain, &s, &a, &r]( auto& d ) {
                d.second.sample( *this, domain, s, a, r );
        } );
    }


    void backup()
    {
        std::for_each( as_.begin(), as_.end(), []( auto& gan ) { gan->backup(); } );
        boost::fusion::for_each( data_, [this]( auto& d ) { d.second.backup( *this ); } );
    }
	
    void backup(RewardType traj_value)
    {
        std::for_each( as_.begin(), as_.end(), [&traj_value]( auto& gan ) { gan->backup(traj_value); } );
        boost::fusion::for_each( data_, [this, &traj_value]( auto& d ) { d.second.backup( *this,traj_value ); } );
    }
    void resetbounds()
    {
        std::for_each( as_.begin(), as_.end(), []( auto& gan ) { gan->resetbounds(); } );
        boost::fusion::for_each( data_, [this]( auto& d ) { d.second.resetbounds( *this ); } );
    }

    void addNode(GroundSuccessorType* gsn, GroundNodeType* gan, Domain const& domain)
    {
        as_.insert(gan);
//        ActionData& aData=gan.getData();
//        boost::fusion::for_each( data_, [this,&aData]( auto& a ) { a.second.addNewData( *this,aData); } );
        RewardType const r = mcplan::get<action_data::tag::ravg>( *gan );
        int const n = mcplan::get<action_data::tag::n>( *gan );
        boost::fusion::for_each( data_, [this, &gsn, &gan, &domain, &r, n]( auto& d ) {
            d.second.sample( *this, domain, gsn->s(), gan->a(), r, n );
        } );
    }
    template<typename Field>
    typename boost::fusion::result_of::value_at_key<ActionData, Field>::type::value_type
     get() const
    {
        return boost::fusion::at_key<Field>(data_).get();
    }

    // -----------------------------------------------------------------------

    friend std::ostream& operator<<( std::ostream& out, ThisType const& an )
    {
        out << "AA [" << (&an) << "] (" << an.label() << ") "
            << an.a() << " " << an.data_ << " [";
        std::for_each( an.as_.begin(), an.as_.end(),
                       [&out]( auto const& a ) { out << *a << ";"; } );
        out << "]";
        return out;
    }

private:
    ActionType a_;
    LabelType label_;
    ActionData data_;
    Container as_;

    SuccessorType* parent_;
    SuccessorContainer successors_;
};

}

#endif

