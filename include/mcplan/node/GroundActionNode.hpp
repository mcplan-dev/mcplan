#ifndef MCPLAN_NODE_GROUNDACTIONNODE_HPP__
#define MCPLAN_NODE_GROUNDACTIONNODE_HPP__

#include "mcplan/node/GroundStateNode.hpp"
#include "mcplan/SamplingContext.hpp"

#include <boost/fusion/sequence/intrinsic/at_key.hpp>
#include <boost/fusion/sequence/intrinsic/value_at_key.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include <memory>
#include <sstream>
#include <type_traits>
#include <vector>

namespace mcplan {

// ---------------------------------------------------------------------------

template<typename Domain, typename Data>
class GroundActionNode;

template<typename Domain, typename Data, typename StatePointer>
class GroundStateNode;

template<typename Domain, typename Data>
std::ostream& operator<< ( std::ostream& out, GroundActionNode<Domain, Data> const& an );

/**
 * A node corresponding to an Action in the State-Action tree.
 * ActionNode owns its Action argument and all nodes in the subtree rooted at 'this'.
 */
template<typename DomainT, typename Data>
class GroundActionNode
{
public:
    using Domain        = DomainT;
    using StateType     = typename Domain::StateType;
    using ActionType    = typename Domain::ActionType;
    using LabelType     = ActionType;
    using RewardType    = typename Domain::RewardType;
    using SuccessorType = GroundStateNode<Domain, Data>;
    using SuccessorStorage = typename Data::StateStorage;

    using successor_iterator        = typename SuccessorStorage::iterator;
    using successor_const_iterator  = typename SuccessorStorage::const_iterator;


    using StateData     = typename Data::StateData;
    using ActionData    = typename Data::ActionData;

public:
    GroundActionNode( ActionType a, ActionData&& data = ActionData() )
        : a_( a ), data_( std::move(data) )
    { }

    GroundActionNode( GroundActionNode<Domain, Data>&& that )
        : a_( std::move(that.a_) ), data_( std::move(that.data_) ),
          successors_( std::move(that.successors_) )
    { }
    boost::container::flat_set<GroundActionNode*>& contents()
    {
        boost::container::flat_set<GroundActionNode*> single_element;
        single_element.insert(this);
        return single_element;
    }


    GroundActionNode<Domain, Data>& operator=( GroundActionNode<Domain, Data>&& that )
    {
        if( this != &that ) {
            a_ = std::move(that.a_);
            data_ = std::move(that.data_);
            successors_ = std::move(that.successors_);
        }
        return *this;
    }

    // -----------------------------------------------------------------------

    LabelType label() const
    { return a_; }

    ActionType const& a() const
    { return a_; }

    ActionType const &sampleAction( SamplingContext& /*ctx*/ ) const
    {
        return a_;
    }

    // -----------------------------------------------------------------------
    ActionData& getData()
    {
        return data_;
    }

    bool isLeaf() const
    { return !successors_.hasSuccessors(); }

    template<typename Functor>
    void visitSuccessors( Functor f )
    { successors_.visitSuccessors( f ); }

    template<typename Functor>
    void visitSuccessors( Functor f ) const
    { successors_.visitSuccessors( f ); }

    SuccessorType* addSuccessor( std::unique_ptr<StateType const> s )
    {
        typename SuccessorType::LabelType label( s.get() );
        SuccessorType* sn = successors_.addSuccessor( label, std::move(s) );
        return sn;
    }

    // -----------------------------------------------------------------------
    // Node data operations

    void sample( Domain const& domain, StateType const& s, ActionType const& a, RewardType r )
    {
        boost::fusion::for_each( data_, [this, &domain, &s, &a, &r]( auto& d ) {
            d.second.sample( *this, domain, s, a, r );
        } );
    }

    void backup()
    {
        boost::fusion::for_each( data_, [this]( auto& d ) { d.second.backup( *this ); } );
    }

    void backup(RewardType traj_val)
    {
        boost::fusion::for_each( data_, [this,&traj_val]( auto& d ) { d.second.backup( *this,traj_val ); } );
    }

    void resetbounds()
    {
        boost::fusion::for_each( data_, [this]( auto& d ) { d.second.resetbounds( *this ); } );
    }

    template<typename Field>
    typename boost::fusion::result_of::value_at_key<ActionData, Field>::type::value_type
    get() const
    {
        return boost::fusion::at_key<Field>(data_).get();
    }

    // -----------------------------------------------------------------------

    friend std::ostream& operator<< <Domain, Data> ( std::ostream& out, GroundActionNode const& an );

private:
    ActionType a_;
    ActionData data_;
    //SuccessorType* parent;
    SuccessorStorage successors_;
};

template<typename Domain, typename Data>
std::ostream& operator<<( std::ostream& out, GroundActionNode<Domain, Data> const& an )
{
    out << "GA [" << (&an) << "] " << an.a_ << " " << an.data_;
    return out;
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

}

#endif

