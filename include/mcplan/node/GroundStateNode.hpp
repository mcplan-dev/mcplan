#ifndef MCPLAN_NODE_GROUNDSTATENODE_HPP__
#define MCPLAN_NODE_GROUNDSTATENODE_HPP__

#include "mcplan/node/NodeData.hpp"
#include "mcplan/SamplingContext.hpp"

#include <boost/container/flat_set.hpp>
#include <boost/fusion/sequence/intrinsic/at_key.hpp>
#include <boost/fusion/sequence/intrinsic/value_at_key.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/sequence/io.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include <functional>
#include <memory>
#include <ostream>
#include <vector>

namespace mcplan {

// ---------------------------------------------------------------------------

namespace {
    template<typename T>
    auto get_raw_pointer_to_target( T const& p ) -> decltype(&(*p))
    {
        return &(*p);
    }
}

// ---------------------------------------------------------------------------

template<typename T>
class PointerTargetLabel
{
public:
    explicit PointerTargetLabel( T const* t )
        : t_( t )
    { }

    T const* get() const
    { return t_; }

private:
    T const* t_;
};

template<typename T>
bool operator ==( PointerTargetLabel<T> a, PointerTargetLabel<T> b )
{ return *a.get() == *b.get(); }

template<typename T>
bool operator !=( PointerTargetLabel<T> a, PointerTargetLabel<T> b )
{ return !(a == b); }

template<typename T>
bool operator <( PointerTargetLabel<T> a, PointerTargetLabel<T> b )
{ return *a.get() < *b.get(); }

template<typename T>
std::ostream& operator <<( std::ostream& out, PointerTargetLabel<T> a )
{
    out << *a.get();
    return out;
}

// ---------------------------------------------------------------------------

template<typename Domain, typename Data>
class GroundActionNode;

template<typename Domain, typename Data, typename StatePointerT>
class GroundStateNode;

template<typename Domain, typename Data, typename StatePointerT>
std::ostream& operator<< ( std::ostream& out, GroundStateNode<Domain, Data, StatePointerT> const& sn );

//namespace node {

//    template<typename Domain, typename Data, typename StatePointerT, typename Field>
//    struct get_impl<mcplan::GroundStateNode<Domain, Data, StatePointerT>, Field>;

//    template<typename Domain, typename Data, typename Field>
//    struct set_impl<mcplan::GroundStateNode<Domain, Data>, Field>;
//}

/**
 * A node corresponding to a single ground State in a State-Action tree.
 * StateNode owns all nodes in the subtree rooted at 'this'.
 *
 * Notes on 'Data':
 *  Valid expressions:
 *      data.sample()
 *      data.backup()
 *      data.get<tag>()
 *      data.set<tag>( value )
 *  We store data in the node via the 'get<tag>/set<tag>' mechanism so that we
 *  can choose which statistics to accumulate independently of the search
 *  algorithm. The code will not compile if the node does not store the
 *  statistics necessary for the algorithm. For example, SS needs visit
 *  counts and avg/max returns, while FSSS additionally needs upper and lower
 *  value bounds.
 *
 * Currently, Data is assumed to be a type with nested types Data::StateData
 * and Data::ActionData. Both of these must be models of boost::fusion::AssociativeSequence.
 */
template<typename DomainT, typename Data,
         typename StatePointerT = std::unique_ptr<typename DomainT::StateType const>>
class GroundStateNode
{
public:
    using Domain        = DomainT;
    using StatePointer  = StatePointerT;
    using ThisType      = GroundStateNode<Domain, Data, StatePointer>;
    using ParametersType = typename Domain::ParametersType;
    using StateType     = typename Domain::StateType;
    using ActionType    = typename Domain::ActionType;
    using RewardType    = typename Domain::RewardType;
    using SuccessorType = GroundActionNode<Domain, Data>;
    using SuccessorStorage = typename Data::ActionStorage;
    using StateData     = typename Data::StateData;
    using ActionData    = typename Data::ActionData;
    using LabelType     = PointerTargetLabel<StateType>;

    GroundStateNode( StatePointer s, StateData&& data = StateData() )
        : s_( std::move(s) ), data_( std::move(data) )
    { }

    GroundStateNode( GroundStateNode<Domain, Data> const& /*that*/ ) = delete;

    GroundStateNode( GroundStateNode<Domain, Data>&& that )
        : s_( std::move(that.s_) ), data_( std::move(that.data_) ),
          successors_( std::move(that.successors_) )
    { }

    GroundStateNode<Domain, Data>& operator=( GroundStateNode<Domain, Data>&& that )
    {
        if( this != &that ) {
            s_ = std::move(that.s_);
            data_ = std::move(that.data_);
            successors_ = std::move(that.successors_);
        }
        return *this;
    }

    // -----------------------------------------------------------------------

    LabelType label() const
    { return LabelType( s_.get() ); }

    StateType const& s() const
    { return *s_; }


    auto states() const -> decltype(boost::make_iterator_range(static_cast<StateType const*>(nullptr),
                                                               static_cast<StateType const*>(nullptr)))
    {
        using namespace boost;
        // The &(*s_) gets a raw pointer even if s_ is a smart pointer
        StateType const* p( get_raw_pointer_to_target(s_) );
        return make_iterator_range( p, p+1 );
    }

    StateType const& sampleState( SamplingContext& /*ctx*/ ) const
    {
        return *s_;
    }

    bool isTerminal( ParametersType const& params ) const
    { return s_->isTerminal( params ); }

    bool isLeaf() const
    { return successors_.empty(); }
    void addNode(GroundStateNode<Domain,Data>& )
    {}

    // -----------------------------------------------------------------------
    // Successors

    template<typename Functor>
    void visitSuccessors( Functor f )
    { successors_.visitSuccessors( f ); }

    template<typename Functor>
    void visitSuccessors( Functor f ) const
    { successors_.visitSuccessors( f ); }

    SuccessorType* addSuccessor( ActionType const& a )
    {
        typename SuccessorType::LabelType label( a );
        SuccessorType* sn = successors_.addSuccessor( label, a );
        return sn;
    }

    // -----------------------------------------------------------------------
    // Node data operations

    void sample( Domain const& domain, StateType const& s, RewardType r )
    {
        boost::fusion::for_each( data_, [this, &domain, &s, &r]( auto& a ) { a.second.sample( *this, domain, s, r ); } );
    }


    void backup()
    {
        boost::fusion::for_each( data_, [this]( auto& a ) { a.second.backup( *this ); } );
    }
    void resetbounds()
    {
        boost::fusion::for_each( data_, [this]( auto& a ) { a.second.resetbounds( *this ); } );
    }
    void backup(RewardType traj_val)
    {
        boost::fusion::for_each( data_, [this,&traj_val]( auto& a ) { a.second.backup( *this,traj_val ); } );
    }
    StateData& getData()
    { return data_;}

    void leaf( RewardType r )
    {
        boost::fusion::for_each( data_, [this, &r]( auto& a ) { a.second.leaf( *this, r ); } );
    }

    template<typename Field>
    typename boost::fusion::result_of::value_at_key<StateData, Field>::type::value_type
    get() const
    {
        return boost::fusion::at_key<Field>(data_).get();
    }

    template<typename Field>
    void set( typename boost::fusion::result_of::value_at_key<StateData, Field>::type::value_type const& x )
    {
        boost::fusion::at_key<Field>(data_).set( x );
    }

    // -----------------------------------------------------------------------

    friend std::ostream& operator <<( std::ostream& out, GroundStateNode const& sn )
    {
        out << "GS [" << (&sn) << "] " << *sn.s_ << " " << sn.data_;
        return out;
    }

private:
    StatePointer s_;
    StateData data_;
    SuccessorStorage successors_;
};

template<typename Domain, typename Data>
bool operator==( GroundStateNode<Domain, Data> const& a, GroundStateNode<Domain, Data> const& b )
{
    return a.label() == b.label();
}

template<typename Domain, typename Data>
bool operator!=( GroundStateNode<Domain, Data> const& a, GroundStateNode<Domain, Data> const& b )
{
    return !(a == b);
}

template<typename Domain, typename Data>
std::size_t hash( GroundStateNode<Domain, Data> const& a )
{
    return std::size_t( a.label() );
}

template<typename Domain, typename Data>
bool operator<( GroundStateNode<Domain, Data> const& a, GroundStateNode<Domain, Data> const& b )
{
    return a.label() < b.label();
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

}

#endif
