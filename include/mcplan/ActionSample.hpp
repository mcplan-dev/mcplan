#ifndef MCPLAN_ACTIONSAMPLE_HPP__
#define MCPLAN_ACTIONSAMPLE_HPP__


#include <memory>

namespace mcplan {


template<typename Domain>
class ActionSample
{
    using ActionType    = typename Domain::ActionType;
    using RewardType    = typename Domain::RewardType;

public:
    ActionSample( ActionType const a, RewardType const r )
        : a_( a ), r_( r )
    { }

    ActionSample( ActionSample<Domain>&& that )
        : a_( std::move(that.a_) ), r_( std::move(that.r_) )
    { }

    ActionSample<Domain>& operator=( ActionSample<Domain>&& that )
    {
        a_ = std::move(that.a_);
        r_ = std::move(that.r_);
        return *this;
    }

    RewardType r() const { return r_; }

    ActionType a() const { return a_; }

private:
    ActionType a_;
    RewardType r_;
};


}


#endif
